from rest_framework import serializers

from .models import *


class AddressModificationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Address
        fields = '__all__'

    def create(self, validated_data):
        return Address.objects.create(**validated_data)


class UserAddressModificationSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s %s %s %s %s" % (self.id, self.user.username, self.address.type, self.address.city, self.address.pincode)

    class Meta:
        model = UserAddress
        fields = '__all__'

    def create(self, validated_data):
        return UserAddress.objects.create(**validated_data)


class DiscountModificationSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s" % (self.name, self.type.name)

    class Meta:
        model = Discount
        fields = '__all__'

    def create(self, validated_data):
        return Discount.objects.create(**validated_data)


class LivestockvaccinationModificationSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s" % (self.name)

    class Meta:
        model = Livestockvaccination
        fields = '__all__'

    def create(self, validated_data):
        return Livestockvaccination.objects.create(**validated_data)


class OrderModificationSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s - %s" % (self.id, self.buyer.username, self.items_count)

    class Meta:
        model = Order
        fields = '__all__'

    def create(self, validated_data):
        return Order.objects.create(**validated_data)


class DeliveryDetailModificationSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s" % (self.delivered_by, self.delivered_on)

    class Meta:
        model = DeliveryDetail
        fields = '__all__'

    def create(self, validated_data):
        return DeliveryDetail.objects.create(**validated_data)


class InvoiceModificationSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s" % (self.invoice_number, self.is_payment_done)

    class Meta:
        model = Invoice
        fields = '__all__'

    def create(self, validated_data):
        return Invoice.objects.create(**validated_data)


class PaymentDetailModificationSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s - %s" % (self.received_on, self.mode, self.amount_received)

    class Meta:
        model = PaymentDetail
        fields = '__all__'

    def create(self, validated_data):
        return PaymentDetail.objects.create(**validated_data)

class ProductRequirementModificationSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s - %s - %s" % (self.id, self.name, self.posted_by.username, self.categorytype)

    class Meta:
        model = ProductRequirementPosted
        fields = '__all__'

class ThreadModificationSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s - %s - %s" % (self.id, self.Category.name, self.PostedBy.username)

    class Meta:
        model = Thread
        fields = '__all__'

class ThreadReplyModificationSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s - %s - %s" % (self.id, self.reply.name, self.RepliedBy.username)

    class Meta:
        model = ThreadReply
        fields = '__all__'