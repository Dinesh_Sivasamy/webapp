from ast import Or
from multiprocessing.sharedctypes import Value
import os
from datetime import *
from unicodedata import name
from django.core.files import File
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework import status
from django.core.paginator import Paginator
from django.core.files.base import ContentFile
from django.db import transaction
from django.db.models import Sum

from .datamodifierserializer import *
from .models import *
from .serializer import *
# from .services.whatsappmessagingservice import *
from .helpers.viewshelper import getdiscountforuser, insertProduct

from .services.smsservice import *
from .services.authenticationservices import *
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, user_passes_test

import logging
logger = logging.getLogger(__name__)


class globalvariable:
    notification_to_seller_sent_on = date.today()


class ProductListView(APIView):

    def get(self, request, top, page, cat, breed, state, city, seller, format=None):

        kwargs = {}

        if cat:
            kwargs['category'] = cat
        if breed:
            kwargs['breed'] = breed
        if state:
            kwargs['state'] = state
        if city:
            kwargs['city'] = city
        if seller:
            kwargs['seller'] = seller

        products = Product.objects.filter(**kwargs)
        paginator = Paginator(products, top)

        page_number = page
        page_obj = paginator.get_page(page_number)
        serialize = ProductlistSerializer(page_obj, many=True)
        return Response(serialize.data)


class ProductView(APIView):

    def post(self, request, format=None):
        seller_mobile = request.user.phone.as_e164
        seller_name = request.user
        try:
            product_insert = insertProduct(request)
            if product_insert:
                send_product_registered_notification(
                    seller_mobile, seller_name)
                return Response(product_insert, status=status.HTTP_201_CREATED)
        except Exception as e:
            logger.error(
                'Caught exception while inserting product record: {e}')
            return Response('Failed product insert', status=status.HTTP_400_BAD_REQUEST)


class ProductdetailView(APIView):

    def get(self, request, pk, format=None):
        kwargs = {}
        kwargs['id'] = pk
        # kwargs['is_verified'] = True
        productdetail = Product.objects.filter(**kwargs)
        serialize = ProductlistSerializer(productdetail, many=True)
        return Response(serialize.data)
    

class ProductTopListView(APIView):

    def get(self, request, top, format=None):        
        TopViewProducts = ProductViewDetail.objects.values('product_id').order_by('product_id').annotate(total_views=Sum('viewcount')).order_by('-total_views')[:4]
        # TopViewProducts = ProductViewDetail.objects.all().order_by('-viewcount')[:10]
        productdetail = Product.objects.filter(id__in=TopViewProducts.values('product_id'))
        serialize = ProductlistSerializer(productdetail, many=True)
        return Response(serialize.data)


class ProductImagesView(APIView):

    parser_classes = (MultiPartParser, FormParser)

    def post(self, request, format=None):
        logger.info(request.data)
        haserror = False
        productvariantId = request.data['product']
        images = dict((request.data).lists())['images']
        for img in images:
            newProductimg = {
                "product": productvariantId,
                "image": img
            }
            serializer = ProductimagesSerializer(data=newProductimg)
            if serializer.is_valid():
                serializer.save()
                logger.info('save successful')
            else:
                logging.error(
                    f'failed to insert product image: {serializer.errors} + {serializer.error_messages}')
                haserror = True
                logger.info('save failed')

        if haserror:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

# @method_decorator(user_passes_test(check_if_veterinary), name='dispatch')


class ProductdetailListView(APIView):

    def get(self, request, top, categorypk, page, format=None):

        kwargs = {}

        if request.user.is_authenticated:
            loggedInUser = User.objects.get(username=request.user)
            logger.info(f'Fetching list for loggedin username: {request.user},' +
                        f' type: {loggedInUser.usertype.name}')

            # Fetching verified and available products alone if the loggedin user is not of type internal
            if loggedInUser.usertype.id != 9:
                kwargs['is_verified'] = True
                # kwargs['is_sold'] = False
        else:
            # Fetching verified and available products alone if the unauthenticate user tries to access the page
            logger.info(f'Fetching list for Unauthenticated user')
            kwargs['is_verified'] = True
            # kwargs['is_sold'] = False

        # kwargs['product_variant__stock__gt'] = 0
        category_detail = None
        if categorypk > 0:
            kwargs['category'] = categorypk
            category_detail = Productcategory.objects.get(
                id=categorypk)
            logger.debug(f'{request.user if request.user.is_authenticated else "Unauthenticated user"} '
                         f'fetching product list for category: {category_detail.name}')

        if page.lower() != 'home':
            if category_detail:
                page = f'productlist&category={category_detail.name}'
            else:
                page = f'productlist'

        logger.info(f'Fetching {page.lower()} page data for: ' +
                    f'{request.user if request.user.is_authenticated else "Unauthenticated user"}')

        productlist = Product.objects.filter(**kwargs)

        if (top > 0):
            # Show top number of products per page.
            paginator = Paginator(productlist, top)

            page_number = 1
            product_obj = paginator.get_page(page_number)
        else:
            product_obj = productlist

        logger.info('fetched product list successfully')

        try:
            pageview = ApplicationPages.objects.get(
                pageinfo=page.lower())
            pageview.viewcount += 1
            pageview.save()
        except Exception as e:
            logger.error(f'Failed to update page viewcount for {page.lower()} - accessed by user: ' +
                         f'{request.user if request.user.is_authenticated else "Unauthenticated user"}, with exception {e}')

        serialize = ProductlistSerializer(product_obj, many=True)
        return Response(serialize.data)


class ProductVariantListView(APIView):

    def get(self, request,  format=None):
        productvariantlist = ProductVariant.objects.all()
        serialize = ProductVariantlistSerializer(productvariantlist, many=True)
        return Response(serialize.data)


class VetCheckListView(APIView):

    def get(self, request, checkcomplete, top, format=None):

        kwargs = {}

        if request.user.is_authenticated:
            loggedInUser = User.objects.get(username=request.user)
            logger.info(f'Fetching list for loggedin username: {request.user},' +
                        f' type: {loggedInUser.usertype.name}')

            # If the user is not of type Veterinary, then return null
            if loggedInUser.usertype.id != 5:
                logger.info(
                    'Returing null, since the logged in user is not of typeid 5')
                return Response('UnAuthorized', status=status.HTTP_403_FORBIDDEN)
        else:
            # If there is no active sesstion, then return null
            logger.info('Returing null, since there is no active user session')
            return Response('UnAuthorized', status=status.HTTP_403_FORBIDDEN)

        logger.info(
            f'Fetching list for: {request.user} with checked status {checkcomplete}')
        kwargs['is_verified'] = True
        kwargs['is_sold'] = False

        kwargs['product_variant__id__isnull'] = False
        kwargs['product_variant__stock__gt'] = 0

        if checkcomplete == 'true':
            kwargs['veterinary_check__id__isnull'] = False
        else:
            kwargs['veterinary_check__id__isnull'] = True

        productlist = Product.objects.filter(**kwargs)
        serialize = ProductlistSerializer(productlist, many=True)
        return Response(serialize.data)


class LivestockVeterinaryCheckView(APIView):

    def post(self, request, format=None):
        serializer = LivestockVeterinaryCheckSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# orders that are in user cart and are yet to be confirmed by user (adding to cart)
class BuyerCartView(APIView):

    def get(self, request, buyerpk, format=None):
        order = Order.objects.filter(status=0, buyer=buyerpk)
        serialize = OrderSerializer(order, many=True)
        return Response(serialize.data)

    def post(self, request, buyerpk, format=None):
        request.data['status'] = 0
        serializer = OrderModificationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            logging.error(
                f'failed to insert into buyer cart: {serializer.errors} + {serializer.error_messages}')
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# orders that are listed by seller
class SellerProductListView(APIView):

    def get(self, request, sellerpk, usertype, fetchtype, top, format=None):

        kwargs = {}

        if fetchtype.lower() == "ordered":
            if usertype != 9:
                kwargs['product__seller'] = sellerpk

            productlist = Order.objects.filter(**kwargs)

            if (top > 0):
                # Show top number of products per page.
                paginator = Paginator(productlist, top)

                page_number = 1
                product_obj = paginator.get_page(page_number)
            else:
                product_obj = productlist

            serialize = OrderSerializer(product_obj, many=True)
            return Response(serialize.data)

        elif fetchtype.lower() == "sold":

            kwargs['is_sold'] = True

        else:
            kwargs['is_sold'] = False
            kwargs['product_variant__stock__gt'] = 0

        if usertype != 9:
            kwargs['seller'] = sellerpk

        productlist = Product.objects.filter(**kwargs)

        if (top > 0):
            # Show top number of products per page.
            paginator = Paginator(productlist, top)

            page_number = 1
            product_obj = paginator.get_page(page_number)
        else:
            product_obj = productlist

        serialize = ProductlistSerializer(product_obj, many=True)
        return Response(serialize.data)


class ConfirmOrderView(APIView):

    def put(self, request, buyerpk, addresspk, userrefcode,  format=None):

        orders = list(Order.objects.filter(status=0, buyer=buyerpk))
        logger.info('fetched orders')
        logger.info(f'order count ')
        logger.info(len(orders))

        if len(orders) == 0:
            logger.error("No order data found for this buyer")
            return Response("No Orders to confirm", status=status.HTTP_400_BAD_REQUEST)

        ordercount = 0

        useraddress = UserAddress.objects.filter(id=addresspk).first()
        productpricing = ProductPriceSet.objects.filter(isactive=True).first()

        updatedcount = 0
        totalrequest = len(orders)
        for order in orders:
            try:
                with transaction.atomic():
                    logger.info("Updating delivery for order")
                    logger.info(order.id)

                    ordercount += 1

                    newinvoice = {
                        "is_payment_done": False
                    }

                    invoiceserializer = InvoiceModificationSerializer(
                        data=newinvoice)

                    if invoiceserializer.is_valid():
                        invoiceserializer.save()
                        logger.info("Invoice data saved")

                        newdelivery = {
                            "pickup_address": order.product.pickup.id,
                            "delivery_address": useraddress.id
                        }

                        deliveryserializer = DeliveryDetailModificationSerializer(
                            data=newdelivery)

                        if deliveryserializer.is_valid():
                            deliveryserializer.save()
                            logger.info("Delivery data saved")

                            discountamount = 0
                            deliverycharge = 0

                            productdetail = ProductVariant.objects.filter(
                                product__id=order.product.id).first()

                            productprice = productdetail.offer_price if productdetail.offer_price and productdetail.offer_price > 0 else productdetail.original_price

                            discountapplicable = 0
                            userdiscount = None

                            if ordercount == 1:
                                userdiscount = getdiscountforuser(
                                    buyerpk, userrefcode)

                                if userdiscount:
                                    discountapplicable = 0
                                    totalorderprice = (
                                        float(order.items_count) * float(productprice))

                                    if float(userdiscount.discount_percent) > 0:
                                        discountcalculated = (float(
                                            totalorderprice) * float(userdiscount.discount_percent)) + float(totalorderprice)
                                    elif float(userdiscount.discount_rupees) > 0:
                                        discountcalculated = float(
                                            userdiscount.discount_rupees)

                                    if float(userdiscount.max_discount_rupees) > 0 and float(discountcalculated) > float(userdiscount.max_discount_rupees):
                                        discountapplicable = float(
                                            userdiscount.max_discount_rupees)
                                    else:
                                        discountapplicable = float(
                                            discountcalculated)

                            totalsaleprice = float(
                                order.items_count) * float(productprice)

                            totalpayable = float(
                                totalsaleprice) - float(discountapplicable) + float(deliverycharge)

                            logger.info(f'product price: {productprice}')
                            logger.info(f'total payable: {totalpayable}')

                            logger.info("Delivery data saved")
                            orderdetail = {
                                "buyer": order.buyer.id,
                                "product": order.product.id,
                                "items_count": order.items_count,
                                "price_per_item": productprice,
                                "total_payable":  totalpayable,
                                "discounttype": None if userdiscount is None else userdiscount.type.id,
                                "discountedamount": discountapplicable,
                                "delivery_charge": deliverycharge,
                                "delivery": deliveryserializer.data['id'],
                                "status": 1,
                                "invoice": invoiceserializer.data['id']
                            }

                            orderserializer = OrderModificationSerializer(
                                order, data=orderdetail)

                            if orderserializer.is_valid():
                                orderserializer.save()

                                logger.info("Order serializer data saved")
                                orderserializer = OrderSerializer(
                                    order, many=False)
                                productdata = orderserializer.data['product']
                                productvariantdata = productdata['product_variant'][0]

                                logger.info(productvariantdata['id'])

                                # upload product image into order table
                                if productvariantdata:
                                    productimage = Productimages.objects.filter(
                                        product=productvariantdata['id']).first().image
                                    logger.info(productimage)
                                    order.product_image = File(
                                        productimage, productimage.name.split("/")[-1])
                                    order.save()
                                    logger.info(
                                        "Order image data saved successfully")

                                # update product stocks
                                productvaraiant = ProductVariant.objects.filter(
                                    id=productvariantdata['id']).first()
                                exitingstock = productvaraiant.stock
                                orderedquantity = order.items_count
                                productvaraiant.stock = exitingstock - \
                                    orderedquantity if exitingstock - orderedquantity > 0 else 0
                                logger.info(
                                    'updating stock count for the product')
                                logger.info(productvaraiant)
                                productvaraiant.save()
                                logger.info('product stock updated')

                                farmzonncommission = float(
                                    totalsaleprice) * (float(productpricing.profitmargin) / 100)

                                saleinvoicedetail = {
                                    "order": order.id,
                                    "status": 0,
                                    "profitpercent": productpricing.profitmargin,
                                    "totalprice": totalpayable,
                                    "farmzonncommission": farmzonncommission,
                                    "deliverycharge": deliverycharge,
                                    "sellershare": totalsaleprice,
                                }

                                saleinvoceserializer = SaleOrderInvoiceSerializer(
                                    data=saleinvoicedetail)

                                if saleinvoceserializer.is_valid():
                                    saleinvoceserializer.save()
                                    logger.info('new sale invoice saved')
                                    logger.info(
                                        f"{order.buyer.phone.as_e164}, {order.buyer.username}, {order.product.seller.phone.as_e164}, {order.product.seller.username}, {order.product.id}")
                                    send_order_placed_notification(
                                        order.buyer.phone.as_e164, order.buyer.username, order.product.seller.phone.as_e164, order.product.seller.username, order.product.id)
                                    logger.info('new sale invoice saved')
                                    updatedcount += 1
                                else:
                                    logging.error(
                                        f'failed in saleinvoice: {saleinvoceserializer.errors} + {saleinvoceserializer.error_messages}')
                                    raise ValueError(
                                        'Failed to update sale invoice detail')
                            else:
                                logging.error(
                                    f'failed to update order: {orderserializer.errors} + {orderserializer.error_messages}')
                                raise ValueError(
                                    'Failed to update order detail with delivery and invoice')
                        else:
                            logging.error(
                                f'failed to insert delivery: {deliveryserializer.errors} + {deliveryserializer.error_messages}')
                            raise ValueError(
                                'Failed to insert delivery detail')
                    else:
                        logging.error(
                            f'failed to insert invoice: {invoiceserializer.errors} + {invoiceserializer.error_messages}')
                        raise ValueError('Failed to insert invoice detail')
            except Exception as e:
                logging.error(
                    f'Failed order insert inside loop, proceeding next order: + {e}')
                logging.error(e)

        logger.info(
            f"Total updated: {updatedcount}, total order in request: {totalrequest}")
        if updatedcount != totalrequest:
            return Response("Failed to save order confirmation", status=status.HTTP_400_BAD_REQUEST)
        return Response("Successfully saved order confirmation", status=status.HTTP_201_CREATED)


# orders that are placed and confirmed by user (cart to order placed)
class BuyerOrderView(APIView):

    def get(self, request, buyerpk, usertype, format=None):
        if usertype != 9:
            order = Order.objects.filter(status__gt=0, buyer=buyerpk)
        else:
            order = Order.objects.filter(status__gt=0)

        serialize = OrderSerializer(order, many=True)
        return Response(serialize.data)


class OrderStatusView(APIView):

    def get(self, request, format=None):
        orderstatus = OrderStatus.objects.all()
        serialize = OrderStatusSerializer(orderstatus, many=True)
        return Response(serialize.data)


class OrderDetailView(APIView):

    def get(self, request, pk, format=None):
        order = Order.objects.filter(id=pk)
        serialize = OrderSerializer(order, many=True)
        return Response(serialize.data)

    def put(self, request, pk, format=None):
        request.data['status'] = 0
        order = Order.objects.filter(id=pk).first()
        serializer = OrderModificationSerializer(order, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        order = Order.objects.filter(id=pk).first()
        order.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class OrderDetailStatusView(APIView):

    @transaction.non_atomic_requests
    def put(self, request, pk, statuspk, format=None):

        order = Order.objects.filter(id=pk).first()
        orderstatus = OrderStatus.objects.filter(id=statuspk).first()
        loggedInUser = User.objects.get(username=request.user)

        if order and orderstatus:

            order.status = orderstatus
            order.save()

            orderdelivery = order.delivery
            orderedproduct = order.product

            try:
                if 'delivered' in orderstatus.name.lower() and orderdelivery:

                    completedinvoicestatus = InvoiceStatus.objects.filter(
                        id=1).first()

                    logger.info('marking the order as delivered')
                    orderdelivery.delivered_on = timezone.now()
                    orderdelivery.save()

                    # updating payment mode as cash on delivery for all now
                    paymentmode = PaymentMode.objects.filter(
                        name='COD').first()

                    logger.info(
                        f'Updating payment mode as {paymentmode.name} with Id {paymentmode.id}')

                    paymentdetail = {
                        "amount_received": order.total_payable,
                        "received_by": loggedInUser.id,
                        "received_on": timezone.now(),
                        "delivery_charge": 0,
                        "modeofpayment": paymentmode.id
                    }

                    logger.info(f'Creating new payment: {paymentdetail}')

                    paymentserializer = PaymentDetailModificationSerializer(
                        data=paymentdetail)

                    if paymentserializer.is_valid():
                        logger.info('marking the order payment as done')
                        savedpayment = paymentserializer.save()

                        paymentmade = PaymentDetail.objects.filter(
                            id=paymentserializer.data['id']).first()

                        logger.info(
                            f'Payment detail to be updated: {savedpayment} || {paymentmade}')

                        invoice = order.invoice
                        invoice.is_payment_done = True
                        invoice.payment_detail = savedpayment
                        invoice.status = completedinvoicestatus
                        logger.info('updating order payment in invoice')
                        invoice.save()
                    else:
                        logger.error(
                            f'Failed to update payment detail for order {order.id} with error {paymentserializer.errors}, hence Invoice is not updated')

                    saleinvoice = SaleOrderInvoice.objects.filter(
                        order=pk).first()
                    if saleinvoice:
                        logger.info(
                            'updating sale invoice detail as sale completed')
                        saleinvoice.status = completedinvoicestatus
                        saleinvoice.save()

                if 'cancel' in orderstatus.name.lower():
                    logger.info('cancelling the order')

                    inactiveinvoicestatus = InvoiceStatus.objects.filter(
                        id=0).first()

                    invoice = order.invoice
                    if invoice:
                        logger.info('cancelling invoice detail')
                        invoice.status = inactiveinvoicestatus
                        invoice.save()

                    saleinvoice = SaleOrderInvoice.objects.filter(
                        order=pk).first()
                    if saleinvoice:
                        logger.info('cancelling sale invoice detail')
                        saleinvoice.status = inactiveinvoicestatus
                        saleinvoice.save()

                    productvariantid = orderedproduct.get_variant().id

                    logger.info(f'Updating stock count for variant id: ' +
                                str(productvariantid))

                    # update product varaint stock count
                    if productvariantid:

                        # update product stocks
                        productvaraiant = ProductVariant.objects.filter(
                            id=productvariantid).first()
                        exitingstock = productvaraiant.stock
                        orderedquantity = order.items_count
                        productvaraiant.stock = exitingstock + orderedquantity
                        logger.info('updating stock count for the product')
                        logger.info(productvaraiant)
                        productvaraiant.save()
                    else:
                        logger.info(
                            'Product variant data is null, hence not updated product stock count')
            except Exception as e:
                logger.error('caught exception when updating invoice')
                logger.error(e)
            return Response({'Order status saved successfully'}, status=status.HTTP_200_OK)
        return Response({'Order Id or Order status not found'}, status=status.HTTP_400_BAD_REQUEST)

# all orders for admin to check


class OrderListView(APIView):

    def get(self, request, format=None):
        order = Order.objects.all()
        serialize = OrderSerializer(order, many=True)
        return Response(serialize.data)


class LivestockVaccinationView(APIView):

    def get(self, request, productid, vaccineid, format=None):

        kwargs = {}

        if productid:
            kwargs['product'] = productid
        if vaccineid:
            kwargs['vaccine'] = vaccineid

        livestock_vaccinations = Livestockvaccinetaken.objects.filter(**kwargs)
        serialize = LivestockvaccinationSerializer(
            livestock_vaccinations, many=True)
        return Response(serialize.data)


class PaymentListView(APIView):

    def get(self, request, format=None):
        payment = PaymentDetail.objects.all()
        serialize = PaymentDetailSerializer(payment, many=True)
        return Response(serialize.data)


class PaymentView(APIView):

    def get(self, request, orderpk, format=None):
        order = Order.objects.filter(order=orderpk)
        if order:
            payment = order.payment
            serialize = PaymentDetailSerializer(payment, many=True)
            return Response(serialize.data)
        return Response('Payment detail not found', status=status.HTTP_404_NOT_FOUND)

    def post(self, request, orderpk, format=None):
        serializer = PaymentDetailSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, orderpk, format=None):
        order = Order.objects.filter(order=orderpk)
        if order:
            payment = order.payment
            serializer = PaymentDetailSerializer(payment, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response('Payment detail not found', status=status.HTTP_404_NOT_FOUND)


class InvoiceView(APIView):

    def get(self, request, pk, format=None):
        invoice = Invoice.objects.filter(id=pk)
        serialize = InvoiceSerializer(invoice, many=True)
        return Response(serialize.data)

    def post(self, request, pk, format=None):
        serializer = InvoiceSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        invoice = Invoice.objects.filter(id=pk)
        serializer = InvoiceSerializer(invoice, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DeliveryView(APIView):

    def get(self, request, pk, format=None):
        delivery = DeliveryDetail.objects.filter(id=pk)
        serialize = DeliveryDetailSerializer(delivery, many=True)
        return Response(serialize.data)

    def post(self, request, pk, format=None):
        serializer = DeliveryDetailSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        delivery = DeliveryDetail.objects.filter(id=pk)
        serializer = DeliveryDetailSerializer(delivery, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProductModificationView(APIView):

    def put(self, request, pk, quantity, sold, isfarmzonnsale, format=None):

        if request.user.is_authenticated:
            loggedInUser = User.objects.get(username=request.user)

            product = Product.objects.filter(id=pk).first()

            if loggedInUser.usertype.id == 9 or loggedInUser == product.seller:

                if quantity and quantity > 0:
                    variant = ProductVariant.objects.get(product=pk)
                    variant.stock = quantity
                    if request.data and request.data['originalprice'] and int(request.data['originalprice']) > 0:
                        variant.original_price = float(
                            request.data['originalprice'])
                        variant.offer_price = float(request.data['offerprice'])
                        variant.weight = float(request.data['weight'])
                    variant.save()
                else:
                    if sold and sold.lower() == 'true':
                        product.is_sold = True
                        product.sold_though_farmzonn = True if isfarmzonnsale == 'true' else False
                    else:
                        product.is_verified = request.data['is_verified']
                        product.verified_by = loggedInUser
                        product.verification_comment = request.data['comment']
                    logger.info(product)
                    logger.info(f'updating product details')
                    product.save()
                return Response('Saved Successfully', status=status.HTTP_200_OK)
            else:
                return Response('Only Admin or Seller can modify Product detail', status=status.HTTP_403_FORBIDDEN)
        return Response('UnAuthorized', status=status.HTTP_401_UNAUTHORIZED)


class ProductAccessView(APIView):

    def post(self, request, pk):

        try:

            loggedInUser = None
            if request.user.is_authenticated:
                loggedInUser = User.objects.get(username=request.user)

            if loggedInUser is None or loggedInUser.usertype.id != 9:
                kwargs = {}
                kwargs['product'] = pk
                kwargs['user'] = loggedInUser.id if loggedInUser is not None else None
                productviewdetail = ProductViewDetail.objects.filter(
                    **kwargs).first()
                
                logger.info(
                        f'Product view detail fetched: {productviewdetail}')

                if productviewdetail:
                    logger.info(
                        f'Updating existing data in product view detail table for {pk}, user: {loggedInUser.id if loggedInUser is not None else None}')
                    productviewdetail.viewcount += 1
                    productviewdetail.save()
                    return Response('View count saved Successfully', status=status.HTTP_200_OK)
                else:
                    logger.info(
                        f'Enter new data in product view detail table for {pk}, user: {loggedInUser.id if loggedInUser is not None else None}')
                    productviewdetail = {
                        "product": pk,
                        "user": loggedInUser.id if loggedInUser is not None else None,
                        "viewcount": 1
                    }

                    productviewdetailserializer = ProductViewDetailSerializer(
                        data=productviewdetail)

                    if productviewdetailserializer.is_valid():
                        productviewdetailserializer.save()
                        return Response('View count saved Successfully', status=status.HTTP_200_OK)
                    else:
                        logger.error(
                            f'Failed to update view count for productid {pk}, user: {request.user}')
                        return Response('Failed to update view count', status=status.HTTP_400_BAD_REQUEST)

            logger.error(
                f'User is of type admin, hence not updated view count for productid {pk}, user: {request.user}')
            return Response('Failed to update view count for admin user', status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.error(
                f'Failed to update view count for productid {pk}, user: {request.user} with ex: {e}')
            return Response('Failed to update view count', status=status.HTTP_400_BAD_REQUEST)


class ProductPriceSetView(APIView):

    def get(self, request, format=None):
        priceset = ProductPriceSet.objects.get(isactive=True)
        serialize = ProductPriceSetSerializer(priceset, many=False)
        return Response(serialize.data, status=status.HTTP_200_OK)


class SaleOrderInvoiceView(APIView):

    def get(self, request, orderpk, format=None):

        saleinvoice = SaleOrderInvoice.objects.get(order=orderpk)

        if saleinvoice:
            serialize = SaleOrderInvoiceSerializer(saleinvoice, many=False)
            return Response(serialize.data, status=status.HTTP_200_OK)
        return Response(None, status=status.HTTP_400_BAD_REQUEST)


class ProductRequirementView(APIView):

    def get(self, request, pk, format=None):
        enquiries = ProductRequirementPosted.objects.get(id=pk)
        serialize = ProductRequirementPostedSerializer(enquiries, many=False)
        return Response(serialize.data, status=status.HTTP_200_OK)

    def put(self, request, pk, format=None):
        enquiry = ProductRequirementPosted.objects.get(id=pk)
        serializer = ProductRequirementModificationSerializer(
            enquiry, data=request.data)

        if serializer.is_valid():
            serializer.save()

            logger.info(
                f'Updated verification status for product requirement enquiry')
            logger.info(
                f'{serializer.data}')
            isverified = serializer.data['isverified']
            logger.info(f'{isverified}')

            logger.info(globalvariable.notification_to_seller_sent_on)
            today = date.today()
            logger.info(today)
            datediff = today - globalvariable.notification_to_seller_sent_on
            logger.info(datediff.days)
            logger.info(
                f'is sending notification to seller: {isverified == 1 and datediff.days > 5}')

            if isverified == 1 and datediff.days > 5:
                sellerlist = User.objects.filter(
                    usertype__in=[2, 3, 4], is_active=True)
                logger.info(f'{sellerlist}')
                if sellerlist and sellerlist.count() > 0:
                    sellerphone = []
                    for seller in sellerlist:
                        sellerphone.append(seller.phone.as_e164)

                    sellerphonestring = ','.join(sellerphone)
                    logger.info(f'{sellerphonestring}')
                    send_enquiry_approval_notification(sellerphonestring, pk)
                    # restting the notification last sent date
                    globalvariable.notification_to_seller_sent_on = date.today()

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProductRequirementsView(APIView):

    def get(self, request, format=None):

        kwargs = {}
        loggedInUser = None

        if request.user.is_authenticated:
            loggedInUser = User.objects.get(username=request.user)

        if loggedInUser is None or loggedInUser.usertype.id != 9:
            kwargs['status'] = 0
            kwargs['isverified'] = 1

        enquiries = ProductRequirementPosted.objects.filter(**kwargs)

        serialize = ProductRequirementPostedSerializer(enquiries, many=True)
        return Response(serialize.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):

        serializer = ProductRequirementModificationSerializer(
            data=request.data)

        if serializer.is_valid():
            serializer.save()

            posteduser_phone = ''
            if request.user.is_authenticated:
                loggedInUser = User.objects.get(username=request.user)
                posteduser_phone = ',' + loggedInUser.phone.as_e164

            notification_phone = serializer.data['contact'] + \
                posteduser_phone
            logger.info(
                f'created new product requirement enquiry')
            send_enquiry_registration_notification(
                notification_phone, serializer.data['id'])
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProductUserRequirementsView(APIView):

    def get(self, request, format=None):

        kwargs = {}
        loggedInUser = None

        if request.user.is_authenticated:
            loggedInUser = User.objects.get(username=request.user)

            if loggedInUser.usertype.id != 9:
                kwargs['posted_by'] = loggedInUser.id

            enquiries = ProductRequirementPosted.objects.filter(**kwargs)

            serialize = ProductRequirementPostedSerializer(
                enquiries, many=True)
            return Response(serialize.data, status=status.HTTP_200_OK)
        return Response("User not authenticated", status=status.HTTP_403_FORBIDDEN)
