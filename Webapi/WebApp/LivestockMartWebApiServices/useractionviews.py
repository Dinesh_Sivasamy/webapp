from http import HTTPStatus
import json
import datetime
from random import randint
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework import status
from django.core.paginator import Paginator
from django.contrib.auth import authenticate, login, logout

from .models import *
from .serializer import *
from .datamodifierserializer import *

from .helpers.viewshelper import *

from .services.smsservice import *

import logging
logger = logging.getLogger(__name__)


class UserView(APIView):

    def get(self, request, format=None):
        if request.user.is_authenticated:
            loggedInUser = User.objects.get(username=request.user)

            if loggedInUser and loggedInUser.usertype.id == 9:
                user = User.objects.all()
                serialize = UserSerializer(user, many=True)
                return Response(serialize.data)
                return Response()
        
        return Response("You are not authorized", status=status.HTTP_403_FORBIDDEN)

    def post(self, request, format=None):
        name = request.data['username']
        phone = request.data['phone']
        
        try:
            existingUsers = User.objects.filter(username__contains=name)
        except:
            logger.info(f'Username {name}, not available. Hence proceeding with registration')

        logger.info(f'Got {existingUsers.count() if existingUsers is not None else 0} existing users with matching name {name}')

        if existingUsers is not None and existingUsers.count() > 0:
            d = datetime.now()
            existingUserscount = existingUsers.count()
            name = f"{name}_{existingUserscount}"
            request.data['username'] = name

        randomint = randint(100001, 999999)
        request.data['otp'] = randomint
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            send_otp(phone, randomint)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        logger.error(f'Error while registering user: {name}')
        logger.error(serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserProfileView(APIView):

    def get(self, request, pk, format=None):

        if request.user.is_authenticated:
            loggedInUser = User.objects.get(username=request.user)

            if loggedInUser and (loggedInUser.usertype.id == 9 or loggedInUser.id == pk):
                userprofile = User.objects.get(id=pk)
                serializer = UserSerializer(userprofile, many=False)
                return Response(serializer.data)
        
        return Response("You are not authorized", status=status.HTTP_403_FORBIDDEN)
    
        

    def put(self, request, pk, format=None):
        userprofile = User.objects.get(id=pk)
        serializer = UserSerializer(userprofile, data=request.data)

        receivedotp = request.data['otp']
        verifyotp = request.data['verify_otp']
        logger.info(f'Verify OTP {verifyotp} received: {receivedotp}, ' +
                    f'expected: {userprofile.otp}')
        if serializer.is_valid():
            if (verifyotp):
                logger.info(f'Into Verify OTP received: ' +
                            f'{receivedotp}, expected: {userprofile.otp}')
                if (receivedotp and userprofile.otp and int(receivedotp) == int(userprofile.otp)):
                    request.data['otp'] = ''
                    serializer.save()
                    send_user_registered_notification(
                        userprofile.phone.as_e164, userprofile.username)
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
                else:
                    return Response('OTP verification failed, Please try again.', status=status.HTTP_400_BAD_REQUEST)
            else:
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    


class UserForgotPasswordInitiateView(APIView):

    def patch(self, request, userphone, format=None):
        userprofile = User.objects.get(phone=userphone)

        randomint = randint(100001, 999999)
        receivedotp = randomint

        # this is the only field we want to update
        updatedata = {"otp": receivedotp}

        serializer = UserSerializer(userprofile, data=updatedata, partial=True)

        logger.info(f'Into update OTP for forgot password with OTP: {receivedotp}')
        if serializer.is_valid():
            serializer.save()
            send_forgot_password_otp_to_user(receivedotp,
                        userprofile.phone.as_e164)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserResetPasswordView(APIView):

    def patch(self, request, userphone, format=None):
        userprofile = User.objects.get(phone=userphone)

        userpassword = request.data['password']
        receivedotp = request.data['otp']

        logger.info(f'Into Verify OTP received: ' +
                            f'{receivedotp}, expected: {userprofile.otp}')
        if (receivedotp and userprofile.otp and int(receivedotp) == int(userprofile.otp)):
            udpatedata = {"otp": None, "password": userpassword, "is_active": True}

            logger.info(f'OTP is same hence updating password')

            serializer = UserSerializer(userprofile, data=udpatedata, partial=True)
        
            if serializer.is_valid():            
                serializer.save()
                logger.info(f'Reset password completed for user: {userprofile.username}')

                user = authenticate(request, username=userphone, password=userpassword)

                if user is not None:
                    login(request, user)
                    userobject = User.objects.filter(username=user).first()
                    userResponse = {
                        'id':  str(user.pk),
                        'phone': str(user.get_username()),
                        'name': str(user.__dict__["username"]),
                        'admin': str(user.__dict__["is_admin"]),
                        'staff': str(user.__dict__["is_staff"]),
                        'superuser': str(user.__dict__["is_superuser"]),
                        'usertype': str(userobject.usertype.id),
                        'referralcode': str(user.__dict__["userreferralcode"])
                    }

                    logger.info(f'Reset password & logged in successful for user: {userprofile.username}')
                    return Response(json.dumps(userResponse), status=status.HTTP_202_ACCEPTED)
                return Response('Failed to authenticate User', status=status.HTTP_401_UNAUTHORIZED)
            logger.info(f'Serializer validation failed with: {serializer.errors}')
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response('User OTP not matched', status=status.HTTP_400_BAD_REQUEST)
    
    


class UserProfileImageView(APIView):
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request, format=None):
        serializer = UserProfileImageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LoginView(APIView):

    def post(self, request, format=None):
        username = request.data['username']
        password = request.data['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            userobject = User.objects.filter(username=user).first()
            userResponse = {
                'id':  str(user.pk),
                'phone': str(user.get_username()),
                'name': str(user.__dict__["username"]),
                'admin': str(user.__dict__["is_admin"]),
                'staff': str(user.__dict__["is_staff"]),
                'superuser': str(user.__dict__["is_superuser"]),
                'usertype': str(userobject.usertype.id),
                'referralcode': str(user.__dict__["userreferralcode"])
            }

            loggedInUsername = str(user.__dict__["username"])

            # userobject.last_login = datetime.now()
            # user_login_update = UserSerializer(data=userobject)

            # if user_login_update.is_valid():
            #     user_login_update.save()
            # else:
            #     logger.info(
            #         f'Failed to update last login for user: {loggedInUsername}')

            logger.info(f'login successful for user: {loggedInUsername}')
            return Response(json.dumps(userResponse), status=status.HTTP_202_ACCEPTED)
        else:
            return Response("User Authentication failed", status=status.HTTP_401_UNAUTHORIZED)


class LogoutView(APIView):

    def post(self, request, format=None):
        if request.user.is_authenticated:
            logout(request)
            return Response('User logged out successfully', status=status.HTTP_204_NO_CONTENT)
        else:
            return Response('User already logged out', status=status.HTTP_204_NO_CONTENT)


class SessionView(APIView):

    def post(self, request, format=None):      
        
        if request.user.is_authenticated:
            user = request.user            
            userobject = User.objects.filter(username=user).first()
            logger.info(f'Fetched user details for existing user session successful for user: {userobject}')
            
            userResponse = {
                'id':  str(userobject.id),
                'phone': str(userobject.phone),
                'name': str(userobject.username),
                'admin': str(userobject.is_admin),
                'staff': str(userobject.is_staff),
                'superuser': str(userobject.is_superuser),
                'usertype': str(userobject.usertype.id),
                'referralcode': str(userobject.userreferralcode)
            }

            loggedInUsername = str(user)

            logger.info(f'Validate existing user session successful for user: {loggedInUsername}')
            return Response(json.dumps(userResponse), status=status.HTTP_202_ACCEPTED)
        else:
            return Response("Existing session validation failed", status=status.HTTP_401_UNAUTHORIZED)
        

class UserAddressView(APIView):

    def get(self, request, userpk, format=None):
        useraddress = UserAddress.objects.filter(user=userpk)
        serialize = UserAddressSerializer(
            useraddress, many=True)
        return Response(serialize.data)

    def post(self, request, userpk, format=None):
        addressserializer = AddressModificationSerializer(data=request.data)
        if addressserializer.is_valid():
            addressresponse = addressserializer.save()
            logger.info(f'addressresponse: ' + str(addressresponse.id))
            useraddressrequest = {
                "user": userpk,
                "address": addressresponse.id
            }
            logger.info(f'useraddress request: ' + str(useraddressrequest))
            serializer = UserAddressModificationSerializer(
                data=useraddressrequest)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
class UserDeliveryAddressView(APIView):

    def get(self, request, pk, format=None):
        useraddress = UserAddress.objects.get(id=pk)
        serialize = UserAddressSerializer(
            useraddress, many=False)
        return Response(serialize.data)


class ReferredUsersCountView(APIView):

    def get(self, request, referralcode, foramt=None):
        referredusercount = User.objects.filter(
            loginreferralcode__contains=referralcode)
        if referredusercount:
            return Response(referredusercount.count(), status=status.HTTP_200_OK)
        return Response(0, status=status.HTTP_400_BAD_REQUEST)


class UserDiscountView(APIView):

    def get(self, request, buyerpk, userrefcode, format=None):
        userdiscount = getdiscountforuser(buyerpk, userrefcode)

        if userdiscount:
            serialize = DiscountSerializer(
            userdiscount, many=False)
            return Response(serialize.data, status=status.HTTP_200_OK)
        return Response(None, status=status.HTTP_200_OK)


class SellerReviewView(APIView):

    def get(self, request, format=None):
        review = SellerReview.objects.all()
        serialize = SellerReviewSerializer(review, many=True)
        return Response(serialize.data)

    def post(self, request, format=None):
        serializer = SellerReviewSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)