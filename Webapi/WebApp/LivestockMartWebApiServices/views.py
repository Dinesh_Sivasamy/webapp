from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from .services.smsservice import *
from .models import *
from .serializer import *
from .datamodifierserializer import *

import logging
logger = logging.getLogger(__name__)

# Create your views here.


class CountryView(APIView):

    def get(self, request, format=None):
        country = Country.objects.all()
        serialize = CountrySerializer(country, many=True)
        return Response(serialize.data)

    def post(self, request, format=None):
        serializer = CountrySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class StateView(APIView):

    def get(self, request, pk, format=None):
        state = State.objects.filter(country=pk)
        serialize = StateSerializer(state, many=True)
        return Response(serialize.data)

    def post(self, request, pk, format=None):
        serializer = StateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CityView(APIView):

    def get(self, request, pk, format=None):
        city = City.objects.filter(state=pk)
        serialize = CitySerializer(city, many=True)
        return Response(serialize.data)

    def post(self, request, pk, format=None):
        serializer = CitySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UsertitleView(APIView):

    def get(self, request, format=None):
        title = Usertitle.objects.all()
        serialize = UsertitleSerializer(title, many=True)
        return Response(serialize.data)

    def post(self, request, format=None):
        serializer = UsertitleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AddresstypeView(APIView):

    def get(self, request, format=None):
        addresstype = Addresstype.objects.all()
        serialize = AddresstypeSerializer(addresstype, many=True)
        return Response(serialize.data)

    def post(self, request, format=None):
        serializer = AddresstypeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AddressView(APIView):

    def get(self, request, format=None):
        address = Address.objects.all()
        serialize = AddressSerializer(address, many=True)
        return Response(serialize.data)

    def post(self, request, format=None):
        serializer = AddressModificationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AddressDetailView(APIView):

    def get(self, request, pk,  format=None):
        address = Address.objects.filter(id=pk)
        serialize = AddressSerializer(address, many=True)
        return Response(serialize.data)


class UsertypeView(APIView):

    def get(self, request, format=None):
        usertype = Usertype.objects.all()
        serialize = UsertypeSerializer(usertype, many=True)
        return Response(serialize.data)

    def post(self, request, format=None):
        serializer = UsertypeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProductCategorytypeView(APIView):

    def get(self, request, format=None):
        categorytype = Productcategorytype.objects.all()
        serialize = ProductCategorytypeSerializer(categorytype, many=True)
        return Response(serialize.data)

    def post(self, request, format=None):
        serializer = ProductCategorytypeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProductCategoryView(APIView):

    def get(self, request, pk, format=None):
        category = Productcategory.objects.filter(type=pk)
        serialize = ProductCategorySerializer(category, many=True)
        return Response(serialize.data)

    def post(self, request, pk, format=None):
        serializer = ProductCategorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DiscounttypeView(APIView):

    def get(self, request, format=None):
        discounttype = Discounttype.objects.all()
        serialize = DiscounttypeSerializer(discounttype, many=True)
        return Response(serialize.data)

    def post(self, request, format=None):
        discount_type_serializer = DiscounttypeSerializer(data=request.data)
        if discount_type_serializer.is_valid():
            discount_type_serializer.save()
            return Response(discount_type_serializer.data, status=status.HTTP_201_CREATED)
        return Response(discount_type_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DiscountView(APIView):

    def get(self, request, format=None):
        discount = Discount.objects.all()
        serialize = DiscountSerializer(discount, many=True)
        return Response(serialize.data)

    def post(self, request, format=None):
        serializer = DiscountModificationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # def put(self, request, pk, format=None):
    #     discount = self.get_object(pk)
    #     serializer = DiscountModificationSerializer(
    #         discount, data=request.DATA)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return Response(serializer.data, status=status.HTTP_201_CREATED)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LivestockbreedView(APIView):

    def get(self, request, pk, format=None):
        breed = Livestockbreed.objects.filter(category=pk)
        serialize = LivestockbreedSerializer(breed, many=True)
        return Response(serialize.data)

    def post(self, request, pk, format=None):
        serializer = LivestockbreedSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LivestockvaccinationView(APIView):

    def get(self, request, pk, format=None):
        vaccines = Livestockvaccination.objects.filter(category=pk)
        serialize = LivestockvaccinationSerializer(vaccines, many=True)
        return Response(serialize.data)

    def post(self, request, pk, format=None):
        serializer = LivestockvaccinationModificationSerializer(
            data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LivestockgenderView(APIView):

    def get(self, request, format=None):
        gender = Livestockgender.objects.all()
        serialize = LivestockgenderSerializer(gender, many=True)
        return Response(serialize.data)

    def post(self, request, format=None):
        serializer = LivestockgenderSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LivestockageView(APIView):

    def get(self, request, format=None):
        age = LivestockAge.objects.all()
        serialize = LivestockAgeSerializer(age, many=True)
        return Response(serialize.data)

    def post(self, request, format=None):
        serializer = LivestockAgeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LivestockcolorView(APIView):

    def get(self, request, format=None):
        color = Livestockcolor.objects.all()
        serialize = LivestockcolorSerializer(color, many=True)
        return Response(serialize.data)

    def post(self, request, format=None):
        serializer = LivestockcolorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LivestockfeedtypeView(APIView):

    def get(self, request, format=None):
        feedtype = Livestockfeedtype.objects.all()
        serialize = LivestockfeedtypeSerializer(feedtype, many=True)
        return Response(serialize.data)

    def post(self, request, format=None):
        serializer = LivestockfeedtypeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class HeaderListView(APIView):

    def get(self, request, format=None):
        categorytype = Productcategorytype.objects.all()
        serialize = ProductCategorytypeHeaderSerializer(
            categorytype, many=True)
        return Response(serialize.data)


class DeliverablePincodeView(APIView):

    def get(self, request, format=None):
        pincodes = DeliverablePincode.objects.all()
        serialize = DeliverablePincodeSerializer(
            pincodes, many=True)
        return Response(serialize.data)

class ThreadView(APIView):

    def get(self, request, format=None):
        thread = Thread.objects.all().prefetch_related()
        serialize = ThreadSerializer(thread, many=True)
        return Response(serialize.data)

    def post(self, request, format=None):

        serializer = ThreadModificationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()

            userlist = User.objects.filter(
                        usertype__in=[1, 2, 3, 4], is_active=True)
            logger.info(f'{userlist}')
            
            if userlist and userlist.count() > 0:
                logger.info(f'Sending new thread notification to {userlist.count()} users')
                
                userphone = []
                for user in userlist:
                    if request.user is not None and request.user != user.username:
                        logger.info(f"Sending new thread notification for {user.username} user with phone {user.phone}")
                        userphone.append(user.phone.as_e164)
                
                logger.info(f"constructing string to send new thread notification")
                userphonestring = ','.join(userphone)
                logger.info(f'{userphonestring}')
                send_new_forum_thread(userphonestring)
                logger.info(f"Sent new thread notification for {userlist.count()} users")
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class ThreadReplyView(APIView):

    def get(self, request, threadpk, format=None):
        threadreply = ThreadReply.objects.filter(thread = threadpk)
        serialize = ThreadReplySerializer(threadreply, many=True)
        return Response(serialize.data)

    def post(self, request, threadpk, format=None):
        
        number = None
        if request.user.is_authenticated:
            loggedInUser = User.objects.get(username=request.user)
            number = loggedInUser.phone.as_e164
        
        serializer = ThreadReplyModificationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()

            if number is not None:
                send_new_forum_thread_reply(number)

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class ThreadLikeView(APIView):

    def post(self, request, threadpk, format=None):
        thread = Thread.objects.filter(id=threadpk).first()
        if thread:
            thread.likescount += 1
            thread.save()
            return Response("Saved successfully", status=status.HTTP_201_CREATED)
        return Response("Failed to save like count", status=status.HTTP_400_BAD_REQUEST)