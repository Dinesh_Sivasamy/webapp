# Download the helper library from https://www.twilio.com/docs/python/install
import os
import json
import requests

from django.conf import settings

import logging
logger = logging.getLogger(__name__)

# # Find your Account SID and Auth Token at twilio.com/console
# # and set the environment variables. See http://twil.io/secure

# def send_sms(to_number, flowid, var1, var2):
#     # os.environ['TWILIO_ACCOUNT_SID']
#     account_sid = 'ACa047e6ca219c985043a94d4fb0d5defb'
#     # os.environ['TWILIO_AUTH_TOKEN']
#     auth_token = '40848f3327472662a61bda73c2eed714'
#     from_number = '+17472986769'  # os.environ['SMS_FROM_NUMBER']
#     send_sms = False  # os.environ['SEND_SMS']

#     if send_sms:
#         client = Client(account_sid, auth_token)

#         message = client.messages \
#                         .create(
#                             body=message,
#                             from_=from_number,
#                             to=to_number
#                         )

#         logger.info(f'SMS to: {to_number} with message: {message} ' +
#               f'has got twillo Id: {message.sid}, error_code: {message.error_code} ' +
#               f'and error response: {message.error_message}')

#         return message.error_code
#     else:
#         logger.info(
#             f'Send SMS is false, hence message: {message}, to: {to_number} is not sent')


def send_sms(to_number, message):

    try:
        # 'riJfzmTQ5nVk06xf'
        api_key = settings.MTALKZ_API_KEY
        # 'MTAMOI'
        sender_id = settings.SMS_SENDER_ID

        api_url = "http://msg.mtalkz.com/V2/http-api-post.php"

        send_sms = settings.SEND_SMS

        if send_sms:
            sms_detail = {
                "apikey": api_key,
                "senderid": sender_id,
                "number": to_number,
                "message": message,
                "format": "json"
            }

            logger.info(f'SMS options: {json.dumps(sms_detail)}')

            sms_response = requests.post(api_url, data=json.dumps(sms_detail))

            logger.info(f'SMS to: {to_number} with message: {message} ' +
                        f'has got response: {sms_response.json()}, status code: {sms_response.status_code}')

            return sms_response.status_code
        else:
            logger.info(
                f'Send SMS is false, hence message: {message}, to: {to_number} is not sent/skipped')

    except Exception as ex:
        logger.error(
            f"Failed to send SMS message: {message} to: {to_number} with exception: {ex}")

# Send SMS for User activity


def send_otp(to_number, otp):
    try:
        send_sms(
            to_number, f'{otp} is your OTP for registering in Farmzonn.com.')

    except Exception as ex:
        logger.error(
            f"Failed to send SMS OTP {otp} to {to_number} with exception: {ex}")


def send_user_registered_notification(to_number, username):
    try:
        send_sms(
            to_number, f'Dear {username}, \n You have successfully completed your account registration in Farmzonn.com \nThanks, \nTeam Farmzonn')

    except Exception as ex:
        logger.error(
            f"Failed to send user registered notification SMS to {to_number} with exception: {ex}")

# ADMIN related Send SMS


def send_admin_notification(type):

    try:

        sms_content = ''
        admin_phone = settings.ADMIN_CONTACT_PHONE

        if type == 'PRODUCT_REGISTERED_SUCCESSFULLY':
            sms_content = 'Dear Admin, \nNew Product has been registered for listing. You can view the details in www.Farmzonn.com/seller-product/. Please review it and verify it for listing. \n- Farmzonn.com'

        elif type == 'ORDER_PLACED_SUCCESSFULLY':
            sms_content = 'Dear Admin, \nA new order has been placed in Farmzonn.com! You can view order details here: www.Farmzonn.com/orders.\n- Farmzonn.com'

        elif type == 'NEW_ENQUIRY_REGISTERED_SUCCESSFULLY':
            sms_content = 'Hi Admin, A new enquiry has been submitted. Please verify it soon. - Team Farmzonn'

        send_sms(admin_phone, sms_content)

    except Exception as ex:
        logger.error(
            f"Failed to send {type} notification SMS to Admin phone {admin_phone} with exception: {ex}")


# product registration notification to Seller and ADMIN
def send_product_registered_notification(to_number, sellername):
    try:

        send_sms(to_number, f'Dear {sellername}, \n Successfully registered your livestock for sale through Farmzonn.com, your livestock will be listed for buyers once our verification process is done. \nThanks,\n Farmzonn.com')

        send_admin_notification('PRODUCT_REGISTERED_SUCCESSFULLY')

    except Exception as ex:
        logger.error(
            f"Failed to send Product registration notification SMS to {to_number} with exception: {ex}")


# Order placed notification to Buyer, Seller and ADMIN
def send_order_notification_to_buyer(to_number, buyername):
    try:
        send_sms(
            to_number, f'Dear {buyername}, \n your Farmzonn.com order has been placed successfully! View your order details here: www.Farmzonn.com/orders')

    except Exception as ex:
        logger.error(
            f"Failed to send order notification to buyer number {to_number} with exception: {ex}")


def send_order_notification_to_seller(to_number, sellername, productid):
    try:

        send_sms(
            to_number, f'Dear Seller, \nAn order has been placed on livestock that you have listed in Farmzonn.com. One of our agent will contact you to verify listed livestock and proceed with delivery process. Ordered livestock details here: https://farmzonn.com/product/{productid}.  You can also find your livestocks listing status in https://farmzonn.com/seller-product/. \nThanks, \nTeam Farmzonn.com')

    except Exception as ex:
        logger.error(
            f"Failed to send order notification SMS to seller number {to_number} with exception: {ex}")


def send_order_placed_notification(buyer_number, buyername, seller_number, sellername, productid):
    try:

        send_order_notification_to_buyer(buyer_number, buyername)

        send_order_notification_to_seller(seller_number, sellername, productid)

        send_admin_notification('ORDER_PLACED_SUCCESSFULLY')

    except Exception as ex:
        logger.error(
            f"Failed to send order placed notification SMS for product Id: {productid}, buyername: {buyername}, sellername: {sellername} with exception: {ex}")


def send_enquiry_registration_notification_to_user(to_number):
    try:

        send_sms(
            to_number, f'Hello Sir/madam, Your new enquiry has been submitted successfully. Our team will review your request and approve it sooner.' +
            ' Thanks for using farmzonn. - Team Farmzonn.com')

    except Exception as ex:
        logger.error(
            f"Failed to send enquiry registration notification SMS to user number {to_number} with exception: {ex}")


def send_enquiry_registration_notification(user_number, enquiryid):
    try:

        send_enquiry_registration_notification_to_user(user_number)

        send_admin_notification('NEW_ENQUIRY_REGISTERED_SUCCESSFULLY')

    except Exception as ex:
        logger.error(
            f"Failed to send enquiry registration notification SMS for enquiry Id: {enquiryid} with exception: {ex}")


def send_enquiry_approval_notification_to_user(to_numbers):
    try:

        send_sms(
            to_numbers, f'Hello Sir/madam, new requirement has been posted in farmzonn. Checkout farmzonn.com/view-requirements for more. - Team Farmzonn.com')

    except Exception as ex:
        logger.error(
            f"Failed to send enquiry approval SMS to user numbers {to_numbers} with exception: {ex}")


def send_enquiry_approval_notification(user_numbers, enquiryid):
    try:

        send_enquiry_approval_notification_to_user(user_numbers)

    except Exception as ex:
        logger.error(
            f"Failed to send enquiry approval notification SMS for enquiry Id: {enquiryid} with exception: {ex}")      


def send_forgot_password_otp_to_user(otp, to_number):
    try:

        send_sms(
            to_number, f'{otp} is your OTP for resetting your password in Farmzonn.com.')

    except Exception as ex:
        logger.error(
            f"Failed to send forgot password OTP: {otp} SMS to user number {to_number} with exception: {ex}")
        

def send_new_product_listing_notification_to_user(to_numbers):
    try:
        
        send_sms(
            to_numbers, f'Hello Sir/madam, new livestock has been added in farmzonn.com for your enquiry. Checkout farmzonn.com/buy/0 for more. - Team Farmzonn.com')

    except Exception as ex:
        logger.error(
            f"Failed to send new product listing SMS to user numbers {to_numbers} with exception: {ex}")
        

def send_enquiry_approval_notification_sellers(seller_numbers):
    try:

        send_sms(seller_numbers, 
                 f'Hello Sir/Madam, new livestock wanted list has been added in farmzonn for your post. Checkout farmzonn.com/view-requirements to view requirements and post your livestock for sale in farmzonn.com/sell. - Team Farmzonn.com')

    except Exception as ex:
        logger.error(
            f"Failed to send enquiry approval notification to sellers : {seller_numbers} with exception: {ex}")

def send_common_notification(number, msg):
    try:

        send_sms(number, msg)

    except Exception as ex:
        logger.error(f'Failed to send common notification {msg} to {number} with exception: {ex}')


def send_product_registration_expiry(sellers_number):
    try:

        send_sms(sellers_number, f'Hello Sir/Madam, Your product listing in farmzonn.com will get expired after 90 days of listing. Please update your product registration or add new registration to continue listing product to wider buyers around world. Thanks, Team Farmzonn.com')

    except Exception as ex:
        logger.error(f'Failed to send post successful product registration notification to {sellers_number} with exception: {ex}')

def send_new_forum_thread(number):
    try:

        send_sms(number, f'Hello from Farmzonn Forum! A new discussion thread on your favorite topic has just been posted. Join the conversation and share your insights! Click farmzonn.com/forum to participate. Happy farming!')

    except Exception as ex:
        logger.error(f'Failed to send new forum thread notification to {number} with exception: {ex}')


def send_new_forum_thread_reply(number):
    try:

        send_sms(number, f'Hello! Someone has replied to your recent thread on Farmzonn Forum. Check out the response and keep the conversation going! Click farmzonn.com/forum to view. Happy discussing!')

    except Exception as ex:
        logger.error(f'Failed to send new forum thread notification to {number} with exception: {ex}')