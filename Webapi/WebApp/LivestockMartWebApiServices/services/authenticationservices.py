from django.http.response import HttpResponseForbidden
import logging
logger = logging.getLogger(__name__)


def is_user_active_authenticated(user):
    return user and user.is_authenticated and user.is_active

# checks if user has usertype, usertype in lowercase


def check_user_is_of_type(user, usertype):
    logger.info(
        f'User: {user}, type: {user.usertype.name}, check value: {usertype}')
    return user.usertype.name.lower() == usertype


def check_if_buyer(user):
    return is_user_active_authenticated(user) and check_user_is_of_type(user, 'buyer')


def check_if_seller(user):
    return is_user_active_authenticated(user) and check_user_is_of_type(user, 'seller')


def check_if_veterinary(user):
    return is_user_active_authenticated(user) and check_user_is_of_type(user, 'veterinary')


def check_if_expert(user):
    return is_user_active_authenticated(user) and check_user_is_of_type(user, 'expert')
