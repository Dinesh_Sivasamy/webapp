from django.contrib import admin
from .models import *

# Register your models here.
# user
admin.site.register(User)
admin.site.register(Usertitle)
admin.site.register(Usertype)
admin.site.register(UserProfileimage)

# location and address
admin.site.register(Addresstype)
admin.site.register(Address)
admin.site.register(UserAddress)
admin.site.register(Country)
admin.site.register(State)
admin.site.register(City)
admin.site.register(DeliverablePincode)

# product Category
admin.site.register(Productcategorytype)
admin.site.register(Productcategory)

# discounts
admin.site.register(Discounttype)
admin.site.register(Discount)
admin.site.register(ProductPriceSet)

# Livestock
admin.site.register(Livestockbreed)
admin.site.register(Livestockcolor)
admin.site.register(LivestockAge)
admin.site.register(Livestockfeedtype)
admin.site.register(Livestockgender)
admin.site.register(Livestockvaccination)

# product and product related details
admin.site.register(Product)
admin.site.register(Variant)
admin.site.register(ProductVariant)
admin.site.register(LivestockVeterinaryCheck)
admin.site.register(Livestockvaccinetaken)
admin.site.register(Productimages)
admin.site.register(ProductViewDetail)

# order and payment and delivery
admin.site.register(PaymentDetail)
admin.site.register(PaymentMode)
admin.site.register(Order)
admin.site.register(OrderStatus)
admin.site.register(InvoiceStatus)
admin.site.register(DeliveryDetail)
admin.site.register(DeliveryMode)
admin.site.register(SaleOrderInvoice)

#threads
admin.site.register(Thread)
admin.site.register(ThreadReply)