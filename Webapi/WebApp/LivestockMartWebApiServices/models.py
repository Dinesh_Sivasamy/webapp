import uuid
import os
import string
import random  # define the random module
from django.utils.deconstruct import deconstructible
from django.db import models
from django.db.models.deletion import CASCADE, PROTECT, DO_NOTHING
from django.db.models.fields import DateField, IntegerField, DecimalField, CharField, DateTimeField, BooleanField, NullBooleanField, PositiveSmallIntegerField, UUIDField, EmailField, PositiveIntegerField
from django.db.models.fields.related import ForeignKey, OneToOneField
from django.template.defaultfilters import slugify, truncatechars
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.utils.timezone import datetime
from .managers import UserManager

from io import BytesIO
from PIL import Image
from django.core.files import File

import logging
logger = logging.getLogger(__name__)

# from .invoicemodels import *

# Create your models here.

# users
class Usertitle(models.Model):
    id = PositiveSmallIntegerField(primary_key=True)
    title = CharField(max_length=20, null=False,
                      blank=False, unique=True)

    def __str__(self):
        return "%s - %s" % (self.id, self.title)

    class Meta:
        db_table = 'USERTITLE'
        ordering = ['title']


class Country(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = CharField(max_length=200, unique=True)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    class Meta:
        db_table = 'COUNTRY'
        ordering = ['name']


class State(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = CharField(max_length=200, unique=True)
    country = ForeignKey(Country, related_name='states', on_delete=CASCADE)

    def __str__(self):
        return "%s - %s - %s" % (self.id, self.name, self.country.name)

    class Meta:
        db_table = 'STATE'
        ordering = ['name']


class City(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = CharField(max_length=200, unique=True)
    state = ForeignKey(State, related_name='cities', on_delete=CASCADE)

    def __str__(self):
        return "%s - %s -  %s" % (self.id, self.name, self.state.name)

    class Meta:
        db_table = 'CITY'
        ordering = ['name']


class Usertype(models.Model):
    id = PositiveSmallIntegerField(primary_key=True)
    name = CharField(max_length=200, unique=True)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    class Meta:
        db_table = 'USERTYPE'
        ordering = ['name']


class Addresstype(models.Model):
    id = PositiveSmallIntegerField(primary_key=True)
    name = CharField(max_length=200, unique=True)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    class Meta:
        db_table = 'ADDRESSTYPE'
        ordering = ['name']


class Address(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = CharField(max_length=2000, blank=True, null=True)
    address_1 = CharField(max_length=2000, blank=False, null=False)
    address_2 = CharField(max_length=2000)
    type = ForeignKey(
        Addresstype, related_name='addresstype', on_delete=PROTECT)
    city = CharField(max_length=500, null=True)
    state = CharField(max_length=500, null=True)
    pincode = IntegerField(blank=False, null=False)
    phone = CharField(max_length=20, blank=True, null=True)

    def __str__(self):
        return "%s - %s - %s" % (self.id, self.name, self.type.name)

    class Meta:
        db_table = 'ADDRESS'

#image compression method
def compress(image):
    im = Image.open(image)
    rgb_im = im.convert('RGB')
    rgb_im.save(image)
    im_io = BytesIO()
    rgb_im.save(im_io, 'JPEG', quality=50) 
    new_image = File(im_io, name=image.name)
    return new_image


def get_profile_image_filename(instance, filename):
    title = instance.user.id
    slug = slugify(title)
    return "profile-photo/%s-%s" % (slug, filename)


def get_random_referral_code():
    S = 10  # number of characters in the string.
    # call random.choices() string module to find the string in Uppercase + numeric data.
    ran = ''.join(random.choices(string.ascii_uppercase + string.digits, k=S))
    return ran


class User(AbstractBaseUser):
    id = UUIDField(primary_key=True, default=uuid.uuid4)
    phone = PhoneNumberField(verbose_name="phone",
                             unique=True, blank=False, null=False)
    email = models.EmailField(verbose_name="email",
                              max_length=60, null=True)
    username = models.CharField(
        max_length=100, unique=True, default='username_test')
    userdegree = models.CharField(
        max_length=30, blank=True, null=True)
    pannumber = models.CharField(max_length=30, null=True)
    created = models.DateTimeField(
        verbose_name='created', auto_now_add=True, null=True)
    last_login = models.DateTimeField(
        verbose_name='last login', auto_now=True, null=True)
    is_admin = models.BooleanField(default=False)
    is_staff = BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)
    is_verified = BooleanField(default=False)
    verified_by = ForeignKey(
        "self", related_name='verified_admin', null=True, on_delete=PROTECT)
    verified_on = DateTimeField(null=True)
    title = ForeignKey(Usertitle, related_name='usertitle',
                       null=True, on_delete=PROTECT, default=0)
    usertype = ForeignKey(Usertype, related_name='usertype',
                          on_delete=PROTECT, default=1)
    otp = PositiveIntegerField(null=True) 
    userreferralcode = models.CharField(
        max_length=30, null=True)
    loginreferralcode = models.CharField(
        max_length=30, null=True)

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = ['username']

    objects = UserManager()

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.created:
            self.created = timezone.now()

        if self.password:
            self.set_password(self.password)

        if not self.userreferralcode:
            self.userreferralcode = get_random_referral_code()

        return super(User, self).save(*args, **kwargs)

    def __str__(self):
        return "%s" % (self.username)

    def has_perm(self, perm, obj=None):
        return self.is_admin

    # For checking permissions. to keep it simple all admin have ALL permissons
    def is_adminUser(self, perm, obj=None):
        return self.is_admin

    # For checking if the user is a buyer/seller/veterinary/SME
    def get_usertype(self, perm, obj=None):
        return self.usertype.name

    # For checking if the user is internal user
    def is_internalUser(self, perm, obj=None):
        return self.is_staff

    # Does this user have permission to view this app? (ALWAYS YES FOR SIMPLICITY)
    def has_module_perms(self, app_label):
        return True

    class Meta:
        db_table = 'USER'


class UserProfileimage(models.Model):
    user = ForeignKey(User, related_name='user_image',
                      on_delete=PROTECT, default=None)
    image = models.ImageField(upload_to=get_profile_image_filename,
                              verbose_name='Image')

    def __str__(self):
        return "%s - %s" % (self.user.id, self.image)

    #calling image compression function before saving the data
    def save(self, *args, **kwargs):
                new_image = compress(self.image)
                self.image = new_image
                super().save(*args, **kwargs)

    class Meta:
        db_table = 'USERPROFILEIMAGE'
        ordering = ['user']


# product category type and product category details
class Productcategorytype(models.Model):
    id = PositiveSmallIntegerField(primary_key=True)
    name = CharField(max_length=200, unique=True, blank=False, null=False)
    shortname = CharField(max_length=100, null=True)
    description = CharField(max_length=1000)
    listing_precedence = PositiveSmallIntegerField()

    def __str__(self):
        return "%s - %s - %s" % (self.id, self.name, self.description)

    class Meta:
        db_table = 'PRODUCTCATEGORYTYPE'
        ordering = ['name']


class Variant(models.Model):
    id = PositiveSmallIntegerField(primary_key=True)
    name = CharField(max_length=200, unique=True, blank=False, null=False)
    description = CharField(max_length=1000, null=True)

    def __str__(self):
        return "%s - %s - %s" % (self.id, self.name, self.description)

    class Meta:
        db_table = 'VARIANT'
        ordering = ['id']


class Productcategory(models.Model):
    id = PositiveSmallIntegerField(primary_key=True)
    name = CharField(max_length=200, unique=True, blank=False, null=False)
    shortname = CharField(max_length=100, null=True)
    description = CharField(max_length=1000, null=True)
    varianttype = ForeignKey(
        Variant, related_name='variant_type', null=True, on_delete=PROTECT)
    type = ForeignKey(Productcategorytype,
                      related_name='categories', on_delete=CASCADE)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    class Meta:
        db_table = 'PRODUCTCATEGORY'
        ordering = ['name']


# discounts
class Discounttype(models.Model):
    id = IntegerField(primary_key=True)
    name = CharField(max_length=200, unique=True, blank=False, null=False)
    description = CharField(max_length=200)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    class Meta:
        db_table = 'DISCOUNTTYPE'
        ordering = ['name']


class Discount(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = CharField(max_length=200, blank=False, null=False)
    type = ForeignKey(
        Discounttype, related_name='type', on_delete=PROTECT)
    description = CharField(max_length=200)
    discount_percent = DecimalField(
        max_digits=5, decimal_places=2, blank=True, null=True)
    discount_rupees = DecimalField(
        max_digits=10, decimal_places=2,  blank=True, null=True)
    max_discount_rupees = DecimalField(
        max_digits=10, decimal_places=2,  blank=True, null=True)
    is_active = BooleanField(default=True)
    applicable_from = DateTimeField()
    applicable_to = DateTimeField()
    created_date = DateTimeField(auto_now_add=True)
    modified_date = DateTimeField(auto_now=True, blank=True, null=True)
    created_by = ForeignKey(
        User, related_name='%(class)s_requests_created', on_delete=PROTECT, blank=True, null=True)
    modified_by = ForeignKey(User, related_name='%(class)s_requests_modified',
                             on_delete=PROTECT, blank=True, null=True)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.created_date:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(Discount, self).save(*args, **kwargs)

    class Meta:
        db_table = 'DISCOUNT'
        ordering = ['created_date']


# livestock details
class Livestockbreed(models.Model):
    id = PositiveIntegerField(primary_key=True)
    name = CharField(max_length=200, unique=True, blank=False, null=False)
    shortname = CharField(max_length=100, null=True)
    description = CharField(max_length=2000, blank=True, null=True)
    category = ForeignKey(
        Productcategory, related_name='breeds', on_delete=PROTECT)

    def __str__(self):
        return "%s - %s - %s" % (self.id, self.name, self.description)

    class Meta:
        db_table = 'LIVESTOCKBREED'
        ordering = ['name']


class Livestockgender(models.Model):
    id = PositiveSmallIntegerField(primary_key=True)
    name = CharField(max_length=200, unique=True, blank=False, null=False)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    class Meta:
        db_table = 'LIVESTOCKGENDER'
        ordering = ['name']


class Livestockcolor(models.Model):
    id = PositiveSmallIntegerField(primary_key=True)
    name = CharField(max_length=200, unique=True, blank=False, null=False)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    class Meta:
        db_table = 'LIVESTOCKCOLOR'
        ordering = ['name']


class Livestockvaccination(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = CharField(max_length=200, blank=False, null=False)
    primary_dose = CharField(max_length=250, blank=True, null=True)
    booster_dose = CharField(max_length=250, blank=True, null=True)
    revaccination = CharField(max_length=250, blank=True, null=True)
    category = ForeignKey(
        Productcategory, related_name='vaccines', on_delete=PROTECT)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    class Meta:
        db_table = 'LIVESTOCKVACCINATION'
        ordering = ['name']


class Livestockfeedtype(models.Model):
    id = PositiveSmallIntegerField(primary_key=True)
    name = CharField(max_length=200, unique=True, blank=False, null=False)
    description = CharField(max_length=2000, blank=True, null=True)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    class Meta:
        db_table = 'LIVESTOCKFEEDTYPE'
        ordering = ['name']


class LivestockAge(models.Model):
    id = PositiveSmallIntegerField(primary_key=True)
    age_range = CharField(max_length=200, unique=True, blank=False, null=False)

    def __str__(self):
        return "%s - %s" % (self.id, self.age_range)

    class Meta:
        db_table = 'LIVESTOCKAGE'
        ordering = ['age_range']


class UserAddress(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = ForeignKey(User, related_name='user_address', on_delete=PROTECT)
    address = ForeignKey(
        Address, related_name='user', on_delete=PROTECT)

    def __str__(self):
        return "%s - %s" % (self.user.username, self.address.id)

    class Meta:
        db_table = 'USERADDRESS'

# product and product details


def increment_unique_identifier(typename, catname):
    last_product = Product.objects.all().order_by('created_date').last()
    if not last_product:
        return 'FZN-LS-CAT-000001'
    logger.info(last_product.uniqueidentifier)
    last_product_no = last_product.uniqueidentifier
    last_uniqueid_number = int(last_product_no.split('-')[-1])
    logger.info(last_uniqueid_number)
    new_uniqueid_int = last_uniqueid_number + 1
    logger.info(new_uniqueid_int)
    new_uniqueid = 'FMZ-' + typename + '-' + \
        catname + '-' + f'{new_uniqueid_int:06d}'
    return new_uniqueid


class Product(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    uniqueidentifier = CharField(
        max_length=2000, blank=False, null=False)
    name = CharField(max_length=200, blank=False, null=False)
    description = CharField(max_length=2000, blank=False, null=False)
    seller = ForeignKey(
        User, related_name='products_sell', on_delete=PROTECT)
    categorytype = ForeignKey(
        Productcategorytype, related_name='products_cat_type', null=True, on_delete=PROTECT)
    category = ForeignKey(
        Productcategory,  related_name='livestock_category', null=True, on_delete=PROTECT)
    breed = ForeignKey(
        Livestockbreed, related_name='livestock_breed', null=True, on_delete=PROTECT)
    pickup = ForeignKey(
        UserAddress, related_name='product_address', null=True, on_delete=PROTECT)

    is_verified = BooleanField(default=False)
    verified_by = ForeignKey(
        User, related_name='verified_product', null=True, on_delete=PROTECT)
    verification_comment = CharField(max_length=500, null=True)
    is_sold = BooleanField(default=False)
    sold_though_farmzonn = BooleanField(default=False)
    # city = ForeignKey(City, related_name='products_city', on_delete=PROTECT)
    # state = ForeignKey(State, related_name='products_state', on_delete=PROTECT)
    created_date = DateTimeField(null=True)
    created_by = ForeignKey(
        User, related_name='product_createdby', null=True, on_delete=PROTECT)
    modified_date = DateTimeField(null=True)
    modified_by = ForeignKey(
        User, related_name='product_modifiedby', null=True, on_delete=PROTECT)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    def get_variant(self):
        return self.product_variant.first()
    
    def get_variant(self):
        return self.product_variant.first()

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.created_date:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()

        if not self.uniqueidentifier:
            self.uniqueidentifier = increment_unique_identifier(
                self.categorytype.shortname, self.category.shortname)

        return super(Product, self).save(*args, **kwargs)

    class Meta:
        db_table = 'PRODUCT'
        ordering = ['-created_date', '-modified_date']


# class Livestockdetail(models.Model):
#     id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

#     product = ForeignKey(
#         Product,  related_name='livestock_detail', null=True, on_delete=PROTECT)

#     def __str__(self):
#         return "%s" % (self.id)

#     class Meta:
#         db_table = 'LIVESTOCKDETAIL'
#         ordering = ['category']


class ProductVariant(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    product = ForeignKey(
        Product,  related_name='product_variant', null=True, on_delete=PROTECT)
    age = ForeignKey(LivestockAge, related_name='product_age', null=True,
                     on_delete=PROTECT)
    gender = ForeignKey(
        Livestockgender, related_name='product_gender', null=True, on_delete=PROTECT)
    feedtype = ForeignKey(
        Livestockfeedtype,  related_name='product_feed', null=True, on_delete=PROTECT)
    stock = PositiveSmallIntegerField()
    adult_teeth_pair_count = PositiveSmallIntegerField()
    original_price = DecimalField(max_digits=10, decimal_places=2)
    discount = ForeignKey(
        Discount, related_name='product_discount', null=True, on_delete=DO_NOTHING)
    offer_price = DecimalField(max_digits=10, decimal_places=2)
    weight = DecimalField(max_digits=20, decimal_places=2)
    color = ForeignKey(
        Livestockcolor, related_name='product_color', on_delete=PROTECT)
    is_pregnant = BooleanField(default=False)
    milk_produce_per_Day = DecimalField(max_digits=10, decimal_places=2)
    no_of_offsprings = PositiveSmallIntegerField()
    modified_date = DateTimeField(null=True)

    def __str__(self):
        return "%s - %s" % (self.id, self.original_price)

    class Meta:
        db_table = 'PRODUCTVARIANT'
        ordering = ['-modified_date']


class LivestockVeterinaryCheck(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    product = ForeignKey(
        Product, related_name='veterinary_check', null=True, on_delete=PROTECT)
    veterinary = ForeignKey(User,
                            related_name='veterinary_check_done', on_delete=PROTECT)
    checked_on = DateTimeField(null=False)
    is_vet_approved = BooleanField()
    vet_comments = CharField(max_length=200, blank=False, null=False)

    def __str__(self):
        return "%s - %s" % (self.id, self.veterinary.username)

    class Meta:
        db_table = 'LIVESTOCKVETCHECK'
        ordering = ['-checked_on']


class Livestockvaccinetaken(models.Model):
    uuid = UUIDField(default=uuid.uuid4, editable=False)
    product = ForeignKey(
        Product, related_name='vaccinated', null=True, on_delete=PROTECT)
    vaccine = ForeignKey(Livestockvaccination,
                         related_name='vaccinated_vaccines', on_delete=PROTECT)
    completed_dose = PositiveIntegerField(null=True)
    vaccinated_on = DateTimeField(null=True)

    def __str__(self):
        return "%s - %s" % (self.livestock.id, self.vaccine.name)

    class Meta:
        db_table = 'LIVESTOCKVACCINETAKEN'
        ordering = ['-vaccinated_on']


def get_image_filename(instance, filename):
    title = instance.product.id
    slug = slugify(title)
    return "post_images/%s-%s" % (slug, filename)


class Productimages(models.Model):
    product = ForeignKey(ProductVariant, related_name='product_images',
                         on_delete=PROTECT, default=None)
    image = models.ImageField(upload_to=get_image_filename,
                              verbose_name='Image')

    def __str__(self):
        return "%s - %s" % (self.product.id, self.image)

     #calling image compression function before saving the data
    def save(self, *args, **kwargs):
                new_image = compress(self.image)
                self.image = new_image
                super().save(*args, **kwargs)
    class Meta:
        db_table = 'PRODUCTIMAGE'
        ordering = ['product']

# orde and payment and delivery details


class DeliveryMode(models.Model):
    id = PositiveIntegerField(primary_key=True)
    name = CharField(max_length=200, unique=True, blank=False, null=False)
    description = CharField(max_length=1000)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    class Meta:
        db_table = 'DELIVERYMODE'
        ordering = ['name']


class DeliveryDetail(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    pickup_address = ForeignKey(
        UserAddress, related_name='order_pickup', null=True, on_delete=PROTECT)
    delivery_address = ForeignKey(
        UserAddress, related_name='order_delivery', null=True, on_delete=PROTECT)
    delivered_by = ForeignKey(
        User, related_name='deliveries', null=True, on_delete=PROTECT)
    delivered_on = DateTimeField(null=True)
    distance = PositiveIntegerField(default=0)
    package_weight = PositiveIntegerField(default=0)
    mode = ForeignKey(
        DeliveryMode, related_name='delivery_mode', null=True, on_delete=PROTECT)
    comments = CharField(max_length=500, null=True)

    def __str__(self):
        return "%s - %s" % (self.id, self.delivered_on)

    class Meta:
        db_table = 'DELIVERY'
        ordering = ['delivered_on']


class PaymentMode(models.Model):
    id = PositiveIntegerField(primary_key=True)
    name = CharField(max_length=200, unique=True, blank=False, null=False)
    description = CharField(max_length=1000)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    class Meta:
        db_table = 'PAYMENTMODE'
        ordering = ['name']


class PaymentDetail(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    amount_received = PositiveIntegerField()
    received_by = ForeignKey(
        User, related_name='received_payments', on_delete=PROTECT)
    received_on = DateTimeField(null=False)
    delivery_charge = PositiveIntegerField(default=0)
    modeofpayment = ForeignKey(
        PaymentMode, related_name='payment_mode', on_delete=PROTECT)
    Ref_transaction = CharField(max_length=200, null=True)
    created_date = DateTimeField(null=True)
    modified_date = DateTimeField(null=True)

    def __str__(self):
        return "%s - %s" % (self.received_on, self.amount_received)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.created_date:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(PaymentDetail, self).save(*args, **kwargs)

    class Meta:
        db_table = 'PAYMENT'
        ordering = ['received_on']


class OrderStatus(models.Model):
    id = PositiveIntegerField(primary_key=True)
    name = CharField(max_length=200, unique=True, blank=False, null=False)
    description = CharField(max_length=1000)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    class Meta:
        db_table = 'ORDERSTATUS'
        ordering = ['-name']

class InvoiceStatus(models.Model):
    id = PositiveIntegerField(primary_key=True)
    name = CharField(max_length=200, unique=True, blank=False, null=False)
    description = CharField(max_length=1000)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    class Meta:
        db_table = 'INVOICESTATUS'
        ordering = ['-name']


def get_product_order_image_filename(instance, filename):
    title = instance.id
    slug = slugify(title)
    return "product-order-image/%s-%s" % (slug, filename)


def increment_invoice_number():
    last_invoice = Invoice.objects.all().order_by('created_date').last()
    if not last_invoice:
        return 'FMZ000001'
    invoice_no = last_invoice.invoice_number
    invoice_int = int(invoice_no.split('FMZ')[-1])
    new_invoice_int = invoice_int + 1
    new_invoice_no = 'FMZ' + f'{new_invoice_int:06d}'
    return new_invoice_no


class Invoice(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    invoice_number = CharField(
        max_length=150, default=increment_invoice_number, unique=True, blank=False, null=False)
    invoice_date = DateTimeField(null=True)
    status = ForeignKey(
        InvoiceStatus, related_name='invoice_status', null=True, on_delete=PROTECT)
    is_payment_done = BooleanField(default=False)
    payment_detail = ForeignKey(
        PaymentDetail, related_name='payment_invoice', null=True, on_delete=PROTECT)
    created_date = DateTimeField(null=True)
    modified_date = DateTimeField(null=True)

    def __str__(self):
        return "%s - %s" % (self.invoice_number, self.is_payment_done)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.invoice_date:
            self.invoice_date = timezone.now()

        if not self.created_date:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(Invoice, self).save(*args, **kwargs)

    class Meta:
        db_table = 'INVOICE'
        ordering = ['-modified_date']


def increment_order_number():
    last_order = Order.objects.all().order_by('created_date').last()
    if not last_order:
        return 'FMZ-ORD-000001'
    last_order_no = last_order.order_no
    order_int = int(last_order_no.split('FMZ-ORD-')[-1])
    new_order_int = order_int + 1
    new_order_no = 'FMZ-ORD-' + f'{new_order_int:06d}'
    return new_order_no


class Order(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    order_no = CharField(
        max_length=150, default=increment_order_number, blank=False, null=False)
    buyer = ForeignKey(User, related_name='user_order', on_delete=PROTECT)
    product = ForeignKey(
        Product, related_name='product_order', on_delete=PROTECT)
    sellerpannumber = models.CharField(
        max_length=30, null=True)
    invoice = ForeignKey(
        Invoice, related_name='order_invoice', null=True, on_delete=PROTECT)
    items_count = PositiveSmallIntegerField()
    price_per_item = DecimalField(default=0.0, decimal_places=2, max_digits=10)
    discounttype = ForeignKey(
        Discounttype, related_name='discount_type', null=True, on_delete=PROTECT)
    discountedamount = DecimalField(default=0, decimal_places=2, max_digits=10)
    delivery_charge = DecimalField(default=0, decimal_places=2, max_digits=10)
    tax_precentage = DecimalField(default=0, decimal_places=2, max_digits=10)
    tax_amount = DecimalField(default=0, decimal_places=2, max_digits=10)
    total_payable = DecimalField(default=0, decimal_places=2, max_digits=10)
    product_image = models.ImageField(
        upload_to=get_product_order_image_filename, null=True)
    status = ForeignKey(
        OrderStatus, related_name='order_status', on_delete=PROTECT)
    delivery = ForeignKey(
        DeliveryDetail, related_name='order_delivery', null=True, on_delete=PROTECT)
    created_date = DateTimeField(null=True)
    modified_date = DateTimeField(null=True)

    def __str__(self):
        return "%s - %s" % (self.buyer.username, self.items_count)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.created_date:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(Order, self).save(*args, **kwargs)

    class Meta:
        db_table = 'ORDER'
        ordering = ['-modified_date']


class ApplicationPages(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    pageinfo = CharField(
        max_length=150, unique=True, blank=False, null=False)
    viewcount = IntegerField(default=0)
    createdOn = DateTimeField(auto_now_add=True, null=True)
    last_viewedOn = DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return "%s - %s - %s" % (self.pagename, self.viewcount, self.last_viewedOn)

    class Meta:
        db_table = 'APPLICATIONPAGES'
        ordering = ['-last_viewedOn']


class DeliverablePincode(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    pincode = IntegerField(blank=False, null=False)
    isdeliverable = BooleanField(default=True)
    city = CharField(max_length=500, null=True)
    state = CharField(max_length=500, null=True)
    createdOn = DateTimeField(auto_now_add=True, null=True)
    last_modifiedOn = DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return "%s - %s - %s" % (self.pincode, self.city, self.isdeliverable)

    class Meta:
        db_table = 'DELIVERABLEPINCODE'
        ordering = ['pincode']


class ProductViewDetail(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    product = ForeignKey(
        Product, related_name='product_viewed', on_delete=PROTECT)
    user = ForeignKey(
        User, related_name='product_viewedby', null=True, on_delete=PROTECT)
    viewcount = IntegerField(default=0)
    lastviewedon = DateTimeField(auto_now=True, null=True)

    class Meta:
        db_table = 'PRODUCTVIEWDETAIL'
        ordering = ['lastviewedon']


class ProductPriceSet(models.Model):
    id = IntegerField(primary_key=True, default=1)
    profitmargin = DecimalField(max_digits=5, decimal_places=2)
    recommendedOfferpercent = DecimalField(max_digits=5, decimal_places=2)
    isactive = BooleanField(default=False)
    createdon = DateTimeField(auto_now=False, null=True)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.createdon:
            self.createdon = timezone.now()
        return super(ProductPriceSet, self).save(*args, **kwargs)

    class Meta:
        db_table = 'PRODUCTPRICESET'
        ordering = ['createdon']

def increment_sale_invoice_number():
    last_order = SaleOrderInvoice.objects.all().order_by('createdon').last()
    if not last_order:
        return 'FMZ-SINV-000001'
    last_order_no = last_order.invoicenumber
    order_int = int(last_order_no.split('FMZ-SINV-')[-1])
    new_order_int = order_int + 1
    new_order_no = 'FMZ-SINV-' + f'{new_order_int:06d}'
    return new_order_no

class SaleOrderInvoice(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    invoicenumber = CharField(
        max_length=150, default=increment_sale_invoice_number, blank=False, null=False)
    order = ForeignKey(Order, related_name='sale_invoice', on_delete=PROTECT)
    status = ForeignKey(
        InvoiceStatus, related_name='sale_invoice_status', null=True, on_delete=PROTECT)
    invoice_date = DateTimeField(null=True)
    profitpercent = DecimalField(max_digits=10, decimal_places=2)
    totalprice = DecimalField(max_digits=10, decimal_places=2)
    farmzonncommission = DecimalField(max_digits=10, decimal_places=2)
    deliverycharge = DecimalField(max_digits=10, decimal_places=2)
    isdeliverychargepaid = BooleanField(default=False)
    deliverychargepaidon = DateTimeField(null=True)
    sellershare = DecimalField(max_digits=10, decimal_places=2)
    issellershareamountpaid = BooleanField(default=False)
    sellersharepaidon = DateTimeField(null=True)
    createdon = DateTimeField(auto_now=False, null=True)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.createdon:
            self.createdon = timezone.now()
        if not self.invoice_date:
            self.invoice_date = timezone.now()
        return super(SaleOrderInvoice, self).save(*args, **kwargs)

    class Meta:
        db_table = 'SALEINVOICE'
        ordering = ['createdon']


def increment_requirement_unique_identifier(typename, catname):
    last_product = ProductRequirementPosted.objects.all().order_by('createdon').last()
    if not last_product:
        return 'FZN-REQ-' + typename + '-' + catname + '-000001'
    logger.info(last_product.uniqueidentifier)
    last_product_no = last_product.uniqueidentifier
    last_uniqueid_number = int(last_product_no.split('-')[-1])
    logger.info(last_uniqueid_number)
    new_uniqueid_int = last_uniqueid_number + 1
    logger.info(new_uniqueid_int)
    new_uniqueid = 'FMZ-REQ-' + typename + '-' + \
        catname + '-' + f'{new_uniqueid_int:06d}'
    return new_uniqueid

class ProductRequirementPosted(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    uniqueidentifier = CharField(
        max_length=2000, null=True)
    description = CharField(max_length=2000, blank=False, null=False)
    posted_by = ForeignKey(
        User, related_name='requirement_posted_by', on_delete=PROTECT)
    postedfor = CharField(max_length=200, blank=False, null=False)
    contact = PhoneNumberField(verbose_name="phone",
                             unique=False, null=True)
    categorytype = ForeignKey(
        Productcategorytype, related_name='requirement_cat_type', null=True, on_delete=PROTECT)
    category = ForeignKey(
        Productcategory,  related_name='requirement_livestock_category', null=True, on_delete=PROTECT)
    age = ForeignKey(LivestockAge, related_name='requirement_product_age', null=True,
                     on_delete=PROTECT)
    breed = ForeignKey(
        Livestockbreed, related_name='requirement_livestock_breed', null=True, on_delete=PROTECT)        
    gender = ForeignKey(
        Livestockgender, related_name='requirement_product_gender', null=True, on_delete=PROTECT)
    requiredcount = PositiveSmallIntegerField()    
    acceptableprice = DecimalField(max_digits=10, decimal_places=2)
    weight = DecimalField(max_digits=20, decimal_places=2)
    color = ForeignKey(
        Livestockcolor, related_name='requirement_product_color', null=True, on_delete=PROTECT)
    requiredaroundpincode = IntegerField(blank=False, null=False)
    requiredaroundcity = CharField(max_length=200, blank=True, null=True)
    requirearoundstate = CharField(max_length=200, blank=True, null=True)
    isverified = PositiveSmallIntegerField(default=0)
    verifiedby = ForeignKey(
        User, related_name='requirement_verified_by', null=True, on_delete=PROTECT)
    verificationcomment = CharField(max_length=200, null=True)
    status = PositiveSmallIntegerField(default=1)
    recieveduserresponse = PositiveSmallIntegerField(default=0)
    createdon = DateTimeField(auto_now=False, null=True)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.createdon:
            self.createdon = timezone.now()

        if not self.uniqueidentifier:
            self.uniqueidentifier = increment_requirement_unique_identifier(
                self.categorytype.shortname, self.category.shortname)

        return super(ProductRequirementPosted, self).save(*args, **kwargs)

    class Meta:
        db_table = 'PRODUCTREQUIREMENT'
        ordering = ['-createdon']


class Notification(models.Model):
    id = PositiveSmallIntegerField(primary_key=True)
    type = CharField(max_length=30, null=True)
    mode = CharField(max_length=30, null=True)
    lastsenton = DateTimeField(auto_now=False, null=True)

    def __str__(self):
        return "%s - %s - %s" % (self.id, self.type, self.lastsenton)

    class Meta:
        db_table = 'NOTIFICATION'
        ordering = ['lastsenton']

class SellerReview(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    reviewcomment = CharField(max_length=2000, blank=True, null=True)
    seller = ForeignKey(
        User, related_name='seller_reviews', on_delete=PROTECT)
    postedby = ForeignKey(
        User, related_name='review_posted_by', on_delete=PROTECT)
    reviewrating = PositiveSmallIntegerField(default=0)
    createdon = DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return "%s - %s - %s" % (self.id, self.seller, self.postedby)

    class Meta:
        db_table = 'SELLERREVIEW'
        ordering = ['createdon']


class Thread(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    Category = ForeignKey(
        Productcategory, related_name='thread_related_category', null=True, on_delete=PROTECT)
    PostedBy = ForeignKey(
        User, related_name='thread_createdby', on_delete=PROTECT)
    content = CharField(max_length=2000, blank=True, null=True)
    likescount = PositiveIntegerField(default=0)
    createdon = DateTimeField(auto_now=True, null=True)

    class Meta:
        db_table = 'THREAD'
        ordering = ['-createdon']

class ThreadReply(models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    thread = ForeignKey(
        Thread, related_name='thread_reply', on_delete=PROTECT)
    RepliedBy = ForeignKey(
        User, related_name='thread_Repliedby', on_delete=PROTECT)
    reply = CharField(max_length=2000, blank=True, null=True)
    createdon = DateTimeField(auto_now=True, null=True)

    class Meta:
        db_table = 'THREADREPLY'
        ordering = ['-createdon']