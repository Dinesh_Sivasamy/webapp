from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from datetime import *

from WebApp.LivestockMartWebApiServices.models import Notification, User
from WebApp.LivestockMartWebApiServices.serializer import NotificationSerializer

import os
from django.conf import settings

import logging

from WebApp.LivestockMartWebApiServices.services.smsservice import *
logger = logging.getLogger(__name__)

# Create your views here.

class SendNotificationView(APIView):

    def get(self, request, type, format=None):
            notifications = Notification.objects.filter(type=type)
            serialize = NotificationSerializer(notifications, many=True)
            return Response(serialize.data)

    def post(self, request, type, format=None):
        notification = Notification.objects.filter(type=type).first()

        if notification is None:
            return Response("No notification type found", status=status.HTTP_404_NOT_FOUND)
        
        today = datetime.today()
        logger.info(f'today date: {today}')
        logger.info(f'notification last sent date: {notification.lastsenton.replace(tzinfo=None) if notification.lastsenton else None}')

        if notification.lastsenton is None or (today - notification.lastsenton.replace(tzinfo=None)).days > 2:

            if type == 'newenquiry':
                sellerlist = User.objects.filter(
                        usertype__in=[2, 3, 4], is_active=True)
                ## logger.info(f'{sellerlist}')
                
                if sellerlist and sellerlist.count() > 0:
                    logger.info(f'Pushing notification to {sellerlist.count()} users')
                    sellerphone = []
                    
                    for seller in sellerlist:
                        sellerphone.append(seller.phone.as_e164)

                    sellerphonestring = ','.join(sellerphone)
                    logger.info(f'{sellerphonestring}')
                    send_enquiry_approval_notification_sellers(sellerphonestring)
                    logger.info(f'{notification.type} type notification has been sent succesfully on: {today}')
                else:
                    logger.error(f'{notification.type} type notification has been not been sent since seller list is not available')

            elif type == 'newproductlisting':     
                buyerlist = User.objects.filter(
                        usertype__in=[1, 3, 4], is_active=True)
                logger.info(f'{buyerlist}')
                if buyerlist and buyerlist.count() > 0:
                    logger.info(f'Pushing notification to {buyerlist.count()} users')
                    buyerphone = []
                    
                    for buyer in buyerlist:
                        buyerphone.append(buyer.phone.as_e164)

                    buyerphonestring = ','.join(buyerphone)
                    logger.info(f'{buyerphonestring}')
                    send_new_product_listing_notification_to_user(buyerphonestring)
                    logger.info(f'{notification.type} type notification has been sent succesfully on: {today}')
                else:
                    logger.error(f'{notification.type} type notification has been not been sent since buyer list is not available')
            
            elif type == 'productregistrationexpiry':     
                sellerlist = User.objects.filter(
                        usertype__in=[2, 3, 4], is_active=True)
                ## logger.info(f'{sellerlist}')
                
                if sellerlist and sellerlist.count() > 0:
                    logger.info(f'Pushing notification to {sellerlist.count()} users')
                    sellerphone = []
                    
                    for seller in sellerlist:
                        sellerphone.append(seller.phone.as_e164)

                    sellerphonestring = ','.join(sellerphone)
                    logger.info(f'{sellerphonestring}')
                    send_product_registration_expiry(sellerphonestring)
                    logger.info(f'{notification.type} type notification has been sent succesfully on: {today}')
                else:
                    logger.error(f'{notification.type} type notification has been not been sent since seller list is not available')
            elif type == 'newforumthread':
                userlist = User.objects.filter(
                        usertype__in=[1, 2, 3, 4], is_active=True)
                logger.info(f'{userlist}')

                if userlist and userlist.count() > 0:
                    logger.info(f'Sending {type} type notification for {userlist.count()} users')
                    
                    for user in userlist:
                        userphone.append(user.phone.as_e164)

                    userphonestring = ','.join(userphone)
                    logger.info(f'{userphonestring}')
                    send_new_forum_thread(userphonestring)
                logger.info(f'{type} type notification has been sent succesfully')
            else:
                userlist = User.objects.filter(
                        usertype__in=[1, 2, 3, 4], is_active=True)
                logger.info(f'{userlist}')
                if userlist and userlist.count() > 0:
                    notificationmsg = 'Hello  Sir/Madam, Thanks for your recent visit to farmzonn.com. Now you can review and rate your sale experience with the seller in our website and help others to filter out best.'
                    logger.info('Sending common notification to registered Users: {notificationmsg}')
                    userphone = []

                    for user in userlist:
                        userphone.append(user.phone.as_e164)

                    userphonestring = ','.join(userphone)
                    logger.info(f'{userphonestring}')
                    send_common_notification(userphonestring,notificationmsg)
                    logger.info('Common notification has been sent successsfully to registered Users')

            logger.info(f'Updating last sent on for {notification.type} type notification with: {today}')
            # restting the notification last sent date
            notificationdata = { 
                        "lastsenton": today
                    }
            serializer = NotificationSerializer(instance=notification,
            data=notificationdata, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            logger.info(f'skipping notification')
            logger.info(f'notification last sent date: {notification.lastsenton}')
            logger.info(f'today date: {today}')
            logger.info(f'diff between last sent date: {(today - notification.lastsenton.replace(tzinfo=None)).days if (notification.lastsenton is not None) else None}')
        
        return Response('Failed to Send Notification, Please check with Administrator.', status=status.HTTP_400_BAD_REQUEST)

class SendManualNotification(APIView):

    def post(self, request, type, fomat=None):

        file_contacts = open(os.path.join(settings.BASE_DIR, 'sms_list.txt'),'r')

        contact_list = file_contacts.read() 

        if contact_list:
            print(f'contact list: {contact_list}')

            numbers = contact_list.split('contacts: ')
            if numbers:
                for number in numbers:
                    print(f'Pushing {type} notification to {number.count(",") + 1} users')
                    
                    if type == 'newenquiry':           
                        
                        print(f'Sending {type} type notification')
                        send_enquiry_approval_notification_sellers(number)
                        print(f'{type} type notification has been sent succesfully')

                    elif type == 'newproductlisting':
                        print(f'Sending {type} type notification')
                        send_new_product_listing_notification_to_user(number)
                        print(f'{type} type notification has been sent succesfully')
                    
                    elif type == 'newforumthread':
                        print(f'Sending {type} type notification')

                        send_new_forum_thread(number)
                        print(f'{type} type notification has been sent succesfully')
                    else:
                        notificationmsg = 'Hello  Sir/Madam, Thanks for your recent visit to farmzonn.com. Now you can review and rate your sale experience with the seller in our website and help others to filter out best.'
                        print(f'Sending common notification: {notificationmsg}')
                        send_common_notification(number,notificationmsg)
                        print('Common notification has been sent successsfully')
            else:
                print(f'contact split failed')            
        else:
            print(f'request doesn\'t have contacts to send notification')
        return Response("Notification Send completed", status=status.HTTP_200_OK)
    
    #FARMZONN-NEW-ENQUIRY-REG-USER: Hello Sir/Madam, new livestock wanted list has been added in farmzonn for your post. Checkout farmzonn.com/view-requirements to view requirements and post your livestock for sale in farmzonn.com/sell. - Team Farmzonn.com
