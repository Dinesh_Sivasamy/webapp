from django.db.models import manager
from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField
from rest_framework.serializers import IntegerField, DecimalField, CharField, DateTimeField, BooleanField, UUIDField

from .models import *

# product and product details


class ProductSerializer(serializers.ModelSerializer):
    id = UUIDField()
    name = CharField(max_length=200, blank=False, null=False)
    description = CharField(max_length=2000, blank=False, null=False)

    def __str__(self):
        return "%s  %s %s" % (self.id, self.name, self.seller.username)

    class Meta:
        model = Product
        fields = '__all__'


class LivestockAgeSerializer(serializers.ModelSerializer):
    id = PositiveSmallIntegerField(primary_key=True)
    age_range = CharField(max_length=200, unique=True, blank=False, null=False)

    def __str__(self):
        return "%s  %s" % (self.id, self.age_range)

    class Meta:
        model = LivestockAge
        fields = '__all__'


class LivestockVeterinaryCheckSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s" % (self.product.id, self.veterinary.username)

    class Meta:
        model = LivestockVeterinaryCheck
        fields = '__all__'


class LivestockVeterinaryChecklistSerializer(serializers.ModelSerializer):
    veterinary = PrimaryKeyRelatedField(
        source='veterinary.username', read_only=True)
    veterinary_degree = PrimaryKeyRelatedField(
        source='veterinary.userdegree', read_only=True)
    checked_on = DateField(null=False)
    is_vet_approved = BooleanField()
    vet_comments = CharField(max_length=200, blank=False, null=False)

    def __str__(self):
        return "%s - %s" % (self.id, self.veterinary.username)

    class Meta:
        model = LivestockVeterinaryCheck
        fields = '__all__'


class ProductVariantSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s" % (self.id, self.product.id)

    class Meta:
        model = ProductVariant
        fields = '__all__'


# class LivestockdetailSerializer(serializers.ModelSerializer):

#     def __str__(self):
#         return "%s - %s" % (self.id, self.product.name)

#     class Meta:
#         model = Livestockdetail
#         fields = '__all__'


class LivestockvaccinetakenSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s" % (self.product.id, self.vaccine.id)

    class Meta:
        model = Livestockvaccinetaken
        fields = '__all__'


class LivestockvaccinetakenListSerializer(serializers.ModelSerializer):

    vaccine = PrimaryKeyRelatedField(source='vaccine.name', read_only=True)

    def __str__(self):
        return "%s - %s" % (self.product.id, self.vaccine.id)

    class Meta:
        model = Livestockvaccinetaken
        fields = '__all__'


class ProductimagesSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s" % (self.product.id)

    class Meta:
        model = Productimages
        fields = '__all__'

# users


class UsertitleSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s" % (self.name)

    class Meta:
        model = Usertitle
        fields = '__all__'


class UsertypeSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s" % (self.name)

    class Meta:
        model = Usertype
        fields = '__all__'


class CitySerializer(serializers.ModelSerializer):
    state = PrimaryKeyRelatedField(source='state.name', read_only=True)

    def __str__(self):
        return "%s %s %s" % (self.id, self.name, self.state.name)

    class Meta:
        model = City
        fields = '__all__'


class StateSerializer(serializers.ModelSerializer):
    country = PrimaryKeyRelatedField(source='country.name', read_only=True)
    cities = CitySerializer(many=True, read_only=True)

    def __str__(self):
        return "%s %s %s" % (self.id, self.name, self.country.name)

    class Meta:
        model = State
        fields = ('id', 'name', 'country', 'cities')


class CountrySerializer(serializers.ModelSerializer):
    states = StateSerializer(many=True, read_only=True)

    def __str__(self):
        return "%s %s" % (self.id, self.name)

    class Meta:
        model = Country
        fields = ('id', 'name', 'states')


class AddresstypeSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s %s" % (self.name)

    class Meta:
        model = Addresstype
        fields = '__all__'


class AddressSerializer(serializers.ModelSerializer):
    type = PrimaryKeyRelatedField(source='type.name', read_only=True)
    pincode = IntegerField(blank=False, null=False)

    def __str__(self):
        return "%s %s %s %s" % (self.address_1, self.type, self.city, self.pincode)

    class Meta:
        model = Address
        fields = '__all__'


class UserAddressSerializer(serializers.ModelSerializer):
    user = PrimaryKeyRelatedField(source='user.username', read_only=True)
    address = AddressSerializer(many=False, read_only=True)

    def __str__(self):
        return "%s %s %s %s %s" % (self.id, self.user.username, self.address.type, self.address.city, self.address.pincode)

    class Meta:
        model = UserAddress
        fields = '__all__'


class UserProfileImageSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s" % (self.user.id)

    class Meta:
        model = UserProfileimage
        fields = '__all__'

#seller review
class SellerReviewSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s" % (self.seller)

    class Meta:
        model = SellerReview
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    usertypename = PrimaryKeyRelatedField(
        source='usertype.name', read_only=True)
    usertitlename = PrimaryKeyRelatedField(
        source='title.title', read_only=True)
    user_image = UserProfileImageSerializer(many=True, read_only=True)
    seller_reviews = SellerReviewSerializer(many=True, read_only=True)

    def __str__(self):
        return "%s %s %s" % (self.id, self.username, self.phone)

    class Meta:
        model = User
        fields = '__all__'

# livestock breed and vaccination


class LivestockbreedSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s" % (self.name, self.description)

    class Meta:
        model = Livestockbreed
        fields = '__all__'


class LivestockvaccinationSerializer(serializers.ModelSerializer):
    category = PrimaryKeyRelatedField(source='category.name', read_only=True)

    def __str__(self):
        return "%s" % (self.name)

    class Meta:
        model = Livestockvaccination
        fields = '__all__'


class ProductCategorySerializer(serializers.ModelSerializer):
    breeds = LivestockbreedSerializer(many=True, read_only=True)
    vaccines = LivestockvaccinationSerializer(many=True, read_only=True)

    def __str__(self):
        return "%s %s" % (self.name, self.type)

    class Meta:
        model = Productcategory
        fields = '__all__'


class ProductCategorytypeSerializer(serializers.ModelSerializer):
    categories = ProductCategorySerializer(many=True, read_only=True)

    def __str__(self):
        return "%s - %s" % (self.name, self.description)

    class Meta:
        model = Productcategorytype
        fields = '__all__'


class ProductCategoryHeaderSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s %s" % (self.name, self.type)

    class Meta:
        model = Productcategory
        fields = '__all__'


class ProductCategorytypeHeaderSerializer(serializers.ModelSerializer):
    categories = ProductCategoryHeaderSerializer(many=True, read_only=True)

    def __str__(self):
        return "%s - %s" % (self.name, self.description)

    class Meta:
        model = Productcategorytype
        fields = '__all__'


class DiscountSerializer(serializers.ModelSerializer):
    type = PrimaryKeyRelatedField(source='type.name', read_only=True)
    typeid = PrimaryKeyRelatedField(source='type.id', read_only=True)

    def __str__(self):
        return "%s - %s" % (self.name, self.type.name)

    class Meta:
        model = Discount
        fields = '__all__'


class DiscounttypeSerializer(serializers.ModelSerializer):
    discounts = DiscountSerializer(many=True, read_only=True)

    def __str__(self):
        return "%s" % (self.name)

    class Meta:
        model = Discounttype
        fields = ('id', 'name', 'description', 'discounts')


# livestock details
class LivestockgenderSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s" % (self.name)

    class Meta:
        model = Livestockgender
        fields = '__all__'


class LivestockcolorSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s" % (self.name)

    class Meta:
        model = Livestockcolor
        fields = '__all__'


class LivestockfeedtypeSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s" % (self.name)

    class Meta:
        model = Livestockfeedtype
        fields = '__all__'


class ProductlistdetailSerializer(serializers.ModelSerializer):
    id = UUIDField()
    name = CharField(max_length=200, blank=False, null=False)
    description = CharField(max_length=2000, blank=False, null=False)
    pickup = UserAddressSerializer(many=False, read_only=True)
    seller = UserSerializer(many=False, read_only=True)
    category = PrimaryKeyRelatedField(source='category.name', read_only=True)
    breed = PrimaryKeyRelatedField(source='breed.name', read_only=True)
    verified_user = PrimaryKeyRelatedField(
        source='verified_product.username', read_only=True)
    veterinary_check = LivestockVeterinaryChecklistSerializer(
        many=True, read_only=True)
    vaccinated = LivestockvaccinetakenListSerializer(
        many=True, read_only=True)
    # city = PrimaryKeyRelatedField(source='city.name', read_only=True)
    # state = PrimaryKeyRelatedField(source='state.name', read_only=True)

    def __str__(self):
        return "%s  %s - %s" % (self.id, self.name, self.seller.username)

    class Meta:
        model = Product
        fields = '__all__'

class ProductViewDetailSerializer(serializers.ModelSerializer):
    # productlist = ProductlistdetailSerializer(many=True, read_only=True)

    def __str__(self):
        return "%s" % (self.id)

    class Meta:
        model = ProductViewDetail
        fields = '__all__'

    def create(self, validated_data):
        return ProductViewDetail.objects.create(**validated_data)

# product list
class ProductVariantlistSerializer(serializers.ModelSerializer):
    age = PrimaryKeyRelatedField(source='age.age_range', read_only=True)
    color = PrimaryKeyRelatedField(source='color.name', read_only=True)
    gender = PrimaryKeyRelatedField(source='gender.name', read_only=True)
    feedtype = PrimaryKeyRelatedField(source='feedtype.name', read_only=True)
    discount = PrimaryKeyRelatedField(
        source='discount.discount_percent', read_only=True)
    discount_rate = PrimaryKeyRelatedField(
        source='discount.discount_rupees', read_only=True)
    product_images = ProductimagesSerializer(many=True, read_only=True)
    # product = ProductlistdetailSerializer(many=False, read_only=True)

    def __str__(self):
        return "%s - %s - %s" % (self.id, self.livestock.id, self.vaccine.id)

    class Meta:
        model = ProductVariant
        fields = '__all__'


# class LivestockdetaillistSerializer(serializers.ModelSerializer):
#     category = PrimaryKeyRelatedField(source='category.name', read_only=True)
#     breed = PrimaryKeyRelatedField(source='breed.name', read_only=True)
#     veterinary_check = LivestockVeterinaryChecklistSerializer(
#         many=True, read_only=True)
#     product_variant = ProductVariantlistSerializer(
#         many=True, read_only=True)

#     def __str__(self):
#         return "%s - %s" % (self.id)

#     class Meta:
#         model = Livestockdetail
#         fields = '__all__'


class ProductlistSerializer(serializers.ModelSerializer):
    id = UUIDField()
    name = CharField(max_length=200, blank=False, null=False)
    description = CharField(max_length=2000, blank=False, null=False)
    pickup = UserAddressSerializer(many=False, read_only=True)
    seller = UserSerializer(many=False, read_only=True)
    category = PrimaryKeyRelatedField(source='category.name', read_only=True)
    variant_type = PrimaryKeyRelatedField(
        source='category.varianttype', read_only=True)
    breed = PrimaryKeyRelatedField(source='breed.name', read_only=True)
    veterinary_check = LivestockVeterinaryChecklistSerializer(
        many=True, read_only=True)
    vaccinated = LivestockvaccinetakenListSerializer(
        many=True, read_only=True)
    # city = PrimaryKeyRelatedField(source='city.name', read_only=True)
    # state = PrimaryKeyRelatedField(source='state.name', read_only=True)
    product_variant = ProductVariantlistSerializer(
        many=True, read_only=True)
    product_viewed = ProductViewDetailSerializer(
        many=True, read_only=True)

    def __str__(self):
        return "%s - %s - %s" % (self.id, self.name, self.seller.username)

    class Meta:
        model = Product
        fields = '__all__'


# order, payment and delivery details
class DeliveryModeSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    class Meta:
        model = DeliveryMode
        fields = '__all__'


class DeliveryDetailSerializer(serializers.ModelSerializer):
    pickup_address = UserAddressSerializer(many=False, read_only=True)
    delivery_address = UserAddressSerializer(many=False, read_only=True)
    delivered_by = PrimaryKeyRelatedField(
        source='delivered_by.username', read_only=True)
    delivered_on = DateTimeField(null=True)
    mode = PrimaryKeyRelatedField(
        source='mode.name', read_only=True)
    comments = CharField(max_length=500)

    def __str__(self):
        return "%s - %s" % (self.delivered_by, self.delivered_on)

    class Meta:
        model = DeliveryDetail
        fields = '__all__'


class PaymentModeSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    class Meta:
        model = PaymentMode
        fields = '__all__'


class PaymentDetailSerializer(serializers.ModelSerializer):

    received_by = PrimaryKeyRelatedField(
        source='received_by.username', read_only=True)
    received_on = DateTimeField(null=False)
    mode = PrimaryKeyRelatedField(source='mode.name', read_only=True)

    def __str__(self):
        return "%s - %s - %s" % (self.received_on, self.mode, self.amount_received)

    class Meta:
        model = PaymentDetail
        fields = '__all__'


class OrderStatusSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s" % (self.id, self.name)

    class Meta:
        model = OrderStatus
        fields = '__all__'


class InvoiceOrderSerializer(serializers.ModelSerializer):
    payment_detail = PaymentDetailSerializer(many=False, read_only=True)

    def __str__(self):
        return "%s - %s" % (self.invoice_number, self.is_payment_done)

    class Meta:
        model = Invoice
        fields = '__all__'


class SaleOrderInvoiceSerializer(serializers.ModelSerializer):
    status = PrimaryKeyRelatedField(
        source='status.name', read_only=True)

    def __str__(self):
        return "%s - %s - %s - %s" % (self.id, self.totalamountreceivedfromBuyer, self.commissionreceived, self.amounttoseller)

    class Meta:
        model = SaleOrderInvoice
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):
    id = UUIDField()
    buyer = PrimaryKeyRelatedField(source='buyer.username', read_only=True)
    product = ProductlistSerializer(many=False, read_only=True)
    status = PrimaryKeyRelatedField(source='status.name', read_only=True)
    statusId = PrimaryKeyRelatedField(source='status.id', read_only=True)
    delivery = DeliveryDetailSerializer(many=False, read_only=True)
    invoice = InvoiceOrderSerializer(many=False, read_only=True)
    sale_invoice = SaleOrderInvoiceSerializer(many=True, read_only=True)

    def __str__(self):
        return "%s - %s - %s" % (self.id, self.buyer.username, self.items_count)

    class Meta:
        model = Order
        fields = '__all__'


class InvoiceSerializer(serializers.ModelSerializer):
    order = OrderSerializer(many=True, read_only=True)
    payment_done_on = PrimaryKeyRelatedField(
        source='payment_detail.received_on', read_only=True)
    payment_mode = PrimaryKeyRelatedField(
        source='payment_detail.modeofpayment', read_only=True)

    def __str__(self):
        return "%s - %s" % (self.invoice_number, self.is_payment_done)

    class Meta:
        model = Invoice
        fields = '__all__'


class ApplicationPagesSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s - %s" % (self.pagename, self.viewcount, self.last_viewedOn)

    class Meta:
        model = ApplicationPages
        fields = '__all__'


class DeliverablePincodeSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s - %s" % (self.pincode, self.city, self.isdeliverable)

    class Meta:
        model = DeliverablePincode
        fields = '__all__'


class ProductPriceSetSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s - %s - %s" % (self.id, self.profitmargin, self.recommendedOfferpercent, self.isactive)

    class Meta:
        model = ProductPriceSet
        fields = '__all__'

class ProductRequirementPostedSerializer(serializers.ModelSerializer):
    age_name = PrimaryKeyRelatedField(source='age.age_range', read_only=True)
    color_name = PrimaryKeyRelatedField(source='color.name', read_only=True)
    gender_name = PrimaryKeyRelatedField(source='gender.name', read_only=True)
    posted_by_name = PrimaryKeyRelatedField(source='posted_by.username', read_only=True)
    category_name = PrimaryKeyRelatedField(source='category.name', read_only=True)
    breed_name = PrimaryKeyRelatedField(source='breed.name', read_only=True)
    categorytype_name = PrimaryKeyRelatedField(source='categorytype.name', read_only=True)

    def __str__(self):
        return "%s - %s - %s - %s" % (self.id, self.name, self.posted_by.username, self.categorytype)

    class Meta:
        model = ProductRequirementPosted
        fields = '__all__'


#notification

class NotificationSerializer(serializers.ModelSerializer):

    def __str__(self):
        return "%s - %s - %s" % (self.id, self.type, self.lastsenton)

    class Meta:
        model = Notification
        fields = '__all__'

class ThreadReplySerializer(serializers.ModelSerializer):
    threadcontent = PrimaryKeyRelatedField(source='thread.content', read_only=True)
    RepliedBy_name = PrimaryKeyRelatedField(source='RepliedBy.username', read_only=True)    

    def __str__(self):
        return "%s - %s" % (self.id, self.reply)

    class Meta:
        model = ThreadReply
        fields = '__all__'

class ThreadSerializer(serializers.ModelSerializer):
    category_name = PrimaryKeyRelatedField(source='Category.name', read_only=True)
    postedby_name = PrimaryKeyRelatedField(source='PostedBy.username', read_only=True)
    thread_reply = ThreadReplySerializer(many=True, read_only=True)

    def __str__(self):
        return "%s - %s" % (self.id, self.content)

    class Meta:
        model = Thread
        fields = '__all__'