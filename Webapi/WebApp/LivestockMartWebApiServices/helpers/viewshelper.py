import json
from django.db.models.query import QuerySet
from django.db import transaction

from ..models import *
from ..serializer import *

import logging
logger = logging.getLogger(__name__)


def increment_unique_id_number(framedid):
    last_unique_id = Product.objects.all().order_by('uniqueidentifier').last()
    if not last_unique_id:
        return 'FZ-LS-CAT-000001'
    unique_id_no = last_unique_id.uniqueidentifier
    unique_id_int = int(unique_id_no.split('-')[-1])
    new_unique_id_int = unique_id_int + 1
    new_unique_id_no = framedid.upper() + f'{new_unique_id_int:06d}'
    return new_unique_id_no


def insertProduct(request):

    if request and request.data:
        try:
            product = request.data['product']

            categorytypename = product['categorytypename']
            categoryname = product['categoryname']
            breedname = '-' + product['breedname'] + \
                '-' if product['breedname'] else None

            product['uniqueidentifier'] = increment_unique_id_number(categorytypename +
                                                                     '-' + categoryname + breedname)
            logger.info(product)
            product_serializer = ProductSerializer(data=product)

            if product_serializer.is_valid():
                with transaction.atomic():
                    product_serializer.save()

                    productvariantinsert = insertProductVariant(
                        request, product_serializer.data)

                    # check if the product variant id has not been sent
                    if productvariantinsert == None:
                        logger.error(
                            'Failed during product variant details insert')
                        raise ValueError(
                            'Failed during product variant details insert')
                    else:
                        vaccinationinsert = insertLivestockVaccinated(
                            request,  product_serializer.data)

                        logger.info('vaccination insert completed')
                        logger.info(vaccinationinsert)

                        # check if any error response has been sent
                        if vaccinationinsert:
                            logger.info(
                                f'Failed during vaccination detail insert: {vaccinationInsert}')
                            raise ValueError(
                                f'Failed during vaccination detail insert: {vaccinationInsert}')

                        productresponse = {
                            "productid": product_serializer.data['id']}
                        productvariantresponse = json.loads(
                            productvariantinsert)
                        productvariantresponse.update(productresponse)
                        return productvariantresponse
            else:
                logger.info(
                    f'Invalid product request: {product_serializer.errors}')
                raise ValueError(
                    f'Invalid product request: {product_serializer.errors}')
        except Exception as ex:
            logger.error(
                f'Failed during product insert with error: {ex}')
            raise ValueError(f'Failed during product insertion')
    else:
        logger.error(
            f'Insert product detail request/request data in NULL - is request null: {request is None}')
        # return raise ValueError(f'Insert product detail request/request data in NULL - is request null: {request is None}')

    return None


def insertProductVariant(request, productdata):

    if request and productdata:
        try:
            productid = productdata['id']
            productvariants = request.data['product_variant']

            if productvariants:

                for productvariant in productvariants:

                    try:
                        if productvariant:
                            productvariant['product'] = productid
                            logger.info(
                                f'Product variant data: {productvariant}')
                            prod_variant_serializer = ProductVariantSerializer(
                                data=productvariant)

                            if prod_variant_serializer.is_valid():
                                prod_variant_serializer.save()

                                # productImageInsert = insertProductVariantImages(
                                #    request, productdata, livestockdata, productvariant, prod_variant_serializer.data)

                                return json.dumps({"productVariantId": prod_variant_serializer.data['id']})
                            else:
                                logger.info(
                                    f'Invalid product variant request: {prod_variant_serializer.errors}')
                                raise ValueError(
                                    f'Invalid product variant request: {prod_variant_serializer.errors}')

                    except Exception as ex:
                        logger.error(
                            f'Failed during product variant insert inside loop with error: {ex}')
                        raise ValueError(
                            f'Failed during one of the product variant insertion')

        except Exception as ex:
            logger.error(
                f'Failed during product variant insert with error: {ex}')
            raise ValueError(f'Failed during product variant insertion')
    else:
        logger.error(
            f'One of the request to insert product variant in NULL, checking if the input is null - request: {request is None} and productdata: {productdata is None}')

        raise ValueError(
            f'One of the request to insert product variant in NULL, checking if the input is null - request: {request is None} and productdata: {productdata is None}')


def insertLivestockVaccinated(request, productdata):

    errorresponse = []
    if request and productdata:
        try:
            productid = productdata['id']
            vaccines_taken = request.data['vaccines']

            logger.info('inside vaccine insert')
            logger.info(request.data)
            logger.info(request.data['vaccines'])

            if vaccines_taken is None:
                logger.info('vaccine is not found hence returning none')
                return None

            logger.info('printing vaccine taken')
            logger.info(len(vaccines_taken))

            for vaccinetaken in vaccines_taken:

                try:
                    if vaccinetaken:
                        vaccinetaken['product'] = productid
                        logger.info(
                            f'livestockvaccination data: {vaccinetaken}')
                        livestock_vaccination_serializer = LivestockvaccinetakenSerializer(
                            data=vaccinetaken)

                        if livestock_vaccination_serializer.is_valid():
                            livestock_vaccination_serializer.save()
                        else:
                            logger.error(
                                f'invalid livestockvaccination request: {livestock_vaccination_serializer.errors}')

                except Exception as ex:
                    logger.error(
                        f'Failed during livestockvaccination insert inside loop with error: {ex}')

        except Exception as ex:
            logger.error(
                f'Failed during livestockvaccination insert with error: {ex}')
            raise ValueError(
                f'Failed to insert livestockvaccination for Id: {productid}')
    else:
        logger.error(
            f'One of the request to insert livestock vaccine taken in NULL, checking if the input is null - request: {request is None} and productdata: {productdata is None}')

        raise ValueError(
            f'One of the request to insert product vaccine taken in NULL, checking if the input is null - request: {request is None} and productdata: {productdata is None}')

    return errorresponse


def getdiscountforuser(buyerpk, userrefcode):

    try:

        isdiscountapplicable = False
        discounts = Discount.objects.filter(
            is_active=True, applicable_to__gt= datetime.now().date()).order_by('type_id')
        print('got discount details')
        print(discounts)
        userexistingorders = Order.objects.filter(
            buyer__id=buyerpk).exclude(status__id__in=[0, 100, 101, 102, 103])
        print('got existing orders')
        print(userexistingorders)
        print(len(userexistingorders))

        if discounts and len(discounts) > 0:
            for discount in discounts:
                print(discount)
                print(discount.type.id)
                print(discount.type.id == 1 and len(userexistingorders))
                if discount.type.id == 1 and len(userexistingorders) == 0:
                    print('inside type 1')
                    isdiscountapplicable = True
                elif discount.type.id == 2 and len(userexistingorders) > 0:
                    print('inside type 2')
                    usedreferrals = userexistingorders.count(
                        discounttype=2)
                    referralcount = User.objects.filter(
                        loginreferralcode=userrefcode, is_active=True).count()
                    if referralcount > 0 and usedreferrals < 10 and ((referralcount/25) < usedreferrals):
                        isdiscountapplicable = True

                if isdiscountapplicable:
                    print(f'applicable discount: {discount}')
                    return discount
        print('discount not applicable')
        return None
    except Exception as e:
        logger.error('caught exception while calculating user discount', e)
        return None


# def insertProductLivestockdetail(request, productdata):

#     errorresponse = []
#     if request and productdata:
#         try:
#             productid = productdata['id']
#             livestock = request.data['livestock']
#             livestock['product'] = productid
#             logger.info(f'livestock data: {livestock}')
#             livestock_serializer = LivestockdetailSerializer(data=livestock)

#             if livestock_serializer.is_valid():
#                 livestock_serializer.save()

#                 productvariantinsert = insertProductVariant(
#                     request, productdata)

#                 if productvariantinsert == None:
#                     logger.info(
#                         f'Product variant insertion error response: {productvariantinsert}')
#                     raise ValueError(
#                         f'Product variant detail insertion error response: {productvariantinsert}')
#                 else:
#                     livestockresponse = {
#                         "livestockDetailId": livestock_serializer.data['id']}
#                     prodvariantresponse = json.loads(productvariantinsert)
#                     prodvariantresponse.update(livestockresponse)
#                     return json.dumps(prodvariantresponse)
#             else:
#                 logger.info(
#                     f'Invalid livestock request: {livestock_serializer.errors}')
#                 raise ValueError(
#                     f'Invalid livestock request: {livestock_serializer.errors}')
#         except Exception as ex:
#             logger.info(
#                 f'Failed during livestock insert with error: {ex}')
#             raise ValueError(f'Failed during livestock insertion')
#     else:
#         logger.info(
#             f'One of the request to insert livestock detail in NULL, checking if the input is null - request: {request is None} and productdata: {productdata is None}')

#         # return raise ValueError(f'One of the request to insert livestock detail in NULL, checking if the input is null - request: {request is None} and productdata: {productdata is None}')

#     return None


# def insertProductVariantImages(request, productdata, livestockdata, productvariantrequest, productvaraintdata):

#     errorresponse = []
#     if request and productdata and livestockdata and productvariantrequest and productvaraintdata:
#         try:
#             prodvariantid = productvaraintdata['id']
#             images = productvariantrequest['images']

#             for productimage in images:
#                 try:
#                     productimage['product'] = prodvariantid
#                     logger.info(f'product images data: {productimage}')
#                     productimage_serializer = ProductimagesSerializer(
#                         data=productimage)

#                     if productimage_serializer.is_valid():
#                         productimage_serializer.save()
#                     else:
#                         logger.info(f'invalid product image request')
#                         raise ValueError(
#                             f'Invalid product image request: {productimage_serializer.errors}')
#                 except Exception as ex:
#                     logger.info(
#                         f'Failed during productvariant image insert inside loop with error: {ex}')
#                     raise ValueError(
#                         f'Failed to insert one product image for Id: {prodvariantid}')
#         except Exception as ex:
#             logger.info(
#                 f'Failed during productvariant image insert with error: {ex}')
#             raise ValueError(
#                 f'Failed to insert product image for Id: {prodvariantid}')
#     else:
#         logger.info(
#             f'One of the request to insert images in NULL, checking if the input is null - request: {request is None} and productdata: {productdata is None} and livestockdata: {livestockdata is None} and productvariantrequest: {productvariantrequest is None} and productvaraintdata: {productvaraintdata is None}')

#         return raise ValueError(f'One of the request to insert images in NULL, checking if the input is null - request: {request is None} and productdata: {productdata is None} and livestockdata: {livestockdata is None} and productvariantrequest: {productvariantrequest is None} and productvaraintdata: {productvaraintdata is None}')

#     return errorresponse
