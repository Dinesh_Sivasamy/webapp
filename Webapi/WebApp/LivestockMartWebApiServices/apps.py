from django.apps import AppConfig


class LivestockmartwebapiservicesConfig(AppConfig):
    name = 'WebApp.LivestockMartWebApiServices'
