"""WebApp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
     https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
     1. Add an import:  from my_app import views
     2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
     1. Add an import:  from other_app.views import Home
     2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
     1. Import the include() function: from django.urls import include, path
     2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path

from django.conf import settings
from django.conf.urls.static import static

from .LivestockMartWebApiServices.views import *
from .LivestockMartWebApiServices.productactionviews import *
from .LivestockMartWebApiServices.useractionviews import *
from .LivestockMartWebApiServices.internal.internalviews import *

urlpatterns = [

    path('admin/', admin.site.urls),

    # user detail
    path('api/v1/user-title', UsertitleView.as_view()),
    path('api/v1/user-type', UsertypeView.as_view()),
    path('api/v1/referredusers/code=<str:referralcode>',
         ReferredUsersCountView.as_view()),

    # address and location
    path('api/v1/address-type', AddresstypeView.as_view()),
    path('api/v1/country', CountryView.as_view()),
    path('api/v1/state/country=<uuid:pk>', StateView.as_view()),
    path('api/v1/city/state=<uuid:pk>', CityView.as_view()),
    path('api/v1/address', AddressView.as_view()),
    path('api/v1/address-detail/id=<uuid:pk>', AddressDetailView.as_view()),
    path('api/v1/deliverable-pincodes', DeliverablePincodeView.as_view()),

    # product category & discount
    path('api/v1/category-type', ProductCategorytypeView.as_view()),
    path('api/v1/category/type=<int:pk>', ProductCategoryView.as_view()),
    path('api/v1/Header-List', HeaderListView.as_view()),
    path('api/v1/discount-type', DiscounttypeView.as_view()),
    path('api/v1/discount', DiscountView.as_view()),
    path('api/v1/recommendprice', ProductPriceSetView.as_view()),

    # livestock
    path('api/v1/livestock-breed/cat=<int:pk>', LivestockbreedView.as_view()),
    path('api/v1/livestock-feedtype', LivestockfeedtypeView.as_view()),
    path('api/v1/livestock-gender', LivestockgenderView.as_view()),
    path('api/v1/livestock-color', LivestockcolorView.as_view()),
    path('api/v1/livestock-age', LivestockageView.as_view()),
    path('api/v1/livestock-vaccine/cat=<int:pk>',
         LivestockvaccinationView.as_view()),

    # product list and details
    path('api/v1/product-list/top=<int:top>&page=<int:page>&cat=<int:cat>&breed=<int:breed>&state=<uuid:state>&city=<uuid:city>&seller=<uuid:seller>',
         ProductListView.as_view()),
    path('api/v1/product', ProductView.as_view()),
    path('api/v1/productimages', ProductImagesView.as_view()),
    path('api/v1/product-detail-list/top=<int:top>&category=<int:categorypk>&page=<str:page>',
         ProductdetailListView.as_view()),
    path('api/v1/product/id=<uuid:pk>', ProductdetailView.as_view()),

    path('api/v1/product-variant', ProductVariantListView.as_view()),
    path('api/v1/product-top/top=<int:top>', ProductTopListView.as_view()),

    # livestock prod details
    path('api/v1/livestock-vaccine/product=<uuid:productid>&Vaccine=<uuid:vaccineid>',
         LivestockVaccinationView.as_view()),
    path('api/v1/livestock-veterinary-check',
         LivestockVeterinaryCheckView.as_view()),

    # orders that are placed and confirmed by user
    path('api/v1/order/buyer=<uuid:buyerpk>&type=<int:usertype>',
         BuyerOrderView.as_view()),
    path('api/v1/order-detail/id=<uuid:pk>', OrderDetailView.as_view()),
    path('api/v1/order-detail-status/id=<uuid:pk>/<int:statuspk>',
         OrderDetailStatusView.as_view()),
    path('api/v1/order-list', OrderListView.as_view()),
    path('api/v1/order-status', OrderStatusView.as_view()),

    # orders that are in user cart and are yet to be confirmed by user
    path('api/v1/buyercart/buyer=<uuid:buyerpk>', BuyerCartView.as_view()),
    path('api/v1/confirm-order/buyer=<uuid:buyerpk>&address=<uuid:addresspk>&code=<str:userrefcode>',
         ConfirmOrderView.as_view()),

    # products that are listed by seller
    path('api/v1/sellerproducts/seller=<uuid:sellerpk>&type=<int:usertype>&getitems=<str:fetchtype>&top=<int:top>',
         SellerProductListView.as_view()),
    path('api/v1/sellerrating',
         SellerReviewView.as_view()),

    # livestocks list for vet
    path('api/v1/vet-productlist/checkcomplete=<str:checkcomplete>&top=<int:top>',
         VetCheckListView.as_view()),

    # payment details
    path('api/v1/payment-list', PaymentListView.as_view()),
    path('api/v1/payment-detail/order=<uuid:orderpk>', PaymentView.as_view()),

    # invoice
    path('api/v1/invoice/id=<uuid:pk>', InvoiceView.as_view()),
    # invoice to seller
    path('api/v1/saleinvoice/orderid=<uuid:orderpk>',
         SaleOrderInvoiceView.as_view()),

    # delivery
    path('api/v1/delivery/id=<uuid:pk>', DeliveryView.as_view()),

    # user details
    path('api/v1/user', UserView.as_view()),
    path('api/v1/userprofile/id=<uuid:pk>', UserProfileView.as_view()),
    path('api/v1/userprofileimage', UserProfileImageView.as_view()),
    path('api/v1/user-address/user=<uuid:userpk>', UserAddressView.as_view()),
    path('api/v1/user-delivery-address/id=<uuid:pk>',
         UserDeliveryAddressView.as_view()),
    path('api/v1/userdiscount/buyer=<uuid:buyerpk>&code=<str:userrefcode>',
         UserDiscountView.as_view()),
    path('api/v1/init-forgot-password/userphone=<str:userphone>', UserForgotPasswordInitiateView.as_view()),
    path('api/v1/reset-password/userphone=<str:userphone>', UserResetPasswordView.as_view()),
     

    # product detail update api
    path('api/v1/update-product/id=<uuid:pk>&quantity=<int:quantity>&sold=<str:sold>&isfarmzonnsale=<str:isfarmzonnsale>',
         ProductModificationView.as_view()),

    # product access detail insert/update api
    path('api/v1/product-access-detail/id=<uuid:pk>',
         ProductAccessView.as_view()),

    # product requirement enquiry get/update api
    path('api/v1/product-requirement-enquiry/id=<uuid:pk>',
         ProductRequirementView.as_view()),

    # product requirement enquiries get/create api
    path('api/v1/product-requirement-enquiries',
         ProductRequirementsView.as_view()),

    # product requirement enquiries created by user
    path('api/v1/user-product-requirement-enquiries',
         ProductUserRequirementsView.as_view()),

    # user authentication
    path('api/v1/login', LoginView.as_view()),
    path('api/v1/logout', LogoutView.as_view()),
    path('api/v1/validatesession', SessionView.as_view()),

    # user notifications
    path('api/v1/notifications/type=<str:type>', SendNotificationView.as_view()),
    path('api/v1/manual-notifications/type=<str:type>', SendManualNotification.as_view()),

    #threads & discussions
    path('api/v1/thread', ThreadView.as_view()),
    path('api/v1/thread/Replies/id=<uuid:threadpk>', ThreadReplyView.as_view()),
    path('api/v1/thread/like/id=<uuid:threadpk>', ThreadLikeView.as_view()),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
