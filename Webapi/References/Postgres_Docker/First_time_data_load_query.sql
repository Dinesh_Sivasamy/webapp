-- create extension for generating guid
CREATE EXTENSION "uuid-ossp";

-- set Address type
INSERT INTO public."ADDRESSTYPE" (id, name) VALUES (1, 'Home');
INSERT INTO public."ADDRESSTYPE" (id, name) VALUES (2, 'Company/Office space');

-- set country
INSERT INTO public."COUNTRY" (id, name) VALUES (uuid_generate_v4(), 'INDIA');

-- set deliverable pincodes
INSERT INTO public."DELIVERABLEPINCODE" (id, pincode, isdeliverable, city, state, "createdOn") VALUES 
(uuid_generate_v4(), 641105, true, 'Coimbatore', 'Tamil Nadu', current_timestamp),
(uuid_generate_v4(), 641021, true, 'Coimbatore', 'Tamil Nadu', current_timestamp),
(uuid_generate_v4(), 641023, true, 'Coimbatore', 'Tamil Nadu', current_timestamp);

-- set delivery mode
INSERT INTO public."DELIVERYMODE" (id, name, description) VALUES (1, 'Private vendor', 'Delivered through Private delivery vendor');

-- set discount type
INSERT INTO public."DISCOUNTTYPE" (id, name, description) VALUES 
(1, 'Site Promotion', 'Discounts that are offered for website promotion'),
(2, 'User Referral', 'Discounts that are offered for user referral');

-- set discount
INSERT INTO public."DISCOUNT" (id, name, type_id, description, discount_percent,  discount_rupees,
    max_discount_rupees, is_active, applicable_from, applicable_to, created_date) VALUES
    (uuid_generate_v4(), 'User Referral discount', 2, 'Upto 5000 Rs discount for user referral', 0, 500.00, 500, true,	current_timestamp, current_timestamp + interval '1 year', current_timestamp);

------- set price
INSERT INTO public."PRODUCTPRICESET" (id, profitmargin, "recommendedOfferpercent", isactive, createdon) VALUES
(1, 10, 30, true, current_timestamp);

-- set Invoice status
INSERT INTO public."INVOICESTATUS" (id, name, description) VALUES (0, 'InActive', 'Invoice has been cancelled');
INSERT INTO public."INVOICESTATUS" (id, name, description) VALUES (1, 'Active', 'Invoice is active');

-- set Order status
INSERT INTO public."ORDERSTATUS" (id, name, description) VALUES 
(0,	'In Cart',	'Item is added to buyers cart'),
(1,	'Order Placed',	'Order has been placed successfully'),
(2,	'Order Verification',	'Order verification is in progress with seller'),
(3,	'Order Verified',	'Order has been verified with seller'),
(4,	'Item dispatched',	'Item has been dispatched'),
(5,	'Delivery in progress',	'item is in delivery'),
(6,	'Item Delivered',	'Item has been delivered'),
(7,	'Payment Pending',	'Payment is pending for the item'),
(8,	'Payment done',	'Payment cash received on order delivery'),
(100,	'Order Cancelled By User before verification',	'User has requested to cancel the order during order verification'),
(101,	'Order Cancelled By User at delivery',	'User has requested to cancel the order at the time of delivery'),
(102,	'Order has been cancelled by Seller during verification',	'Seller has requested to cancel the order at the time of verification'),
(103,	'Order Cancelled by Admin during verification',	'Admin has cancelled the order during verification since the product is not a per described');

-- set payment mode
INSERT INTO public."PAYMENTMODE" (id, name, description) VALUES (1, 'COD', 'Cash On Delivery');

-- set User title
INSERT INTO public."USERTITLE" (id, title) VALUES 
(0, 'MR'),
(1, 'MISS'),
(2, 'MRS'),
(3, 'DR');

-- set user type
INSERT INTO public."USERTYPE" (id, name) VALUES 
(1,	'Buyer'),
(2,	'Seller'),
(3,	'Agent'),
(4,	'Company'),
(5,	'Vet'),
(9,	'Internal User');

-- set variant
INSERT INTO public."VARIANT" (id, name, description) VALUES 
(1,	'Mamal', 'Mamals of all type'),
(2,	'Bird', 'Birds of all type');

-- set product category type
INSERT INTO public."PRODUCTCATEGORYTYPE" (id, name, description, listing_precedence, shortname) VALUES
(1,	'Livestock', 'Livestocks for farmers', 1, 'LS');

-- set product category
INSERT INTO public."PRODUCTCATEGORY" (id, name, description, type_id, varianttype_id, shortname) VALUES
(1,	'Cattle', 'Cows and Bulls of various breeds', 1, 1,	'CAT'),
(2,	'Goat',	'goats for meat and other uses', 1, 1, 'GOT'),
(3,	'Sheep', 'Sheep for woll and meat',	1, 1, 'SHP'),
(4,	'Chicken', 'all types of chicken', 1, 2, 'CHN');


-- set Livestock Age
INSERT INTO public."LIVESTOCKAGE" (id, age_range) VALUES
(1,	'<3 months'),
(2,	'3 months - 6 months'),
(3,	'6 months - 1 year'),
(4,	'1 Year - 2 years'),
(5,	'> 2 Years');

-- set Livestock color
INSERT INTO public."LIVESTOCKCOLOR" (id, name) VALUES
(1,	'Black'),
(2,	'Brown'),
(3,	'White');

-- set Livestock feedtype
INSERT INTO public."LIVESTOCKFEEDTYPE" (id, name, description) VALUES 
(1,	'Grazing',	'feed given through grazing'),
(2,	'Stall feeding',	'feed provided in stalls');

-- set Livestock GENDER
INSERT INTO public."LIVESTOCKGENDER" (id, name) VALUES 
(0,	'Female'),
(1,	'Male');

-- set Livestock Breed
INSERT INTO public."LIVESTOCKBREED" (id, name, shortname, description, category_id) VALUES 
(1, 'Red Sindhi', 'RSI', 'High milk yields and the male animals are slow or poor work animals', 1),
(2, 'Sahiwal', 'SAH', 'High milk yields and the male animals are slow or poor work animals', 1),
(3, 'Gir', 'GIR', 'Gir Cows are good Milk - yielder', 1),
(4, 'Deoni', 'DNI', 'Cows are good milk producers and bullocks are good for work', 1),
(5, 'Hariana', 'HRN', 'Bullocks are powerful for road transport and rapid ploughing', 1),
(6, 'Tharparkar', 'THK', 'Dairy breed', 1),
(7, 'Kankrej', 'KKJ', 'Bullocks are fast, active and strong. Good for plough and cart purpose', 1),
(8, 'Kangayam', 'KNG', 'Best suited for ploughing and transport. Withstands hardy conditions', 1),
(9, 'Amritmahal', 'AMH', 'Best suitable for ploughing and transport', 1),
(10, 'Hallikar', 'HAL', 'Bullocks are strong, well spirited, quick and steady in the field as well as on road', 1),
(11, 'Umblacherry', 'UMC', 'Bulls are fearly temperament. They are used for ploughing in Thanjore delta area', 1),
(12, 'Jersey', 'JRS', 'One of the oldest dairy breeds', 1),
(13, 'Holstein Friesian', 'HFT', 'Far best diary breed among exotic cattle regarding milk yield', 1),
(14, 'Tellicherry', 'TLC', 'Best known for meat', 2),
(15, 'Jamnunapari', 'JMP', 'Thrive best under range conditions with plenty of shrubs for browsing', 2),
(16, 'Selam karupu', 'SKR', 'Important meat goat breed in the north-western part of Tamil Nadu', 2),
(17, 'Kanni', 'KNI', 'Tallest goat breeds found in sout Tamilnadu', 2),
(18, 'Boer', 'BOR', 'Best known for meat', 2),
(19, 'Kodi', 'KDI', 'Grown for the purpose of guiding the goat flocks, which goes for grazing', 2),
(20, 'Country Goat', 'CTG', 'Raised in households mainly used for meat', 2),
(21, 'Madras red', 'MRD', 'Best known for meat', 3),
(22, 'Mecheri', 'MCH', 'Best known for meat', 3),
(23, 'Ramnad White', 'RWT', 'Best known for meat', 3),
(24, 'Trichy Black', 'TBL', 'Best known for meat', 3),
(25, 'Vembur', 'VMB', 'Best known for meat', 3),
(26, 'Coimbatore', 'CBT', 'Best known for meat', 3),
(27, 'Kilakarsal', 'KKL', 'Best known for meat', 3),
(28, 'Broiler', 'BRL', 'Best known for meat', 4),
(29, 'Aseel', 'ASL', 'Noted for its pugnacity, high stamina, majestic gait and dogged fighting qualities', 4),
(30, 'Karaknath', 'KRN', 'Adult plumage varies from silver and gold-spangled to bluish-black without any spangling', 4),
(31, 'Country Breed', 'CNB', 'Raised in households mainly used for meat', 4);

-- set Livestock Vaccinations
INSERT INTO public."LIVESTOCKVACCINATION" (id, name, primary_dose, booster_dose, revaccination, category_id) VALUES 
(uuid_generate_v4(), 'Foot and Mouth Disease (FMD)', '>= 4 Months', 'After 1 month' , '6 monthly', 1),
(uuid_generate_v4(), 'Haemorrhagic Septicaemia (HS)', '>= 6 Months', '', 'Once a year', 1),
(uuid_generate_v4(), 'Black Quarter (BQ)', '>= 6 Months', '', 'Once a year', 1),
(uuid_generate_v4(), 'Foot and Mouth Disease (FMD)', '>= 4 Months', 'After 1 month' , '6 monthly', 2),
(uuid_generate_v4(), 'Haemorrhagic Septicaemia (HS)', '>= 6 Months', '', 'Once a year', 2),
(uuid_generate_v4(), 'Black Quarter (BQ)', '>= 6 Months', '', 'Once a year', 2),
(uuid_generate_v4(), 'Foot and Mouth Disease (FMD)', '>= 4 Months', 'After 1 month' , '6 monthly', 3),
(uuid_generate_v4(), 'Haemorrhagic Septicaemia (HS)', '>= 6 Months', '', 'Once a year', 3),
(uuid_generate_v4(), 'Black Quarter (BQ)', '>= 6 Months', '', 'Once a year', 3),
(uuid_generate_v4(), 'Ranikhet disease (Newcastle disease)', '1-7 days' , '', '', 4),
(uuid_generate_v4(), 'Infectious bronchitis', '3-4 weeks', '', '', 4),
(uuid_generate_v4(), 'Infectious bursal disease', '18-21 days', '', '', 4),
(uuid_generate_v4(), 'Mareks disease', '5-10 days or 18-21 days', '', '', 4),
(uuid_generate_v4(), 'RD', 'day-1 to 3 weeks', '3-4 weeks', '>= 8 weeks', 4);

-- set Application pages
INSERT INTO public."APPLICATIONPAGES" (id, pageinfo, "createdOn") VALUES
(uuid_generate_v4(), 'home', current_timestamp),
(uuid_generate_v4(), 'productlist', current_timestamp),
(uuid_generate_v4(), 'productlist&category=cattle', current_timestamp),
(uuid_generate_v4(), 'productlist&category=goat', current_timestamp),
(uuid_generate_v4(), 'productlist&category=sheep', current_timestamp),
(uuid_generate_v4(), 'productlist&category=chicken', current_timestamp);


---------Adding new category for countrydogs

-- set variant
INSERT INTO public."VARIANT" (id, name, description) VALUES
(3,	'Country Dog', 'Country Dogs of all type')

-- set product category type
INSERT INTO public."PRODUCTCATEGORYTYPE" (id, name, description, listing_precedence, shortname) VALUES
(2,	'Country Dogs', 'Country Dogs farmers', 2, 'CD');

-- set product category
INSERT INTO public."PRODUCTCATEGORY" (id, name, description, type_id, varianttype_id, shortname) VALUES
(5,	'Country Dogs', 'Country Dogs of all types', 2, 3, 'CDG');

-- set Livestock Breed
INSERT INTO public."LIVESTOCKBREED" (id, name, shortname, description, category_id) VALUES 
(32, 'Rajapalayam', 'RJM', 'Raised in South part of tamil nadu Rajapalayam', 5),
(33, 'Chippiparai', 'CHP', 'Raised in South part of tamil nadu', 5),
(34, 'Alanku', 'ALK', 'Raised in South part of tamil nadu', 5),
(35, 'Kombai', 'KMB', 'Raised in South part of tamil nadu', 5),
(36, 'Malayeri', 'MLY', 'Raised in South part of tamil nadu', 5),
(37, 'Kanni', 'KND', 'Raised in South part of tamil nadu', 5),
(38, 'Himalayan Gaddi', 'HNG', 'Raised in South part of tamil nadu', 5),
(39, 'Rampur Greyhound', 'RPG', 'Raised in South part of tamil nadu', 5);


-- set Livestock Vaccinations
INSERT INTO public."LIVESTOCKVACCINATION" (id, name, primary_dose, booster_dose, revaccination, category_id) VALUES 
(uuid_generate_v4(), 'Nobivac Puppy DP', '4th week', '', '', 5),
(uuid_generate_v4(), 'Nobivac DHPPi + Nobivac Lepto', '8th week', '10 - 12 weeks', '> 1 year', 5),
(uuid_generate_v4(), 'Anti Rabies Vaccine', '12th week', '', '> 1 year', 5);

-- set Application pages
INSERT INTO public."APPLICATIONPAGES" (id, pageinfo, viewcount, "createdOn") VALUES
(uuid_generate_v4(), 'productlist&category=Country Dog', 0, current_timestamp);




---------Adding new dog breeds for countrydogs

-- set Livestock Breed
INSERT INTO public."LIVESTOCKBREED" (id, name, shortname, description, category_id) VALUES 
(40, 'Labrador Retriever', 'LRR', 'Agile, gentle, kind, Intelligent', 5),
(41, 'German Shepherd', 'GSD', 'Loyal, obedient, watchful, courageous', 5),
(42, 'Golden Retriever', 'GRR', 'friendly, reliable, kind, intelligent', 5),
(43, 'Dachshund', 'DCH', 'stubborn, lively, playful, clever', 5),
(44, 'Beagle', 'BGL', 'Determined, excitable, amiable, Gentle', 5),
(45, 'Boxer', 'BXR', 'Devoted, Loyal, Bright, Fearless', 5),
(46, 'Tibetan Mastiff', 'TMF', 'Tenacious, Aloof, Strong-willed, Stubborn', 5),
(47, 'English Cocker Spaniel', 'ECS', 'Affectionate, Quiet, Friendly, Playful', 5);
(48, 'Pug', 'PUG', 'Docile, Clever, Quiet, Charming', 5),
(49, 'Rottweiler', 'RTW', 'Steady, Alert, Courageous,Obedient', 5),
(50, 'Doberman', 'DBM', 'Fearless, Alert, Confident, Energetic', 5),
(51, 'Great Dane', 'GRD', 'Devoted, Confident, Friendly, Reserved', 5),
(52, 'Pomeranian', 'PMN', 'Extroverted, Intelligent, Friendly, Sociable', 5),
(53, 'Dalmatian', 'DLN', 'Energetic, Sensitive, Active, Outgoing', 5),
(54, 'Indian Spitz', 'ISZ', 'Kind, Intelligent, Active, Athletic', 5),

