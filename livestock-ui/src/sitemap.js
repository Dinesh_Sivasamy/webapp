const sitemap = require("sitemap");
const hostname = "https://farmzonn.com";
const fs = require("fs");

const urls = [
  {
    url: "/",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/refcode/:loginrefcode",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/buy/:typeId",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/product/:productId",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/product/related/:productId",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/sell",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/seller-product/",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/vet-verify/",
    changefreq: "monthly",
    priority: 0.7,
  },
  {
    url: "/userprofile/:userId",
    changefreq: "monthly",
    priority: 0.7,
  },
  {
    url: "/my-cart",
    changefreq: "monthly",
    priority: 0.7,
  },
  {
    url: "/confirm-order/:addressId",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/orders",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/my-addresses/:type/:orderId",
    changefreq: "monthly",
    priority: 0.7,
  },
  {
    url: "/aboutus",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/how-it-works",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/careers",
    changefreq: "monthly",
    priority: 0.7,
  },
  {
    url: "/support",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/logout",
    changefreq: "monthly",
    priority: 0.7,
  },
  {
    url: "/other-services",
    changefreq: "monthly",
    priority: 0.7,
  },
  {
    url: "/terms",
    changefreq: "monthly",
    priority: 0.7,
  },
  {
    url: "/privacy-policy",
    changefreq: "monthly",
    priority: 0.7,
  },
  {
    url: "/add-enquiry",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/view-enquiry/:enquiryid",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/view-requirements",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/my-enquiries",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/forum",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/services",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/forum/thread/:threadid/replies",
    changefreq: "monthly",
    priority: 0.8,
  },

  {
    url: "/jersey-cattle-sale-coimbatore",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/farm-animals-pets-in-coimbatore",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/goat-farming-in-coimbatore",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/pet-shops-in-coimbatore",
    changefreq: "monthly",
    priority: 0.8,
  },
  {
    url: "/indian-milk-cow-breeds-sale",
    changefreq: "monthly",
    priority: 0.8,
  },
  // Add additional pages here
];

const sitemapInstance = sitemap.createSitemap({
  hostname,
  urls,
});

sitemapInstance.toXML();

fs.writeFileSync("./public/sitemap.xml", sitemapInstance.toString());
