export default function Loader() {
  return (
    // <div className="loader-container">
    //     <div className="loader"></div>
    // </div>
    <div className="spinner-container">
      <div className="loading-spinner"></div>
    </div>
  );
}
