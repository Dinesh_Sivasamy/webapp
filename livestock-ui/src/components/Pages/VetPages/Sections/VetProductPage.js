import React from 'react'
import { Card, Col, Container, Row } from 'react-bootstrap'
import Image from 'react-bootstrap/Image'
import Moment from 'react-moment';

import configData from "../../../../config.json";
import '../VetPage.css'

export default function VetProductPage(props) {

    const renderImage = (image) => {
        
        if (image) {
            return `${configData.IMG_FOLDER_PATH}${image}`
        }
        return `${configData.DEFAULT_PRODUCT_IMG}`
    }

    return (
        <div className="vet-product-item-card">
            <Card style={{ width: '80%' }}>
                <Card.Header>
                    <Container>
                        <Row>
                            <Col>
                                <a className="vet-product-item-name" href={`/product/${props.productdetail.id}`}>
                                    {props.productdetail.name}
                                </a>
                            </Col>
                            <Col>
                                <div className="vet-product-category-header">
                                    {props.productdetail.category} (Breed Name: {props.productdetail.breed})
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </Card.Header>
                <Card.Body>
                    <Card.Text>
                        <Container>
                            <Row xs={1} sm={2} md={4}>
                                <Col>
                                    <a href={`/product/${props.productdetail.id}`}>
                                        <div className="vet-product-img-div">
                                            <Image className="vet-product-img" src={renderImage(props.productImage[0].image)} fluid />
                                        </div>
                                    </a>
                                </Col>
                                <Col>
                                    <div className="vet-product-total-div">
                                        <div>
                                            {props.productdetail.description}
                                        </div>
                                        <div>
                                            #: {props.productdetail.uniqueidentifier}
                                        </div>
                                        <div>
                                            {props.productdetail.breed} Breed,  {props.productdetail.product_variant[0].age} of age
                                        </div>
                                        <div>
                                            {props.productdetail.product_variant[0].gender} - {props.productdetail.product_variant[0].weight} kg
                                        </div>
                                        <div>
                                            Original Price: <span> </span>&#8377; {props.productdetail.product_variant[0].original_price}
                                            {props.productdetail.product_variant[0].offer_price
                                                && props.productdetail.product_variant[0].offer_price > 0 ?
                                                <div> Offer Price:
                                                    <span> </span> &#8377; {props.productdetail.product_variant[0].offer_price}
                                                </div>
                                                :
                                                null
                                            }
                                        </div>
                                    </div>
                                </Col>
                                <Col>
                                    <div className='vet-product-list-detail'>
                                        <div>
                                            Stock(s) available: <span> </span> {props.productdetail.product_variant[0].stock}
                                        </div>
                                        <div>
                                            Listed on: <span> </span>
                                            <Moment format="DD-MMMM-yyyy">
                                                {props.productdetail.created_date}
                                            </Moment>
                                        </div>
                                    </div>
                                </Col>
                              
                                <Col>
                                    {props.productdetail.veterinary_check && props.productdetail.veterinary_check.length > 0 ?
                                        <div className='vet-product-admin-changes'>
                                            <div className='vet-product-admin-changes-heading'>Vet-Check Detail</div>
                                            <div className='vet-product-admin-change-row'>
                                                <div>
                                                    Result:
                                                </div>
                                                <div className='vet-product-admin-change-row-detail'>
                                                    {props.productdetail.veterinary_check[0].is_vet_approved ? "Pass" :
                                                        <span className='error'>Fail</span>}
                                                </div>
                                            </div>

                                            <div className='vet-product-admin-change-row'>
                                                <div>
                                                    Comments:
                                                </div>
                                                <div className='vet-product-admin-change-row-detail'>
                                                    {props.productdetail.veterinary_check[0].vet_comments}
                                                </div>
                                            </div>
                                            
                                            <div className='vet-product-admin-change-row'>
                                                <div>
                                                    Vet:
                                                </div>
                                                <div className='vet-product-admin-change-row-detail'>
                                                    {'Dr. ' + props.productdetail.veterinary_check[0].veterinary + ', ' + props.productdetail.veterinary_check[0].veterinary_degree}
                                                </div>
                                            </div>
                                            
                                            <div className='vet-product-admin-change-row'>
                                                <div>
                                                    Completed On:
                                                </div>
                                                <div className='vet-product-admin-change-row-detail'>
                                                    <Moment format="DD-MMMM-yyyy">
                                                        {props.productdetail.veterinary_check[0].checked_on}
                                                    </Moment>
                                                </div>
                                            </div>
                                        
                                        </div>
                                        :
                                        null
                                    }
                                </Col>
                            </Row>
                        </Container>
                        
                    </Card.Text>
                </Card.Body>
            </Card>
            <br />
        </div>
    )
}