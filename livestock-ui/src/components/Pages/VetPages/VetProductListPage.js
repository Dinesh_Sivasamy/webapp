import React, { useEffect, useState } from 'react'
import { Card, Container, Row, Col, Button  } from 'react-bootstrap';
import Cookies from 'js-cookie'
import CryptoJS from 'crypto-js';
import Tab from 'react-bootstrap/Tab'
import Tabs from 'react-bootstrap/Tabs'
import Loader from '../../Loader'

import './VetPage.css'
import SignInModal from '../User/SignInModal';
import configData from '../../../config.json'
import VetProductPage from './Sections/VetProductPage';

export default function VetProductListPage() {

    const [isLoading, setLoading] = useState(true);
    const [show, setShow] = useState(false);

    const handleClose = () => {
        setShow(false)
        window.location.reload()
    };

    const handleShow = () => setShow(true);
   
    const [vetCheckDoneList, setVetCheckDoneList] = useState([])
    const [vetCheckPendingList, setVetCheckPendingList] = useState([])

    useEffect(() => {
        if (Cookies.get('id')) {
            
            fetch(`/api/v1/vet-productlist/checkcomplete=true&top=0`, {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': Cookies.get('csrftoken')
                },
            })
                .then((resp) => {
                    if (resp.ok) {
                        return resp.json();
                    } else {
                        throw new Error(resp.statusText);
                    }
                })
                .then(function (resp) {
                    console.log(resp);
                    setVetCheckDoneList(resp);
                    setLoading(false);
                })
                .catch((err) => {
                    console.log(err);
                    setLoading(false);
                })
            
            fetch(`/api/v1/vet-productlist/checkcomplete=false&top=0`, {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': Cookies.get('csrftoken')
                },
            })
                .then((resp) => {
                    if (resp.ok) {
                        return resp.json();
                    } else {
                        throw new Error(resp.statusText);
                    }
                })
                .then(function (resp) {
                    console.log(resp);
                    setVetCheckPendingList(resp);
                    setLoading(false);
                })
                .catch((err) => {
                    console.log(err);
                    setLoading(false);
                })
        }
        setLoading(false);
    }, [])

    return (
        <div>
            {isLoading ? <Loader /> :
                <div>
                    <div className='vet-products-heading'>
                        CheckUp Completed
                    </div>
                    <hr />
                    <Container className="vet-product-container">
                        <Tabs position="left" defaultActiveKey={2} tabWidth={3}>
                            <Tab eventKey={1} className="Vet-check-completed-product-title" title="Check Completed">
                                <br />
                                {vetCheckDoneList && vetCheckDoneList.length > 0 ?
                                    <div className="vet-product-card-list">
                                        {vetCheckDoneList.map((product, id) => {
                                            return <div>
                                                <VetProductPage
                                                    key={id}
                                                    productImage={product.product_variant[0].product_images}
                                                    productdetail={product}
                                                />
                                                <br />
                                            </div>
                                        })}
                                    </div>
                                    : null
                                }
                                <Row>
                                    <Col>
                                        {!vetCheckDoneList || vetCheckDoneList.length == 0 ?
                              
                                            <div style={{
                                                width: '100%', display: 'flex', flexDirection: 'column',
                                                justifyContent: 'center'
                                            }}>

                                                {Cookies.get('name') == null ?
                                                    <div>
                                                        <div className="vet-product-empty-header">
                                                            You have not logged in yet!
                                                        </div>
                                                        <div className="vet-product-empty-login-link-div">
                                                            <Button className="vet-product-empty-login-link" onClick={handleShow}>Signin here to view livestocks list </Button>
                                                        </div>
                                                        <div>
                                                            <SignInModal
                                                                show={show}
                                                                setModalClose={handleClose}
                                                                onHide={handleClose}
                                                            />
                                                        </div>
                                                    </div>
                                                    :
                                                    <div>
                                                        <div className="vet-product-empty-header">
                                                            Checkup completed livestock list is empty
                                                        </div>
                                                        <div className="vet-product-empty-message">
                                                            As of now, there seems to be no livestock that has completed Vet checkup.
                                                        </div>
                                                    </div>
                                                }
                                            </div>
                                            : null
                                        }
                                    </Col>
                                </Row>
                            </Tab>
                            <Tab eventKey={2} className="Pending-vet-check-title" title="Livestocks with CheckUp pending">
                                <br />
                                {vetCheckPendingList && vetCheckPendingList.length > 0 ?
                                    <div className="vet-product-card-list">
                                        
                                        {vetCheckPendingList.map((product, id) => {
                                            return <div>
                                                <VetProductPage
                                                    key={id}
                                                    productImage={product.product_variant[0].product_images}
                                                    productdetail={product}
                                                />
                                            </div>
                                        })}
                                    </div>
                                    : null
                                }
                                <Row>
                                    <Col>
                                        {!vetCheckPendingList || vetCheckPendingList.length == 0 ?
                              
                                            <div style={{
                                                width: '100%', display: 'flex', flexDirection: 'column',
                                                justifyContent: 'center'
                                            }}>

                                                {Cookies.get('name') == null ?
                                                    <div>
                                                        <div className="vet-product-empty-header">
                                                            You have not logged in yet!
                                                        </div>
                                                        <div className="vet-product-empty-login-link-div">
                                                            <Button className="vet-product-empty-login-link" onClick={handleShow}>Signin here to view listed livestock details</Button>
                                                        </div>
                                                        <div>
                                                            <SignInModal
                                                                show={show}
                                                                setModalClose={handleClose}
                                                                onHide={handleClose}
                                                            />
                                                        </div>
                                                    </div>
                                                    :
                                                    <div>
                                                        <div className="vet-product-empty-header">
                                                            Checkup pending livestock list is empty
                                                        </div>
                                                        <div className="vet-product-empty-message">
                                                            As of now, there are no livestock pending for Vet checkup.
                                                        </div>
                                                    </div>
                                                }
                                        
                                            </div>
                                            : null
                                        }
                                    </Col>
                                </Row>
                            </Tab>
                        </Tabs>
                    </Container>
                </div>
            }
        </div>
    )
}