import React, { useEffect, useState } from 'react'
import { Card, Container, Row, Col, Button  } from 'react-bootstrap';
import Cookies from 'js-cookie'
import CryptoJS from 'crypto-js';
import Tab from 'react-bootstrap/Tab'
import Tabs from 'react-bootstrap/Tabs'
import SellerProduct from './Sections/SellerProduct';
import Loader from '../../Loader'

import './SellerProductsPage.css'
import SignInModal from '../User/SignInModal';
import configData from '../../../config.json'

export default function SellerProductsPage() {

    const [isLoading, setLoading] = useState(true);
    const [show, setShow] = useState(false);

    const [userType, setUserType] = useState(Cookies.get('type') != null ? CryptoJS.AES.decrypt(Cookies.get('type'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) : null)

    const [userId, setUserId] = useState(Cookies.get('id') != null ? CryptoJS.AES.decrypt(Cookies.get('id'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) : null)

    const handleClose = () => {
        setShow(false)
        window.location.reload()
    };

    const handleShow = () => setShow(true);
   
    const [orderedList, setOrderedList] = useState()
    const [soldProductList, setSoldProductList] = useState()
    const [inStockProductList, setInStockProductList] = useState()

    const getInstockProducts = () => {
        
        if (Cookies.get('id') && !inStockProductList) {
            setLoading(true);
            fetch(`/api/v1/sellerproducts/seller=${userId}&type=${userType}&getitems=instock&top=0`, {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': Cookies.get('csrftoken')
                },
            })
                .then((resp) => {
                    if (resp.ok) {
                        return resp.json();
                    } else {
                        throw new Error(resp.statusText);
                    }
                })
                .then(function (resp) {
                    console.log('instock');
                    console.log(resp);
                    console.log(resp.length);
                    setInStockProductList(resp);                
                    setLoading(false);
                })
                .catch((err) => {
                    console.log(err);
                    setLoading(false);
                })
        }
    }

    const getSoldProducts = () => {

        if (Cookies.get('id') && !soldProductList) {
            setLoading(true);
            fetch(`/api/v1/sellerproducts/seller=${userId}&type=${userType}&getitems=sold&top=0`, {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': Cookies.get('csrftoken')
                },
            })
                .then((resp) => {
                    if (resp.ok) {
                        return resp.json();
                    } else {
                        throw new Error(resp.statusText);
                    }
                })
                .then(function (resp) {
                    console.log('sold');
                    console.log(resp);
                    console.log(resp.length);
                    setSoldProductList(resp);
                    setLoading(false);
                })
                .catch((err) => {
                    console.log(err);
                    setLoading(false);
                })
        }        
    }

    const getOrderedProducts = () => {

        if (Cookies.get('id') && !orderedList) {
            localStorage.removeItem('productlist');
            
            fetch(`/api/v1/sellerproducts/seller=${userId}&type=${userType}&getitems=ordered&top=0`, {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': Cookies.get('csrftoken')
                },
            })
                .then((resp) => {
                    if (resp.ok) {
                        return resp.json();
                    } else {
                        throw new Error(resp.statusText);
                    }
                })
                .then(function (resp) {
                    console.log('ordered');
                    console.log(resp);
                    console.log(resp.length);
                    setOrderedList(resp);
                    setLoading(false);                  
                })
                .catch((err) => {
                    console.log(err);
                    setLoading(false);
                })
        }
    }

    const getProducts = (type) => {

        if (type == "1") {
            getOrderedProducts();
        } else if (type == "2") {
            getSoldProducts();
        } else if(type == "3") {
            getInstockProducts();
        }
    }

    useEffect(() => {
        getInstockProducts()
        setLoading(false);
        getSoldProducts()
        getOrderedProducts()
    }, [])

    return (
        <div>
            {isLoading ? <Loader /> :
                <div>
                    <div className='seller-products-heading'>
                        Listed products
                    </div>
                    <hr />
                    <Container className="seller-product-container">
                        <Tabs position="left" defaultActiveKey={3} tabWidth={3} onSelect={(k) => getProducts(k)}>
                            <Tab eventKey={1} className="sold-product-detail-title" title="Ordered" onClick={x => getProducts("ordered")}>
                                <br />
                                {orderedList && orderedList.length > 0 ?
                                    <div className="seller-product-card-list">
                                            {orderedList.map((order, id) => {
                                                return <div>
                                                    <SellerProduct
                                                        productImage={order.product.product_variant[0].product_images}
                                                        quantity={order.items_count}
                                                        status={order.status}
                                                        issoldout={true}
                                                        type = 'ORDERED'
                                                        orderdetail={order}
                                                        productdetail={order.product}
                                                    />
                                                    <br />
                                                </div>
                                            })}
                                    </div>
                                    : null
                                }
                                <Row>
                                    <Col>
                                        {!orderedList || orderedList.length == 0 ?
                              
                                            <div style={{
                                                width: '100%', display: 'flex', flexDirection: 'column',
                                                justifyContent: 'center'
                                            }}>

                                                {Cookies.get('name') == null ?
                                                    <div>
                                                        <div className="seller-product-empty-header">
                                                            You have not logged in yet!
                                                        </div>
                                                        <div className="seller-product-empty-login-link-div">
                                                            <Button className="seller-product-empty-login-link" onClick={handleShow}>Signin here to view your listed product details </Button>
                                                        </div>
                                                        <div>
                                                            <SignInModal
                                                                show={show}
                                                                setModalClose={handleClose}
                                                                onHide={handleClose}
                                                            />
                                                        </div>
                                                    </div>
                                                    :
                                                    <div>
                                                        <div className="seller-product-empty-header">
                                                            Your Ordered product list is empty
                                                        </div>
                                                        <div className="seller-product-empty-message">
                                                            You can list and sell any livestock, livestock accesseries and related products from any where in india through our website in <a className="seller-product-empty-buy-link" href="/sell">here</a>. And we also take care of your product delivery for areas we serve.
                                                        </div>
                                                    </div>
                                                }
                                            </div>
                                            : null
                                        }
                                    </Col>
                                </Row>
                            </Tab>
                            <Tab eventKey={2} className="sold-product-detail-title" title="Soldout" onClick={x => getProducts("sold")}>
                                <br />
                                {soldProductList && soldProductList.length > 0 ?
                                    <div className="seller-product-card-list">
                                        
                                        {soldProductList.map((product, id) => {
                                            return <div>
                                                <SellerProduct
                                                    productImage={product.product_variant[0].product_images}
                                                    productdetail={product}
                                                    issoldout={true}
                                                    type = 'SOLD'
                                                    isFarmzonnsale = {product.sold_though_farmzonn}
                                                />
                                            </div>
                                        })}
                                    </div>
                                    : null
                                }
                                <Row>
                                    <Col>
                                        {!soldProductList || soldProductList.length == 0 ?
                              
                                            <div style={{
                                                width: '100%', display: 'flex', flexDirection: 'column',
                                                justifyContent: 'center'
                                            }}>

                                                {Cookies.get('name') == null ?
                                                    <div>
                                                        <div className="seller-product-empty-header">
                                                            You have not logged in yet!
                                                        </div>
                                                        <div className="seller-product-empty-login-link-div">
                                                            <Button className="seller-product-empty-login-link" onClick={handleShow}>Signin here to view your listed product details </Button>
                                                        </div>
                                                        <div>
                                                            <SignInModal
                                                                show={show}
                                                                setModalClose={handleClose}
                                                                onHide={handleClose}
                                                            />
                                                        </div>
                                                    </div>
                                                    :
                                                    <div>
                                                        <div className="seller-product-empty-header">
                                                            Your Sold product list is empty
                                                        </div>
                                                        <div className="seller-product-empty-message">
                                                            You can list and sell any livestock, livestock accesseries and related products from any where in india through our website in <a className="seller-product-empty-buy-link" href="/sell">here</a>. And we also take care of your product delivery for areas we serve.
                                                        </div>
                                                    </div>
                                                }
                                        
                                            </div>
                                            : null
                                        }
                                    </Col>
                                </Row>
                            </Tab>
                            <Tab eventKey={3} className="sold-product-detail-title" title="Products with available stocks" onclick={x => getProducts("instock")}>
                                <br />
                                {inStockProductList && inStockProductList.length > 0 ?
                                    <div className="seller-product-card-list">
                                        
                                        {inStockProductList.map((product, id) => {
                                            return <div>
                                                <SellerProduct
                                                    productImage={product.product_variant[0].product_images}
                                                    productdetail={product}
                                                    type = 'INSTOCK'
                                                    issoldout={false}
                                                />
                                            </div>
                                        })}
                                    </div>
                                    : null
                                }
                                <Row>
                                    <Col>
                                        {!inStockProductList || inStockProductList.length == 0 ?
                              
                                            <div style={{
                                                width: '100%', display: 'flex', flexDirection: 'column',
                                                justifyContent: 'center'
                                            }}>

                                                {Cookies.get('name') == null ?
                                                    <div>
                                                        <div className="seller-product-empty-header">
                                                            You have not logged in yet!
                                                        </div>
                                                        <div className="seller-product-empty-login-link-div">
                                                            <Button className="seller-product-empty-login-link" onClick={handleShow}>Signin here to view your listed product details </Button>
                                                        </div>
                                                        <div>
                                                            <SignInModal
                                                                show={show}
                                                                setModalClose={handleClose}
                                                                onHide={handleClose}
                                                            />
                                                        </div>
                                                    </div>
                                                    :
                                                    <div>
                                                        <div className="seller-product-empty-header">
                                                            Your Product list is empty
                                                        </div>
                                                        <div className="seller-product-empty-message">
                                                            You can list and sell any livestock, livestock accesseries and related products from any where in india through our website in <a className="seller-product-empty-buy-link" href="/sell">here</a>. And we also take care of your product delivery for areas we serve.
                                                        </div>
                                                    </div>
                                                }
                                        
                                            </div>
                                            : null
                                        }
                                    </Col>
                                </Row>
                            </Tab>
                        </Tabs>
                    </Container>
                </div>
            }
        </div>
    )
}