    import React, { useState } from 'react'
import { Button, Card, Col, Container, Modal, Row } from 'react-bootstrap'
import Image from 'react-bootstrap/Image'
import Moment from 'react-moment';

import Cookies from 'js-cookie'
import CryptoJS from 'crypto-js';

import configData from "../../../../config.json";
import '../SellerProductsPage.css'

import SellerInvoice from '../Sections/SellerInvoice';

export default function SellerProduct(props) {

    const [markProductasSoldShow, setMarkProductasSoldShow] = useState(false);
    const [isSoldtoFarmzonnContact, setIsSoldtoFarmzonnContact] = useState(false)

    const [invoicedetail, setInvoicedetail] = useState({})
    const [showInvoice, setShowInvoice] = useState(false)

    let indianRupeeLocale = Intl.NumberFormat('en-IN');

    const totalproductviews = (props.productdetail.product_viewed.reduce((total, currentItem) => total = total + currentItem.viewcount, 0));
    
    const productlastviewedon = props.productdetail.product_viewed.length > 0 ? new Date(Math.max.apply(null, props.productdetail.product_viewed.map(function (e) {
        return new Date(e.lastviewedon);
    }))) : null;

    let usertype = CryptoJS.AES.decrypt(Cookies.get('type'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8)

    const renderImage = (image) => {
        
        if (image) {
            return `${configData.IMG_FOLDER_PATH}${image}`
        }
        return `${configData.DEFAULT_PRODUCT_IMG}`
    }

    const handleSoldResponseSave = (isSoldtoFarmzonnContact) => {

        fetch(`/api/v1/update-product/id=${encodeURIComponent(props.productdetail.id)}&quantity=0&sold=true&isfarmzonnsale=${encodeURIComponent(isSoldtoFarmzonnContact)}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
        })
            .then((response) => {
                if (response.ok) {
                    window.location.reload()
                } else {
                    throw new Error(response);
                }
            })
            .then(function (resp) {
                console.log(resp);
            })
            .catch((err) => {
                console.log(err);
            })
    }

    const fetchsaleinvoice = (orderid) => {

        fetch(`/api/v1/saleinvoice/orderid=${encodeURIComponent(orderid)}`, {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response);
                }
            })
            .then(function (resp) {
                console.log(resp);
                setInvoicedetail(resp)
                setShowInvoice(true)
            })
            .catch((err) => {
                console.log(err);
                return null;
            })
    }

    const handleViewInvoice = () => {
        fetchsaleinvoice(props.orderdetail.id)
    }

    return (
        <div className="seller-product-item-card">
            {showInvoice && props.orderdetail ?
                <div>
                    <SellerInvoice
                        order={props.orderdetail}
                        showmodal={showInvoice}
                        setshowmodal={setShowInvoice}
                    />
                </div>
                :
                null
            }
            <div>
                <Modal
                    size="sm"
                    centered
                    backdrop="static"
                    onHide={() => setMarkProductasSoldShow(false)}
                    show={markProductasSoldShow}
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="seller-product-save-response">
                            Tell us about the sale
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className='seller-product-admin-change-row'>
                            <div className='seller-product-save-sold-response-heading'>
                                Sale was done to
                            </div>
                        
                            <div className='seller-product-save-sold-response'>
                                <Button variant="primary" onClick={() => { handleSoldResponseSave(true) }}>FarmZonn contact</Button>
                            </div>
                
                            <div className='seller-product-save-sold-response'>
                                <Button variant="primary" onClick={() => { handleSoldResponseSave(false) }}>Others</Button>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
            <Card style={{ width: '80%' }}>
                <Card.Header>
                    <Container>
                        <Row>
                            <Col>
                                <a className="seller-product-item-name" href={`/product/${props.productdetail.id}`}>
                                    {props.productdetail.name}
                                </a>
                            </Col>
                            <Col>
                                <div className="seller-product-category-header">
                                    {props.productdetail.category} (Breed Name: {props.productdetail.breed})
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </Card.Header>
                <Card.Body>
                    <Card.Text>
                        <Container>
                            <Row xs={1} sm={2} md={4}>
                                <Col>
                                    {/* <div className="seller-product-item-card-text"> */}
                                    <a href={`/product/${props.productdetail.id}`}>
                                        <div className="seller-product-img-div">
                                            <Image className="seller-product-img" src={props.productImage.length > 0 ? renderImage(props.productImage[0].image): ""} fluid />
                                        </div>
                                    </a>
                                </Col>
                                <Col>
                                    <div className="seller-product-total-div">
                                        <div>
                                            {props.productdetail.description}
                                        </div>
                                        <div>
                                            #: {props.productdetail.uniqueidentifier}
                                        </div>
                                        <div>
                                            {props.productdetail.breed} Breed,  {props.productdetail.product_variant[0].age} of age
                                        </div>
                                        <div>
                                            {props.productdetail.product_variant[0].gender} - {props.productdetail.product_variant[0].weight} kg
                                        </div>
                                        <div>
                                            Original Price: <span> </span>&#8377; {indianRupeeLocale.format(parseFloat(props.productdetail.product_variant[0].original_price))}
                                            {props.productdetail.product_variant[0].offer_price
                                                && props.productdetail.product_variant[0].offer_price > 0 ?
                                                <div> Offer Price:
                                                    <span> </span> &#8377; {indianRupeeLocale.format(parseFloat(props.productdetail.product_variant[0].offer_price))}
                                                </div>
                                                :
                                                null
                                            }
                                        </div>
                                        {props.type == 'ORDERED' && (usertype == 9 || props.status.includes('Delivered')) ?
                                            <div className='seller-product-invoice'>
                                                <Button onClick={handleViewInvoice} >View Invoice</Button>
                                            </div>
                                            :
                                            null
                                        }
                                    </div>
                                </Col>
                                <Col>
                                    <div className='seller-product-list-detail'>
                                        <div>
                                            {props.issoldout ?
                                                <div>
                                                    <div className='seller-product-list-sold'> SOLDOUT </div>
                                                    {props.quantity ? <div> Sold quantity: {props.quantity}</div> : null}
                                                </div>
                                                :
                                                <div>  Stock(s) available: <span> </span> {props.productdetail.product_variant[0].stock} </div>
                                            }
                                        </div>
                                        <div>
                                            Listed on: <span> </span> <Moment format="DD-MMMM-yyyy">{props.productdetail.created_date}</Moment>
                                        </div>
                                        <div className='seller-product-admin-change-row'>
                                            Total visits: <span> </span>
                                            {totalproductviews > 0 ?
                                                <div className='seller-product-view-detail'>
                                                    {totalproductviews}+ </div>
                                                :
                                                null
                                            }
                                        </div>
                                        {productlastviewedon != null ?
                                            <div className='seller-product-admin-change-row'>
                                                Last visited: <span> </span>
                                                <div className='seller-product-view-detail'>
                                                    <Moment format="DD-MMMM-yyyy">
                                                        {productlastviewedon}
                                                    </Moment>
                                                </div>
                                            </div>
                                            :
                                            null
                                        }
                                            
                                        {props.issoldout ?
                                            <div className='seller-product-list-sold-stock-message'>
                                                *Available stocks if any, will be listed in next tab
                                            </div>
                                            : null
                                        }
                                    </div>
                                </Col>
                                <Col>
                                    <div>
                                        <div className="order-delivery-address">
                                            <div className="order-delivery-address-heading">Pickup address</div>
                                            <div>
                                                {props.productdetail.pickup.address.name}
                                            </div>
                                            <div>
                                                {props.productdetail.pickup.address.address_1 + ', ' + props.productdetail.pickup.address.address_2}
                                            </div>
                                            <div>
                                                {props.productdetail.pickup.address.city + ', ' + props.productdetail.pickup.address.state + ' - ' + props.productdetail.pickup.address.pincode}
                                            </div>
                                            <div>
                                                Phone: {props.productdetail.pickup.address.phone}
                                            </div>
                                            <div className="order-delivery-type">
                                                <div>Address type: </div>
                                                <div className="order-delivery-address-type">
                                                    {props.productdetail.pickup.address.type}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                                {props.status ?
                                    <Col>
                                        <div>
                                            <div className='seller-product-order-status'>
                                                <div>
                                                    Order Status:
                                                </div>
                                                <div className='seller-product-order-status-detail'>
                                                    {props.status}
                                                </div>
                                            </div>
                                        </div>
                                    </Col>
                                    :
                                    <Col>
                                        <div className='seller-product-admin-changes'>
                                            <div className='seller-product-admin-change-row'>
                                                <div>
                                                    Verification Status:
                                                </div>
                                                <div className='seller-product-admin-change-row-detail'>
                                                    {props.productdetail.is_verified ? "Pass" :
                                                        props.productdetail.verification_comment ?
                                                            <span className='error'>Fail</span>
                                                            : <span className='error'>Pending</span>
                                                    }
                                                </div>
                                            </div>
                                            {props.productdetail.verification_comment ?
                                                <div className='seller-product-admin-change-row'>
                                                    <div>
                                                        Verification Comment:
                                                    </div>
                                                    <div className='seller-product-admin-change-row-detail'>
                                                        {props.productdetail.verification_comment}
                                                    </div>
                                                </div>
                                                :
                                                null
                                            }
                                            {!props.issoldout ?
                                                <div className='seller-product-admin-change-row'>
                                                    <div>
                                                        <Button variant="primary" onClick={() => setMarkProductasSoldShow(true)}>
                                                            Mark Product as Sold
                                                        </Button>
                                                    </div>
                                                </div>
                                                :
                                                <div className='seller-product-admin-change-row'>
                                                    <div>
                                                        Thanks for Sharing your sale detail.
                                                    </div>
                                                </div>
                                            }
                                            {props.isFarmzonnsale ?
                                                <div className='seller-product-admin-change-row'>
                                                    <div>
                                                        Sold to FarmZonn buyer
                                                    </div>
                                                </div>
                                                :
                                                null
                                            }
                                        </div>
                                    </Col>
                                }
                            </Row>
                        </Container>
                        
                    </Card.Text>
                </Card.Body>
            </Card>
            <br />
        </div>
    )
}