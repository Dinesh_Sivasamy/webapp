import React, { useEffect, useState } from "react";
import { Card, Container, Row, Col, Button } from "react-bootstrap";
import Cookies from "js-cookie";
import CryptoJS from "crypto-js";
import OrderedItem from "./Sections/OrderedItem";
import Loader from "../../Loader";

import "./OrderPage.css";
import SignInModal from "../User/SignInModal";
import configData from "../../../config.json";

export default function OrderPage() {
  const [isLoading, setLoading] = useState(true);
  const [show, setShow] = useState(false);

  const [orderStatus, setOrderStatus] = useState([]);

  const handleClose = () => {
    setShow(false);
    window.location.reload();
  };

  const handleShow = () => setShow(true);

  const [OrderList, setOrderList] = useState([]);

  useEffect(() => {
    window.scrollTo(0, 0);

    if (Cookies.get("id")) {
      localStorage.removeItem("productlist");
      let usertype = CryptoJS.AES.decrypt(
        Cookies.get("type"),
        `${configData.CRYPTO_SECRET_KEY}`
      ).toString(CryptoJS.enc.Utf8);

      fetch(
        `/api/v1/order/buyer=${encodeURIComponent(
          CryptoJS.AES.decrypt(
            Cookies.get("id"),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(CryptoJS.enc.Utf8)
        )}&type=${usertype}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "X-CSRFToken": Cookies.get("csrftoken"),
          },
        }
      )
        .then((resp) => {
          if (resp.ok) {
            return resp.json();
          } else {
            throw new Error(resp.statusText);
          }
        })
        .then(function (resp) {
          console.log(resp);
          setOrderList(resp);
          setLoading(false);
        })
        .catch((err) => {
          console.log(err);
          setLoading(false);
        });

      fetch(`/api/v1/order-status`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
      })
        .then((resp) => {
          if (resp.ok) {
            return resp.json();
          } else {
            throw new Error(resp.statusText);
          }
        })
        .then(function (resp) {
          console.log(resp);
          setOrderStatus(resp);
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      setLoading(false);
    }
  }, []);

  return (
    <div>
      {isLoading ? (
        <Loader />
      ) : (
        <div>
          <Container className="order-container">
            <div className="order-heading">Your Orders</div>
            <hr />
            {OrderList && OrderList.length > 0 ? (
              <div className="order-card-list">
                {OrderList.map((orderItem, id) => {
                  return (
                    <div>
                      <Card style={{ width: "90%" }}>
                        <OrderedItem
                          id={orderItem.id}
                          order={orderItem}
                          ordernumber={orderItem.order_no}
                          buyer={orderItem.buyer}
                          buyerId={CryptoJS.AES.decrypt(
                            Cookies.get("id"),
                            `${configData.CRYPTO_SECRET_KEY}`
                          ).toString(CryptoJS.enc.Utf8)}
                          loggedInUserType={CryptoJS.AES.decrypt(
                            Cookies.get("type"),
                            `${configData.CRYPTO_SECRET_KEY}`
                          ).toString(CryptoJS.enc.Utf8)}
                          product={orderItem.product}
                          productImage={orderItem.product_image}
                          status={orderItem.status}
                          statusId={orderItem.statusId}
                          delivery={orderItem.delivery}
                          totalpayable={orderItem.total_payable}
                          discountamount={orderItem.discountedamount}
                          count={orderItem.items_count}
                          deliveryCharge={orderItem.delivery_charge}
                          tax={orderItem.tax_amount}
                          orderedOn={orderItem.created_date}
                          orderstatusList={orderStatus}
                        />
                      </Card>
                      <br />
                    </div>
                  );
                })}
              </div>
            ) : null}
            <Row>
              <Col>
                {!OrderList || OrderList.length == 0 ? (
                  <div
                    style={{
                      width: "100%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                    }}
                  >
                    {Cookies.get("name") == null ? (
                      <div>
                        <div className="order-empty-header">
                          You have not logged in yet!
                        </div>
                        <div className="order-empty-login-link-div">
                          <Button
                            className="order-empty-login-link"
                            onClick={handleShow}
                          >
                            Signin here to view your orders
                          </Button>
                        </div>
                        <div>
                          <SignInModal
                            show={show}
                            setModalClose={handleClose}
                            onHide={handleClose}
                          />
                        </div>
                      </div>
                    ) : (
                      <div>
                        <div className="order-empty-header">
                          Your Order list is empty
                        </div>
                        <div className="order-empty-message">
                          You can buy any livestock, livestock accesseries and
                          related products from any where in india through our
                          website in{" "}
                          <a className="order-empty-buy-link" href="/buy/0">
                            here
                          </a>
                          . We take care of your product delivery for areas we
                          are in operational, also we guarantee that the product
                          you buy is of good quality and hygiene.
                        </div>
                      </div>
                    )}
                  </div>
                ) : null}
              </Col>
            </Row>
          </Container>
        </div>
      )}
    </div>
  );
}
