import React, { useEffect, useState } from "react";
import Select from "react-select";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Button } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
import NumericInput from "react-bootstrap-input-spinner";
import { Label, UncontrolledTooltip } from "reactstrap";
import { useHistory, useParams, Link } from "react-router-dom";
import Cookies from "js-cookie";
import CryptoJS from "crypto-js";
import PhoneInput from "react-phone-input-2";
import pincodeDirectory from "india-pincode-lookup";

import SignInModal from "../User/SignInModal";

import configData from "../../../config.json";
import Loader from "../../Loader";
import "./Enquiry.css";

export default function Enquiry(props) {
  const { enquiryid } = useParams();

  const [isLoading, setLoading] = useState(false);

  const history = useHistory();

  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);
  const handleClose = () => {
    setShow(false);
    window.location.reload();
  };

  const [allCategoryTypes, setAllCategoryTypes] = useState([]);
  const [categoryList, setCategoryList] = useState([]);

  const [savedrequest, setSavedrequest] = useState(null);
  const [requirementId, setRequirementId] = useState(null);

  // livestock list
  const [breedList, setBreedList] = useState([]);
  const [ageList, setAgeList] = useState([]);
  const [colorList, setColorList] = useState([]);
  const [genderList, setGenderList] = useState([]);

  const [uniqueId, setUniqueId] = useState();

  // set user inputs
  const [username, setUsername] = useState(
    Cookies.get("name")
      ? CryptoJS.AES.decrypt(
          Cookies.get("name"),
          `${configData.CRYPTO_SECRET_KEY}`
        ).toString(CryptoJS.enc.Utf8)
      : null
  );
  const [UserPhone, setUserPhone] = useState(
    Cookies.get("phone")
      ? CryptoJS.AES.decrypt(
          Cookies.get("phone"),
          `${configData.CRYPTO_SECRET_KEY}`
        ).toString(CryptoJS.enc.Utf8)
      : null
  );
  const [usertype, setUserType] = useState(
    Cookies.get("type")
      ? CryptoJS.AES.decrypt(
          Cookies.get("type"),
          `${configData.CRYPTO_SECRET_KEY}`
        ).toString(CryptoJS.enc.Utf8)
      : null
  );

  const [postedby, setPostedby] = useState(null);
  const [categoryType, setCategoryType] = useState();
  const [category, setCategory] = useState();
  const [requirementdescription, setRequirementdescription] = useState(null);
  const [price, setPrice] = useState(0);
  const [requiredcount, setRequiredcount] = useState(0);

  // set livestock user input
  const [breed, setBreed] = useState([]);
  const [age, setAge] = useState([]);
  const [color, setColor] = useState([]);
  const [gender, setGender] = useState([]);
  const [weight, setWeight] = useState();

  // set location user input
  const [pincode, setPincode] = useState(null);
  const [city, setCity] = useState(null);
  const [state, setState] = useState(null);

  const [agename, setAgename] = useState(null);
  const [colorname, setColorname] = useState(null);
  const [gendername, setGendername] = useState(null);
  const [breedname, setBreedname] = useState(null);
  const [postedbyname, setPostedbyname] = useState(null);
  const [categoryname, setCategoryname] = useState(null);
  const [categorytypename, setCategorytypename] = useState(null);

  //error handling
  const [validationErrors, setValidationErrors] = useState({});
  const [saverequirementresponse, setSaverequirementresponse] = useState(false);
  const [approverequirementresponse, setApproverequirementresponse] =
    useState(false);

  //status
  const [approvedBy, setApprovedBy] = useState(null);
  const [verificationComment, setVerificationComment] = useState(null);
  const [status, setStatus] = useState(1);
  const [recieveduserresponse, setRecieveduserresponse] = useState(0);
  const [createdOn, setCreatedOn] = useState(null);
  const [doesEnquiryPassverification, setDoesEnquiryPassverification] =
    useState(false);

  useEffect(() => {
    window.scrollTo(0, 0);

    console.log("entering enquiry page");
    console.log(enquiryid);

    fetch("/api/v1/category-type", {
      method: "GET",
      headers: {
        "content-type": "application/json",
      },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then(function (resp) {
        console.log(resp);
        setAllCategoryTypes(resp);
      })
      .catch((err) => {
        console.log(err);
      });

    if (enquiryid) {
      fetch(
        `/api/v1/product-requirement-enquiry/id=${encodeURIComponent(
          enquiryid
        )}`,
        {
          method: "GET",
          headers: {
            "content-type": "application/json",
          },
        }
      )
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error(response);
          }
        })
        .then(function (resp) {
          console.log(resp);
          setSavedrequest(resp);
          setRequirementId(resp.id);
          setPostedby(resp.posted_by);
          setUniqueId(resp.uniqueidentifier);
          setRequirementdescription(resp.description);
          setUsername(resp.postedfor);
          setUserPhone(resp.contact);
          setCategoryType(resp.categorytype);
          setCategory(resp.category);
          setBreed(resp.breed);
          setAge(resp.age);
          setGender(resp.gender);
          setRequiredcount(resp.requiredcount);
          setPrice(resp.acceptableprice);
          setWeight(resp.weight);
          setColor(resp.color);
          setPincode(resp.requiredaroundpincode);
          setCity(resp.requiredaroundcity);
          setState(resp.requirearoundstate);
          setDoesEnquiryPassverification(resp.isverified);
          setApprovedBy(resp.verifiedby);
          setStatus(resp.status);
          setRecieveduserresponse(resp.recieveduserresponse);
          setCreatedOn(resp.createdon);

          setAgename(resp.age_name);
          setColorname(resp.color_name);
          setGendername(resp.gender_name);
          setBreedname(resp.breed_name);
          setPostedbyname(resp.posted_by_name);
          setCategoryname(resp.category_name);
          setCategorytypename(resp.categorytype_name);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, []);

  const handleCategoryTypeChange = (obj) => {
    setCategoryType(obj);
    setCategoryList(obj.categories);
    setCategory(null);
  };

  const handleCategoryChange = (obj) => {
    setCategory(obj);
    setBreedList(obj.breeds);

    //calling age api on category change
    fetch("/api/v1/livestock-age", {
      method: "GET",
      headers: {
        "content-type": "application/json",
      },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then(function (resp) {
        setAgeList(resp);
      })
      .catch((err) => {
        console.log(err);
      });

    //calling color api on category change
    fetch("/api/v1/livestock-color", {
      method: "GET",
      headers: {
        "content-type": "application/json",
      },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then(function (resp) {
        setColorList(resp);
      })
      .catch((err) => {
        console.log(err);
      });

    //calling gender api on category change
    fetch("/api/v1/livestock-gender", {
      method: "GET",
      headers: {
        "content-type": "application/json",
      },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then(function (resp) {
        setGenderList(resp);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handlePhoneNumberChange = (value, data, event, formattedValue) => {
    setUserPhone(value);
    validationErrors.phone = "";
  };

  const handleUserNameChange = (e) => {
    setUsername(e.target.value);
  };

  // handle change event of the Breed dropdown
  const handleBreedChange = (obj) => {
    setBreed(obj);
  };

  // handle change event of the Age dropdown
  const handleAgeChange = (obj) => {
    setAge(obj);
  };

  // handle change event of the Color dropdown
  const handleColorChange = (obj) => {
    setColor(obj);
  };

  // handle change event of the Gender dropdown
  const handleGenderChange = (obj) => {
    setGender(obj);
  };

  // handle change event of the weight field
  const handleWeightChange = (obj) => {
    setWeight(obj);
  };

  const setPincodeRelatedFiledValues = (
    state,
    city,
    errors,
    warn,
    isdeliverable
  ) => {
    setState(state);
    setCity(city);
    setValidationErrors(errors);
  };

  const handlePincodeChange = (e) => {
    const errors = {};
    const warns = {};

    if (e.target.value.length == 6) {
      const locationjson = pincodeDirectory.lookup(e.target.value);
      if (locationjson && locationjson.length > 0) {
        setPincode(e.target.value);
        setPincodeRelatedFiledValues(
          locationjson[0].stateName,
          locationjson[0].districtName,
          {},
          {},
          true
        );
      } else {
        errors.pincode =
          "This Pincode is not valid. Please try different code.";
        setPincodeRelatedFiledValues(null, null, errors, {}, false);
      }
    } else {
      setPincode(e.target.value);
      errors.pincode = "This Pincode is not valid. Please try different code.";
      setPincodeRelatedFiledValues(null, null, errors, {}, false);
    }
  };

  const handlerequirementdescriptionChange = (e) => {
    setRequirementdescription(e.target.value);
  };

  const handlepriceChange = (e) => {
    setPrice(e.target.value);
  };

  const handlecountChange = (e) => {
    setRequiredcount(e);
  };

  const handleverificationstatuschange = (e) => {
    setDoesEnquiryPassverification(e.target.checked);
  };

  const handleverificationcommentchange = (e) => {
    setVerificationComment(e.target.value);
  };

  const saveRequirement = (requestJson) => {
    setLoading(true);
    fetch("/api/v1/product-requirement-enquiries", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
      body: JSON.stringify(requestJson),
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then((result) => {
        console.log(result);
        setLoading(false);
        setSaverequirementresponse(true);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
        setSaverequirementresponse(true);
      });
  };

  const buildJsondata = () => {
    var JsonRequestData = {
      id: requirementId ? requirementId : null,
      uniqueidentifier: uniqueId,
      description: requirementdescription,
      posted_by: postedby
        ? postedby
        : CryptoJS.AES.decrypt(
            Cookies.get("id"),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(CryptoJS.enc.Utf8),
      postedfor: username,
      contact:
        UserPhone && UserPhone.startsWith("+") ? UserPhone : "+" + UserPhone,
      categorytype: categoryType.id,
      category: category.id,
      breed: breed ? breed.id : null,
      age: age ? age.id : null,
      gender: gender ? gender.id : null,
      requiredcount: requiredcount,
      acceptableprice: price,
      weight: weight,
      color: color ? color.id : null,
      requiredaroundpincode: pincode,
      requiredaroundcity: city,
      requirearoundstate: state,
      isverified: doesEnquiryPassverification ? 1 : 0,
      verifiedby: approvedBy
        ? approvedBy
        : verificationComment
        ? CryptoJS.AES.decrypt(
            Cookies.get("id"),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(CryptoJS.enc.Utf8)
        : null,
      verificationcomment: verificationComment,
      status: 0,
      recieveduserresponse: 0,
      createdon: createdOn,
    };

    return JsonRequestData;
  };

  const handlesubmit = (evt) => {
    evt.preventDefault();
    setValidationErrors({});
    const errors = {};

    if (!UserPhone) {
      errors.phone = "Phone number cannot be blank";
      errors.hasvalue = true;
    }

    if (UserPhone && UserPhone.length < 12) {
      errors.phone = "Phone number is not in correct format";
      errors.hasvalue = true;
    }

    if (!username) {
      errors.username = "Name cannot be blank";
      errors.hasvalue = true;
    }

    if (!categoryType) {
      errors.categorytype =
        "Please select category type to fill out your requirement";
      errors.hasvalue = true;
    }

    if (categoryType && categoryType.id && !category) {
      errors.category =
        "Please select on which category of livestock you are looking for";
      errors.hasvalue = true;
    }

    if (!requirementdescription) {
      errors.requirementdescription =
        "Please add details about your requirement";
      errors.hasvalue = true;
    }

    if (!requiredcount || requiredcount == 0) {
      errors.requiredcount = "Required count cannot be blank";
      errors.hasvalue = true;
    }

    if (!weight) {
      errors.weight = "Weight cannot be blank";
      errors.hasvalue = true;
    }

    if (!price || price == 0) {
      errors.price = "Required price cannot be blank";
      errors.hasvalue = true;
    }

    if (!pincode || pincode == 0) {
      errors.pincode = "Pincode cannot be blank";
      errors.hasvalue = true;
    }

    setValidationErrors(errors);

    if (errors.hasvalue) {
      return;
    }

    let JsonRequestData = buildJsondata();

    saveRequirement(JsonRequestData);
  };

  const handleapprove = (e) => {
    e.preventDefault();
    setValidationErrors({});
    const errors = {};

    if (!verificationComment) {
      errors.verificationComment =
        "Please enter approval comment and save your response.";
      setValidationErrors(errors);
      return;
    }

    setLoading(true);

    let JsonRequestData = buildJsondata();

    fetch(
      `/api/v1/product-requirement-enquiry/id=${encodeURIComponent(
        requirementId
      )}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
        body: JSON.stringify(JsonRequestData),
      }
    )
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then((result) => {
        console.log(result);
        setLoading(false);
        setApproverequirementresponse(true);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
        setApproverequirementresponse(true);
      });
  };

  const handleEnquiryRespond = (e) => {
    history.push(`/sell`);
  };

  const customStyles = {
    control: (base, state) => ({
      ...base,
      background: "#green",
      // match with the menu
      borderRadius: state.isFocused ? "3px 3px 0 0" : 3,
      // Overwrittes the different states of border
      borderColor: state.isFocused ? "blue" : "green",
      // Removes weird border around container
      boxShadow: state.isFocused ? null : null,
      "&:hover": {
        // Overwrittes the different states of border
        borderColor: state.isFocused ? "red" : "red",
      },
    }),
    menu: (base) => ({
      ...base,
      // override border radius to match the box
      borderRadius: 0,
      // kill the gap
      marginTop: 0,
    }),
    menuList: (base) => ({
      ...base,
      background: "white",
      // kill the white space on first and last option
      padding: 0,
    }),
  };

  return (
    <div>
      {isLoading ? (
        <Loader />
      ) : (
        <div>
          <div>
            <Modal
              size="lg"
              centered
              backdrop="static"
              show={saverequirementresponse || approverequirementresponse}
            >
              <Modal.Header>
                <Modal.Title id="enquiry-product-save-response">
                  Enquiry posted successfully
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                {approverequirementresponse
                  ? "Succesfully approved requirement request for posting."
                  : "Thanks for posting your requirement, our team will validate your request and list it in our website."}
              </Modal.Body>
              <Modal.Footer>
                <Button>
                  <a href="/" className="modal-btn-close">
                    Close
                  </a>
                </Button>
              </Modal.Footer>
            </Modal>
          </div>

          <div>
            <div>
              <br />
              {requirementId ? (
                <div>
                  <div className="page-title-with-border-space">
                    Click respond to list farm animals for sale!!
                  </div>
                  <Container className="square border border-4 rounded">
                    <div className="enquiry-userinput">
                      <Row xs={1} sm={2} md={2} className="sell-row">
                        <Col>
                          <Label className="enquiry-readonly-label">
                            Product Category type:
                          </Label>
                          <span> </span> {categorytypename}
                        </Col>
                      </Row>

                      <Row xs={1} sm={2} md={2} className="sell-row">
                        {categoryname ? (
                          <Col>
                            <Label className="enquiry-readonly-label">
                              Product Category:
                            </Label>
                            <span> </span> {categoryname}
                          </Col>
                        ) : null}
                        {breedname ? (
                          <Col>
                            <Label className="enquiry-readonly-label">
                              Livestock Breed:
                            </Label>
                            <span> </span> {breedname}
                          </Col>
                        ) : null}
                        {agename ? (
                          <Col>
                            <Label className="enquiry-readonly-label">
                              Livestock Age:
                            </Label>
                            <span> </span> {agename}
                          </Col>
                        ) : null}
                      </Row>
                      <Row xs={1} sm={2} md={2} className="sell-row">
                        {colorname ? (
                          <Col>
                            <Label className="enquiry-readonly-label">
                              Livestock Color:
                            </Label>
                            <span> </span> {colorname}
                          </Col>
                        ) : null}
                        {gendername ? (
                          <Col>
                            <Label className="enquiry-readonly-label">
                              Livestock Gender:
                            </Label>
                            <span> </span> {gendername}
                          </Col>
                        ) : null}
                      </Row>
                      <Row xs={1} sm={2} md={2} className="sell-row">
                        <Col>
                          <Label className="enquiry-readonly-label">
                            More details on required product:
                          </Label>
                          <span> </span> {requirementdescription}
                        </Col>
                        <Col>
                          <Label className="enquiry-readonly-label">
                            Required count:
                          </Label>
                          <span> </span> {requiredcount}
                        </Col>
                      </Row>
                      <Row xs={1} sm={2} md={2} className="sell-row">
                        <Col>
                          <Label className="enquiry-readonly-label">
                            Weight:
                          </Label>
                          <span> </span>{" "}
                          {weight > 0 ? weight + " kg/item" : "Not Mentioned"}
                        </Col>
                        <Col>
                          <Label className="enquiry-readonly-label">
                            Looking for Price around:
                          </Label>
                          <span> </span> &#8377; {price} /item
                        </Col>
                      </Row>
                      <Row lg={2} md={2} xs={1} className="sell-row">
                        <Col>
                          <Label className="enquiry-readonly-label">
                            Looking for Product in and around:
                          </Label>
                          <Label className="sell-label">
                            <span> </span>
                            {city + " - " + state}
                          </Label>
                        </Col>
                      </Row>
                      <Row lg={6} md={4} xs={1} className="sell-row">
                        <Col>
                          <div className="form-group">
                            <Label className="enquiry-readonly-label">
                              Posting enquiry for User:
                            </Label>
                            <span> </span> {username}
                          </div>
                        </Col>
                        {usertype == 9 ? (
                          <Col>
                            <div className="form-group">
                              <Label className="enquiry-readonly-label">
                                User Phone number:
                              </Label>
                              <span> </span> {UserPhone}
                            </div>
                          </Col>
                        ) : null}
                      </Row>
                      <Row lg={2} md={2} xs={1} className="sell-row">
                        <Col>
                          {usertype == 9 ? (
                            <div>
                              <br />
                              <div className="sell-display-inline">
                                <div>Tick if enquiry is genuine:</div>
                                <div className="sell-verf">
                                  <input
                                    type="checkbox"
                                    checked={doesEnquiryPassverification}
                                    onChange={handleverificationstatuschange}
                                  />{" "}
                                  <span> </span>
                                  {doesEnquiryPassverification
                                    ? " Ok to list"
                                    : " Not fit for Listing"}
                                </div>
                              </div>
                              <div className="sell-verf-comment">
                                <input
                                  type="text"
                                  className="form-control"
                                  placeholder="Verification comments"
                                  value={verificationComment}
                                  onChange={handleverificationcommentchange}
                                />
                                {validationErrors.verificationComment ? (
                                  <span className="error">
                                    *{validationErrors.verificationComment}
                                  </span>
                                ) : null}
                                <br />
                              </div>
                              <div>
                                <Button
                                  type="submit"
                                  className="sell-submitbutton"
                                  disabled={approvedBy != null}
                                  onClick={handleapprove}
                                >
                                  Approve
                                </Button>
                              </div>
                            </div>
                          ) : (
                            <div>
                              <Button>
                                <a href="/sell" className="modal-btn-close">
                                  Respond
                                </a>
                              </Button>
                            </div>
                          )}
                        </Col>
                      </Row>
                    </div>
                  </Container>
                  <br />
                </div>
              ) : Cookies.get("name") == null ? (
                <div>
                  <div className="page-title-with-border-space">
                    Post details of Farm animals here, to let sellers knows
                    about it!
                    <hr />
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                    }}
                    className="product-list-empty"
                  >
                    <div className="product-list-empty-heading">
                      <div className="enquiry-header">
                        You have not logged in yet!
                      </div>
                      <br />
                      <div className="enquiry-login-link-div">
                        <Button
                          className="enquiry-login-link"
                          onClick={handleShow}
                        >
                          Signin here to post requirement
                        </Button>
                      </div>
                      <div>
                        <SignInModal
                          show={show}
                          setModalClose={handleClose}
                          onHide={handleClose}
                        />
                      </div>
                    </div>
                    <br />
                  </div>
                </div>
              ) : (
                <div>
                  <div className="page-title-with-border-space">
                    Post details of Farm animals, you are looking for & let
                    sellers knows about it!!!
                  </div>
                  <Container className="square border border-4">
                    <div className="enquiry-userinput">
                      <Row lg={6} md={4} xs={1} className="sell-row">
                        <Col>
                          <div className="form-group">
                            <label>Posting enquiry for User</label>
                            <input
                              className="form-control"
                              onChange={handleUserNameChange}
                              placeholder="Name"
                              value={username}
                            />
                            {validationErrors.username ? (
                              <span className="error">
                                *{validationErrors.username}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                        <Col>
                          <div className="form-group">
                            <label>User Phone number</label>
                            <PhoneInput
                              placeholder="Enter Phone number"
                              country={"in"}
                              value={UserPhone}
                              onChange={handlePhoneNumberChange}
                            />
                            {validationErrors.phone ? (
                              <span className="error">
                                *{validationErrors.phone}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                      <Row xs={1} sm={2} md={2} className="sell-row">
                        <Col>
                          <Label className="sell-label">
                            Product Category type
                          </Label>
                          <div className="sell-dropdown-field">
                            <Select
                              className="sell-dropdown"
                              placeholder="Category Type"
                              value={categoryType}
                              options={allCategoryTypes}
                              required
                              onChange={handleCategoryTypeChange}
                              styles={customStyles}
                              getOptionLabel={(x) => x.name}
                              getOptionValue={(x) => x.id}
                            />
                          </div>
                          {validationErrors.categorytype ? (
                            <span className="error">
                              *{validationErrors.categorytype}
                            </span>
                          ) : null}
                        </Col>
                      </Row>

                      {categoryType &&
                      (categoryType.id == 1 || categoryType.id == 2) ? (
                        <div>
                          <Row xs={1} sm={2} md={2} className="sell-row">
                            <Col>
                              <Label className="sell-label">
                                Product Category
                              </Label>
                              <div className="sell-dropdown-field">
                                <Select
                                  className="sell-dropdown"
                                  placeholder="Category"
                                  value={category}
                                  options={categoryList}
                                  onChange={handleCategoryChange}
                                  styles={customStyles}
                                  getOptionLabel={(x) => x.name}
                                  getOptionValue={(x) => x.id}
                                />
                                {validationErrors.category ? (
                                  <span className="error">
                                    *{validationErrors.category}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                            <Col>
                              <Label className="sell-label">
                                Livestock Breed
                              </Label>
                              <div className="sell-dropdown-field">
                                <Select
                                  className="sell-dropdown"
                                  placeholder="Breed"
                                  value={breed}
                                  options={breedList}
                                  onChange={handleBreedChange}
                                  styles={customStyles}
                                  getOptionLabel={(x) => x.name}
                                  getOptionValue={(x) => x.id}
                                />
                              </div>
                            </Col>
                            <Col>
                              <Label className="sell-label">
                                Livestock Age
                              </Label>
                              <div className="sell-dropdown-field">
                                <Select
                                  className="sell-dropdown"
                                  placeholder="Age"
                                  value={age}
                                  options={ageList}
                                  onChange={handleAgeChange}
                                  styles={customStyles}
                                  getOptionLabel={(x) => x.age_range}
                                  getOptionValue={(x) => x.id}
                                />
                              </div>
                            </Col>
                          </Row>
                          <Row xs={1} sm={2} md={2} className="sell-row">
                            <Col>
                              <Label className="sell-label">
                                Livestock Color
                              </Label>
                              <div className="sell-dropdown-field">
                                <Select
                                  className="sell-dropdown"
                                  placeholder="Color"
                                  value={color}
                                  options={colorList}
                                  onChange={handleColorChange}
                                  styles={customStyles}
                                  getOptionLabel={(x) => x.name}
                                  getOptionValue={(x) => x.id}
                                />
                              </div>
                            </Col>
                            <Col>
                              <Label className="sell-label">
                                Livestock Gender
                              </Label>
                              <div className="sell-dropdown-field">
                                <Select
                                  className="sell-dropdown"
                                  placeholder="Gender"
                                  value={gender}
                                  options={genderList}
                                  onChange={handleGenderChange}
                                  styles={customStyles}
                                  getOptionLabel={(x) => x.name}
                                  getOptionValue={(x) => x.id}
                                />
                              </div>
                            </Col>
                          </Row>
                        </div>
                      ) : null}
                      <Row xs={1} sm={2} md={2} className="sell-row">
                        <Col>
                          <Label className="sell-label">
                            More details on required product
                          </Label>
                          <div className="sell-name-field">
                            <input
                              type="text"
                              className="form-control"
                              placeholder="Enter any specific details of what you are looking for"
                              value={requirementdescription}
                              onChange={handlerequirementdescriptionChange}
                            />
                            {validationErrors.requirementdescription ? (
                              <span className="error">
                                *{validationErrors.requirementdescription}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                        <Col>
                          <Label className="sell-label">Required count</Label>
                          <br />
                          <div className="sell-numericfield">
                            <NumericInput
                              type={"real"}
                              variant={"secondary"}
                              size="sm"
                              step={1}
                              precision={0}
                              min={0}
                              max={10}
                              value={requiredcount}
                              onChange={handlecountChange}
                            />
                            {validationErrors.requiredcount ? (
                              <span className="error">
                                *{validationErrors.requiredcount}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                      <Row xs={1} sm={2} md={2} className="sell-row">
                        <Col>
                          <Label className="sell-label">
                            Weight (in Kg/item)
                          </Label>
                          <br />
                          <div className="sell-numericfield">
                            <NumericInput
                              type={"decimal"}
                              variant={"secondary"}
                              size="sm"
                              step={0.25}
                              size="5"
                              precision={2}
                              min={0}
                              value={weight}
                              onChange={handleWeightChange}
                            />
                            {validationErrors.weight ? (
                              <span className="error">
                                *{validationErrors.weight}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                        <Col>
                          <Label className="sell-label">
                            Looking for Price around (in &#8377; /item)
                          </Label>
                          <br />
                          <div className="sell-numericfield">
                            <input
                              type="number"
                              className="form-control"
                              placeholder="Price"
                              value={price}
                              onChange={handlepriceChange}
                            />
                            {validationErrors.price ? (
                              <span className="error">
                                *{validationErrors.price}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                      <Row lg={6} md={4} xs={1} className="add-address-row">
                        <Label className="sell-label">
                          Pincode of location where your looking for the Product
                        </Label>
                        <br />
                        <Col className="add-address-col">
                          <div className="sell-name-field">
                            <input
                              type="number"
                              className="form-control"
                              placeholder="Pincode"
                              maxLength="10"
                              value={pincode}
                              onChange={handlePincodeChange}
                            />
                            {validationErrors.pincode ? (
                              <span className="error">
                                *{validationErrors.pincode}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                        {city && state ? (
                          <Col>
                            <Label className="sell-label">
                              Your search location is
                            </Label>
                            <br />
                            <Label className="sell-label">
                              {city + " - " + state}
                            </Label>
                          </Col>
                        ) : null}
                      </Row>
                    </div>
                    <Row xs={1} sm={2} md={2} className="sell-row">
                      <Col>
                        {requirementId == null ? (
                          <Button
                            type="submit"
                            className="sell-submitbutton"
                            onClick={handlesubmit}
                          >
                            Submit
                          </Button>
                        ) : (
                          <Button
                            type="submit"
                            className="sell-submitbutton"
                            onClick={handleEnquiryRespond}
                          >
                            Respond
                          </Button>
                        )}
                      </Col>
                    </Row>
                  </Container>
                </div>
              )}
              <br />
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
