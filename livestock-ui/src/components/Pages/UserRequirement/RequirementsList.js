import React, { useEffect, useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Button } from "react-bootstrap";
import { MdVerifiedUser } from "react-icons/md";

import goat_img from "../Home/images/enquiry/Goat.png";
import cattle_img from "../Home/images/enquiry/Cattle.png";
import chicken_img from "../Home/images/enquiry/Chicken.png";
import sheep_img from "../Home/images/enquiry/Sheep.png";
import dog_img from "../Home/images/enquiry/dog.png";
import default_img from "../Home/images/enquiry/Default.png";

export default function RequirementList() {
  const [enquiryRequests, setEnquiryRequests] = useState([]);

  let indianRupeeLocale = Intl.NumberFormat("en-IN");

  // To set two dates to two variables
  var createddate = new Date();
  var currentdate = new Date();

  // To calculate the time difference of two dates
  var Difference_In_Time = currentdate.getTime() - createddate.getTime();

  // To calculate the no. of days between two dates
  var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);

  useEffect(() => {
    fetch(`/api/v1/product-requirement-enquiries`, {
      method: "GET",
      headers: {
        "content-type": "application/json",
      },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then(function (resp) {
        console.log(resp);
        setEnquiryRequests(resp);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      {enquiryRequests && enquiryRequests.length > 0 ? (
        <Container>
          <div className="page-title">Wanted</div>
          <div className="cards__container">
            <div className="cards__wrapper">
              <Row xs={1} sm={1} md={2} lg={2}>
                {enquiryRequests.map((enquiry, id) => (
                  <Col>
                    <div>
                      <li className="cards__item">
                        <a
                          className={
                            enquiry.status
                              ? "cards__item__link disabledCursor"
                              : "cards__item__link"
                          }
                          href={`/view-enquiry/${enquiry.id}`}
                        >
                          <div>
                            <figure
                              className="cards__item__pic--wrap"
                              data-category={enquiry.categorytype_name}
                            >
                              {/* <figure className={enquiry.isp === "" ? '' : 'cards_item_pic--wrap--pregnant'} is-pregnant={enquiry.isp}> */}
                              <figure
                                className="cards_item_pic--wrap--new--enquiry"
                                is-new={
                                  (new Date().getDate() -
                                    new Date(enquiry.createdon).getDate()) /
                                    (1000 * 3600 * 24) <
                                  10
                                    ? "NEW ENQUIRY"
                                    : "WANTED"
                                }
                              >
                                <img
                                  src={
                                    enquiry.category_name == "Cattle"
                                      ? cattle_img
                                      : enquiry.category_name == "Goat"
                                      ? goat_img
                                      : enquiry.category_name == "Sheep"
                                      ? sheep_img
                                      : enquiry.category_name == "Chicken"
                                      ? chicken_img
                                      : enquiry.category_name == "Country Dogs"
                                      ? dog_img
                                      : default_img
                                  }
                                  alt="Items images"
                                  className={
                                    enquiry.isclosed
                                      ? "cards__item__img img-greyscale"
                                      : "cards__item__img"
                                  }
                                />
                                {/* {enquiry.posted_by_name ? 
                                <figure className='cards_item_pic--wrap-verifiedseller'>
                                <div className='cards_item_pic--wrap-verifiedseller-box'>
                                    <MdVerifiedUser /> 
                                    {enquiry.posted_by_name}
                                </div>
                            </figure>
                            :
                            null
                                } */}
                              </figure>
                            </figure>
                            <div className="cards__item__info">
                              <div className="cards__item__name">
                                {enquiry.description}
                              </div>
                              <div className="cards__item__about">
                                {enquiry.weight > 0
                                  ? enquiry.weight +
                                    " Kgs of weight | " +
                                    enquiry.breed_name
                                  : enquiry.breed_name}
                              </div>
                              <div className="cards__item__gender_milkproduce">
                                {enquiry.gender_name}
                              </div>
                              <div className="cards__item__gender_milkproduce">
                                {enquiry.requiredcount > 0
                                  ? " | " + enquiry.requiredcount + " Qty"
                                  : null}
                              </div>
                              <br />
                              {/* <div>
                                  <div className="cards__item__discountprice">
                                    &#8377;{" "}
                                    {indianRupeeLocale.format(
                                      enquiry.acceptableprice
                                    )}
                                  </div>
                                </div> */}
                              <div className="cards__item__text">
                                {enquiry.requiredaroundcity +
                                  ", " +
                                  enquiry.requirearoundstate}
                              </div>
                              {/* <div>
                                <Button type="submit">Enquire Seller</Button> 
                              </div> */}
                            </div>
                          </div>
                        </a>
                      </li>
                    </div>
                  </Col>
                ))}
              </Row>
            </div>
          </div>
        </Container>
      ) : (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
          }}
          className="product-list-empty"
        >
          <div className="product-list-empty-heading">
            Sorry! there are no enquiries open as of now, please try again
            later.
          </div>
        </div>
      )}
    </div>
  );
}
