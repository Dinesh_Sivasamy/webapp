import React, { useEffect, useState } from "react";
import { Button, Card, Col, Container, Row } from "react-bootstrap";
import Image from "react-bootstrap/Image";
import Moment from "react-moment";

import Cookies from "js-cookie";
import CryptoJS from "crypto-js";

import SignInModal from "../User/SignInModal";
import configData from "../../../config.json";

import goat_img from "../Home/images/enquiry/Goat.png";
import cattle_img from "../Home/images/enquiry/Cattle.png";
import chicken_img from "../Home/images/enquiry/Chicken.png";
import sheep_img from "../Home/images/enquiry/Sheep.png";
import dog_img from "../Home/images/enquiry/dog.png";
import default_img from "../Home/images/enquiry/Default.png";

export default function UserRequirementList() {
  const [enquiryRequests, setEnquiryRequests] = useState([]);

  let indianRupeeLocale = Intl.NumberFormat("en-IN");

  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false);
    window.location.reload();
  };

  const handleShow = () => setShow(true);

  useEffect(() => {
    fetch(`/api/v1/user-product-requirement-enquiries`, {
      method: "GET",
      headers: {
        "content-type": "application/json",
      },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then(function (resp) {
        console.log(resp);
        setEnquiryRequests(resp);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <div className="seller-products-heading">Enqueries you have posted!</div>
      <hr />
      <Container className="seller-product-container">
        {!enquiryRequests || enquiryRequests.length == 0 ? (
          <div
            style={{
              width: "100%",
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
            }}
          >
            {Cookies.get("name") == null ? (
              <div>
                <div className="seller-product-empty-header">
                  You have not logged in yet!
                </div>
                <div className="seller-product-empty-login-link-div">
                  <Button
                    className="seller-product-empty-login-link"
                    onClick={handleShow}
                  >
                    Signin here to view your listed enquiries details{" "}
                  </Button>
                </div>
                <div>
                  <SignInModal
                    show={show}
                    setModalClose={handleClose}
                    onHide={handleClose}
                  />
                </div>
              </div>
            ) : (
              <div>
                <div className="seller-product-empty-header">
                  Your enquiries list is empty
                </div>
                <div className="seller-product-empty-message">
                  You can check for available list of livestocks and other
                  products in{" "}
                  <a className="seller-product-empty-buy-link" href="/buy/0">
                    here
                  </a>{" "}
                  or post a new enquiry in{" "}
                  <a
                    className="seller-product-empty-buy-link"
                    href="/add-enquiry"
                  >
                    here
                  </a>
                  .
                </div>
              </div>
            )}
          </div>
        ) : (
          <div>
            {enquiryRequests.map((enquiry, id) => (
              <div key={id}>
                <Card style={{ width: "80%" }}>
                  <Card.Header>
                    <Container>
                      <Row>
                        <Col>
                          <a
                            className="seller-product-item-name"
                            href={`/view-enquiry/${enquiry.id}`}
                          >
                            {enquiry.uniqueidentifier}
                          </a>
                        </Col>
                      </Row>
                    </Container>
                  </Card.Header>
                  <Card.Body>
                    <Card.Text>
                      <Container>
                        <Row xs={1} sm={2} md={4}>
                          <Col>
                            {/* <div className="seller-product-item-card-text"> */}
                            <a href={`/view-enquiry/${enquiry.id}`}>
                              <div className="seller-product-img-div">
                                <Image
                                  className="seller-product-img"
                                  src={
                                    enquiry.category_name == "Cattle"
                                      ? cattle_img
                                      : enquiry.category_name == "Goat"
                                      ? goat_img
                                      : enquiry.category_name == "Sheep"
                                      ? sheep_img
                                      : enquiry.category_name == "Chicken"
                                      ? chicken_img
                                      : enquiry.category_name == "Country Dogs"
                                      ? dog_img
                                      : default_img
                                  }
                                  fluid
                                />
                              </div>
                            </a>
                          </Col>
                          <Col>
                            <div className="seller-product-total-div">
                              <div>{enquiry.description}</div>
                              <div>#: {enquiry.uniqueidentifier}</div>
                              <div>
                                {enquiry.categorytype_name +
                                  " - " +
                                  enquiry.category_name}
                              </div>
                              <div>
                                {enquiry.breed_name
                                  ? enquiry.breed_name +
                                    " Breed || " +
                                    enquiry.age_name
                                    ? enquiry.age_name + " of age "
                                    : null
                                  : enquiry.age_name
                                  ? enquiry.age_name + " of age "
                                  : null}
                              </div>
                              <div>{enquiry.gendername}</div>
                              <div>
                                {enquiry.weight
                                  ? " || " + enquiry.weight + " kg"
                                  : null}
                              </div>
                              <div>
                                {enquiry.color_name
                                  ? enquiry.color_name + " colored"
                                  : null}
                              </div>
                            </div>
                          </Col>
                          <Col>
                            <div className="seller-product-total-div">
                              <div>
                                Required Price: <span> </span>&#8377;{" "}
                                {indianRupeeLocale.format(
                                  parseFloat(enquiry.acceptableprice)
                                )}
                              </div>
                              <div>
                                Required Count: <span> </span>{" "}
                                {enquiry.requiredcount}
                              </div>
                            </div>
                          </Col>
                          <Col>
                            <div>
                              <div className="order-delivery-address">
                                <div className="order-delivery-address-heading">
                                  Posted For
                                </div>
                                <div>Name: {enquiry.postedfor}</div>
                                <div>
                                  Place:{" "}
                                  {enquiry.requiredaroundcity +
                                    ", " +
                                    enquiry.requirearoundstate}
                                </div>

                                <div>Phone: {enquiry.contact}</div>
                              </div>
                            </div>
                          </Col>
                          <Col>
                            <div>
                              <div className="order-delivery-address">
                                <div className="order-delivery-address-heading">
                                  Posted By
                                </div>
                                <div>Name: {enquiry.posted_by_name}</div>
                                <div>
                                  On:{" "}
                                  <Moment format="DD-MMMM-yyyy">
                                    {enquiry.createdon}
                                  </Moment>
                                </div>
                              </div>
                            </div>
                          </Col>
                          <Col>
                            <div>
                              <div className="seller-product-order-status">
                                <div>Status:</div>
                                <div className="seller-product-order-status-detail">
                                  {enquiry.isverified
                                    ? "Verified"
                                    : "Not Verified / Pending Verification"}
                                </div>
                              </div>
                            </div>
                          </Col>
                        </Row>
                      </Container>
                    </Card.Text>
                  </Card.Body>
                </Card>
              </div>
            ))}
          </div>
        )}
      </Container>
    </div>
  );
}
