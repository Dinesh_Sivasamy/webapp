import React from "react";

import "./Banner.css";
import { MdVerifiedUser } from "react-icons/md";
import { FaUserMd } from "react-icons/fa";

export default function BannerDetail(props) {
  let indianRupeeLocale = Intl.NumberFormat("en-IN");

  // To set two dates to two variables
  var modifieddate = new Date(props.detail.modifieddate);
  var currentdate = new Date();

  // To calculate the time difference of two dates
  var Difference_In_Time = currentdate.getTime() - modifieddate.getTime();

  // To calculate the no. of days between two dates
  var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);

  return (
    <div>
      <figure
        className="cards__item__pic--wrap"
        data-category={props.detail.label}
      >
        {/* <figure className={props.detail.isp === "" ? '' : 'cards_item_pic--wrap--pregnant'} is-pregnant={props.detail.isp}> */}
        <figure
          className="cards_item_pic--wrap--new"
          is-new={
            Difference_In_Days < 10 ? props.detail.new : "Direct from Farm"
          }
        >
          <img
            src={props.detail.src}
            alt="Items images"
            className={
              props.detail.isSold || Difference_In_Days > 90
                ? "cards__item__img img-greyscale"
                : "cards__item__img"
            }
          />
          <figure
            className={
              props.detail.isSold || Difference_In_Days > 90
                ? "cards_item_pic--wrap-sold-box"
                : ""
            }
            is-sold={Difference_In_Days <= 90 ? "SOLD OUT" : "SALE EXPIRED"}
          >
            {props.detail.issellerverified ? (
              <figure className="cards_item_pic--wrap-verifiedseller">
                <div className="cards_item_pic--wrap-verifiedseller-box">
                  <MdVerifiedUser /> Verified Seller
                </div>
              </figure>
            ) : null}
          </figure>
          {/* </figure> */}
        </figure>
      </figure>
      <div className="cards__item__info">
        <div className="cards__item__name">{props.detail.name}</div>
        {props.detail.vetcheck && props.detail.vetcheck.length > 0 ? (
          <div className="cards__item__vetcheck">
            <FaUserMd />
            Vet-Checked
          </div>
        ) : null}
        <div className="cards__item__about">
          {props.detail.breed + " | " + props.detail.weight + " Kgs of weight"}
        </div>
        <div className="cards__item__gender_milkproduce">
          {props.detail.gender}
        </div>
        <div className="cards__item__gender_milkproduce">
          {props.detail.stock > 0 ? " | " + props.detail.stock + " Qty" : null}
        </div>
        <div className="cards__item__gender_milkproduce">
          {props.detail.milkproduce != 0
            ? " | Produces " + props.detail.milkproduce + " lts milk(avg)"
            : null}
        </div>
        <br />
        {/* {props.detail.discountprice && props.detail.discountprice > 0 ?
                    <div>
                        <div className="cards__item__originalprice">
                            &#8377; {indianRupeeLocale.format(props.detail.orignialprice)}
                        </div>
                        <div className="cards__item__discountprice">
                            &#8377; {indianRupeeLocale.format(props.detail.discountprice)}
                        </div>
                    </div>
                    :
                    <div>
                        <div className="cards__item__discountprice">
                            &#8377; {indianRupeeLocale.format(props.detail.orignialprice)}
                        </div>
                    </div>
                } */}
        <div className="cards__item__text">
          {props.detail.city + ", " + props.detail.state}
        </div>
      </div>
    </div>
  );
}
