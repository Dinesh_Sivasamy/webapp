import React, { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { useParams, Link } from "react-router-dom";
import Modal from "react-bootstrap/Modal";
import { Button } from "react-bootstrap";
import { FaWhatsappSquare } from "react-icons/fa";
import { BsPhone } from "react-icons/bs";
import { Label, UncontrolledTooltip } from "reactstrap";
import ProductImage from "./Sections/ProductImage";
import ProductInfo from "./Sections/ProductInfo";
import ProductVariantInfo from "./Sections/ProductVariantInfo";
import {
  IoMdCheckmarkCircleOutline,
  IoMdCloseCircleOutline,
} from "react-icons/io";

import Cookies from "js-cookie";
import CryptoJS from "crypto-js";
import "./Banner.css";
import configData from "../../../config.json";
import Banner from "./Banner";

export default function ProductDetail(props) {
  const { productId } = useParams();

  const [isdeliverable, setIsdeliverable] = useState(false);
  const [productDetail, setProductDetail] = useState([]);
  const [sellerName, setSellerName] = useState(null);
  const [sellerId, setSellerId] = useState(null);
  const [sellerPhone, setSellerPhone] = useState(null);
  const [sellerreview, setSellerreview] = useState([]);
  const [loggedUserSellerReview, setLoggedUserSellerReview] = useState({});
  const [isSellerViewing, setIsSellerViewing] = useState(false);
  const [isSellerVerified, setIsSellerVerified] = useState(false);
  const [isLoggedInUserSellerType, setIsLoggedInUserSellerType] =
    useState(false);

  const [pickupCity, setPickupCity] = useState(null);
  const [pickupState, setPickupState] = useState(null);
  const [productVariantDetail, setProductVariantDetail] = useState([]);
  const [productImage, setProductImage] = useState([]);
  const [showOrderSuccessMessage, setshowOrderSuccessMessage] = useState(false);
  const [showOrderErrorMessage, setshowOrderErrorMessage] = useState(false);

  const localstoragelist = JSON.parse(localStorage.getItem("productlist"));
  let userproductclicks = parseInt(Cookies.get("userproductclicks")) || 0;
  const [relatedproductlist, setRelatedproductlist] = useState([]);

  const [showContactSellerModal, setShowContactSellerModal] = useState(false);

  const handleContactSellerModalClose = () => setShowContactSellerModal(false);
  const handleContactSellerModalShow = () => setShowContactSellerModal(true);

  const showSaveOrderSuccess = () => {
    setshowOrderSuccessMessage(true);
  };

  const showSaveOrderError = () => {
    setshowOrderErrorMessage(true);
  };

  const checkifdeliverable = (pickpup_pincode) => {
    let deliverablePincodes;
    if (Cookies.get("deliveredpincodesdict") == null) {
      // fetching deliverable pincode details
      fetch("/api/v1/deliverable-pincodes", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error(response);
          }
        })
        .then(function (resp) {
          console.log(resp);
          //setDeliverablePincodes(resp)
          if (resp && resp.length > 0) {
            var deliveredpincodesdict = {};

            resp.forEach((pincodes) => {
              deliveredpincodesdict[pincodes.pincode] = pincodes.isdeliverable;
            });

            if (
              deliveredpincodesdict &&
              deliveredpincodesdict[pickpup_pincode]
            ) {
              setIsdeliverable(true);
            }

            Cookies.set(
              "deliveredpincodesdict",
              CryptoJS.AES.encrypt(
                JSON.stringify(deliveredpincodesdict),
                `${configData.CRYPTO_SECRET_KEY}`
              ).toString(),
              { path: "/" }
            );
          }
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      deliverablePincodes = JSON.parse(
        CryptoJS.AES.decrypt(
          Cookies.get("deliveredpincodesdict"),
          `${configData.CRYPTO_SECRET_KEY}`
        ).toString(CryptoJS.enc.Utf8)
      );
      if (deliverablePincodes && deliverablePincodes[pickpup_pincode]) {
        setIsdeliverable(true);
      }
    }
  };

  const updateproductviewcount = () => {
    fetch(`/api/v1/product-access-detail/id=${encodeURIComponent(productId)}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
    })
      .then((response) => {
        if (response.ok) {
          return response;
        } else {
          throw new Error(response);
        }
      })
      .then(function (resp) {
        console.log(resp);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    userproductclicks += 1;
    console.log(userproductclicks);

    Cookies.set("userproductclicks", userproductclicks, { path: "/" });
    console.log(Cookies.get("userproductclicks"));

    let product = null;
    if (localstoragelist) {
      product = localstoragelist.find((a) => a.id === productId);
    }

    console.log(product);

    if (product == null) {
      fetch(`/api/v1/product/id=${encodeURIComponent(productId)}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error(response);
          }
        })
        .then(function (resp) {
          console.log(resp);
          console.log(resp[0]);
          setProductDetail(resp[0]);
          setPickupCity(resp[0].pickup.address.city);
          setPickupState(resp[0].pickup.address.state);
          setSellerName(resp[0].seller.username);
          setSellerPhone(resp[0].seller.phone);
          setSellerId(resp[0].seller.id);
          setSellerreview(resp[0].seller.seller_reviews);
          setIsSellerVerified(resp[0].seller.is_verified);
          setProductVariantDetail(resp[0].product_variant[0]);
          setProductImage(resp[0].product_variant[0].product_images);
          checkifdeliverable(resp[0].pickup.address.pincode);

          if (
            Cookies.get("id") != null &&
            resp[0].seller.id ==
              CryptoJS.AES.decrypt(
                Cookies.get("id"),
                `${configData.CRYPTO_SECRET_KEY}`
              ).toString(CryptoJS.enc.Utf8)
          ) {
            setIsSellerViewing(true);
          } else if (Cookies.get("type")) {
            setIsLoggedInUserSellerType(
              CryptoJS.AES.decrypt(
                Cookies.get("type"),
                `${configData.CRYPTO_SECRET_KEY}`
              ).toString(CryptoJS.enc.Utf8) == 2
                ? true
                : false
            );
          }
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      setProductDetail(product);
      setPickupCity(product.pickup.address.city);
      setPickupState(product.pickup.address.state);
      setSellerName(product.seller.username);
      setSellerPhone(product.seller.phone);
      setSellerId(product.seller.id);
      setIsSellerVerified(product.seller.is_verified);
      setSellerreview(product.seller.seller_reviews);
      setProductVariantDetail(product.product_variant[0]);
      setProductImage(product.product_variant[0].product_images);
      checkifdeliverable(product.pickup.address.pincode);

      if (
        Cookies.get("id") != null &&
        product.seller.id ==
          CryptoJS.AES.decrypt(
            Cookies.get("id"),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(CryptoJS.enc.Utf8)
      ) {
        setIsSellerViewing(true);
      } else if (Cookies.get("type")) {
        setIsLoggedInUserSellerType(
          CryptoJS.AES.decrypt(
            Cookies.get("type"),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(CryptoJS.enc.Utf8) == 2
            ? true
            : false
        );
      }
    }

    if (
      !Cookies.get("id") ||
      (Cookies.get("id") &&
        product &&
        product.seller &&
        product.seller.id !=
          CryptoJS.AES.decrypt(
            Cookies.get("id"),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(CryptoJS.enc.Utf8))
    ) {
      updateproductviewcount();
    }

    if (localstoragelist) {
      setRelatedproductlist(
        localstoragelist.filter((prod) => prod.id !== productId)
      );
    }

    window.scrollTo(0, 0);
  }, []);

  const getproductimage = (productvariantimages) => {
    if (
      productvariantimages &&
      productvariantimages.length > 0 &&
      productvariantimages[0].image
    ) {
      return `${configData.IMG_FOLDER_PATH}${productvariantimages[0].image}`;
    } else {
      return `${configData.DEFAULT_PRODUCT_IMG}`;
    }
  };

  return (
    <div className="postPage" style={{ width: "100%" }}>
      <div className="product-detail-name">{productDetail.name}</div>
      <div>
        <div>
          <Modal
            size="lg"
            centered
            backdrop="static"
            show={showOrderSuccessMessage}
          >
            <Modal.Header>
              <Modal.Title id="Order-Item-save-response">
                <div className="modal-success modal-message-title">
                  Congrats! Item added
                </div>
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="modal-success modal-message-content">
                <IoMdCheckmarkCircleOutline /> The selected item has been
                successfully added to your cart.
              </div>
            </Modal.Body>
            <Modal.Footer>
              <div className="product-detail-btn">
                <Link to="/buy/0" className="btn btn-primary">
                  Continue Buy
                </Link>
                <span> </span>
                <Link to="/my-cart" className="btn btn-primary">
                  View Mycart
                </Link>
              </div>
            </Modal.Footer>
          </Modal>
        </div>

        <div>
          <Modal
            size="lg"
            centered
            backdrop="static"
            show={showOrderErrorMessage}
          >
            <Modal.Header>
              <Modal.Title id="Order-Item-save-response-error">
                <div className="modal-failure modal-message-title">
                  Failed to add item
                </div>
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="modal-failure modal-message-content">
                <IoMdCloseCircleOutline /> Sorry! There seems to be some issue
                while adding selected item to your cart, please{" "}
                <Link className="help-link" to="/support">
                  {" "}
                  contact our support team{" "}
                </Link>{" "}
                for more details.
              </div>
            </Modal.Body>
            <Modal.Footer>
              <div className="product-detail-btn">
                <Link to="/buy" className="btn btn-primary">
                  Continue Buy
                </Link>
              </div>
            </Modal.Footer>
          </Modal>
        </div>

        <div>
          <Modal
            show={showContactSellerModal}
            centered
            onHide={handleContactSellerModalClose}
          >
            <Modal.Header closeButton>
              <Modal.Title>Contact Seller</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div>
                <div className="product-detail-display-inline">
                  <label className="product-list-empty-heading">Warning:</label>
                  <label className="span-one">
                    Exercise caution with online money transfers; do not send
                    any payment in advance until you physically receive the
                    livestock.
                  </label>
                </div>
                <div className="product-detail-display-inline">
                  <span
                    className="span-phone-number"
                    href="#"
                    id="tooltip-sellerphone-number"
                  >
                    <label className="product-list-empty-heading product-detail-related-products">
                      Call:{" "}
                      <a href={"tel:" + sellerPhone}>
                        <BsPhone />
                      </a>
                    </label>
                    <label className="product-list-empty-heading product-detail-related-products">
                      WhatsApp:{" "}
                      <a
                        href={
                          "https://api.whatsapp.com/send/?text=Hi, I would like to get more details on https://farmzonn.com/product/" +
                          productDetail.id +
                          " listed in Farmzonn.com website.&phone=" +
                          sellerPhone
                        }
                        target="_blank"
                      >
                        <FaWhatsappSquare />
                      </a>
                    </label>
                  </span>
                  <UncontrolledTooltip
                    placement="right"
                    target="tooltip-sellerphone-number"
                  >
                    {props.sellerPhone}
                  </UncontrolledTooltip>
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button
                variant="secondary"
                onClick={handleContactSellerModalClose}
              >
                Close
              </Button>
            </Modal.Footer>
          </Modal>
        </div>
      </div>

      <br />
      <Container>
        {/* <Row gutter={[10, 10]} > */}
        <Row xs={1} md={2}>
          <Col>
            {/* <div className="product-detail-image"> */}
            <ProductImage detail={productImage} />
            {/* </div> */}
          </Col>
          <Col>
            <ProductInfo
              id={productDetail.id}
              userproductclicks={userproductclicks}
              loggedInUserId={
                Cookies.get("id")
                  ? CryptoJS.AES.decrypt(
                      Cookies.get("id"),
                      `${configData.CRYPTO_SECRET_KEY}`
                    ).toString(CryptoJS.enc.Utf8)
                  : null
              }
              loggedInUserName={
                Cookies.get("name")
                  ? CryptoJS.AES.decrypt(
                      Cookies.get("name"),
                      `${configData.CRYPTO_SECRET_KEY}`
                    ).toString(CryptoJS.enc.Utf8)
                  : null
              }
              loggedInUserType={
                Cookies.get("type")
                  ? CryptoJS.AES.decrypt(
                      Cookies.get("type"),
                      `${configData.CRYPTO_SECRET_KEY}`
                    ).toString(CryptoJS.enc.Utf8)
                  : null
              }
              description={productDetail.description}
              vetCheck={productDetail.veterinary_check}
              productidentifier={productDetail.uniqueidentifier}
              sellerName={sellerName}
              sellerPhone={sellerPhone}
              sellerId={sellerId}
              sellerreviews={sellerreview}
              issellerveified={isSellerVerified}
              location={pickupCity + ", " + pickupState}
              isdeliverable={isdeliverable}
              category={productDetail.category}
              weight={productVariantDetail.weight}
              originalPrice={productVariantDetail.original_price}
              discountPrice={productVariantDetail.offer_price}
              stock={productVariantDetail.stock}
              detail={productDetail}
              isSellerViewing={isSellerViewing}
              isSellerTypeLoggedIn={isLoggedInUserSellerType}
              showOrderSuccess={showSaveOrderSuccess}
              showOrderFail={showSaveOrderError}
              handleContactSellerModalShow={handleContactSellerModalShow}
            />
          </Col>
        </Row>
        <br />
        <Row gutter={[16, 16]}>
          <Col lg={12} xs={24}>
            <ProductVariantInfo
              loggedInUserId={
                Cookies.get("id")
                  ? CryptoJS.AES.decrypt(
                      Cookies.get("id"),
                      `${configData.CRYPTO_SECRET_KEY}`
                    ).toString(CryptoJS.enc.Utf8)
                  : null
              }
              loggedInUserName={
                Cookies.get("name")
                  ? CryptoJS.AES.decrypt(
                      Cookies.get("name"),
                      `${configData.CRYPTO_SECRET_KEY}`
                    ).toString(CryptoJS.enc.Utf8)
                  : null
              }
              loggedInUserType={
                Cookies.get("type")
                  ? CryptoJS.AES.decrypt(
                      Cookies.get("type"),
                      `${configData.CRYPTO_SECRET_KEY}`
                    ).toString(CryptoJS.enc.Utf8)
                  : null
              }
              productId={productDetail.id}
              breed={productDetail.breed}
              age={productVariantDetail.age}
              gender={productVariantDetail.gender}
              color={productVariantDetail.color}
              feedtype={productVariantDetail.feedtype}
              teethCount={productVariantDetail.adult_teeth_pair_count}
              milkProduce={productVariantDetail.milk_produce_per_Day}
              ispregnant={productVariantDetail.is_pregnant}
              offsprings={productVariantDetail.no_of_offsprings}
              vaccinationDone={productDetail.vaccinated}
              pickup={productDetail.pickup}
              vetCheckDetail={productDetail.veterinary_check}
              variant={productDetail.variant_type}
              // addToCart={addToCartHandler}
              detail={productDetail}
              modifiedon={
                productDetail.modified_date
                  ? productDetail.modified_date
                  : productDetail.created_date
              }
            />
          </Col>
        </Row>
      </Container>
      {relatedproductlist.length > 0 ? (
        <div
          className="product-detail-related-products"
          style={{ width: "90%" }}
        >
          <hr />
          <div className="product-detail-related-products-heading">
            Related Products
          </div>
          <div>
            <Container>
              <div className="cards__container">
                <div className="cards__wrapper">
                  <Row xs={1} md={5}>
                    {relatedproductlist.slice(0, 5).map((product, id) => (
                      <Col key={id}>
                        <div>
                          <Banner
                            key={id}
                            src={getproductimage(
                              product.product_variant[0].product_images
                            )}
                            text={product.description}
                            productId={product.id}
                            label={product.category}
                            isp={
                              product.product_variant[0].is_pregnant
                                ? "Pregnant"
                                : ""
                            }
                            new="newest"
                            path="/Product-Detail"
                            name={product.name}
                            issellerverified={product.seller.is_verified}
                            breed={product.breed}
                            gender={product.product_variant[0].gender}
                            milkproduce={
                              product.product_variant[0].milk_produce_per_Day
                            }
                            offsprings={
                              product.product_variant[0].no_of_offsprings
                            }
                            discountprice={
                              product.product_variant[0].offer_price
                            }
                            orignialprice={
                              product.product_variant[0].original_price
                            }
                            weight={product.product_variant[0].weight}
                            city={product.pickup.address.city}
                            state={product.pickup.address.state}
                            vetcheck={product.veterinary_check}
                            isSold={
                              product.product_variant[0].stock == 0 ||
                              product.is_sold
                            }
                            modifieddate={
                              product.modified_date
                                ? product.modified_date
                                : product.created_date
                            }
                            stock={product.product_variant[0].stock}
                          />
                        </div>
                        <br />
                      </Col>
                    ))}
                  </Row>
                </div>
              </div>
            </Container>
          </div>
        </div>
      ) : null}
    </div>
  );
}
