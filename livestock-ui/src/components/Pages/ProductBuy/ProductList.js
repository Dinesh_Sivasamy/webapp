import React, { useEffect, useState } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Cookies from "js-cookie";
import CryptoJS from "crypto-js";

import Banner from "./Banner";
import "./Banner.css";
import Loader from "../../Loader";
import defaultimg from "../Home/images/default_product_img.jpg";
import FilterComponent from "./Sections/Filter";
import Category from "../LandingPage/Category";

import configData from "../../../config.json";
import "./Sections/Filter.css";

import { Container } from "react-bootstrap";

import "../Home/CardItem.css";
import all from "../Home/images/all.png";
import cattle from "../Home/images/cattle.png";
import goat from "../Home/images/goat.png";
import sheep from "../Home/images/sheep.png";
import hen from "../Home/images/hen.png";
import dog from "../Home/images/dog.png";

export default function ProductList(props) {
  var halfADayExpiry = 0.5;
  const [isLoading, setLoading] = useState(true);
  const [allProductDetail, setAllProductDetail] = useState([]);
  const [productDetail, setProductDetail] = useState([]);

  const [selectedCategoryFilters, setSelectedCategoryFilters] = useState([]);
  const [selectedBreedFilters, setSelectedBreedFilters] = useState([]);

  const [filtersAvailable, setFiltersAvailable] = useState({});
  const [breedFiltersAvailable, setBreedFiltersAvailable] = useState([]);

  const categories = [
    { id: 1, name: "Cattle" },
    { id: 2, name: "Goat" },
    { id: 3, name: "Sheep" },
    { id: 4, name: "Chicken" },
    { id: 5, name: "Country Dogs" },
  ];

  const handleCategorySelect = (filter, removeOld = false) => {
    const isSelected = selectedCategoryFilters.includes(filter);
    const newSelection = removeOld
      ? [...filter]
      : isSelected
      ? selectedCategoryFilters.filter((current) => current !== filter)
      : [...selectedCategoryFilters, filter];
    setSelectedCategoryFilters(newSelection);
    console.log(newSelection);

    let respbreed = [];

    newSelection.map((selected) => {
      console.log(selected);
      console.log(selected.breeds);
      respbreed = [...respbreed, ...selected.breeds];
    });

    var breedlist = respbreed;
    console.log(breedlist);
    setBreedFiltersAvailable(breedlist);
  };

  const handleBreedSelect = (filter) => {
    const isSelected = selectedBreedFilters.includes(filter);
    const newSelection = isSelected
      ? selectedBreedFilters.filter((current) => current !== filter)
      : [...selectedBreedFilters, filter];
    setSelectedBreedFilters(newSelection);
  };

  const checkRequestParam = () => {
    if (props.type > 0 && props.type < 6) {
      console.log(props.type);
      handleCategoryChange(props.type);
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);

    const filtersFromStorage = JSON.parse(localStorage.getItem("filterlist"));
    console.log(filtersFromStorage);

    if (filtersFromStorage == null) {
      fetch(`/api/v1/category-type`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error(response);
          }
        })
        .then(function (resp) {
          console.log(resp);
          let respcategories = [];
          resp.map((response) => {
            console.log(response);
            console.log(response.categories);
            respcategories = [...respcategories, ...response.categories];
          });
          console.log(respcategories);
          setFiltersAvailable(respcategories);
          localStorage.setItem("filterlist", JSON.stringify(respcategories));
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      console.log(filtersFromStorage);
      setFiltersAvailable(filtersFromStorage);
    }

    console.log(Cookies.get("allproductlist"));

    if (Cookies.get("allproductlist") != null) {
      var productlist = JSON.parse(
        CryptoJS.AES.decrypt(
          Cookies.get("allproductlist"),
          `${configData.CRYPTO_SECRET_KEY}`
        ).toString(CryptoJS.enc.Utf8)
      );

      console.log("fetching product details from cookie");
      setAllProductDetail(productlist);
      setProductDetail(productlist);
    } else {
      console.log("fetching product details from api call");
      fetch(
        `/api/v1/product-detail-list/${encodeURIComponent(
          "top=0&category=0&page=buy"
        )}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "X-CSRFToken": Cookies.get("csrftoken"),
          },
        }
      )
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error(response);
          }
        })
        .then(function (resp) {
          console.log(resp);
          setAllProductDetail(resp);
          setProductDetail(resp);
          console.log("writing the response in cookie");
          Cookies.set("allproductlist", JSON.stringify(resp), {
            expires: halfADayExpiry,
          });
          setLoading(false);
        })
        .catch((err) => {
          console.log(err);
          setLoading(false);
        });
    }
  }, []);

  useEffect(() => {
    console.log("Inside use effect 2");
    if (
      allProductDetail != null &&
      allProductDetail.length > 0 &&
      filtersAvailable != undefined &&
      filtersAvailable.length > 0
    ) {
      console.log(
        "Filters available is not null, hence filtering out the result"
      );
      checkRequestParam();
    }
  }, [allProductDetail, filtersAvailable]);

  const handlefilterselect = () => {
    var productlist = allProductDetail;

    var filteredlist = [];
    var categoryFilter = selectedCategoryFilters;

    if (selectedBreedFilters.length > 0) {
      console.log(selectedBreedFilters);
      filteredlist = filteredlist.filter((product) =>
        selectedBreedFilters.some((filter) => filter.name == product.breed)
      );

      var categoriesWithoutBreedFilter = categories.filter(
        (breedfilter) =>
          !selectedBreedFilters.some((cat) => cat.category == breedfilter.id)
      );

      categoryFilter = categoryFilter.filter((cat) =>
        categoriesWithoutBreedFilter.find((breedfil) => breedfil.id == cat.id)
      );
    }

    console.log(categoryFilter);
    if (categoryFilter.length > 0) {
      let categoryfilterresult = productlist.filter((product) =>
        categoryFilter.some((filter) => filter.name == product.category)
      );

      filteredlist = [...filteredlist, ...categoryfilterresult];
    }

    console.log(filteredlist);
    setProductDetail(
      selectedCategoryFilters.length > 0 ? filteredlist : productlist
    );
  };

  const resetSelections = () => {
    setSelectedCategoryFilters([]);
    setSelectedBreedFilters([]);
    setBreedFiltersAvailable([]);
  };

  const handleSelectionClear = () => {
    resetSelections();
    setProductDetail(allProductDetail);
  };

  const handleBuyListCategorySelect = (filter) => {
    console.log("called buy list category select");
    console.log(filter);

    if (filter == null) {
      handleSelectionClear();
    } else {
      handleCategorySelect(filter, true);

      let productdetail = allProductDetail;
      var catfilter = filter[0];
      if (catfilter) {
        let categoryfilterresult = productdetail.filter(
          (product) => catfilter.name == product.category
        );

        console.log(categoryfilterresult);
        setProductDetail(categoryfilterresult);
      }
    }
  };

  const handleCategoryChange = (category) => {
    let allfilter = filtersAvailable;

    console.log(allfilter);
    let filteredcategory = allfilter.filter(
      (current) => current.id == category
    );
    console.log(category);
    console.log(filteredcategory);
    handleBuyListCategorySelect(filteredcategory);
  };

  const getproductimage = (productvariantimages) => {
    if (
      productvariantimages &&
      productvariantimages.length > 0 &&
      productvariantimages[0].image
    ) {
      return `${configData.IMG_FOLDER_PATH}${productvariantimages[0].image}`;
    } else {
      return `${defaultimg}`;
    }
  };

  return (
    <div>
      {isLoading ? (
        <Loader />
      ) : (
        <div>
          {/* <Container>
            <div className="buy-list-boxes">
              <Row>
                <div className="buy-list-carditem-box">
                  <Col>
                    <a onClick={() => handleBuyListCategorySelect(null)}>
                      <div className="buy-list-box">
                        <div className="icon">
                          <img
                            src={all}
                            alt="All"
                            width="40px"
                            height="35px"
                          ></img>
                        </div>
                      </div>
                    </a>
                  </Col>
                  <Col>
                    <a onClick={() => handleCategoryChange(1)}>
                      <div className="buy-list-box">
                        <div className="icon">
                          <img
                            src={cattle}
                            alt="cow"
                            width="25%"
                            height="35px"
                          ></img>
                        </div>
                      </div>
                    </a>
                  </Col>
                  <Col>
                    <a onClick={() => handleCategoryChange(2)}>
                      <div className="buy-list-box">
                        <div className="icon">
                          <img
                            src={goat}
                            alt="goat"
                            width="40px"
                            height="35px"
                          ></img>
                        </div>
                      </div>
                    </a>
                  </Col>
                  <Col>
                    <a onClick={() => handleCategoryChange(3)}>
                      <div className="buy-list-box">
                        <div className="icon">
                          <img
                            src={sheep}
                            alt="sheep"
                            width="40px"
                            height="35px"
                          ></img>
                        </div>
                      </div>
                    </a>
                  </Col>
                  <Col>
                    <a onClick={() => handleCategoryChange(4)}>
                      <div className="buy-list-box">
                        <div className="icon">
                          <img
                            src={hen}
                            alt="hen"
                            width="40px"
                            height="35px"
                          ></img>
                        </div>
                      </div>
                    </a>
                  </Col>
                  <Col>
                    <a onClick={() => handleCategoryChange(5)}>
                      <div className="buy-list-box">
                        <div className="icon">
                          <img
                            src={dog}
                            alt="dog"
                            width="40px"
                            height="35px"
                          ></img>
                        </div>
                      </div>
                    </a>
                  </Col>
                </div>
              </Row>
            </div>
          </Container> */}
          <div className="bgcolor-primary-very-mid-light-brown padding-bottom-1">
            <Container>
              <div className="hide-in-mobile padding-top-mobile-7 padding-top-1">
                <Row xs={3} md={6}>
                  <Col>
                    <button
                      className="pill-button bgcolor-bg-line-green"
                      onClick={() => handleCategoryChange(1)}
                    >
                      <div className="display-inline padding-top-3">
                        <img
                          className="icon-size-sm"
                          src="/images/cow.png"
                          alt="livestock"
                        />
                        <p className="header-xsm padding-top-7 color-white">
                          Cattle
                        </p>
                      </div>
                    </button>
                  </Col>
                  <Col>
                    <button
                      className="pill-button bgcolor-bg-line-yellow"
                      onClick={() => handleCategoryChange(2)}
                    >
                      <div className="display-inline padding-top-1">
                        <img
                          className="icon-size-sm"
                          src="/images/goat.png"
                          alt="livestock"
                        />
                        <p className="header-xsm padding-top-10 color-white">
                          Goats
                        </p>
                      </div>
                    </button>
                  </Col>
                  <Col>
                    <button
                      className="pill-button bgcolor-bg-line-blue"
                      onClick={() => handleCategoryChange(3)}
                    >
                      <div className="display-inline padding-top-7">
                        <img
                          className="icon-size-sm"
                          src="/images/categorygoat.png"
                          alt="livestock"
                        />
                        <p className="header-xsm padding-left-10 padding-top-5 color-white">
                          Sheep
                        </p>
                      </div>
                    </button>
                  </Col>
                  <Col>
                    <button
                      className="pill-button bgcolor-bg-line-purple"
                      onClick={() => handleCategoryChange(4)}
                    >
                      <div className="display-inline padding-top-1">
                        <img
                          className="icon-size-sm"
                          src="/images/categorychicken.png"
                          alt="livestock"
                        />
                        <p className="header-xsm padding-left-10 padding-top-10 color-white">
                          Hens
                        </p>
                      </div>
                    </button>
                  </Col>
                  <Col>
                    <button
                      className="pill-button bgcolor-bg-line-brown"
                      onClick={() => handleCategoryChange(5)}
                    >
                      <div className="display-inline padding-top-1">
                        <img
                          className="icon-size-sm"
                          src="/images/categorydog.png"
                          alt="livestock"
                        />
                        <p className="header-xsm padding-top-10 color-white">
                          Dogs
                        </p>
                      </div>
                    </button>
                  </Col>
                  <Col>
                    <button
                      className="pill-button bgcolor-primary-brown"
                      onClick={() => handleBuyListCategorySelect(null)}
                    >
                      <div className="display-inline padding-top-5">
                        <img
                          className="icon-size-sm"
                          src="/images/categoryall.png"
                          alt="livestock"
                        />
                        <p className="header-xsm padding-left-5 padding-top-7 color-white">
                          Animals
                        </p>
                      </div>
                    </button>
                  </Col>
                </Row>
              </div>
              <div className="hide-in-desktop horizontal-scroll-mobile-only padding-top-mobile-7 padding-top-1">
                <Row xs={5} md={6}>
                  <Col>
                    <button
                      className="pill-button bgcolor-bg-line-green"
                      onClick={() => handleCategoryChange(1)}
                    >
                      <div className="display-inline padding-top-3">
                        <img
                          className="icon-size-sm"
                          src="/images/cow.png"
                          alt="livestock"
                        />
                        <p className="header-xsm padding-top-7 color-white">
                          Cattle
                        </p>
                      </div>
                    </button>
                  </Col>
                  <Col></Col>
                  <Col>
                    <button
                      className="pill-button bgcolor-bg-line-yellow"
                      onClick={() => handleCategoryChange(2)}
                    >
                      <div className="display-inline padding-top-1">
                        <img
                          className="icon-size-sm"
                          src="/images/goat.png"
                          alt="livestock"
                        />
                        <p className="header-xsm padding-top-10 color-white">
                          Goats
                        </p>
                      </div>
                    </button>
                  </Col>
                  <Col></Col>
                  <Col>
                    <button
                      className="pill-button bgcolor-bg-line-blue"
                      onClick={() => handleCategoryChange(3)}
                    >
                      <div className="display-inline padding-top-7">
                        <img
                          className="icon-size-sm"
                          src="/images/categorygoat.png"
                          alt="livestock"
                        />
                        <p className="header-xsm padding-left-10 padding-top-5 color-white">
                          Sheep
                        </p>
                      </div>
                    </button>
                  </Col>
                  <Col>
                    <button
                      className="pill-button bgcolor-bg-line-purple"
                      onClick={() => handleCategoryChange(4)}
                    >
                      <div className="display-inline padding-top-1">
                        <img
                          className="icon-size-sm"
                          src="/images/categorychicken.png"
                          alt="livestock"
                        />
                        <p className="header-xsm padding-left-10 padding-top-10 color-white">
                          Hens
                        </p>
                      </div>
                    </button>
                  </Col>
                  <Col></Col>
                  <Col>
                    <button
                      className="pill-button bgcolor-bg-line-brown"
                      onClick={() => handleCategoryChange(5)}
                    >
                      <div className="display-inline padding-top-1">
                        <img
                          className="icon-size-sm"
                          src="/images/categorydog.png"
                          alt="livestock"
                        />
                        <p className="header-xsm padding-top-10 color-white">
                          Dogs
                        </p>
                      </div>
                    </button>
                  </Col>
                  <Col></Col>
                  <Col>
                    <button
                      className="pill-button bgcolor-primary-brown"
                      onClick={() => handleBuyListCategorySelect(null)}
                    >
                      <div className="display-inline padding-top-5">
                        <img
                          className="icon-size-sm"
                          src="/images/categoryall.png"
                          alt="livestock"
                        />
                        <p className="header-xsm padding-left-5 padding-top-7 color-white">
                          Animals
                        </p>
                      </div>
                    </button>
                  </Col>
                </Row>
              </div>
            </Container>
          </div>
          <hr />
          <Container>
            <Row md={5}>
              <Col></Col>
              <Col></Col>
              <Col></Col>
              <Col></Col>
              <Col>
                <FilterComponent
                  label="Filter"
                  onSelect={handlefilterselect}
                  onClear={handleSelectionClear}
                  className="padding-top-3"
                >
                  <label className="product-detail-related-products-heading">
                    By Category
                  </label>
                  <div className="Filters-list">
                    {filtersAvailable != undefined &&
                      filtersAvailable.map((filter, index) => {
                        const isSelected =
                          selectedCategoryFilters.includes(filter);
                        return (
                          <label key={index} className="filter-label">
                            <input
                              type="checkbox"
                              checked={isSelected}
                              onChange={() => handleCategorySelect(filter)}
                            />
                            {filter.name}
                          </label>
                        );
                      })}
                  </div>
                  <div>
                    {breedFiltersAvailable.length > 0 ? (
                      <div className="Filters-list">
                        <label className="product-detail-related-products-heading">
                          By Breed
                        </label>
                        <br />
                        {breedFiltersAvailable.map((filter, index) => {
                          const isSelected =
                            selectedBreedFilters.includes(filter);
                          return (
                            <label key={index} className="filter-label">
                              <input
                                type="checkbox"
                                checked={isSelected}
                                onChange={() => handleBreedSelect(filter)}
                              />
                              {filter.name}
                            </label>
                          );
                        })}
                      </div>
                    ) : null}
                  </div>
                </FilterComponent>
              </Col>
            </Row>
          </Container>
          <hr />
          {productDetail && productDetail.length > 0 ? (
            <Container>
              <div className="cards__container">
                <div className="cards__wrapper">
                  <Row xs={1} sm={1} md={4}>
                    {productDetail.map((product, id) => (
                      <Col key={id}>
                        <div>
                          <Banner
                            key={id}
                            productlist={productDetail}
                            src={getproductimage(
                              product.product_variant[0].product_images
                            )}
                            text={product.description}
                            productId={product.id}
                            label={product.category}
                            isp={
                              product.product_variant[0].is_pregnant
                                ? "Pregnant"
                                : ""
                            }
                            new="newest"
                            path="/Product-Detail"
                            name={product.name}
                            issellerverified={product.seller.is_verified}
                            breed={product.breed}
                            gender={product.product_variant[0].gender}
                            milkproduce={
                              product.product_variant[0].milk_produce_per_Day
                            }
                            offsprings={
                              product.product_variant[0].no_of_offsprings
                            }
                            discountprice={
                              product.product_variant[0].offer_price
                            }
                            orignialprice={
                              product.product_variant[0].original_price
                            }
                            weight={product.product_variant[0].weight}
                            city={product.pickup.address.city}
                            state={product.pickup.address.state}
                            vetcheck={product.veterinary_check}
                            isSold={
                              product.product_variant[0].stock == 0 ||
                              product.is_sold
                            }
                            modifieddate={
                              product.modified_date
                                ? product.modified_date
                                : product.created_date
                            }
                            stock={product.product_variant[0].stock}
                          />
                        </div>
                      </Col>
                    ))}
                  </Row>
                </div>
              </div>
            </Container>
          ) : (
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
              }}
              className="product-list-empty"
            >
              <div className="product-list-empty-heading">
                Sorry! there are no products in the list that matches your
                search condition as of now, please try later again or try with
                other conditions.
              </div>
            </div>
          )}
        </div>
      )}
    </div>
  );
}
