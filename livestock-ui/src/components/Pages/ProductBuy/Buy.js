import { React, useState } from "react";
import { useParams } from "react-router-dom";

import Cookies from "js-cookie";
import CryptoJS from "crypto-js";
import configData from "../../../config.json";

import ProductList from "./ProductList";
import SignInModal from "../User/SignInModal";

export default function Buy() {
  let { typeId } = useParams();

  const loggedInUserId = Cookies.get("id")
    ? CryptoJS.AES.decrypt(
        Cookies.get("id"),
        `${configData.CRYPTO_SECRET_KEY}`
      ).toString(CryptoJS.enc.Utf8)
    : null;

  const userproductclicks = parseInt(Cookies.get("userproductclicks")) || 0;
  console.log(userproductclicks);
  const [show, setShow] = useState(
    !loggedInUserId && userproductclicks > 5 ? true : false
  );

  const handleClose = () => {
    setShow(false);
    window.location.reload();
  };

  return (
    <div>
      <ProductList type={typeId} />
      <div>
        <SignInModal
          show={show}
          header="Login to View Product List"
          hideclosebutton={true}
          onHide={handleClose}
          setModalClose={handleClose}
        />
      </div>
    </div>
  );
}
