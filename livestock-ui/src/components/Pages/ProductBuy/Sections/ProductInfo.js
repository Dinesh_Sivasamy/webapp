import React, { useEffect, useState } from "react";
import Table from "react-bootstrap/Table";
import { FaUserMd, FaWhatsappSquare } from "react-icons/fa";
import { BsPhone } from "react-icons/bs";
import { MdEventAvailable, MdVerifiedUser } from "react-icons/md";
import { CgCloseO } from "react-icons/cg";
import { Label, UncontrolledTooltip } from "reactstrap";
import { Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import NumericInput from "react-bootstrap-input-spinner";
import Cookies from "js-cookie";
import { FaStar } from "react-icons/fa";
import CryptoJS from "crypto-js";

import "../Banner.css";
import SignInModal from "../../User/SignInModal";
import configData from "../../../../config.json";
import ReactGA from "react-ga";

export default function ProductInfo(props) {
  let indianRupeeLocale = Intl.NumberFormat("en-IN");
  const history = useHistory();

  const [Quantity, setQuantity] = useState(1);
  const [show, setShow] = useState(
    !props.loggedInUserId && props.userproductclicks > 5 ? true : false
  );
  const handleClose = () => {
    setShow(false);
    window.location.reload();
  };

  // update product details
  const [updatedStock, setUpdatedStock] = useState();
  // price fields
  const [originalPrice, setOriginalPrice] = useState();
  const [offerPrice, setOfferPrice] = useState();
  const [recommendedproductprice, setRecommendedproductprice] = useState();
  const [suggestedOfferPercent, setSuggestedOfferPercent] = useState(0);
  const [siteprofitshare, setSiteprofitshare] = useState(0);
  const [recommendOriginalPrice, setRecommendOriginalPrice] = useState(0);
  const [recommendedOfferPrice, setRecommendedOfferPrice] = useState(0);
  const [sellerProfit, setSellerProfit] = useState(0);
  const [brokerageamount, setBrokerageamount] = useState(0);
  const [
    offerPriceHigherThanOriginalError,
    setOfferPriceHigherThanOriginalError,
  ] = useState(false);

  const [isProductSoldOutside, setIsProductSoldOutside] = useState(false);

  const handleShow = () => setShow(true);

  const [SaveOrderResponseData, setSaveOrderResponseData] = useState(null);
  const [SaveOrderError, setSaveOrderError] = useState(null);

  const [weight, setWeight] = useState();

  const [loggedInUserSellerreview, setLoggedInUserSellerreview] = useState({});
  const [sellerOverallRating, setSellerOverallRating] = useState(-1);

  useEffect(() => {
    ReactGA.event({
      category: "Product View",
      action: props.category + " :: " + props.id,
      nonInteraction: true, // optional, true/false
      transport: "xhr", // optional, beacon/xhr/image
    });

    let sellerreviewRating = -1;

    if (props.sellerreviews && props.sellerreviews.length > 0) {
      sellerreviewRating += 1; //if any review is available for the seller

      props.sellerreviews.forEach((review, index) => {
        if (props.loggedInUserId == review.postedby) {
          setLoggedInUserSellerreview(review);
        }
        sellerreviewRating += parseInt(review.reviewrating);
      });
    }

    const reviewlen =
      props.sellerreviews && props.sellerreviews.length > 0
        ? props.sellerreviews.length
        : 1;
    setSellerOverallRating(parseInt(sellerreviewRating) / reviewlen);
    console.log(parseInt(sellerreviewRating));
    console.log(props.sellerreviews ? props.sellerreviews.length : 0);
  }, [props.sellerreviews]);

  const [sellerRating, setsellerRating] = useState(0);
  const [sellerRatingHover, setsellerRatingHover] = useState(0);
  const [sellerReviewComments, setsellerReviewComments] = useState(null);
  const [sellerReviewSaveFailure, setsellerReviewSaveFailure] = useState(null);
  const [sellerReviewSaveSuccess, setsellerReviewSaveSuccess] = useState(null);

  const handleQuantityChange = (obj) => {
    setQuantity(obj);
  };

  const saveItemIntoBuyerCart = (requestJson) => {
    fetch(
      `/api/v1/buyercart/buyer=${encodeURIComponent(props.loggedInUserId)}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
        body: JSON.stringify(requestJson),
      }
    )
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then((result) => {
        setSaveOrderResponseData(result.rows);
        props.showOrderSuccess();
      })
      .catch((err) => {
        setSaveOrderError("Failed to add item to cart");
        props.showOrderFail();
      });
  };

  const addToCarthandler = (evt) => {
    evt.preventDefault();

    const jsonData = {
      buyer: props.loggedInUserId,
      product: props.id,
      items_count: Quantity,
      total_payable: 0,
    };

    saveItemIntoBuyerCart(jsonData);
  };

  const handleUpdateSubmit = (e) => {
    e.preventDefault();

    let jsonData = {};

    if (originalPrice > 0) {
      jsonData = {
        originalprice: originalPrice,
        offerprice: offerPrice,
        weight: weight,
      };
    }

    fetch(
      `/api/v1/update-product/id=${encodeURIComponent(
        props.id
      )}&quantity=${encodeURIComponent(
        updatedStock
      )}&sold=false&isfarmzonnsale=false`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
        body: JSON.stringify(jsonData),
      }
    )
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then(function (resp) {
        console.log(resp);
        history.push(`/seller-product/`);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleStockChange = (e) => {
    setUpdatedStock(e);
  };

  // handle change event of the weight field
  const handleWeightChange = (obj) => {
    setWeight(obj);
  };

  const handleUpdateclear = (e) => {
    setUpdatedStock(props.stock);
    setWeight(props.weight);
    setOriginalPrice(0);
    setOfferPrice(0);
    setRecommendOriginalPrice(0);
    setRecommendedOfferPrice(0);
    setOfferPriceHigherThanOriginalError(false);
  };

  const setpricedetails = () => {
    if (!recommendedproductprice) {
      // set stock value once
      setUpdatedStock(updatedStock ? updatedStock : props.stock);
      setWeight(weight ? weight : props.weight);

      fetch("/api/v1/recommendprice", {
        // fetching product price recommendations
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error(response);
          }
        })
        .then(function (resp) {
          console.log(resp);
          setRecommendedproductprice(resp);
          setSuggestedOfferPercent(resp.recommendedOfferpercent);
          setSiteprofitshare(resp.profitmargin);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  const handleOriginalPriceChange = (e) => {
    // handle change event of the Original price field
    const amount = e.target.value;

    if (!amount || amount.match(/^\d{1,}(\.\d{0,4})?$/)) {
      setOriginalPrice(amount);
    }

    setpricedetails();
  };

  const calculaterecommendedprice = () => {
    if (recommendedproductprice) {
      var original = parseFloat(originalPrice);
      setSellerProfit(original);
      var tempbrokerage = Math.ceil(
        original * (parseFloat(siteprofitshare) / 100)
      );
      setBrokerageamount(tempbrokerage);

      var originalwithbrokerage = original + tempbrokerage;
      var temporiginal = Math.ceil(
        originalwithbrokerage / (1 - parseFloat(suggestedOfferPercent) / 100)
      );
      setRecommendOriginalPrice(temporiginal);

      var tempoffer = Math.ceil(
        temporiginal - temporiginal * (parseFloat(suggestedOfferPercent) / 100)
      );
      setRecommendedOfferPrice(tempoffer);
    } else {
      setRecommendOriginalPrice(0);
      setRecommendedOfferPrice(0);
      setBrokerageamount(0);
    }
  };

  const applyrecommendedprice = () => {
    setOriginalPrice(recommendOriginalPrice);
    setOfferPrice(recommendedOfferPrice);
  };

  const handleOfferPriceChange = (e) => {
    const amount = e.target.value;

    if (!amount || amount.match(/^\d{1,}(\.\d{0,4})?$/)) {
      if (parseFloat(amount) >= parseFloat(originalPrice)) {
        setOfferPriceHigherThanOriginalError(true);
      } else {
        setOfferPrice(parseFloat(amount));
        setOfferPriceHigherThanOriginalError(false);
      }
    }
  };

  const handleContactButtonClick = (e) => {
    ReactGA.event({
      category: "Contact Seller",
      action: props.category + " :: " + props.id,
      nonInteraction: true, // optional, true/false
      transport: "xhr", // optional, beacon/xhr/image
    });

    props.handleContactSellerModalShow();
  };

  const handleSaveReview = () => {
    if (sellerRating == 0 && !sellerReviewComments) {
      setsellerReviewSaveFailure(
        "Please add Review rating or Review comments if you want to give 0 rating for this seller before save."
      );
      return;
    } else {
      setsellerReviewSaveFailure(null);
    }

    var requestJson = {
      reviewrating: sellerRating,
      reviewcomment: sellerReviewComments,
      seller: props.sellerId,
      postedby: props.loggedInUserId,
      createon: new Date().toLocaleDateString(),
    };

    fetch(`/api/v1/sellerrating`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
      body: JSON.stringify(requestJson),
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then((result) => {
        setsellerReviewSaveSuccess(true);
        localStorage.removeItem("productlist");
      })
      .catch((err) => {
        setsellerReviewSaveFailure(
          "Failed to update Seller review, please try after sometime."
        );
        console.log(err);
      });
  };

  var modifieddate = new Date(props.detail.modified_date); // To set two dates to two variables
  var currentdate = new Date();

  var Difference_In_Time = currentdate.getTime() - modifieddate.getTime(); // To calculate the time difference of two dates

  var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24); // To calculate the no. of days between two dates

  return (
    <div>
      <div className="product-detail-display-inline">
        <div className="product-detail-title">Product Info</div>

        {props.vetCheck && props.vetCheck.length > 0 ? (
          <div className="product-detail-vetcheck">
            <FaUserMd /> Vet Check Done
          </div>
        ) : null}

        <div className="product-detail-stock-info">
          {!props.detail.is_sold && props.stock > 0 ? (
            <div className="product-detail-instock">
              <MdEventAvailable /> In Stock
            </div>
          ) : (
            <div className="product-detail-outofstock">
              <CgCloseO /> Item Sold out
            </div>
          )}
        </div>
      </div>
      <Table striped bordered hover size="sm">
        <tbody>
          <tr>
            <td>Description</td>
            <td>{props.description}</td>
          </tr>
          <tr>
            <td>Product #</td>
            <td>{props.productidentifier}</td>
          </tr>
          <tr>
            <td>Category</td>
            <td>{props.category}</td>
          </tr>
          <tr>
            <td>Weight</td>
            <td>{props.weight ? props.weight + " Kgs" : "-"}</td>
          </tr>
          <tr>
            <td>Price</td>
            <td>
              {props.discountPrice && props.discountPrice > 0 ? (
                <div className="product-detail-display-inline">
                  <div className="detail_discountprice">
                    &#8377; {indianRupeeLocale.format(props.discountPrice)}
                  </div>
                  <div className="detail_originalprice">
                    &#8377; {indianRupeeLocale.format(props.originalPrice)}
                  </div>
                  <div className="detail_offerpercent">
                    (-{" "}
                    {parseFloat(
                      (props.originalPrice - props.discountPrice) /
                        props.originalPrice
                    ).toFixed(2) * 100}{" "}
                    %)
                  </div>
                </div>
              ) : (
                <div className="detail_discountprice">
                  &#8377; {indianRupeeLocale.format(props.originalPrice)}
                </div>
              )}
            </td>
          </tr>
          <tr>
            <td>Sold By</td>
            <td>
              <div className="product-detail-display-inline">
                <div>{props.sellerName}</div>
                {props.issellerveified ? (
                  <div className="product-detail-seller-verified">
                    <MdVerifiedUser /> Verified Seller
                  </div>
                ) : null}
              </div>
              <div>
                {[...Array(5)].map((item, index) => {
                  const ratingIndex = index + 1;
                  return (
                    <label>
                      <FaStar
                        color={
                          ratingIndex <= sellerOverallRating
                            ? "#BF953F"
                            : "rgb(192,192,192)"
                        }
                      />
                    </label>
                  );
                })}{" "}
                {sellerOverallRating < 0 ? (
                  <Label> {" No rating has been shared yet!"}</Label>
                ) : null}
              </div>
            </td>
          </tr>
          <tr>
            <td>Location</td>
            <td>{props.location}</td>
          </tr>
          <tr>
            <td>Contact Details</td>
            <td>
              <div className="product-detail-display-inline">
                {props.loggedInUserId ? (
                  Difference_In_Days < 100 ? (
                    <div>
                      <span> </span>
                      <Button
                        variant="success"
                        onClick={() => handleContactButtonClick()}
                      >
                        Connect with Seller
                      </Button>
                    </div>
                  ) : (
                    <div className="product-detail-outofstock">Sale Closed</div>
                  )
                ) : (
                  <div className="product-detail-display-inline">
                    <span
                      className="span-one"
                      href="#"
                      id="tooltip-sellerphone"
                    >
                      <div className="product-detail-seller-phone">
                        <BsPhone />
                      </div>
                      <div className="product-detail-seller-phone-number">
                        **********
                      </div>
                    </span>
                    <UncontrolledTooltip
                      placement="top"
                      target="tooltip-sellerphone"
                    >
                      Login to view Seller's phone number
                    </UncontrolledTooltip>
                  </div>
                )}
              </div>
            </td>
          </tr>
        </tbody>
      </Table>
      {props.loggedInUserId ? (
        <div>
          <br />{" "}
          {sellerReviewSaveSuccess ||
          (loggedInUserSellerreview &&
            Object.keys(loggedInUserSellerreview).length > 0) ? (
            <div>
              Your seller review has been saved Successfully. Thanks for Sharing
              your review!
            </div>
          ) : (
            <div>
              <div>
                <Label>Please share your rating for the Seller:</Label>
              </div>
              <div className="product-detail-display-inline">
                {[...Array(5)].map((item, index) => {
                  const currentIndex = (index += 1);
                  return (
                    <div>
                      <Button
                        variant="light"
                        key={index}
                        onClick={() => currentIndex}
                        onMouseEnter={() => setsellerRatingHover(currentIndex)}
                        onMouseLeave={() => setsellerRatingHover(sellerRating)}
                      >
                        <label>
                          <FaStar
                            color={
                              currentIndex <= sellerRatingHover
                                ? "#BF953F"
                                : "rgb(192,192,192)"
                            }
                          />
                        </label>
                      </Button>
                    </div>
                  );
                })}
              </div>
              <div className="product-detail-display-inline">
                <Label>Review Comments:</Label>
                <div className="sell-description-field">
                  <textarea
                    className="form-control"
                    placeholder="Add comments on your interaction with seller"
                    value={sellerReviewComments}
                    onChange={(e) => setsellerReviewComments(e.target.value)}
                  />
                </div>
              </div>
              {sellerReviewSaveFailure ? (
                <div>
                  <span className="error">*{sellerReviewSaveFailure}</span>
                  <br />
                </div>
              ) : null}
              <div>
                <Button onClick={handleSaveReview}>Save Seller Review</Button>
              </div>
            </div>
          )}
          <br />
        </div>
      ) : null}
      <div className="product-detail-display-inline">
        <div className="product-detail-freedelivery">
          {props.stock > 0 && props.isdeliverable && !props.isSellerViewing
            ? "*Free Delivery available for this product"
            : null}
        </div>
      </div>
      <div className="product-detail-min-stock">
        {!props.detail.is_sold &&
        props.stock > 0 &&
        Difference_In_Days < 100 &&
        props.stock < 5 &&
        !props.isSellerViewing ? (
          <Label>Hurry! Only {props.stock} left</Label>
        ) : (
          <div className="product-detail-available-stock">
            {!props.detail.is_sold &&
            props.stock - Quantity < 10 &&
            Difference_In_Days < 100 &&
            !props.isSellerViewing ? (
              <div>**Currently Instock: Only {props.stock} item(s)</div>
            ) : null}
          </div>
        )}
      </div>
      <div className="product-detail-display-inline">
        {!props.detail.is_sold &&
        props.stock > 0 &&
        Difference_In_Days < 100 &&
        !props.isSellerViewing &&
        !props.isSellerTypeLoggedIn &&
        props.isdeliverable ? (
          <div className="product-detail-quantity-value">
            <NumericInput
              type={"real"}
              variant={"secondary"}
              size="sm"
              step={1}
              min={1}
              max={props.stock}
              value={Quantity}
              onChange={handleQuantityChange}
            />
          </div>
        ) : null}
        <div className="btn-addtocart">
          {props.loggedInUserId ? (
            <div>
              {!props.detail.is_sold &&
              props.stock > 0 &&
              Difference_In_Days < 100 &&
              props.isdeliverable &&
              !props.isSellerViewing &&
              !props.isSellerTypeLoggedIn ? (
                <div>
                  <Button className="btn btn-danger" onClick={addToCarthandler}>
                    Add to cart
                  </Button>
                </div>
              ) : (
                <div>
                  {!props.detail.is_sold &&
                  props.isSellerViewing &&
                  props.stock > 0 ? (
                    <div>
                      <div>
                        <div className="product-detail-quantity-value">
                          <Label>In stock: </Label>
                          <NumericInput
                            type={"real"}
                            variant={"secondary"}
                            size="sm"
                            step={1}
                            min={1}
                            value={props.stock}
                            onChange={handleStockChange}
                          />
                        </div>
                      </div>
                      <div className="product-detail-display-inline">
                        <Label className="product-detail-label">
                          Avg. Weight (in Kg/item): <br />
                        </Label>

                        <div>
                          <NumericInput
                            type={"decimal"}
                            variant={"secondary"}
                            size="sm"
                            step={0.5}
                            size="5"
                            precision={2}
                            min={0}
                            value={props.weight}
                            onChange={handleWeightChange}
                          />
                        </div>
                      </div>
                      <div className="product-update-price">
                        <div className="product-detail-display-inline">
                          <div>
                            <Label className="product-detail-label">
                              Product Original Price(in &#8377;/item){" "}
                            </Label>
                            <br />
                            <div className="product-detail-display-inline">
                              <div className="product-detail-numericfield">
                                <input
                                  type="text"
                                  className="form-control"
                                  value={originalPrice}
                                  onChange={handleOriginalPriceChange}
                                />
                                {originalPrice && (weight || props.weight) ? (
                                  <span>
                                    <Label>
                                      &#8377;{" "}
                                      {originalPrice /
                                        (weight ? weight : props.weight)}{" "}
                                      / Kg{" "}
                                    </Label>
                                  </span>
                                ) : null}
                              </div>
                              <div className="product-detail-calculate-recommend-price">
                                <Button
                                  variant="info"
                                  onClick={calculaterecommendedprice}
                                >
                                  Calculate recommended price
                                </Button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <br />
                        {originalPrice > 0 ? (
                          <div>
                            <div>
                              <Label className="product-detail-label">
                                Product Offer Price(in &#8377;/item){" "}
                              </Label>
                              <br />
                              <div className="product-detail-display-inline">
                                <div className="product-detail-numericfield">
                                  <input
                                    type="text"
                                    className="form-control"
                                    value={offerPrice}
                                    onChange={handleOfferPriceChange}
                                  />
                                  {offerPrice && (weight || props.weight) ? (
                                    <span>
                                      <Label>
                                        &#8377;{" "}
                                        {offerPrice /
                                          (weight ? weight : props.weight)}{" "}
                                        / Kg{" "}
                                      </Label>
                                    </span>
                                  ) : null}
                                </div>
                                {/* <div className="product-detail-calculate-recommend-price">
                                                                    {recommendOriginalPrice > 0 ?
                                                                        <Button variant="info" onClick={applyrecommendedprice} >Apply recommended price
                                                                        </Button>
                                                                        :
                                                                        null
                                                                    }
                                                                </div> */}
                              </div>
                            </div>
                            <div>
                              {recommendOriginalPrice > 0 ? (
                                <div>
                                  <div className="product-detail-recommended-price">
                                    Recommended Original price: &#8377;{" "}
                                    {indianRupeeLocale.format(
                                      recommendOriginalPrice
                                    )}
                                  </div>

                                  <div className="product-detail-recommended-price">
                                    Recommended Offer price: &#8377;{" "}
                                    {indianRupeeLocale.format(
                                      recommendedOfferPrice
                                    )}
                                  </div>
                                  <div className="product-detail-recommended-price">
                                    Amount you get after sale: &#8377;{" "}
                                    {indianRupeeLocale.format(sellerProfit)}{" "}
                                    *subject to change
                                  </div>
                                </div>
                              ) : null}
                              {offerPriceHigherThanOriginalError ? (
                                <div className="error">
                                  *Offer price cannot be greater than Original
                                  price
                                </div>
                              ) : null}
                            </div>
                          </div>
                        ) : null}
                      </div>
                      <div className="product-detail-verify-success">
                        **The new price will not be applied on ordered products.
                      </div>
                      <div>
                        <Button
                          className="btn btn-danger"
                          onClick={handleUpdateSubmit}
                        >
                          Update
                        </Button>
                        <span> </span>
                        <Button
                          className="btn btn-secondary"
                          onClick={handleUpdateclear}
                        >
                          clear
                        </Button>
                      </div>
                      <hr></hr>
                      <div>
                        *After successful update, you will be redirected to
                        product listing page!*
                      </div>
                      <hr></hr>
                    </div>
                  ) : null}
                </div>
              )}
            </div>
          ) : (
            <Button onClick={handleShow}>Login to add to cart</Button>
          )}
        </div>
        <div>
          <SignInModal
            show={show}
            header="Login to View Product detail"
            setModalClose={handleClose}
            onHide={handleClose}
          />
        </div>
      </div>
      <hr></hr>
      <div>
        {props.loggedInUserId &&
        !props.detail.is_sold &&
        !props.isdeliverable &&
        !props.isSellerViewing &&
        !props.isSellerTypeLoggedIn ? (
          <Label className="warn">
            Sorry, this Product cannot be delivered, our delivery service is not
            available in this area now. But you can always contact the seller
            through phone.
          </Label>
        ) : null}
      </div>
    </div>
  );
}
