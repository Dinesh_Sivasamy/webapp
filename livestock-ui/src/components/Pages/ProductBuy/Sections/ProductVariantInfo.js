import React, { useEffect, useState } from 'react'
import { Card, Col, Container, Row, Button } from 'react-bootstrap'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import ToggleButton from 'react-bootstrap/ToggleButton'
import Table from 'react-bootstrap/Table'
import Tab from 'react-bootstrap/Tab'
import Tabs from 'react-bootstrap/Tabs'
import Moment from 'react-moment';
import { Label } from 'reactstrap'
import DatePicker from "react-datepicker";
import { FcApproval, FcCancel } from "react-icons/fc";
import {GiStethoscope, GiLoveInjection} from "react-icons/gi"
import Cookies from 'js-cookie'
import CryptoJS from 'crypto-js';

import '../Banner.css'

export default function ProductVariantInfo(props) {

    const [Product, setProduct] = useState([])
    const [VetCheckDoneBy, setVetCheckDoneBy] = useState(null)
    const [VetCheckApprove, setVetCheckApprove] = useState(false)
    const [VetCheckReject, setVetCheckReject] = useState(false)
    const [VetCheckDoneDate, setVetCheckDoneDate] = useState(new Date())
    const [VetCheckComment, setVetCheckComment] = useState(null)
    const [VetCheckDateFieldValidationError, setVetCheckDateFieldValidationError] = useState(null)
    const [VetCheckCommentFieldValidationError, setVetCheckCommentFieldValidationError] = useState(null)
    const [VetCheckStatusFieldValidationError, setVetCheckStatusFieldValidationError] = useState(null)
    const [SaveVetCheckDetailResponseData, setSaveVetCheckDetailResponseData] = useState(null)
    const [SaveVetCheckDetailError, setSaveVetCheckDetailError] = useState(null)
    
    const [existingProductVerify, setExistingProductVerify] = useState(props.detail.is_verified)
    const [doesProductPassverification, setDoesProductPassverification] = useState(props.detail.is_verified)
    const [verificationComment, setVerificationComment] = useState(props.detail.verification_comment)
    const [VerificationCommentMissingError, setVerificationCommentMissingError] = useState(false)

    const [saveProdctverificationbuttondisable, setSaveProdctverificationbuttondisable] = useState(true)
    const [saveProductVerificationSuccess, setSaveProductVerificationSuccess] = useState(false)
    const [saveProductVerificationFail, setSaveProductVerificationFail] = useState(false)

    function handleVetCheckComment(e) {
        setVetCheckComment(e.target.value)
    }

    const clearErrorMessages = () => {
        setSaveVetCheckDetailError(null);
        setVetCheckDateFieldValidationError(null)
        setVetCheckCommentFieldValidationError(null)
        setVetCheckStatusFieldValidationError(null)
    }

    const saveVetCheckInput = (requestJson) => {
        fetch('/api/v1/livestock-veterinary-check', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
            body: JSON.stringify(requestJson),
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response.json());
                }
            })
            .then((result) => setSaveVetCheckDetailResponseData(true))
            .catch((err) => {
                console.log(err);
                setSaveVetCheckDetailError("Failed to save Veterinary check details.");
                setVetCheckComment('');
                setVetCheckApprove(false);
                setVetCheckReject(false);
                setVetCheckDoneDate(new Date());
            })
    }
    
    const handleVetCheckResponseSubmit = () => {
        clearErrorMessages()

        if (!VetCheckApprove && !VetCheckReject) {
            setVetCheckStatusFieldValidationError("Approval status value is required.")
            return
        }
        if (!VetCheckDoneDate) {
            setVetCheckDateFieldValidationError("Checked-On field value is required.")
            return
        }
        if (!VetCheckComment) {
            setVetCheckCommentFieldValidationError("Comments field value is required.")
            return
        }

        const jsonData = {
            product: props.productId,
            veterinary: props.loggedInUserId,
            checked_on: VetCheckDoneDate,
            is_vet_approved: VetCheckApprove,
            vet_comments: VetCheckComment
        };

        saveVetCheckInput(jsonData)
    }

    const handleSaveProductVerification = () => {
        clearErrorMessages()

        if (!verificationComment || verificationComment == "") {
            setVerificationCommentMissingError("Verification Comment is required.")
            return
        }

        const jsonData = {
            is_verified: doesProductPassverification,
            comment: verificationComment
        };

        fetch(`/api/v1/update-product/id=${encodeURIComponent(props.productId)}&quantity=0&sold=false&isfarmzonnsale=false`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
            body: JSON.stringify(jsonData),
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response.json());
                }
            })
            .then((result) => {
                setSaveProductVerificationSuccess(true)
                setSaveProductVerificationFail(false)
                setSaveProdctverificationbuttondisable(true)
            })
            .catch((err) => {
                console.log(err);
                setSaveProductVerificationFail(true)
                setSaveProductVerificationSuccess(false)
                setSaveProdctverificationbuttondisable(true)
            })
    }


    useEffect(() => {

        if (props.vetCheckDetail && props.vetCheckDetail.length > 0) {
            setVetCheckComment(props.vetCheckDetail[0].vet_comments)
            setVetCheckApprove(props.vetCheckDetail[0].is_vet_approved)
            setVetCheckReject(!props.vetCheckDetail[0].is_vet_approved)
            setVetCheckDoneDate(props.vetCheckDetail[0].checked_on)
            setVetCheckDoneBy('Dr. ' + props.vetCheckDetail[0].veterinary + ', ' + props.vetCheckDetail[0].veterinary_degree)
        }

        setExistingProductVerify(props.detail.is_verified)
        setDoesProductPassverification(props.detail.is_verified)

        setProduct(props.detail)

    }, [props.detail])

    const handleproductverificationstatuschange = (e) => {
        setSaveProdctverificationbuttondisable(false)
        setDoesProductPassverification(e.target.checked)
    }

    const handleproductverificationcommentchange = (e) => {
        setSaveProdctverificationbuttondisable(false)
        setVerificationComment(e.target.value)
    }

    return (
        <div>
            <Container>
                <Tabs position="left" defaultActiveKey={props.loggedInUserType == 5 ? 3 : (props.loggedInUserType == 9 ? 4 : 1)} tabWidth={4}>
                    <Tab eventKey={1} className="product-detail-title" title="Product Variant detail">
                        <br />
                        <Row xs={1} md={2}>
                            <Col>
                                <Table className="table-productVariant-detail" striped bordered hover size="sm">
                                    
                                    <tbody>
                                        <tr>
                                            <td>
                                                Breed
                                            </td>
                                            <td>
                                                {props.breed}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Age
                                            </td>
                                            <td>
                                                {props.age}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Gender
                                            </td>
                                            <td>
                                                {props.gender}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Color
                                            </td>
                                            <td>
                                                {props.color}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Livestock Feed type
                                            </td>
                                            <td>
                                                {props.feedtype}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Posted On/Last Updated On
                                            </td>
                                            <td>
                                                {props.modifiedon}
                                            </td>
                                        </tr>
                                    </tbody>
                                </Table>
                                {props.variant == 1 ?
                                    <div>
                                        More details ..
                                    
                                        <Table className="table-productVariant-detail" striped bordered hover size="sm">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        Teeth Count
                                                    </td>
                                                    <td>
                                                        {props.teethCount}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Milk Produced Per day
                                                    </td>
                                                    <td>
                                                        {props.milkProduce > 0 ? props.milkProduce + " lts" : " -"}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Is Currently Pregnant
                                                    </td>
                                                    <td>
                                                        {props.ispregnant ? "Yes" : " -"}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Number of Pregnancies
                                                    </td>
                                                    <td>
                                                        {props.offsprings > 0 ? props.offsprings : " -"}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </Table>
                                    </div>
                                    :
                                    null
                                }
                            </Col>
                        </Row>
                            
                    </Tab>

                    <Tab eventKey={2} title="Livestock Vaccination detail">
                        <br />
                        <Row>
                            {props.vaccinationDone && props.vaccinationDone.length > 0 ?
                                props.vaccinationDone.map((vaccine, index) => (
                                    <Col key={index} sm={4} md={4} lg={3}>
                                        <Card style={{ width: '18rem' }}>
                                            <Card.Header>{vaccine.vaccine}</Card.Header>
                                            <Card.Body>
                                                <Card.Subtitle className="mb-2 text-muted">{"Completed dosages: " + vaccine.completed_dose} </Card.Subtitle>
                                                <Card.Text>
                                                    Latest vaccinated date:<span> </span>
                                                    <Moment format="DD-MMMM-yyyy">{vaccine.vaccinated_on}</Moment>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                )
                                )
                                :
                                <div className='product-detail-vaccination-yet-to-done'>
                                    <GiLoveInjection /> <span>  </span>
                                    Vacinnation information is not available for this product
                                </div>
                            }
                        </Row>
                    </Tab>

                    <Tab eventKey={3} title="Veterinary Check Details">
                        <br />
                        <Row xs={1} sm={2} md={2} lg={3}>
                            {props.pickup && props.loggedInUserType == '5' ?
                                <Col>
                                    <Card style={{ width: '18rem' }}>
                                        <Card.Header>PickUp address</Card.Header>
                                        <Card.Body>
                                            <Card.Subtitle className="mb-2 text-muted">{"Type: " + props.pickup.address.type}</Card.Subtitle>
                                            <Card.Text>
                                                {props.pickup.address.address_1}
                                                <br />
                                                {props.pickup.address.address_2}
                                                <br />
                                                {props.pickup.address.city + ", " + props.pickup.address.state}
                                                <br />
                                                {props.pickup.address.country + " - " + props.pickup.address.pincode}
                                            </Card.Text>
                                        </Card.Body>
                                    </Card>
                                    <br />
                                </Col>
                                : null
                            }
                            <Col>
                                {(props.vetCheckDetail && props.vetCheckDetail.length > 0) || SaveVetCheckDetailResponseData ?
                                    <div className='product-detail-vet-check-done'>
                                        <div>
                                            {SaveVetCheckDetailResponseData ?
                                                "Successfully saved vet response." :
                                                null
                                            }
                                        </div>
                                        Quality check: {VetCheckApprove ?
                                            <span><FcApproval /> Pass</span>
                                            :
                                            <span> <FcCancel /> Fail</span>
                                        }
                                        <br />
                                        Vet Comments: <span> </span>
                                        {VetCheckComment}
                                        <br />
                                        Check Done By: <span> </span>
                                        {VetCheckDoneBy}
                                        <br />
                                        Check Done On: <span> </span>
                                        <Moment format="DD-MMMM-yyyy">{VetCheckDoneDate}</Moment>
                                        <br />
                                        {
                                            props.loggedInUserType == '5' ?
                                            <a className='product-detail-vet-check-back-link' href='/vet-verify/'>
                                                &larr; Back to Vet-check livestock listing
                                            </a>
                                            :
                                            null
                                        }
                                    </div>
                                    :
                                    <div className='product-detail-vet-check-div'>
                                        {
                                            props.loggedInUserType == '5' ?
                                                <div>
                                                    {SaveVetCheckDetailError ?
                                                        <div className="error">
                                                            {SaveVetCheckDetailError}
                                                            <br />
                                                        </div>
                                                        : null
                                                    }
                                                    Is quality check passed: <span> </span>
                                                    <span>  </span>
                                                    <input
                                                        name="VetCheckApproved"
                                                        type="checkbox"
                                                        checked={VetCheckApprove}
                                                        onChange={(e) => { setVetCheckApprove(e.target.checked); setVetCheckReject(!e.target.checked) }} />
                                                    <span>  </span>
                                                    <Label>Yes</Label>
                                                    <span> / </span>
                                                    <span>  </span>
                                                    <input
                                                        name="VetCheckReject"
                                                        type="checkbox"
                                                        checked={VetCheckReject}
                                                        onChange={(e) => { setVetCheckReject(e.target.checked); setVetCheckApprove(!e.target.checked) }} />
                                                    <span>  </span>
                                                    <Label>No</Label>
                                                    {VetCheckStatusFieldValidationError ?
                                                        <span className="error" >
                                                            <br />
                                                            {VetCheckStatusFieldValidationError}
                                                        </span>
                                                        : null}
                                                    <br />

                                                    <Label>
                                                        Comments:
                                                    </Label>
                                                    <span> </span>
                                                    {VetCheckCommentFieldValidationError ?
                                                        <span className="error" >
                                                            <br />
                                                            {VetCheckCommentFieldValidationError}
                                                        </span>
                                                        : null}
                                                    <textarea className="form-control" onChange={handleVetCheckComment}
                                                        value={VetCheckComment} placeholder="Enter your Comments" />
                                                    <Label>Check Done on: </Label>
                                                    <span> </span>
                                                    <DatePicker className="form-control" dateFormat="dd-MM-yyyy" isClearable selected={VetCheckDoneDate} onChange={(date) => setVetCheckDoneDate(date)} />
                                                    {
                                                        VetCheckDateFieldValidationError ?
                                                            <span className="error" >
                                                                <br />
                                                                {VetCheckDateFieldValidationError}
                                                            </span>
                                                            : null
                                                    }
                                                    <div className='product-detail-vet-check-yet-to-done-submit'>
                                                        <Button variant="primary" onClick={handleVetCheckResponseSubmit}>
                                                            Submit Response
                                                        </Button>
                                                        <span> </span>
                                                        <a className='product-detail-vet-check-back-link' href='/vet-verify/'>
                                                            &larr; Back to Vet-check livestock listing
                                                        </a>
                                                    </div>
                                                </div>
                                                :
                                                <div className='product-detail-vet-check-yet-to-done'>
                                                    <GiStethoscope /> <span>  </span>
                                                    Veterinary confirmation yet to be completed!
                                                </div>
                                        }
                                    </div>
                                }
                            </Col>
                        </Row>
                    </Tab>

                    {
                        props.loggedInUserType == 9 ?
                            <Tab eventKey={4} title="Product verification Detail">
                                <br />
                                <div className='product-detail-verify-success'>
                                    {saveProductVerificationSuccess ?
                                        "Successfully saved verification response." :
                                        null
                                    }
                                </div>
                                <div className='product-detail-verify-fail'>
                                    {saveProductVerificationFail ?
                                        "Failed to save verification response." :
                                        null
                                    }
                                </div>
                                <Row xs={1} md={2} className='product-detail-verif-row'>
                                    <Col>
                                        <div className='product-detail-display-inline'>
                                            <div>
                                                Please tick if product matches with details provided:
                                            </div>
                                            <div className='product-detail-verf'>
                                                <input type="checkbox" checked={doesProductPassverification} onChange={handleproductverificationstatuschange} /> <span> </span>
                                                {doesProductPassverification ? " Good to list" : " "}
                                            </div>
                                        </div>
                                        <div className='product-detail-display-inline'>
                                            <div className='product-detail-verf-comment-la*bel'>
                                                Comments:
                                            </div>
                                            <div className='product-detail-verf-comment'>
                                                <input type="text" className="form-control" placeholder="Verification comments" value={verificationComment} onChange={handleproductverificationcommentchange} />
                                            </div>
                                        </div>
                                        <div className='product-detail-display-inline'>
                                            <div>
                                                <Button variant="primary" type="submit"
                                                    disabled={saveProdctverificationbuttondisable}
                                                    onClick={handleSaveProductVerification}>
                                                    Save
                                                </Button>
                                            </div>
                                            {VerificationCommentMissingError ?
                                                <div className="product-detail-verf-comment-error">
                                                    {VerificationCommentMissingError}
                                                </div>
                                                : null
                                            }
                                        </div>
                                        <div>
                                            <a className='product-detail-verf-back-link' href='/seller-product/'>
                                                &larr; Back to livestock listing
                                            </a>
                                        </div>
                                    </Col>
                                    <Col>
                                   
                                    </Col>
                                </Row>
                            </Tab>
                            :
                            null
                    }
                </Tabs>
            </Container>
        </div>
    )
}