import React, { useState, useRef, useEffect } from "react";
import "./Filter.css";
import FilterModal from "./FilterModal";

export default function Filter({ children, onSelect, onClear, label }) {
  const [isOpen, setIsOpen] = useState(false);

  const handleselect = (event) => {
    setIsOpen(false);
    onSelect(event);
  };

  const handleClose = (event) => {
    setIsOpen(false);
  };

  return (
    <div className="filter_wrapper">
      <button onClick={() => setIsOpen(!isOpen)} className="filter_button">
        {label}
      </button>

      <FilterModal
        open={isOpen}
        onSelect={handleselect}
        onCancel={() => setIsOpen(false)}
        onClear={onClear}
      >
        {children}
      </FilterModal>
    </div>
  );
}
