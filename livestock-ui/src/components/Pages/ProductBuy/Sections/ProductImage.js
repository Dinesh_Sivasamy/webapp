import React from 'react'
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

import configData from "../../../../config.json";
import '../Banner.css'

import "react-image-gallery/styles/css/image-gallery.css";
import ImageGallery from 'react-image-gallery';

export default function ProductImage(props) {

    const getproductimage = (image) => {
        if (image) {
            return `${configData.IMG_FOLDER_PATH}${image}`
        }
    }

    const buildcarousel = (items) => {
        const imgdiv = []
        // for (const [i, item] of items.entries()) {
        //     imgdiv.push(
        //         <div>
        //             <img src={getproductimage(item.image)} />
        //         </div>
        //     )
        // }
        // return imgdiv

        for (const [i, item] of items.entries()) {
            
            var img = getproductimage(item.image);
             
            imgdiv.push( {
                original:  img ,
                thumbnail: img 
            });
        }
        
        return imgdiv
    }

    return (
        <div className="product-detail-imagegallery">
            {props.detail && props.detail.length > 0 ?
                // <Carousel showArrows={true} showThumbs={false} >
                //     {buildcarousel(props.detail)}
                // </Carousel>
                <ImageGallery items={buildcarousel(props.detail)} />
                :
                <Carousel showArrows={true} showThumbs={false} >
                    <div>
                        <img src={`${configData.DEFAULT_PRODUCT_IMG}`} />
                    </div>
                </Carousel>
            }
        </div>
    )
}