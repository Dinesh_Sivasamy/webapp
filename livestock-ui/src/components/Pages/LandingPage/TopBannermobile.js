import React from "react";
import { Carousel } from "react-bootstrap";

export default function TopBannermobile() {
  return (
    <div className="hide-in-desktop">
      <Carousel controls={true} interval={5000} indicators={false}>
        <Carousel.Item>
          <div className="height-310">
            <div className="bgcolor-primary-pista display-inline height-310">
              <div className="padding-top-3">
                <br />
                <label className="header-xsm padding-left-5">
                  Free platform to{" "}
                  <span className="color-brown header-xxsm">
                    Buy / Sell / Enquire
                  </span>{" "}
                  livestock with ease!!{" "}
                </label>
                <br />
                <br />
                <label className="header-xs padding-left-5">
                  Access Anywhere, Anytime, Anything!
                </label>
                <label className="header-xs padding-left-5">
                  Revolutionize Your Livestock Experience Today!
                </label>
                <figure className="button--wrap">
                  <p className="button--wrap-box bg-color-brown">SHOP NOW</p>
                </figure>
              </div>
              <div className="text-align-right padding-top-10">
                <img
                  className="img-size-bannercattle-mobile"
                  src="/images/bannercattle.png"
                  alt="livestock"
                />
              </div>
              <div></div>
            </div>
          </div>
        </Carousel.Item>
        <Carousel.Item>
          <div className="height-310">
            <div className="text-align-left">
              <img
                className="img-size-bannerpet-mobile"
                src="/images/bannerpets.jpeg"
                alt="livestock"
              />
            </div>
            <div>
              <label className="header-sm padding-left-5">
                Connect directly with sellers for free and explore your
                interests hassle-free!
              </label>
              <br />
              <label className="header-xs padding-left-5">No Brokerage!</label>
              <label className="header-xs padding-left-5">No Fee!!</label>
            </div>
          </div>
        </Carousel.Item>
        <Carousel.Item>
          <div className="height-310 display-inline bgcolor-primary-sandal">
            <div className="text-align-left padding-top-10">
              <div></div>
              <img
                className="img-size-bannercattle-mobile"
                src="/images/bannerfarmanimals_withoutbg.png"
                alt="livestock"
              />
            </div>
            <div className="padding-left-5 padding-top-10">
              <br />
              <label className="header-xsm">
                Referr & WIN Upto{" "}
                <label className="color-green header-sm"> &#8377;5,000</label>/-
                Off
              </label>
              <br />
              <br />
              <label className="header-xsm padding-left-5">
                * Spl discounts!!
              </label>
              <br />
              <label className="header-xsm padding-left-5">
                * Connect with sellers directly!!
              </label>
            </div>
          </div>
        </Carousel.Item>
      </Carousel>
    </div>
  );
}
