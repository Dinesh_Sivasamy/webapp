import React, { useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import Cookies from "js-cookie";

export default function Header() {
  const [referralcode, setReferralcode] = useState(
    Cookies.get("refcode") &&
      Cookies.get("refcode") != null &&
      Cookies.get("refcode") != "None" &&
      Cookies.get("refcode") != "none"
      ? Cookies.get("refcode")
      : null
  );

  return (
    <div className="bgcolor-primary-pink height-45 padding-bottom-1">
      <Container>
        <Row className="padding-top-3-mobile">
          <Col>
            <div className="display-inline">
              <a
                className="text-decor-none display-inline padding-top-1"
                href={
                  "https://api.whatsapp.com/send/?text=Hi, Easy Livestock Trading in India! Buy healthy, high-quality livestock from verified sellers nationwide, delivered to your doorstep. Sign up now for a chance to WIN up to Rs. 5,000 off! Join us at https://farmzonn.com/?refcode=" +
                  referralcode +
                  " for hassle-free livestock transactions."
                }
                target="_blank"
              >
                <img
                  className="icon-size-xs"
                  src="/icons/whatsapp.png"
                  alt="livestock"
                />
                <label className="header-xxs padding-top-5 padding-left-2">
                  Refer&Win
                </label>
              </a>
            </div>
          </Col>
          <Col>
            <div className="padding-top-1 header-xsm text-align-right">
              <label className="padding-left-2">English |</label>
              <lable className="padding-left-2">INDIA |</lable>
              <lable className="padding-left-2">&#8377;</lable>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
