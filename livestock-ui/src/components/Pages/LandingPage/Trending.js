import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import configData from "../../../config.json";
import cattle from "../Home/images/default_product_img.jpg";

export default function Trending(props) {
  const getproductimage = (productvariantimages) => {
    if (
      productvariantimages &&
      productvariantimages.length > 0 &&
      productvariantimages[0].image
    ) {
      return `${configData.IMG_FOLDER_PATH}${productvariantimages[0].image}`;
    } else {
      return `${cattle}`;
    }
  };

  return (
    <div className="hide-in-mobile padding-top-2">
      <div className="display-inline padding-left-5 padding-bottom-1">
        <span className="header-lg">Trending this week</span> <span> </span>{" "}
        <img
          className="icon-size-sm-x padding-left-2"
          src="/icons/dogwalk.png"
          alt="livestock"
        />{" "}
      </div>
      <div className="padding-bottom-1">
        <Container fluid>
          <Row lg={10}>
            {props.productlist && props.productlist.length > 0
              ? props.productlist.slice(0, 5).map((product, id) => (
                  <Col>
                    <a
                      href={`/product/${product.id}`}
                      className="text-decor-none-onhover"
                    >
                      <div className="list-box bgcolor-primary-lighter-brown">
                        <div>
                          <img
                            className="img-size-list-box img-border-25 padding-7-5"
                            src={getproductimage(
                              product.product_variant[0].product_images
                            )}
                            alt="livestock"
                          />
                        </div>
                        <div className="text-align-left sub-header-xxs padding-left-5">
                          <span className="header-xs">{product.name}</span>
                          <br />
                          {product.breed +
                            " | " +
                            product.product_variant[0].weight +
                            " Kgs"}
                          <br />
                          {product.product_variant[0].gender +
                            " | " +
                            product.product_variant[0].stock +
                            " Qty"}
                          <br />
                          {product.pickup.address.city +
                            ", " +
                            product.pickup.address.state}
                        </div>
                      </div>
                    </a>
                  </Col>
                ))
              : null}

            {/* <Col>
              <div className="list-box bgcolor-primary-lighter-brown">
                <div>
                  <img
                    className="img-size-list-box img-border-25 padding-7-5"
                    src="/images/cattle.jpg"
                    alt="livestock"
                  />
                </div>
                <div className="text-align-left sub-header-xxs padding-left-5">
                  <span className="header-xs">Traditional Country Dogs</span>
                  <br />
                  Country Breed | 2.00 Kgs Weight
                  <br />
                  Female | 4 Qty | Produces 3 lts milk(avg)
                  <br />
                  Coimbatore, Tamil Nadu
                </div>
              </div>
            </Col>
            <Col></Col> */}
          </Row>
        </Container>
      </div>
    </div>
  );
}
