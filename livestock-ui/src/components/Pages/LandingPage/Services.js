import React from "react";
import { Col, Container, Row } from "react-bootstrap";

export default function Services() {
  return (
    <div>
      <div className="hide-in-mobile padding-bottom-1">
        <div className="display-inline padding-left-5 padding-bottom-1">
          <label className="header-lg">Our Services</label>
          <img
            className="icon-size-sm-x padding-left-2"
            src="/icons/settings.png"
            alt="livestock"
          />{" "}
        </div>
        <div>
          <Container>
            <Row>
              <Col>
                <div className="padding-top-10">
                  <img
                    className="img-size-farm-animals-usertestimonial"
                    src="/images/bannerfarm_2.png"
                    alt="livestock"
                  />
                </div>
              </Col>
              <Col>
                <Container>
                  <Row>
                    <Col>
                      <a
                        href="/jersey-cattle-sale-coimbatore"
                        className="text-decor-none"
                      >
                        <div className="service-box border-change-color">
                          <label className="header-sm padding-top-10">
                            Jersey Cattle, Cows & Bulls
                          </label>
                        </div>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href="/farm-animals-pets-in-coimbatore"
                        className="text-decor-none"
                      >
                        <div className="service-box border-change-color">
                          <label className="header-sm padding-top-10">
                            Buying / Selling Farm Animals
                          </label>
                        </div>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href="/goat-farming-in-coimbatore"
                        className="text-decor-none"
                      >
                        <div className="service-box border-change-color">
                          <label className="header-sm padding-top-10">
                            Commercial Goat Farming
                          </label>
                        </div>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href="/pet-shops-in-coimbatore"
                        className="text-decor-none"
                      >
                        <div className="service-box border-change-color">
                          <label className="header-sm padding-top-10">
                            Buy and Sell Puppies
                          </label>
                        </div>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href="/indian-milk-cow-breeds-sale"
                        className="text-decor-none"
                      >
                        <div className="service-box border-change-color">
                          <label className="header-sm padding-top-10">
                            Best Indian Cow Breeds
                          </label>
                        </div>
                      </a>
                    </Col>
                    <Col>
                      <a href="/Buy/0" className="text-decor-none">
                        <div className="service-box border-change-color">
                          <label className="header-sm padding-top-10">
                            Browse thru the Animals Listing
                          </label>
                        </div>
                      </a>
                    </Col>
                  </Row>
                </Container>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
      <div className="hide-in-desktop padding-bottom-1">
        <div className="display-inline padding-left-5 padding-top-5 padding-bottom-1">
          <label className="header-sm">Our Services</label>
          <img
            className="icon-size-xs padding-left-2"
            src="/icons/settings.png"
            alt="livestock"
          />{" "}
        </div>
        <Container>
          <Row>
            <Col>
              <div className="padding-left-10">
                <img
                  className="img-size-service-mobile"
                  src="/images/bannerfarm_2.png"
                  alt="livestock"
                />
              </div>
              <br />
            </Col>

            <Col>
              <Container>
                <Row>
                  <Col>
                    <a
                      href="/jersey-cattle-sale-coimbatore"
                      className="text-decor-none"
                    >
                      <div className="service-box border-change-color">
                        <label className="header-xsm padding-top-12">
                          Jersey Cattles
                        </label>
                      </div>
                    </a>
                  </Col>
                  <Col>
                    <a
                      href="/farm-animals-pets-in-coimbatore"
                      className="text-decor-none"
                    >
                      <div className="service-box border-change-color">
                        <label className="header-xsm padding-top-12">
                          Farm Animals
                        </label>
                      </div>
                    </a>
                  </Col>
                  <Col>
                    <a
                      href="/goat-farming-in-coimbatore"
                      className="text-decor-none"
                    >
                      <div className="service-box border-change-color">
                        <label className="header-xsm padding-top-12">
                          Goat Farming
                        </label>
                      </div>
                    </a>
                  </Col>
                  <Col>
                    <a
                      href="/pet-shops-in-coimbatore"
                      className="text-decor-none"
                    >
                      <div className="service-box border-change-color">
                        <label className="header-xsm padding-top-12">
                          Puppies & Pet shops
                        </label>
                      </div>
                    </a>
                  </Col>
                  <Col>
                    <a
                      href="/indian-milk-cow-breeds-sale"
                      className="text-decor-none"
                    >
                      <div className="service-box border-change-color">
                        <label className="header-xsm padding-top-25">
                          Indian Cows
                        </label>
                      </div>
                    </a>
                  </Col>
                  <Col>
                    <a href="/Buy/0" className="text-decor-none">
                      <div className="service-box border-change-color">
                        <label className="header-xsm padding-top-25">
                          Browse thru Listing
                        </label>
                      </div>
                    </a>
                  </Col>
                </Row>
              </Container>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}
