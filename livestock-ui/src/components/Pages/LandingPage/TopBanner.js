import React from "react";
import { Carousel } from "react-bootstrap";

export default function TopBanner() {
  return (
    <div className="hide-in-mobile">
      <Carousel controls={true} interval={5000} indicators={false}>
        <Carousel.Item>
          <div className="bgcolor-primary-pista display-inline height-350">
            <div className="padding-top-5">
              <label className="header-sm  padding-left-5">
                Free platform to{" "}
                <span className="color-brown header-sm">
                  Buy / Sell / Enquire
                </span>{" "}
                livestock with ease!!{" "}
              </label>
              <br />
              <label className="header-xsm  padding-left-5 padding-top-2">
                No need to transport your livestock to markets, Quote right
                price and buyers will contact you!
              </label>
              <figure className="button--wrap">
                <p className="button--wrap-box bg-color-brown">SHOP NOW</p>
              </figure>
            </div>
            <div className="padding-top-7">
              <img
                className="icon-size-xs"
                src="/icons/cattleface.png"
                alt="livestock"
              />
            </div>
            <div className="text-align-right padding-top-10 position-abs-bottom-minus-150">
              <img
                className="img-size-bannercattle"
                src="/images/bannercattle.png"
                alt="livestock"
              />
            </div>
          </div>
        </Carousel.Item>
        <Carousel.Item>
          <div className="display-inline height-350">
            <div className="text-align-left padding-top-2 padding-left-2">
              <img
                className="img-size-list-box"
                src="/images/bannerpets.jpeg"
                alt="livestock"
              />
            </div>
            <div className="padding-top-5 padding-right-1"></div>
            <div className="padding-top-7">
              <label className="header-sm padding-left-5">
                Connect directly with sellers for free and explore your
                interests hassle-free!
              </label>
              <div>
                <figure className="button--wrap-banner">
                  <p className="button--wrap-box header-xxs bgcolor-bg-line-blue">
                    No Fee | No Brokerage
                  </p>
                </figure>
              </div>
            </div>
          </div>
        </Carousel.Item>
        <Carousel.Item>
          <div className="height-350 display-inline bgcolor-primary-sandal">
            <div className="text-align-center padding-left-5">
              <img
                className="img-size-farm-animals"
                src="/images/bannerfarmanimals_withoutbg.png"
                alt="livestock"
              />
            </div>
            <div className="padding-left-3 padding-top-5">
              <br />
              <label className="header-sm">
                Refer us to your friends & family and WIN upto
                <label className="color-green header-lg">
                  {" "}
                  &#8377;5,000
                </label>{" "}
                /- off!!
              </label>
              <br />
              <div className="display-inline">
                <figure className="button--wrap-banner">
                  <p className="button--wrap-box bgcolor-bg-line-purple diplay-inline">
                    FREE DELIVERY
                  </p>
                </figure>
              </div>
            </div>
            <div className="padding-top-10 align-item-left"></div>
          </div>
        </Carousel.Item>
      </Carousel>
    </div>
  );
}
