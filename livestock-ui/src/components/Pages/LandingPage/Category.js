import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { useHistory } from "react-router-dom";

export default function Category() {
  const history = useHistory();

  const pageredirect = (category) => {
    console.log("redirecting to: " + category);
    history(`/Buy/${category}`);
  };

  return (
    <div className="padding-top-2">
      <div className="display-inline">
        <span className="header-lg padding-left-5 padding-bottom-1 hide-in-mobile">
          Shop by Category
        </span>
        <span className="header-sm padding-left-5 padding-bottom-1 hide-in-desktop">
          Shop by Category
        </span>
        <div className="padding-left-1 padding-top-1">
          <img
            className="icon-size-sm"
            src="/icons/search.png"
            alt="livestock"
          />
        </div>
      </div>

      <div></div>
      <div className="bgcolor-primary-very-mid-light-brown height-180">
        <Container>
          <div className="horizontal-scroll-mobile-only padding-top-mobile-7 padding-top-1">
            <Row xs={5} md={5}>
              <Col>
                <button
                  className="pill-button bgcolor-bg-line-green"
                  onClick={() => {
                    pageredirect(1);
                  }}
                >
                  <div className="display-inline padding-top-3">
                    <img
                      className="icon-size-sm"
                      src="/images/cow.png"
                      alt="livestock"
                    />
                    <p className="header-xsm padding-top-7 color-white">
                      Cattle
                    </p>
                  </div>
                </button>
              </Col>
              <Col></Col>
              <Col>
                <button
                  className="pill-button bgcolor-bg-line-yellow"
                  onClick={() => {
                    pageredirect(2);
                  }}
                >
                  <div className="display-inline padding-top-1">
                    <img
                      className="icon-size-sm"
                      src="/images/goat.png"
                      alt="livestock"
                    />
                    <p className="header-xsm padding-top-10 color-white">
                      Goats
                    </p>
                  </div>
                </button>
              </Col>
              <Col></Col>
              <Col>
                <button
                  className="pill-button bgcolor-bg-line-blue"
                  onClick={() => {
                    pageredirect(3);
                  }}
                >
                  <div className="display-inline padding-top-7">
                    <img
                      className="icon-size-sm"
                      src="/images/categorygoat.png"
                      alt="livestock"
                    />
                    <p className="header-xsm padding-left-10 padding-top-5 color-white">
                      Sheep
                    </p>
                  </div>
                </button>
              </Col>
              <Col>
                <button
                  className="pill-button bgcolor-bg-line-purple"
                  onClick={() => {
                    pageredirect(4);
                  }}
                >
                  <div className="display-inline padding-top-1">
                    <img
                      className="icon-size-sm"
                      src="/images/categorychicken.png"
                      alt="livestock"
                    />
                    <p className="header-xsm padding-left-10 padding-top-10 color-white">
                      Hens
                    </p>
                  </div>
                </button>
              </Col>
              <Col></Col>
              <Col>
                <button
                  className="pill-button bgcolor-bg-line-brown"
                  onClick={() => {
                    pageredirect(5);
                  }}
                >
                  <div className="display-inline padding-top-1">
                    <img
                      className="icon-size-sm"
                      src="/images/categorydog.png"
                      alt="livestock"
                    />
                    <p className="header-xsm padding-top-10 color-white">
                      Dogs
                    </p>
                  </div>
                </button>
              </Col>
              <Col></Col>
              <Col>
                <button
                  className="pill-button bgcolor-primary-brown"
                  onClick={() => {
                    pageredirect(0);
                  }}
                >
                  <div className="display-inline padding-top-5">
                    <img
                      className="icon-size-sm"
                      src="/images/categoryall.png"
                      alt="livestock"
                    />
                    <p className="header-xsm padding-left-5 padding-top-7 color-white">
                      Animals
                    </p>
                  </div>
                </button>
              </Col>
              <Col></Col>
              <Col></Col>
            </Row>
          </div>
        </Container>
      </div>
    </div>
  );
}
