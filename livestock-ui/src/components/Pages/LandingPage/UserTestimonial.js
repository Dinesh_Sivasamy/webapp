import React, { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import Carousel from "react-bootstrap/Carousel";
import Cookies from "js-cookie";
import configData from "../../../config.json";

import cattle from "../Home/images/default_product_img.jpg";

export default function UserTestimonial() {
  const [productTopList, SetProductTopList] = useState([]);
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    if (
      !localStorage.getItem("producttop") ||
      localStorage.getItem("producttop") == []
    ) {
      fetch("/api/v1/product-top/top=4", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            setLoading(false);
            throw new Error(response);
          }
        })
        .then(function (resp) {
          console.log(resp);
          SetProductTopList(resp);
          localStorage.setItem("producttop", JSON.stringify(resp));
          setLoading(false);
        });
    } else {
      SetProductTopList(JSON.parse(localStorage.getItem("producttop")));
    }
  }, []);

  const getproductimage = (productvariantimages) => {
    if (
      productvariantimages &&
      productvariantimages.length > 0 &&
      productvariantimages[0].image
    ) {
      return `${configData.IMG_FOLDER_PATH}${productvariantimages[0].image}`;
    } else {
      return `${cattle}`;
    }
  };

  return (
    <div className="hide-in-mobile">
      <div className="display-inline padding-top-2 padding-bottom-1">
        <span className="header-lg padding-left-5">Top in Market</span>
        <img
          className="icon-size-sm"
          src="/icons/pawprintcolor.png"
          alt="livestock"
        />
      </div>
      <Container className="padding-bottom-1">
        <Row md={2}>
          <Col>
            <Container>
              <Row md={2}>
                {productTopList && productTopList.length > 0
                  ? productTopList.map((product, id) => (
                      <Col key={id}>
                        <a
                          href={`/product/${product.id}`}
                          className="text-decor-none-onhover"
                        >
                          <div className="list-box border-dashed">
                            <div>
                              <img
                                className="img-size-list-box img-border-25 padding-7-5"
                                src={getproductimage(
                                  product.product_variant[0].product_images
                                )}
                                alt="livestock"
                              />
                            </div>
                            <div className="text-align-left sub-header-xxs padding-left-5">
                              <span className="header-xs">{product.name}</span>
                              <br />
                              {product.breed +
                                " | " +
                                product.product_variant[0].weight +
                                " Kgs"}
                              <br />
                              {product.product_variant[0].gender +
                                " | " +
                                product.product_variant[0].stock +
                                " Qty"}
                              <br />
                              {product.pickup.address.city +
                                ", " +
                                product.pickup.address.state}
                            </div>
                          </div>
                        </a>
                      </Col>
                    ))
                  : null}

                {/*  <Col>
                  <div className="list-box border-dashed">
                    <div>
                      <img
                        className="img-size-list-box img-border-25 padding-7-5"
                        src="/images/cattle.jpg"
                        alt="livestock"
                      />
                    </div>
                    <div className="text-align-left sub-header-xxs padding-left-5">
                      <span className="header-xs">
                        Traditional Country Dogs
                      </span>
                      <br />
                      Country Breed | 2.00 Kgs Weight
                      <br />
                      Female | 4 Qty | Produces 3 lts milk(avg)
                      <br />
                      Coimbatore, Tamil Nadu
                    </div>
                  </div>
                </Col> */}
              </Row>
            </Container>
          </Col>
          <Col className="padding-top-2">
            <div className="padding-top-2">
              <div className="bgcolor-primary-pink img-border-6">
                <div className="padding-top-2 padding-left-5">
                  <div className="display-inline">
                    <span className="header-lg align-items-bottom color-brown">
                      Our Happy Clients!!
                    </span>
                    <img
                      className="img-size-bannerpet-mobile"
                      src="/images/pug.png"
                      alt="livestock"
                    />
                  </div>
                  <div className="padding-top-2">
                    <Carousel
                      controls={true}
                      interval={5000}
                      indicators={false}
                    >
                      <Carousel.Item>
                        <h5>
                          Very useful to get animal buyers quicker and easier!
                        </h5>
                        <p>-Yash</p>
                      </Carousel.Item>
                      <Carousel.Item>
                        <h5>
                          Easy to connect with Sellers and get details of
                          cattles!
                        </h5>
                        <p>-Ramamurthy</p>
                      </Carousel.Item>
                      <Carousel.Item>
                        <h5>
                          Transparent and easy to use, very good platform for
                          farmers!
                        </h5>
                        <p>-Balamurugan</p>
                      </Carousel.Item>
                    </Carousel>
                  </div>
                </div>
                <div className="padding-top-10 text-align-right">
                  <div className="padding-top-5">
                    <img
                      className="img-size-farm-animals-usertestimonial"
                      src="/images/Animalgroup.png"
                      alt="livestock"
                    />
                  </div>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
