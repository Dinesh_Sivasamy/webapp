import Image from 'react-bootstrap/Image'

import './Sitedetail.css';

export default function HowItWorks() {

    const renderLandscapeImage = () => {
        return `/Flowchart_landscape.PNG`
    }

    const renderPortraitImage = () => {
        return `/Flowchart.PNG`
    }

    return (
        <>
            <div className="About-us">
                <p className="para-title">How it works?</p>
                <p className="starting-para">
                    Step 1: As a seller, a farmer or agent or farm husbandry can register and click on Sell button in the home page and provide the requested simple direct inputs and save the product for listing in the application.
                </p>
                <p className="starting-para">
                    Step 2: Once the product gets listed, a Veterinary check will happen on behalf of FarmZonn.com and a specialist Veterinarian will check livestock quality and health and provide his approval for the same.
                </p>
                <p className="starting-para">
                    Step 3: Once the Veterinary check is done, the products will have “Vet check done” label. This label will help the buyers to find quality, healthy livestock, and also will help farmers/sellers to find their buyer sooner and get best price for the livestock in the market.
                </p>
                <p className="starting-para">
                    Step 4: Any buyer can register them to the site and start searching for the livestock of their need in the website.
                </p>
                <p className="starting-para">
                    Step 5: Once the buyer finds the product, he can click on the product to get more details on the product and once the buyer is satisfied with the cost and other particulars the buyer can click on add to cart and continue his shopping
                </p>
                <p className="starting-para">
                    Step 6: On buyers demand, we can schedule for a livestock Veterinary check at low cost, to make sure that the livestock is of good health and quality before buying.
                </p>
                <p className="starting-para">
                    Step 7: After the buyer completes his shopping, he can proceed with checkout and provide his delivery address.
                </p>
                <p className="starting-para">
                    Step 8: On delivery day, FarmZonn agent will check the livestock quality and cross verifies the details provided in the site and sends out live onsite video or photos of the livestock to the buyer to get his confirmation
                </p>
                <p className="starting-para">
                    Step 9: Once the buyer and the Farmzonn agent are satisfied with the livestock details and quality the delivery will be initiated and the livestock will get delivered to the buyer. Buyer or Farmzonn agent can cancell the delivery order if they are not satisifed with the livestock quality or if the details provided in the site doesn't matches.
                </p>
            </div>            
            <div className="workflow-img-div">
                <div className="workflow-heading"> Livestock Sale Work Flow </div>
                <Image className="workflow-landscape-img" src={renderLandscapeImage()} fluid />
                <Image className="workflow-portait-img" src={renderPortraitImage()} fluid />
            </div>
        </>
    );
}