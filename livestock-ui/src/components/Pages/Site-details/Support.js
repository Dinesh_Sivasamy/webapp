import "./Sitedetail.css";
import CardDeck from "react-bootstrap/CardDeck";
import Card from "react-bootstrap/Card";
import supportcall from "./images/support_call.jpg";
import supportmail from "./images/emailsupport.jpg";
import whatsappsupport from "./images/whatsappsupport.jpg";
import configData from '../../../config.json'


import { Container, Row, Col, Button } from "react-bootstrap";

import { BsPhone } from "react-icons/bs";
import { AiOutlineMail, AiOutlineWhatsApp } from "react-icons/ai";

export default function Support() {
  return (
    <div >
      {/* <CardDeck> */}
      <Container className="Site-detail-support">
        <Row xs={2} sm={2} md={3}>
          <Col>
            <Card className="site-detail-support-card">
              <Card.Img variant="top" src={supportcall} />
              <Card.Body>
                <Card.Title>Mobile phone support</Card.Title>
                <Card.Text>
                  Click Phone button to start phone call <span> </span>
                  <Button href={'tel:' + `${configData.ADMIN_CONTACT_PHONE}`} target="_blank">
                    <BsPhone />
                  </Button>
                </Card.Text>
              </Card.Body>
              <Card.Footer>
                <small className="text-muted">
                  available 24 * 7
                </small>
              </Card.Footer>
            </Card>
          </Col>
          <Col>
            <Card className="site-detail-support-card">
              <Card.Img variant="top" src={supportmail} />
              <Card.Body>
                <Card.Title>Mail Support</Card.Title>
                <Card.Text>
                  Click Mail button to send us your email <span> </span>
                  <Button href={'mailto:' + `${configData.ADMIN_CONTACT_MAIL}`}>
                    <AiOutlineMail />
                  </Button>
                </Card.Text>
              </Card.Body>
              <Card.Footer>
                <small className="text-muted">
                  will be responded to within 24 hours
                </small>
              </Card.Footer>
            </Card>
          </Col>
          <Col>
            <Card className="site-detail-support-card">
              <Card.Img variant="top" src={whatsappsupport} />
              <Card.Body>
                <Card.Title>WhatsApp Support</Card.Title>
                <Card.Text>
                  Click WhatsApp button to message us through whatsapp <span> </span>
                  <Button href={'https://api.whatsapp.com/send/?text=Hi, Need help with FarmZonn application.&phone=' + `${configData.ADMIN_CONTACT_WHATSAPP}`} target="_blank"><AiOutlineWhatsApp /></Button>
                </Card.Text>
              </Card.Body>
              <Card.Footer>
                <small className="text-muted">
                  will be responded to within 6 hours
                </small>
              </Card.Footer>
            </Card>
          </Col>
        </Row>
      </Container>
      {/* </CardDeck> */}
    </div>
  );
}