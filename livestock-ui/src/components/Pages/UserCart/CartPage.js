import React, { useEffect, useState } from "react";
import { Button, Row, Col, Container } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import NumericInput from "react-bootstrap-input-spinner";
import Cookies from "js-cookie";
import CryptoJS from "crypto-js";
import { UncontrolledTooltip } from "reactstrap";

import { MdVerifiedUser } from "react-icons/md";

import Loader from "../../Loader";
import "./UserCart.css";
import SignInModal from "../User/SignInModal";
import configData from "../../../config.json";

export default function CartPage() {
  const history = useHistory();
  let indianRupeeLocale = Intl.NumberFormat("en-IN");

  const [show, setShow] = useState(false);
  const handleClose = () => {
    setShow(false);
    window.location.reload();
  };

  const handleShow = () => setShow(true);

  const [isLoading, setLoading] = useState(true);
  const [isOutOfStock, setIsOutOfStock] = useState(false);

  const [CartItems, setCartItems] = useState([]);
  const [Total, setTotal] = useState(0);
  const [Discount, setDiscount] = useState(0);
  const [ShowTotal, setShowTotal] = useState(false);
  const [ShowSuccess, setShowSuccess] = useState(false);

  const [quantity, setquantity] = useState([]);
  const [initialQuantity, setInitialQuantity] = useState([]);

  const [OrderError, setOrderError] = useState(null);

  useEffect(() => {
    window.scrollTo(0, 0);
    localStorage.removeItem("productlist");
    if (Cookies.get("id") != null) {
      fetch(
        `/api/v1/buyercart/buyer=${encodeURIComponent(
          CryptoJS.AES.decrypt(
            Cookies.get("id"),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(CryptoJS.enc.Utf8)
        )}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "X-CSRFToken": Cookies.get("csrftoken"),
          },
        }
      )
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error(response);
          }
        })
        .then(function (resp) {
          console.log(resp);
          setCartItems(resp);
          calculateTotal(resp);
        })
        .catch((err) => {
          console.log(err);
        });
    }
    setLoading(false);
  }, []);

  const calculateTotal = (cartDetail) => {
    let total = 0;
    if (cartDetail && cartDetail.length > 0) {
      cartDetail.map((item) => {
        total +=
          parseFloat(
            item.product.product_variant[0].offer_price &&
              item.product.product_variant[0].offer_price > 0
              ? item.product.product_variant[0].offer_price
              : item.product.product_variant[0].original_price
          ) * item.items_count;
        setIsOutOfStock(
          !isOutOfStock
            ? item.product.product_variant[0].stock > 0
              ? false
              : true
            : isOutOfStock
        );
      });

      setTotal(total);
      setShowTotal(true);
    }
  };

  const updateItemQuantity = (
    cartItemId,
    requestJson,
    oldquantity,
    newquantity,
    itemprice
  ) => {
    // api/v1/order-detail/id=<uuid:pk>
    fetch(`/api/v1/order-detail/id=${encodeURIComponent(cartItemId)}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
      body: JSON.stringify(requestJson),
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then(function (resp) {
        console.log(resp);
        if (oldquantity > newquantity) {
          setTotal(
            Number(Total) -
              (Number(oldquantity) - Number(newquantity)) * Number(itemprice)
          );
          setDiscount(
            Number(Discount) -
              (Number(oldquantity) - Number(newquantity)) * Number(itemprice)
          );
        } else {
          setTotal(
            Number(Total) +
              (Number(newquantity) - Number(oldquantity)) * Number(itemprice)
          );
          setDiscount(
            Number(Discount) +
              (Number(newquantity) - Number(oldquantity)) * Number(itemprice)
          );
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleProceedBuySubmit = () => {
    setLoading(true);
    history.push(
      `/my-addresses/buy/${encodeURIComponent(
        CryptoJS.AES.decrypt(
          Cookies.get("id"),
          `${configData.CRYPTO_SECRET_KEY}`
        ).toString(CryptoJS.enc.Utf8)
      )}`
    );
  };

  const removeFromCart = (cartItemId) => {
    fetch(`/api/v1/order-detail/id=${encodeURIComponent(cartItemId)}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
    })
      .then((response) => {
        if (response.ok) {
          return response;
        } else {
          throw new Error(response);
        }
      })
      .then(function (resp) {
        console.log(resp);
        window.location.reload();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const renderCartImage = (images) => {
    if (images.length > 0) {
      return `${configData.IMG_FOLDER_PATH}${images[0].image}`;
    }
    return `${configData.DEFAULT_PRODUCT_IMG}`;
  };

  const renderItemQuantity = (
    stock,
    quantityValue,
    id,
    productId,
    cartItemId,
    itemprice
  ) => {
    let quantityArray = [...initialQuantity];
    if (!quantityArray[id]) {
      quantityArray[id] = quantityValue;
      setInitialQuantity(quantityArray);
    }

    return (
      <div className="cart-item-Quantity-number-input">
        <NumericInput
          type={"real"}
          step={1}
          min={1}
          max={stock}
          value={quantity[id] ? quantity[id] : quantityValue}
          onChange={(e) =>
            handleQuantityChange(e, id, productId, cartItemId, itemprice)
          }
          variant={"secondary"}
          size="sm"
        />
      </div>
    );
  };

  const handleQuantityChange = (e, id, productId, cartItemId, itemprice) => {
    let newArr = [...quantity];

    let oldquantity = 1;
    if (quantity[id]) {
      oldquantity = quantity[id];
    } else {
      oldquantity = initialQuantity[id];
    }

    newArr[id] = e;
    setquantity(newArr);

    const jsonData = {
      buyer: CryptoJS.AES.decrypt(
        Cookies.get("id"),
        `${configData.CRYPTO_SECRET_KEY}`
      ).toString(CryptoJS.enc.Utf8),
      product: productId,
      items_count: e,
      total_payable: 0,
    };

    updateItemQuantity(
      cartItemId,
      jsonData,
      oldquantity,
      newArr[id],
      itemprice
    );
  };

  return (
    <div>
      {isLoading ? (
        <Loader />
      ) : (
        // <div style={{ width: '65%', margin: '3rem auto' }}>
        <div>
          <div>
            <Container className="container-cartitem">
              {CartItems && CartItems.length > 0 ? (
                <div>
                  <div>
                    <Row>
                      <Col>
                        <div className="cart-title">
                          Your Shopping Cart Items
                        </div>
                      </Col>
                    </Row>
                    <Row>
                      <Col></Col>
                      <Col></Col>
                      <Col>
                        <div className="cart-item-header">
                          Price per product
                        </div>
                      </Col>
                    </Row>
                  </div>
                  <div>
                    <hr />
                  </div>
                </div>
              ) : null}

              {CartItems.map((cartItem, id) => (
                <div key={id}>
                  <div>
                    <Row xs={1} sm={1} md={4} key={id}>
                      <Col>
                        <a href={`/product/${cartItem.product.id}`}>
                          <div className="cart-item-img-div">
                            <img
                              alt="product"
                              className="cart-item-img"
                              src={renderCartImage(
                                cartItem.product.product_variant[0]
                                  .product_images
                              )}
                            />
                          </div>
                        </a>
                      </Col>
                      <Col>
                        <div className="cart-item-detail-div">
                          <div className="cart-item-name-div">
                            <a
                              className="cart-item-name"
                              href={`/product/${cartItem.product.id}`}
                            >
                              {cartItem.product.name}
                            </a>
                          </div>
                          <div>#: {cartItem.product.uniqueidentifier}</div>
                          <div>{cartItem.product.description}</div>
                          <div>
                            {cartItem.product.breed} Breed,{" "}
                            {cartItem.product.product_variant[0].age} of age
                          </div>
                          <div>
                            {cartItem.product.product_variant[0].gender} -{" "}
                            {cartItem.product.product_variant[0].weight} kg
                          </div>

                          {cartItem.product.product_variant[0].stock >=
                          cartItem.items_count ? (
                            <div className="cart-item-instock">Instock</div>
                          ) : (
                            <div className="cart-item-outofstock">
                              Out of Stock
                            </div>
                          )}
                          {cartItem.product.product_variant[0].stock > 0 &&
                          cartItem.product.product_variant[0].stock <= 10 ? (
                            <div className="cart-item-left">
                              HURRY! Only{" "}
                              {cartItem.product.product_variant[0].stock}{" "}
                              item(s) left
                            </div>
                          ) : (
                            <div className="cart-item-available-stock">
                              {cartItem.product.product_variant[0].stock -
                                quantity[id] <
                              10 ? (
                                <div>
                                  *Available stock{" "}
                                  {cartItem.product.product_variant[0].stock}{" "}
                                  item(s)
                                </div>
                              ) : null}
                            </div>
                          )}
                        </div>
                      </Col>
                      <Col>
                        <div className="cart-item-quantity-select">
                          <div className="cart-item-Quantity">
                            {renderItemQuantity(
                              cartItem.product.product_variant[0].stock,
                              cartItem.items_count,
                              id,
                              cartItem.product.id,
                              cartItem.id,
                              cartItem.product.product_variant[0].offer_price &&
                                cartItem.product.product_variant[0]
                                  .offer_price > 0
                                ? cartItem.product.product_variant[0]
                                    .offer_price
                                : cartItem.product.product_variant[0]
                                    .original_price
                            )}
                          </div>
                          <div className="cart-item-remove">
                            <Link
                              className="cart-item-remove-link"
                              onClick={() => removeFromCart(cartItem.id)}
                            >
                              Remove
                            </Link>
                          </div>
                          {cartItem.product.seller.is_verified ? (
                            <div className="cart-item--seller-verified">
                              <MdVerifiedUser /> Verified Seller
                            </div>
                          ) : null}
                        </div>
                      </Col>
                      <Col>
                        <div className="cart-item-price">
                          &#8377;{" "}
                          {indianRupeeLocale.format(
                            cartItem.product.product_variant[0].offer_price &&
                              cartItem.product.product_variant[0].offer_price >
                                0
                              ? cartItem.product.product_variant[0].offer_price
                              : cartItem.product.product_variant[0]
                                  .original_price
                          )}
                        </div>
                        {cartItem.product.product_variant[0].offer_price > 0 ? (
                          <h6 className="cart-item-total-discount-amount">
                            you save{" "}
                            <span className="cart-item-total-discount-amount-rupee">
                              {(
                                (cartItem.product.product_variant[0]
                                  .original_price -
                                  cartItem.product.product_variant[0]
                                    .offer_price) /
                                cartItem.product.product_variant[0]
                                  .original_price
                              ).toFixed(2) * 100}{" "}
                              %
                            </span>{" "}
                            on this product
                          </h6>
                        ) : null}
                      </Col>
                    </Row>
                  </div>
                  <div>
                    <hr />
                  </div>
                </div>
              ))}
              {ShowTotal ? (
                <Row xs={1} sm={1} md={2}>
                  <Col></Col>
                  <Col>
                    <div className="cart-item-footer">
                      <h5 className="cart-item-total-amount-label">
                        {" "}
                        Total amount:
                      </h5>
                      <h1 className="cart-item-total-amount">
                        &#8377; {indianRupeeLocale.format(Total)}
                      </h1>
                      {/* <h6 className="cart-item-total-discount-amount">
                                                (you save: <span className="cart-item-total-discount-amount-rupee"> &#8377; {indianRupeeLocale.format(Total)} /- </span>)
                                            </h6> */}
                    </div>
                  </Col>
                  <Col></Col>
                  <Col>
                    <div className="cart-item-proceed-btn">
                      {isOutOfStock ? (
                        <div>
                          <span href="#" id="tooltip-out-of-stock">
                            <Button variant="success" disabled>
                              Continue Order
                            </Button>
                          </span>

                          <UncontrolledTooltip
                            placement="top"
                            target="tooltip-out-of-stock"
                          >
                            Please remove out-of-stock items to Continue your
                            order
                          </UncontrolledTooltip>
                        </div>
                      ) : (
                        <Button
                          variant="success"
                          onClick={handleProceedBuySubmit}
                        >
                          Continue Order
                        </Button>
                      )}
                    </div>
                  </Col>
                </Row>
              ) : null}
              <Row>
                <Col>
                  {!CartItems || CartItems.length == 0 ? (
                    <div
                      style={{
                        width: "100%",
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "center",
                      }}
                    >
                      {Cookies.get("name") == null ? (
                        <div className="cart-empty">
                          <div className="cart-empty-header">
                            You have not logged in yet!
                          </div>
                          <div className="cart-empty-login-link-div">
                            <Button
                              className="cart-empty-login-link"
                              onClick={handleShow}
                            >
                              Signin here to view your cart items
                            </Button>
                          </div>
                          <div>
                            <SignInModal
                              show={show}
                              setModalClose={handleClose}
                              onHide={handleClose}
                            />
                          </div>
                        </div>
                      ) : (
                        <div className="cart-empty">
                          <div className="cart-empty-header">
                            Your Cart is empty
                          </div>
                          <div className="cart-empty-message">
                            You can buy any livestock, livestock accesseries and
                            related products from any where in india through our
                            website in{" "}
                            <a className="cart-empty-buy-link" href="/buy/0">
                              here
                            </a>
                            . We take care of your product delivery for areas we
                            are in operational, also we guarantee that the
                            product you buy is of good quality and hygiene
                          </div>
                        </div>
                      )}
                    </div>
                  ) : null}
                </Col>
              </Row>
            </Container>
          </div>
          <br />
          <br />
          <br />
          <br />
        </div>
      )}
    </div>
  );
}
