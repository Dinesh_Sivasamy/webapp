import slide1 from './images/notification-neworder.png';
import slide2 from './images/buy_adv_banner_1.png';
import slide3 from './images/company-detail.png';
import slide4 from './images/notification-referral.png';


// export const SliderData =[
//     {
//         image: slide1
//     },
//     {
//         image: slide2
//     },
//     {
//         image: slide3
//     }
export const SliderData = [
    { url: slide1 },
    { url: slide2 },
    { url: slide3 },
    { url: slide4 }
];