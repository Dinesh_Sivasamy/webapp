import { SliderData } from "./SliderData";
import Carousel from 'react-bootstrap/Carousel'

export default function Slidercomp() {
  
  return (
    <div>
      {/* <SimpleImageSlider
        width="100%"
        height={500}
        images={SliderData}
        showBullets="true"
        showNavs="true"
        useGPURender="true"
        navStyle="1"
        navSize="50"
        navMargin="30"
        duration="0.2"
        bgColor="#ffffff"
      /> */}
      <Carousel controls={true} interval={5000} indicators={false}>
        <Carousel.Item>
          <img
            src={SliderData[0].url}
          />
        </Carousel.Item>
        <Carousel.Item>
          <img
            src={SliderData[1].url}
          />
        </Carousel.Item>
        <Carousel.Item>
          <img
            src={SliderData[2].url}
          />
        </Carousel.Item>
        <Carousel.Item>
          <img
            src={SliderData[3].url}
          />
        </Carousel.Item>
      </Carousel>
    </div>
  );
}