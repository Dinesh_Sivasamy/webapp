import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import Cookies from "js-cookie";
import CryptoJS from "crypto-js";
import configData from "../../../config.json";
import SignInModal from "../User/SignInModal";
import CategoryList from "../LandingPage/Category";
import Trending from "../LandingPage/Trending";

import TopBanner from "../LandingPage/TopBanner";
import TopBannermobile from "../LandingPage/TopBannermobile";
import Trendingmobile from "../LandingPage/Trendingmobile";
import Services from "../LandingPage/Services";

export default function Home(props) {
  let loginparam = useLocation().search;
  let loginrefcode = loginparam
    ? loginparam.slice(loginparam.indexOf("=") + 1)
    : null;

  console.log(loginparam);
  console.log(loginrefcode);

  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false);
    //window.location.reload();
  };

  const [isLoading, setLoading] = useState(true);
  const [productTrendingList, setProductTrendingList] = useState([]);

  useEffect(() => {
    if (!Cookies.get("name")) {
      validateExistingSession();
    }
  }, []);

  useEffect(() => {
    if (
      !localStorage.getItem("producttrending") ||
      localStorage.getItem("producttrending") == []
    ) {
      fetch("/api/v1/product-detail-list/top=8&category=0&page=home", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            setLoading(false);
            throw new Error(response);
          }
        })
        .then(function (resp) {
          console.log(resp);
          setProductTrendingList(resp);
          localStorage.setItem("producttrending", JSON.stringify(resp));
          setLoading(false);
        });
    } else {
      setProductTrendingList(
        JSON.parse(localStorage.getItem("producttrending"))
      );
    }
  }, []);

  const validateExistingSession = () => {
    fetch("/api/v1/validatesession", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
    })
      .then(function (response) {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then((result) => {
        console.log(JSON.parse(result));
        const resultJson = JSON.parse(result);
        Cookies.set(
          "user",
          CryptoJS.AES.encrypt(
            result,
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(),
          { path: "/" }
        );
        Cookies.set(
          "id",
          CryptoJS.AES.encrypt(
            resultJson.id.toString(),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(),
          { path: "/" }
        );
        Cookies.set(
          "name",
          CryptoJS.AES.encrypt(
            resultJson.name.toString(),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(),
          { path: "/" }
        );
        Cookies.set(
          "phone",
          CryptoJS.AES.encrypt(
            resultJson.phone.toString(),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(),
          { path: "/" }
        );
        Cookies.set(
          "type",
          CryptoJS.AES.encrypt(
            resultJson.usertype.toString(),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(),
          { path: "/" }
        );
        Cookies.set("refcode", resultJson.referralcode.toString());
        window.location.reload();
      })
      .catch((err) => {
        console.log(err);
        setShow(true);
      });
  };

  if (
    loginrefcode != null &&
    loginrefcode.lower != "none" &&
    loginrefcode.length == 10
  ) {
    Cookies.remove("loginrefcode");
    Cookies.set(
      "loginrefcode",
      CryptoJS.AES.encrypt(
        loginrefcode,
        `${configData.CRYPTO_SECRET_KEY}`
      ).toString(),
      { path: "/" }
    );
  }

  const renderImage = (image) => {
    return `${image}`;
  };

  return (
    <div>
      <div>
        <SignInModal
          show={show}
          header="User Login "
          hideclosebutton={false}
          onHide={handleClose}
          setModalClose={handleClose}
        />
      </div>
      {/* <NotificationBoard />
      <Slidercomp />
      <Services /> */}
      <TopBanner />
      <TopBannermobile />
      {/* <Categories /> */}
      {/* {Cookies.get("type") == null ||
      (Cookies.get("type") != null &&
        ["20"].indexOf(
          CryptoJS.AES.decrypt(
            Cookies.get("type"),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(CryptoJS.enc.Utf8)
        ) < 0) ? (
        <Bann />
      ) : null} */}
      {/* <Testimonials /> */}
      <Trending productlist={productTrendingList} />
      <Trendingmobile productlist={productTrendingList} />
      <Services />
      <CategoryList />
    </div>
  );
}
