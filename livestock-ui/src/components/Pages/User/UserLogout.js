import React, { useEffect } from "react";
import Cookies from "js-cookie";
import { useHistory } from "react-router-dom";
import { Button } from "react-bootstrap";

import "./SignIn.css";

export default function Userlogout() {
  const history = useHistory();

  const redirectToHomePage = () => {
    removeCookies();
    // history.push(`/`)
    document.getElementById("redirectbutton").click();
  };

  const removeCookies = () => {
    Cookies.remove("user", { path: "/" });
    Cookies.remove("id", { path: "/" });
    Cookies.remove("name", { path: "/" });
    Cookies.remove("phone", { path: "/" });
    Cookies.remove("rememberMe", { path: "/" });
    Cookies.remove("type", { path: "/" });
    Cookies.remove("refcode", { path: "/" });
    Cookies.remove("refusers");
  };

  useEffect(() => {
    if (performance.navigation.type == 1) {
      redirectToHomePage();
    } else {
      fetch("/api/v1/logout", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error(response);
          }
        })
        .then((result) => {
          console.log(result);
          removeCookies();
          window.location.reload();
        })
        .catch((err) => {
          console.log(err);
          window.location.reload();
        });
    }
  }, []);

  return (
    <div>
      <Button
        href="/"
        id="redirectbutton"
        variant="Link"
        className="logout-redirect-button"
      >
        redirecting to home page
      </Button>
    </div>
  );
}
