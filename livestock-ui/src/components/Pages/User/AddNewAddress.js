import React, { useEffect, useState } from "react";
import { Alert, Button, Col, Container, Row } from "react-bootstrap";
import Select from "react-select";
import PhoneInput from "react-phone-input-2";
import Cookies from "js-cookie";
import { useHistory } from "react-router-dom";
import pincodeDirectory from "india-pincode-lookup";

import { IoMdCheckmarkCircleOutline } from "react-icons/io";
import { TiInfoOutline } from "react-icons/ti";
import "./UserProfile.css";
import { Label } from "reactstrap";

import CryptoJS from "crypto-js";
import configData from "../../../config.json";

export default function AddNewAddress(props) {
  const history = useHistory();

  const [username, setUsername] = useState(props.loggedInUserName);
  const [addressLine1, setaddressLine1] = useState(null);
  const [addressLine2, setaddressLine2] = useState(null);
  const [city, setCity] = useState(null);
  const [state, setState] = useState(null);
  const [country, setCountry] = useState(null);
  const [pincode, setPincode] = useState(null);
  const [deliveryPhone, setDeliveryPhone] = useState(props.loggedInUserPhone);
  const [addressType, setAddressType] = useState(null);

  const [addressTypeList, setAddressTypeList] = useState([]);
  const [deliverablePincodes, setDeliverablePincodes] = useState([]);

  const [validationErrors, setValidationErrors] = useState({});
  const [validationWarn, setValidationWarn] = useState({});
  const [isdeliverablecode, setIsdeliverablecode] = useState(false);
  const [saveSuccess, setSaveSuccess] = useState(false);
  const [deliveryAddressID, setDeliveryAddressID] = useState(null);

  // handle change event of the State dropdown
  const handleStateChange = (obj) => {
    setState(obj);
  };

  const handlePhoneNumberChange = (value, data, event, formattedValue) => {
    setDeliveryPhone(value);
  };

  const handleUserNameChange = (e) => {
    setUsername(e.target.value);
  };

  const handleAddressLine1Change = (e) => {
    setaddressLine1(e.target.value);
  };

  const handleAddressLine2Change = (e) => {
    setaddressLine2(e.target.value);
  };

  const handleCountryChange = (e) => {
    setCountry(e);
  };

  const setPincodeRelatedFiledValues = (
    state,
    city,
    errors,
    warn,
    isdeliverable
  ) => {
    setState(state);
    setCity(city);
    setValidationErrors(errors);
    setValidationWarn(warn);
    setIsdeliverablecode(isdeliverable);
  };

  const handlePincodeChange = (e) => {
    const errors = {};
    const warns = {};
    let isdeliverablepincode = false;
    let selectedpincodedetail = null;

    if (e.target.value.length == 6) {
      setValidationErrors({});
      if (deliverablePincodes && deliverablePincodes[e.target.value]) {
        isdeliverablepincode = true;
        selectedpincodedetail = e.target.value;
      }
    }
    setPincode(e.target.value);
    const locationjson = pincodeDirectory.lookup(e.target.value);
    // if (locationjson && locationjson.length > 0 && isdeliverablepincode && selectedpincodedetail) {
    //     setPincodeRelatedFiledValues(locationjson[0].stateName, locationjson[0].districtName, {}, {}, true)
    // } else if (locationjson && locationjson.length > 0) {
    //     warns.pincodewarning = 'Our Delivery service is not available in this area now. But buyers can always contact to your phone.'
    //     setPincodeRelatedFiledValues(null, null, {}, warns, false)
    // } else {
    //     errors.pincode = 'This Pincode is not valid. Please try different code.'
    //     setPincodeRelatedFiledValues(null, null, errors, {}, false)
    // }

    if (locationjson && locationjson.length > 0) {
      setPincodeRelatedFiledValues(
        locationjson[0].stateName,
        locationjson[0].districtName,
        {},
        {},
        isdeliverablepincode
      );
    } else {
      errors.pincode = "This Pincode is not valid. Please try different code.";
      setPincodeRelatedFiledValues(null, null, errors, {}, false);
    }
  };

  useEffect(() => {
    // fetch("/api/v1/country", {
    //     method: "GET",
    //     headers: {
    //         'Content-Type': 'application/json',
    //         'X-CSRFToken': Cookies.get('csrftoken')
    //     },
    // })
    //     .then((response) => {
    //         if (response.ok) {
    //             return response.json();
    //         } else {
    //             throw new Error(response);
    //         }
    //     })
    //     .then(function (resp) {
    //         console.log(resp);
    //         setCountryList(resp);
    //         setStateList(resp[0].states)
    //         setCityList(resp[0].states[0].cities)
    //     })
    //     .catch((err) => {
    //         console.log(err);
    //     })

    fetch("/api/v1/address-type", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then(function (resp) {
        console.log(resp);
        setAddressTypeList(resp);
      })
      .catch((err) => {
        console.log(err);
      });

    if (Cookies.get("deliveredpincodesdict") == null) {
      fetch("/api/v1/deliverable-pincodes", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error(response);
          }
        })
        .then(function (resp) {
          console.log(resp);
          var deliveredpincodesdict = {};

          resp.forEach((pincodes) => {
            deliveredpincodesdict[pincodes.pincode] = pincodes.isdeliverable;
          });

          setDeliverablePincodes(deliveredpincodesdict);

          Cookies.set(
            "deliveredpincodesdict",
            CryptoJS.AES.encrypt(
              JSON.stringify(deliveredpincodesdict),
              `${configData.CRYPTO_SECRET_KEY}`
            ).toString(),
            { path: "/" }
          );
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      setDeliverablePincodes(
        JSON.parse(
          CryptoJS.AES.decrypt(
            Cookies.get("deliveredpincodesdict"),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(CryptoJS.enc.Utf8)
        )
      );
    }
  }, []);

  const saveNewAddress = (requestJson) => {
    console.log(requestJson);
    fetch(
      `/api/v1/user-address/user=${encodeURIComponent(props.loggedInUserId)}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
        body: JSON.stringify(requestJson),
      }
    )
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then(function (resp) {
        console.log(resp);
        setSaveSuccess(true);
        setDeliveryAddressID(resp.id);
      })
      .catch((err) => {
        console.log(err);
        setValidationErrors({
          saveFail:
            "Failure on address save, Please try again later/contact our support team for more details.",
        });
      });
  };

  const handleAddressSave = (e) => {
    e.preventDefault();
    setValidationErrors({});
    const errors = {};

    if (!username) {
      errors.username = "Name cannot be blank";
      errors.haserror = true;
    }

    if (!deliveryPhone) {
      errors.deliveryPhone = "Phone cannot be blank";
      errors.haserror = true;
    }

    if (deliveryPhone && deliveryPhone.length < 12) {
      errors.deliveryPhone = "Phone number is not in correct format";
      errors.haserror = true;
    }

    if (!addressLine1) {
      errors.addressLine1 = "Address cannot be blank";
      errors.haserror = true;
    }
    if (!addressLine2) {
      errors.addressLine2 = "Address cannot be blank";
      errors.haserror = true;
    }

    if (validationWarn.pincodewarning) {
      if (!state) {
        errors.state = "State cannot be blank";
        errors.haserror = true;
      }
      if (!city) {
        errors.city = "City cannot be blank";
        errors.haserror = true;
      }
      if (!country) {
        errors.country = "Country cannot be blank";
        errors.haserror = true;
      }
    }

    if (!addressType) {
      errors.addressType = "Address type cannot be blank";
      errors.haserror = true;
    }
    if (!pincode) {
      errors.pincode = "Pincode cannot be blank";
      errors.haserror = true;
    }
    if (pincode && pincode.length != 6 && pincode != 641105) {
      errors.pincode = "Pincode not valid";
      errors.haserror = true;
    }

    setValidationErrors(errors);

    if (errors.haserror) {
      return;
    }

    const jsonData = {
      name: username,
      address_1: addressLine1,
      address_2: addressLine2,
      type: addressType.id,
      city: city,
      state: state,
      pincode: pincode,
      isdeliverable: isdeliverablecode,
      phone: deliveryPhone,
      user: props.loggedInUserId,
    };

    saveNewAddress(jsonData);
  };

  const handleSuccessProceed = (e) => {
    e.preventDefault();
    if (props.type == "sell") {
      history.push(`/sell`);
    } else if (props.type == "buy" && deliveryAddressID) {
      // setting up delivery address detail to use in the place order /order confirmation
      fetch(
        `/api/v1/user-delivery-address/id=${encodeURIComponent(
          deliveryAddressID
        )}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "X-CSRFToken": Cookies.get("csrftoken"),
          },
        }
      )
        .then((resp) => {
          if (resp.ok) {
            return resp.json();
          } else {
            throw new Error(resp.statusText);
          }
        })
        .then(function (resp) {
          console.log(resp);

          Cookies.set(
            "deliveryaddressdetail",
            CryptoJS.AES.encrypt(
              JSON.stringify(resp),
              `${configData.CRYPTO_SECRET_KEY}`
            ).toString(),
            { path: "/" }
          );
          history.push(
            `/confirm-order/${encodeURIComponent(deliveryAddressID)}`
          );
        })
        .catch((err) => {
          console.log(err);
        });
    }
    props.addedNewAddress(true);
  };

  return (
    <div className="add-address-container">
      <Container>
        <Row>
          <Col>
            <div className="add-address-heading">Add new address</div>
            <div className="error">
              {validationErrors.saveFail ? (
                <span className="error">{validationErrors.saveFail}</span>
              ) : null}
            </div>
          </Col>
        </Row>
        {saveSuccess ? (
          <div className="add-address-save-success">
            <Alert key={1} variant="success">
              <div className="add-address-heading">
                <IoMdCheckmarkCircleOutline /> Your new address has been saved
                successfully!
                <hr />
              </div>
              <div className="add-address-successproceed-btn">
                <Button variant="primary" onClick={handleSuccessProceed}>
                  Continue
                </Button>
              </div>
            </Alert>
          </div>
        ) : (
          <div>
            <Row lg={6} md={4} xs={1} className="add-address-row">
              <Col className="add-address-col">
                <div className="add-address-input-text">
                  <input
                    className="form-control"
                    onChange={handleUserNameChange}
                    placeholder="Name"
                    value={username}
                  />
                  {validationErrors.username ? (
                    <span className="error">*{validationErrors.username}</span>
                  ) : null}
                </div>
              </Col>
              <Col></Col>
              <Col className="add-address-col">
                <div className="add-address-dropdown-field">
                  <Select
                    placeholder="Address type"
                    value={addressType}
                    options={addressTypeList}
                    onChange={(e) => setAddressType(e)}
                    getOptionLabel={(x) => x.name}
                    getOptionValue={(x) => x.id}
                  />
                  {validationErrors.addressType ? (
                    <span className="error">
                      *{validationErrors.addressType}
                    </span>
                  ) : null}
                </div>
              </Col>

              <Col className="add-address-col">
                <div>
                  <PhoneInput
                    className="add-address-phonenumber"
                    country={"in"}
                    value={deliveryPhone}
                    onChange={handlePhoneNumberChange}
                  />
                  {validationErrors.deliveryPhone ? (
                    <span className="error">
                      *{validationErrors.deliveryPhone}
                    </span>
                  ) : null}
                </div>
              </Col>
            </Row>

            <Row lg={6} md={4} xs={1} className="add-address-row">
              <Col className="add-address-col">
                <div className="add-address-pincode">
                  <input
                    type="number"
                    className="form-control"
                    placeholder="Pincode"
                    maxLength="6"
                    value={pincode}
                    onChange={handlePincodeChange}
                  />
                  {validationErrors.pincode ? (
                    <span className="error">*{validationErrors.pincode}</span>
                  ) : null}
                  {validationWarn.pincodewarning ? (
                    <span className="warn">
                      <TiInfoOutline /> {validationWarn.pincodewarning}
                    </span>
                  ) : null}
                </div>
              </Col>
              <Col className="add-address-col">
                <div className="add-address-input-text">
                  <input
                    className="form-control"
                    onChange={handleAddressLine1Change}
                    placeholder="Flat, House no., Building"
                    value={addressLine1}
                  />
                  {validationErrors.addressLine1 ? (
                    <span className="error">
                      *{validationErrors.addressLine1}
                    </span>
                  ) : null}
                </div>
              </Col>
              <Col></Col>
              <Col className="add-address-col">
                <div className="add-address-input-text">
                  <input
                    className="form-control"
                    onChange={handleAddressLine2Change}
                    placeholder="Area, Colony, Street, Sector, Village"
                    value={addressLine2}
                  />
                  {validationErrors.addressLine2 ? (
                    <span className="error">
                      *{validationErrors.addressLine2}
                    </span>
                  ) : null}
                </div>
              </Col>
            </Row>
            {pincode == null ||
            validationWarn.pincodewarning ||
            validationErrors.pincode ? null : (
              <Row lg={6} md={4} xs={1} className="add-address-row">
                <Col>
                  <div className="add-address-dropdown-field">
                    {city + ", " + state}
                    {validationErrors.city ? (
                      <span className="error">*{validationErrors.city}</span>
                    ) : null}
                  </div>
                </Col>
              </Row>
            )}
            <Row className="add-address-row">
              <Col className="add-address-col">
                <div className="add-address-submit">
                  <Button
                    className="add-address-submitbutton"
                    variant="primary"
                    onClick={handleAddressSave}
                  >
                    Add address
                  </Button>
                </div>
              </Col>
            </Row>
          </div>
        )}
      </Container>
    </div>
  );
}
