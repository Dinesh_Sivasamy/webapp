import React, { useEffect, useState } from "react";
import { Button, Card, Col, Container, Row } from "react-bootstrap";
import { useParams, useHistory } from "react-router-dom";
import Cookies from "js-cookie";
import CryptoJS from "crypto-js";

import "./UserProfile.css";
import AddNewAddress from "../User/AddNewAddress";
import Loader from "../../Loader";

import configData from "../../../config.json";

export default function UserAddress(props) {
  const { type, orderId } = useParams();
  const history = useHistory();

  const [isLoading, setLoading] = useState(true);
  const [addressList, setAddressList] = useState([]);

  const [addedNewAddress, setAddedNewAddress] = useState(false);

  if (addedNewAddress) {
    window.location.reload();
  }

  useEffect(() => {
    window.scrollTo(0, 0);

    if (Cookies.get("id")) {
      fetch(
        `/api/v1/user-address/user=${encodeURIComponent(
          CryptoJS.AES.decrypt(
            Cookies.get("id"),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(CryptoJS.enc.Utf8)
        )}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "X-CSRFToken": Cookies.get("csrftoken"),
          },
        }
      )
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error(response);
          }
        })
        .then(function (resp) {
          console.log(resp);
          setAddressList(resp);
          setLoading(false);
        })
        .catch((err) => {
          console.log(err);
          setLoading(false);
        });
    }
  }, []);

  const updateDeliveryAddress = (addressId) => {
    //'id='+orderId+'&addressId='+addressId
    fetch(
      `/api/v1/update-order-delivery-address/${encodeURIComponent(
        "id=" + orderId + "&addressId=" + addressId
      )}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
      }
    )
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then(function (resp) {
        console.log(resp);
        history.push(`/confirm-order/${encodeURIComponent(addressId)}`);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleProceedClick = (e, addressId) => {
    // setting up delivery address detail to use in the place order /order confirmation
    fetch(`/api/v1/user-delivery-address/id=${encodeURIComponent(addressId)}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
    })
      .then((resp) => {
        if (resp.ok) {
          return resp.json();
        } else {
          throw new Error(resp.statusText);
        }
      })
      .then(function (resp) {
        console.log(resp);

        Cookies.set(
          "deliveryaddressdetail",
          CryptoJS.AES.encrypt(
            JSON.stringify(resp),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(),
          { path: "/" }
        );
        history.push(`/confirm-order/${encodeURIComponent(addressId)}`);
      })
      .catch((err) => {
        console.log(err);
      });

    // updateDeliveryAddress(addressId)
  };

  return (
    <div>
      {isLoading ? (
        <Loader />
      ) : (
        <div className="user-address-container-div">
          <Container>
            <Row>
              <div className="user-address-container-header">
                {type == "buy"
                  ? "Select your delivery address"
                  : type == "sell"
                  ? "Please add an pickup address and proceed Product listing"
                  : "Manage address"}
              </div>
              <div className="user-address-container-headline">
                <hr />
              </div>
            </Row>
            <div className="user-address-card-item">
              <Row>
                {addressList.length > 0 ? (
                  addressList.map((useraddress, id) => {
                    return (
                      <Col className="user-address-card-col" key={id}>
                        <Card
                          border="success"
                          style={{ width: "18rem" }}
                          key={id}
                        >
                          <Card.Body>
                            <Card.Title>{useraddress.address.name}</Card.Title>
                            <Card.Subtitle className="mb-2 text-muted">
                              {"Type: " + useraddress.address.type}
                            </Card.Subtitle>
                            <Card.Text>
                              {useraddress.address.address_1}
                              <br />
                              {useraddress.address.address_2}
                              <br />
                              {useraddress.address.city +
                                ", " +
                                useraddress.address.state +
                                " - " +
                                useraddress.address.pincode}
                              <br />
                              Phone: {useraddress.address.phone}
                            </Card.Text>
                            {type == "buy" ? (
                              <Card.Footer>
                                <div className="user-address-card-footer-deliver-btn">
                                  <Button
                                    variant="primary"
                                    onClick={(e) =>
                                      handleProceedClick(e, useraddress.id)
                                    }
                                  >
                                    Proceed with this address
                                  </Button>
                                </div>
                              </Card.Footer>
                            ) : null}
                          </Card.Body>
                        </Card>
                      </Col>
                    );
                  })
                ) : (
                  <div className="user-address-unavail-error">
                    No existing address available, Please add a new address
                    below.
                  </div>
                )}
              </Row>
            </div>
            <Row>
              <AddNewAddress
                type={type}
                addedNewAddress={setAddedNewAddress}
                loggedInUserId={
                  Cookies.get("id")
                    ? CryptoJS.AES.decrypt(
                        Cookies.get("id"),
                        `${configData.CRYPTO_SECRET_KEY}`
                      ).toString(CryptoJS.enc.Utf8)
                    : null
                }
                loggedInUserName={
                  Cookies.get("name")
                    ? CryptoJS.AES.decrypt(
                        Cookies.get("name"),
                        `${configData.CRYPTO_SECRET_KEY}`
                      ).toString(CryptoJS.enc.Utf8)
                    : null
                }
                loggedInUserPhone={
                  Cookies.get("phone")
                    ? CryptoJS.AES.decrypt(
                        Cookies.get("phone"),
                        `${configData.CRYPTO_SECRET_KEY}`
                      ).toString(CryptoJS.enc.Utf8)
                    : null
                }
              />
            </Row>
          </Container>
        </div>
      )}
    </div>
  );
}
