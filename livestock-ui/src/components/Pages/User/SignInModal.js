import React, { useEffect, useState } from "react";
import Cookies from "js-cookie";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Select from "react-select";
import PhoneInput from "react-phone-input-2";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import ToggleButton from "react-bootstrap/ToggleButton";
import CryptoJS from "crypto-js";

import "./SignIn.css";
import configData from "../../../config.json";
import { Link } from "react-router-dom";

export default function SignInModal(props) {
  const [showSignup, setshowSignup] = useState(false);

  const rememberMeChecked = localStorage.getItem("rememberMe") ? true : false;
  const [rememberMe, setRememberMe] = useState(rememberMeChecked);

  const [SaveUserSignInResponseData, setSaveUserSignInResponseData] =
    useState(null);
  const [SaveUserSignInErrorResponse, setSaveUserSignInErrorResponse] =
    useState(null);

  const handleRememberMe = () => {
    setRememberMe(!rememberMe);
    if (!rememberMe) {
      localStorage.removeItem("rememberMe");
    }
  };

  const [validationErrors, setValidationErrors] = useState({});

  const userPhoneInitial = localStorage.getItem("rememberMe")
    ? localStorage.getItem("rememberMe")
    : "";

  const [UserPhone, setUserPhone] = useState(userPhoneInitial);
  const [Password, setPassword] = useState(null);
  const [UserType, setUserType] = useState(null);
  const [UserName, setUserName] = useState(null);
  const [UserTitle, setUserTitle] = useState(1);
  const [UserDegree, setUserDegree] = useState(null);
  const [UserPanNumber, setUserPanNumber] = useState(null);

  const [newlyCreatedUserId, setNewlyCreatedUserId] = useState(null);
  const [mobileotp, setMobileotp] = useState(null);
  const [otpSent, setOtpSent] = useState(null);
  const [isotpsent, setIsotpsent] = useState(
    Cookies.get("isverificationpending") ? true : false
  );

  const [forgotPasswordOtp, setForgotPasswordOtp] = useState(null);

  const [registerReferralCode, setRegisterReferralCode] = useState(
    Cookies.get("loginrefcode") != null
      ? CryptoJS.AES.decrypt(
          Cookies.get("loginrefcode"),
          `${configData.CRYPTO_SECRET_KEY}`
        ).toString(CryptoJS.enc.Utf8)
      : null
  );
  const [userhasreferralcode, setUserhasreferralcode] = useState(false);

  const [UserTypeList, setUserTypeList] = useState([]);
  const [UserTitleList, setUserTitleList] = useState([]);

  const [isPasswordResetFlow, setIsPasswordResetFlow] = useState(false);
  const [isLoginflow, setIsLoginflow] = useState(true);

  const handleShowSignUp = () => {
    setshowSignup(true);
    setValidationErrors({});
  };

  const handleCloseSigUp = () => {
    setshowSignup(false);
    setValidationErrors({});
  };

  const handleUserNameChange = (e) => {
    setUserName(e.target.value);
    validationErrors.username = "";
  };

  const handleUserDegreeChange = (e) => {
    setUserDegree(e.target.value);
  };

  const handlePhoneNumberChange = (value, data, event, formattedValue) => {
    setUserPhone(value);
    validationErrors.phone = "";
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
    validationErrors.password = "";
  };

  const handleMobileOTPChange = (e) => {
    setMobileotp(e.target.value);
  };

  // handle change event of the Usertitle dropdown
  const handleUserTitleChange = (obj) => {
    setUserTitle(obj);
  };

  const handleUseReferralCodeShow = () => {
    setUserhasreferralcode(!userhasreferralcode);
  };

  const clearFields = () => {
    setUserName(null);
    setUserPhone(null);
    setUserTitle(null);
    setUserType(null);
    setPassword(null);
  };

  let passwordresetotp = "";

  useEffect(() => {
    if (showSignup) {
      //calling user type api on edit button clickchange
      fetch("/api/v1/user-type", {
        method: "GET",
        headers: {
          "content-type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error(response);
          }
        })
        .then(function (resp) {
          console.log(resp);
          setUserTypeList(resp);
        });

      //calling user title api on edit button clickchange
      fetch("/api/v1/user-title", {
        method: "GET",
        headers: {
          "content-type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error(response);
          }
        })
        .then(function (resp) {
          console.log(resp);
          setUserTitleList(resp);
        });
    }
  }, [showSignup]);

  const authenticateUser = (requestJson) => {
    fetch("/api/v1/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
      body: JSON.stringify(requestJson),
    })
      .then(function (response) {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then((result) => {
        console.log(JSON.parse(result));
        const resultJson = JSON.parse(result);
        Cookies.set(
          "user",
          CryptoJS.AES.encrypt(
            result,
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(),
          { path: "/" }
        );
        Cookies.set(
          "id",
          CryptoJS.AES.encrypt(
            resultJson.id.toString(),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(),
          { path: "/" }
        );
        Cookies.set(
          "name",
          CryptoJS.AES.encrypt(
            resultJson.name.toString(),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(),
          { path: "/" }
        );
        Cookies.set(
          "phone",
          CryptoJS.AES.encrypt(
            resultJson.phone.toString(),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(),
          { path: "/" }
        );
        if (rememberMe) {
          localStorage.setItem("rememberMe", resultJson.phone.toString());
        }
        Cookies.set(
          "type",
          CryptoJS.AES.encrypt(
            resultJson.usertype.toString(),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(),
          { path: "/" }
        );
        Cookies.set("refcode", resultJson.referralcode.toString());
        Cookies.remove("isverificationpending");
        setSaveUserSignInResponseData(result);
        props.setModalClose();
      })
      .catch((err) => {
        console.log(err);
        setValidationErrors({
          loginFail:
            "Authentication failed. Try resetting password using Forgot password option below!",
        });
      });
  };

  const activateUser = (requestJson) => {
    fetch("/api/v1/userprofile/id=" + newlyCreatedUserId, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
      body: JSON.stringify(requestJson),
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw response;
        }
      })
      .then((result) => {
        setSaveUserSignInResponseData(result.rows);

        const jsonData = {
          username: "+" + UserPhone,
          password: Password,
        };
        authenticateUser(jsonData);
      })
      .catch((err) => {
        err.text().then((errorMessage) => {
          console.log(errorMessage);
          setValidationErrors({ userRegisterFail: errorMessage });
        });
      });
  };

  const VerifySubmit = () => {
    if (mobileotp) {
      const jsonData = {
        password: Password,
        phone: "+" + UserPhone,
        username: UserName,
        userdegree: UserDegree,
        is_admin: false,
        is_staff: false,
        is_active: true,
        is_superuser: false,
        otp: mobileotp,
        verify_otp: true,
        title: UserTitle.id,
        usertype: UserType,
        sellerpannumber: UserPanNumber,
        loginreferralcode: registerReferralCode,
      };

      activateUser(jsonData);
    }
  };

  const registerUser = (requestJson) => {
    fetch("/api/v1/user", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
      body: JSON.stringify(requestJson),
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw response;
        }
      })
      .then((result) => {
        setNewlyCreatedUserId(result.id);
        var inFifteenMinutes = new Date(new Date().getTime() + 15 * 60 * 1000);
        Cookies.set("isverificationpending", "yes", {
          path: "/",
          expires: inFifteenMinutes,
        });
        setIsotpsent(true);
      })
      .catch((err) => {
        console.log("Caught error");
        err.text().then((errorMessage) => {
          console.log(errorMessage);
          let ind1 = errorMessage.indexOf('["') + 1;
          let ind2 = errorMessage.indexOf('"],');
          let ind3 = errorMessage.lastIndexOf('"]}');

          let error = "";
          if (ind2 > 0) {
            error = errorMessage.substring(ind1 + 1, ind2);
          } else {
            error = errorMessage.substring(ind1, ind3);
          }
          console.log(error);
          setValidationErrors({ userRegisterFail: error });
        });
      });
  };

  const loginSubmit = (evt) => {
    evt.preventDefault();
    setValidationErrors({});
    const errors = {};

    if (!UserPhone) {
      errors.phone = "Phone number cannot be blank";
      errors.hasvalue = true;
    }

    if (!Password) {
      errors.password = "Password cannot be blank";
      errors.hasvalue = true;
    }

    setValidationErrors(errors);

    if (errors.hasvalue) {
      return;
    }

    const jsonData = {
      username: "+" + UserPhone,
      password: Password,
    };

    authenticateUser(jsonData);
  };

  const RegisterSubmit = (evt) => {
    evt.preventDefault();
    setValidationErrors({});
    const errors = {};

    if (!UserPhone) {
      errors.phone = "Phone number cannot be blank";
      errors.hasvalue = true;
    }

    if (UserPhone && UserPhone.length < 12) {
      errors.phone = "Phone number is not in correct format";
      errors.hasvalue = true;
    }

    if (!Password) {
      errors.password = "Password cannot be blank";
      errors.hasvalue = true;
    }
    if (!UserName) {
      errors.username = "Name cannot be blank";
      errors.hasvalue = true;
    }
    if (UserType == null) {
      errors.usertype = "Type is required";
      errors.hasvalue = true;
    }

    if (1 == 2 && UserPanNumber == null) {
      errors.pannumber = "Pan number is required to sell products";
      errors.hasvalue = true;
    }

    if (
      UserPanNumber != null &&
      UserPanNumber != "" &&
      !pancardValidation(UserPanNumber)
    ) {
      errors.pannumber = "Please provide a valid PAN card Number.";
      errors.hasvalue = true;
    }

    setValidationErrors(errors);

    if (errors.hasvalue) {
      return;
    }

    let userTitleSelected = "0";

    if (UserTitle) {
      userTitleSelected = UserTitle.id;
    }

    const jsonData = {
      password: Password,
      phone: "+" + UserPhone,
      username: UserName,
      userdegree: UserDegree,
      is_admin: false,
      is_staff: false,
      is_active: false,
      is_superuser: false,
      title: userTitleSelected,
      usertype: UserType,
      sellerpannumber: UserPanNumber,
      loginreferralcode: registerReferralCode,
    };

    registerUser(jsonData);
  };

  const pancardValidation = (text) => {
    let regex = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
    if (regex.test(text)) {
      return true;
    }
    return false;
  };

  const handleForgotpasswordlinkclick = () => {
    let logindiv = document.getElementById("modal-login");
    logindiv.style.display = "none";

    setIsPasswordResetFlow(true);
    setIsLoginflow(false);
  };

  const handleForgotpasswordCancelclick = () => {
    setIsPasswordResetFlow(false);
    setIsLoginflow(true);

    let logindiv = document.getElementById("modal-login");
    logindiv.style.display = "block";
  };

  const handlePanNumberChange = (e) => {
    setUserPanNumber(e.target.value);
    if (!pancardValidation(e.target.value)) {
      validationErrors.pannumber = "Please provide a valid PAN card Number.";
    } else {
      validationErrors.pannumber = "";
    }
  };

  const handlesendForgotpasswordOtp = () => {
    console.log("sending otp for reset password");

    setValidationErrors({});
    const errors = {};

    if (!UserPhone) {
      errors.phone = "Phone number cannot be blank";
      errors.hasvalue = true;
    }

    if (UserPhone && UserPhone.length < 12) {
      errors.phone = "Phone number is not in correct format";
      errors.hasvalue = true;
    }

    setValidationErrors(errors);

    if (errors.hasvalue) {
      return;
    }

    fetch("/api/v1/init-forgot-password/userphone=+" + UserPhone, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw response;
        }
      })
      .then((result) => {
        setIsotpsent(true);
        setIsPasswordResetFlow(true);
        setForgotPasswordOtp(result.otp);
      })
      .catch((err) => {
        console.log("Caught error");
        err.text().then((errorMessage) => {
          console.log(errorMessage);
          console.log(errorMessage.indexOf("<title>DoesNotExist"));
          if (errorMessage.indexOf("<title>DoesNotExist") > 0) {
            console.log(
              errorMessage.indexOf("inside phone number not present")
            );
            setIsotpsent(false);
            setValidationErrors({
              loginFail:
                "This Phone number doesn't exists in our system, Please create your account through New User Registration button!",
            });
          } else {
            let ind1 = errorMessage.indexOf('["') + 1;
            let ind2 = errorMessage.indexOf('"],');
            let ind3 = errorMessage.lastIndexOf('"]}');

            let error = "";
            if (ind2 > 0) {
              error = errorMessage.substring(ind1 + 1, ind2);
            } else {
              error = errorMessage.substring(ind1, ind3);
            }
            console.log(error);
            setIsotpsent(false);
            setValidationErrors({ loginFail: error });
          }
        });
      });
    setIsotpsent(true);
    setIsPasswordResetFlow(true);
  };

  const handleSaveNewPassword = () => {
    console.log("saving new password");

    setValidationErrors({});
    const errors = {};

    if (!mobileotp) {
      errors.mobileotp = "OTP cannot be blank";
      errors.hasvalue = true;
    }

    if (!Password) {
      errors.password = "Password cannot be blank";
      errors.hasvalue = true;
    }

    if (mobileotp != forgotPasswordOtp) {
      errors.loginFail = "OTP is not matching";
      errors.hasvalue = true;
    }

    setValidationErrors(errors);

    if (errors.hasvalue) {
      return;
    }

    const requestJson = {
      otp: mobileotp,
      password: Password,
    };

    fetch("/api/v1/reset-password/userphone=+" + UserPhone, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
      body: JSON.stringify(requestJson),
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw response;
        }
      })
      .then((result) => {
        console.log(JSON.parse(result));
        setIsotpsent(false);

        const resultJson = JSON.parse(result);
        Cookies.set(
          "user",
          CryptoJS.AES.encrypt(
            result,
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(),
          { path: "/" }
        );
        Cookies.set(
          "id",
          CryptoJS.AES.encrypt(
            resultJson.id.toString(),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(),
          { path: "/" }
        );
        Cookies.set(
          "name",
          CryptoJS.AES.encrypt(
            resultJson.name.toString(),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(),
          { path: "/" }
        );
        Cookies.set(
          "phone",
          CryptoJS.AES.encrypt(
            resultJson.phone.toString(),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(),
          { path: "/" }
        );
        if (rememberMe) {
          localStorage.setItem("rememberMe", resultJson.phone.toString());
        }
        Cookies.set(
          "type",
          CryptoJS.AES.encrypt(
            resultJson.usertype.toString(),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(),
          { path: "/" }
        );
        Cookies.set("refcode", resultJson.referralcode.toString());
        setSaveUserSignInResponseData(result);
        props.setModalClose();
      })
      .catch((err) => {
        console.log("Caught error");
        err.text().then((errorMessage) => {
          console.log(errorMessage);
          let ind1 = errorMessage.indexOf('["') + 1;
          let ind2 = errorMessage.indexOf('"],');
          let ind3 = errorMessage.lastIndexOf('"]}');

          let error = "";
          if (ind2 > 0) {
            error = errorMessage.substring(ind1 + 1, ind2);
          } else {
            error = errorMessage.substring(ind1, ind3);
          }
          console.log(error);
          setValidationErrors({ loginFail: error });
        });
      });
  };

  return (
    <div>
      {!showSignup ? (
        isPasswordResetFlow ? (
          <Modal {...props} backdrop="static" keyboard={true} centered>
            <form>
              <Modal.Header closeButton={true}>
                <Modal.Title>{"Password Reset"}</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                {isotpsent ? (
                  <div>
                    <div className="form-group">
                      <label>Verify OTP</label>
                      {/* <input type="Phone" className="form-control" placeholder="Enter Phone number" /> */}
                      <div className="sign-in-password-field">
                        <input
                          type="text"
                          className="form-control"
                          value={mobileotp}
                          onChange={handleMobileOTPChange}
                          placeholder="Enter reset password OTP"
                        />
                        {validationErrors.mobileotp ? (
                          <span className="error">
                            *{validationErrors.mobileotp}
                          </span>
                        ) : null}
                      </div>
                    </div>
                    {mobileotp && mobileotp.length > 5 ? (
                      <div className="form-group">
                        <label>New Password</label>
                        <div className="sign-in-password-field">
                          <input
                            type="password"
                            className="form-control"
                            placeholder="Enter your new password"
                            value={Password}
                            onChange={handlePasswordChange}
                          />
                          {validationErrors.password ? (
                            <span className="error">
                              *{validationErrors.password}
                            </span>
                          ) : null}
                        </div>
                      </div>
                    ) : null}
                  </div>
                ) : (
                  <div>
                    <div className="form-group">
                      <label>Phone number</label>
                      {/* <input type="Phone" className="form-control" placeholder="Enter Phone number" /> */}
                      <PhoneInput
                        placeholder="Enter Phone number"
                        country={"in"}
                        value={UserPhone}
                        required
                        inputStyle={{ width: "auto" }}
                        onChange={handlePhoneNumberChange}
                      />
                      {validationErrors.phone ? (
                        <span className="error">*{validationErrors.phone}</span>
                      ) : null}
                    </div>
                  </div>
                )}
              </Modal.Body>
              {validationErrors.loginFail ? (
                <span className="error-backend">
                  *{validationErrors.loginFail}
                </span>
              ) : null}
              <Modal.Footer>
                {isotpsent ? (
                  <div className="sign-in-footer">
                    <Button
                      className="btn btn-primary btn-lg btn-block"
                      onClick={handleSaveNewPassword}
                    >
                      Save New Password & Login
                    </Button>
                    <div className="cancel-text">
                      {/* <a onClick={() => setIsPasswordResetFlow(false)}>Cancel</a> */}
                    </div>
                  </div>
                ) : (
                  <div
                    id="div-forgot-password-send-otp"
                    className="sign-in-footer"
                  >
                    <Button
                      type="submit"
                      className="btn btn-primary btn-lg btn-block"
                      onClick={handlesendForgotpasswordOtp}
                    >
                      Send OTP
                    </Button>
                    <div className="cancel-text">
                      <a onClick={handleForgotpasswordCancelclick}>Cancel</a>
                    </div>
                  </div>
                )}
              </Modal.Footer>
            </form>
          </Modal>
        ) : (
          <div>
            <Modal
              id="modal-login"
              {...props}
              backdrop="static"
              keyboard={true}
            >
              <form onSubmit={loginSubmit}>
                <Modal.Header closeButton={true}>
                  <Modal.Title>
                    {props.header ? props.header : "User Login"}
                  </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <div>
                    <div className="form-group">
                      <label>Phone number</label>
                      {/* <input type="Phone" className="form-control" placeholder="Enter Phone number" /> */}
                      <PhoneInput
                        placeholder="Enter Phone number"
                        country={"in"}
                        value={UserPhone}
                        inputStyle={{ width: "auto" }}
                        required
                        onChange={handlePhoneNumberChange}
                      />
                      {validationErrors.phone ? (
                        <span className="error">*{validationErrors.phone}</span>
                      ) : null}
                    </div>

                    <div className="form-group">
                      <label>Password</label>
                      <div className="sign-in-password-field">
                        <input
                          type="password"
                          className="form-control"
                          placeholder="Enter password"
                          value={Password}
                          onChange={handlePasswordChange}
                        />
                        {validationErrors.password ? (
                          <span className="error">
                            *{validationErrors.password}
                          </span>
                        ) : null}
                      </div>
                    </div>
                  </div>
                </Modal.Body>
                {validationErrors.loginFail ? (
                  <span className="error-backend">
                    *{validationErrors.loginFail}
                  </span>
                ) : null}
                <Modal.Footer>
                  <div className="sign-in-footer">
                    <Button
                      type="submit"
                      className="btn btn-primary btn-lg btn-block"
                    >
                      Login
                    </Button>
                    <div className="sign-in-forgot-password-link">
                      <Button
                        variant="link"
                        onClick={handleForgotpasswordlinkclick}
                      >
                        Forgot Password?
                      </Button>
                    </div>
                    <div className="form-group cancel-text">
                      <div className="custom-control custom-checkbox">
                        <input
                          type="checkbox"
                          checked={rememberMe}
                          onChange={handleRememberMe}
                          className="custom-control-input"
                          id="rememberMe"
                        />
                      </div>
                      <label
                        className="custom-control-label"
                        htmlFor="rememberMe"
                      >
                        Remember me
                      </label>
                    </div>
                    <hr />

                    <Button
                      className="btn btn-success btn-lg btn-block"
                      onClick={handleShowSignUp}
                    >
                      New User Registration
                    </Button>
                    <div className="cancel-text">
                      <a href="/" onClick={() => setIsPasswordResetFlow(false)}>
                        Close
                      </a>
                    </div>
                  </div>
                </Modal.Footer>
              </form>
            </Modal>
          </div>
        )
      ) : (
        <Modal {...props} backdrop="static" keyboard={true}>
          <Modal.Header closeButton={true}>
            <Modal.Title>New User Registration</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form class="form-horizontal" onSubmit={RegisterSubmit}>
              {isotpsent ? (
                <div className="user-reg-div-mobile-otp">
                  <label>Verify Mobile OTP</label>
                  {/* <input type="Phone" className="form-control" placeholder="Enter Phone number" /> */}
                  <input
                    type="text"
                    className="form-control"
                    value={mobileotp}
                    onChange={handleMobileOTPChange}
                    placeholder="Enter OTP"
                  />
                  {validationErrors.phone ? (
                    <span className="error">*{validationErrors.phone}</span>
                  ) : null}
                </div>
              ) : (
                <div>
                  <div className="form-group">
                    <label>I am </label>
                    <div className="sign-in-usertype">
                      <ButtonGroup>
                        {UserTypeList.map((radio, idx) =>
                          radio.id != 0 && radio.id != 9 ? (
                            <ToggleButton
                              key={idx}
                              id={`radio-${idx}`}
                              type="radio"
                              variant="outline-success"
                              name="radio"
                              value={radio.id}
                              checked={UserType == radio.id}
                              onChange={(e) =>
                                setUserType(e.currentTarget.value)
                              }
                            >
                              {" " + radio.name}
                            </ToggleButton>
                          ) : null
                        )}
                      </ButtonGroup>
                    </div>
                    {validationErrors.usertype ? (
                      <span className="error">
                        *{validationErrors.usertype}
                      </span>
                    ) : null}
                  </div>
                  <div className="form-group">
                    <label>Phone number</label>
                    {/* <input type="Phone" className="form-control" placeholder="Enter Phone number" /> */}
                    <PhoneInput
                      placeholder="Enter Phone number"
                      country={"in"}
                      value={UserPhone}
                      inputStyle={{ width: "auto" }}
                      onChange={handlePhoneNumberChange}
                    />
                    {validationErrors.phone ? (
                      <span className="error">*{validationErrors.phone}</span>
                    ) : null}
                  </div>
                  <div className="form-group">
                    <label>Name</label>
                    <div className="sign-in-display-inline">
                      <div className="user-reg-title-field">
                        <Select
                          className="user-title-dropdown"
                          placeholder="Title"
                          value={UserTitle}
                          options={UserTitleList}
                          onChange={handleUserTitleChange}
                          getOptionLabel={(x) => x.title}
                          getOptionValue={(x) => x.id}
                        />
                      </div>

                      <div className="user-reg-name-field">
                        <input
                          type="text"
                          className="form-control"
                          value={UserName}
                          onChange={handleUserNameChange}
                          placeholder="Enter Name"
                        />
                        {validationErrors.username ? (
                          <span className="error">
                            *{validationErrors.username}
                          </span>
                        ) : null}
                      </div>

                      {UserType == 5 ? (
                        <div className="user-reg-degree-field">
                          <input
                            type="text"
                            className="form-control"
                            value={UserDegree}
                            onChange={handleUserDegreeChange}
                            placeholder="Enter Degree"
                          />
                        </div>
                      ) : null}
                    </div>
                  </div>
                  <div className="sign-in-display-inline">
                    <div>
                      <label>Password</label>
                      <div className="user-reg-password-field">
                        <input
                          type="password"
                          className="form-control"
                          placeholder="Enter password"
                          value={Password}
                          onChange={handlePasswordChange}
                        />
                        {validationErrors.password ? (
                          <span className="error">
                            *{validationErrors.password}
                          </span>
                        ) : null}
                      </div>
                    </div>
                  </div>
                  {UserType == 2 || UserType == 3 || UserType == 4 ? (
                    <div className="user-reg-pan-number">
                      <label>PAN Number</label>
                      {/* <input type="Phone" className="form-control" placeholder="Enter Phone number" /> */}
                      <input
                        type="text"
                        className="form-control"
                        value={UserPanNumber}
                        onChange={handlePanNumberChange}
                        placeholder="Enter PAN Number"
                      />
                      {validationErrors.pannumber ? (
                        <span className="error">
                          *{validationErrors.pannumber}
                        </span>
                      ) : null}
                    </div>
                  ) : null}
                </div>
              )}
            </form>
          </Modal.Body>
          <Modal.Footer>
            {validationErrors.userRegisterFail ? (
              <span className="error">
                *{validationErrors.userRegisterFail}
              </span>
            ) : null}
            <div className="custom-control">
              <label>
                By signing up, I agree and accept to FarmZonn's <span> </span>
                <Link to="/terms" target="_blank" rel="noopener noreferrer">
                  Terms & Conditions
                </Link>
                <span> </span> and <span> </span>
                <Link
                  to="/privacy-policy"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Privacy Policy
                </Link>
              </label>
            </div>
            {isotpsent ? (
              <Button
                type="submit"
                className="btn btn-primary btn-lg btn-block"
                onClick={VerifySubmit}
              >
                {" "}
                Verify
              </Button>
            ) : (
              <Button
                type="submit"
                className="btn btn-primary btn-lg btn-block"
                onClick={RegisterSubmit}
              >
                {" "}
                Register
              </Button>
            )}
            <div className="cancel-text">
              <a href="/">Close</a>
            </div>
            /
            <div className="signin-text">
              Already registered{" "}
              <a href="#" onClick={handleCloseSigUp}>
                sign in?
              </a>
            </div>
          </Modal.Footer>
        </Modal>
      )}
    </div>
  );
}
