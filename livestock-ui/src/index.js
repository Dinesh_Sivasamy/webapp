import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import "bootstrap/dist/css/bootstrap.css";
import ReactGA from "react-ga4";

import { CookiesProvider } from "react-cookie";

ReactGA.initialize("G-T8SVNTPL8G");

ReactDOM.render(
  <CookiesProvider>
    <App />
  </CookiesProvider>,
  document.getElementById("root")
);
