import React, { useEffect } from "react";
import Footer from "./components/Pages/LandingPage/Footer";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import Buy from "./components/Pages/ProductBuy/Buy";
import ProductDetail from "./components/Pages/ProductBuy/ProductDetail";
import Sell from "./components/Pages/ProductSell/Sell";
import SellerProdct from "./components/Pages/SellerProducts/SellerProductsPage";
import VetProductListPage from "./components/Pages/VetPages/VetProductListPage";
import Cart from "./components/Pages/UserCart/CartPage";
import Home from "./components/Pages/Home/Home";
import AboutUs from "./components/Pages/Site-details/AboutUs";
import Howitworks from "./components/Pages/Site-details/How-it-works";
import Careers from "./components/Pages/Site-details/Careers";
import Support from "./components/Pages/Site-details/Support";
import UserProfileDetail from "./components/Pages/User/UserProfile";
import UserOrders from "./components/Pages/UserOrder/OrderPage";
import PlaceOrder from "./components/Pages/UserOrder/PlaceOrder";
import UserAddresses from "./components/Pages/User/UserAddress";
import Userlogout from "./components/Pages/User/UserLogout";
import ServicesUnderConstruction from "./components/Pages/UnderConstruction";
import Terms from "./components/Pages/Site-details/Terms";
import Privacypolicy from "./components/Pages/Site-details/Privacypolicy";
import Enquiry from "./components/Pages/UserRequirement/Enquiry";
import ViewRequirements from "./components/Pages/UserRequirement/RequirementsList";
import ViewMyEnquiries from "./components/Pages/UserRequirement/UserRequirementsList";
import Threads from "./components/Pages/DiscussionForum/Threads";
import Replies from "./components/Pages/DiscussionForum/Replies";
import ReactGA from "react-ga";
import UserTestimonial from "./components/Pages/LandingPage/UserTestimonial";
import FooterBanner from "./components/Pages/LandingPage/FooterBanner";
import UserTestimonialmobile from "./components/Pages/LandingPage/UserTestimonialmobile";
import Header from "./components/Pages/LandingPage/Header";
import Menubar from "./components/Pages/LandingPage/Menubar";
import Services from "./components/Pages/LandingPage/Services";

import Jerseycattlesaleincoimbatore from "./components/Pages/SEO/jerseycattlesaleincoimbatore";
import Farmanimalspetsincoimbatore from "./components/Pages/SEO/farmanimalspetsincoimbatore";
import Goatfarmingincoimbatore from "./components/Pages/SEO/goatfarmingincoimbatore";
import Petshopsincoimbatore from "./components/Pages/SEO/petshopsincoimbatore";
import Indanmilkcowbreedssale from "./components/Pages/SEO/Indianmilkcowbreedssale";

const TRACKING_ID = "UA-249679917-1"; // OUR_TRACKING_ID
ReactGA.initialize(TRACKING_ID);

//ReactGA.initialize("G-T8SVNTPL8G");

function App() {
  window.dataLayer.push({
    event: "pageview",
    url: window.location.pathname,
  });

  useEffect(() => {
    ReactGA.send({
      hitType: "pageview",
      page: window.location.pathname + window.location.search,
    });
  }, []);

  return (
    <Router>
      <Header />
      <Menubar />
      <Switch>
        <Route exact path="/" element={<Home />} />
        <Route exact path="/refcode/:loginrefcode" element={<Home />} />
        <Route exact path="/buy/:typeId" element={<Buy />} />
        <Route exact path="/product/:productId" element={<ProductDetail />} />
        <Route
          exact
          path="/product/related/:productId"
          element={<ProductDetail />}
        />
        <Route exact path="/sell" element={<Sell />} />
        <Route exact path="/seller-product/" element={<SellerProdct />} />
        <Route exact path="/vet-verify/" element={<VetProductListPage />} />
        <Route
          exact
          path="/userprofile/:userId"
          element={<UserProfileDetail />}
        />
        <Route exact path="/my-cart" element={<Cart />} />
        <Route
          exact
          path="/confirm-order/:addressId"
          element={<PlaceOrder />}
        />
        <Route exact path="/orders" element={<UserOrders />} />
        <Route
          exact
          path="/my-addresses/:type/:orderId"
          element={<UserAddresses />}
        />
        <Route exact path="/aboutus" element={<AboutUs />} />
        <Route exact path="/how-it-works" element={<Howitworks />} />
        <Route exact path="/careers" element={<Careers />} />
        <Route exact path="/support" element={<Support />} />
        <Route exact path="/logout" element={<Userlogout />} />
        <Route
          exact
          path="/other-services"
          element={<ServicesUnderConstruction />}
        />
        <Route exact path="/terms" element={<Terms />} />
        <Route exact path="/privacy-policy" element={<Privacypolicy />} />
        <Route exact path="/add-enquiry" element={<Enquiry />} />
        <Route exact path="/view-enquiry/:enquiryid" element={<Enquiry />} />
        <Route exact path="/view-requirements" element={<ViewRequirements />} />
        <Route exact path="/my-enquiries" element={<ViewMyEnquiries />} />
        <Route exact path="/forum" element={<Threads />} />
        <Route exact path="/services" element={<Services />} />
        <Route
          exact
          path="/forum/thread/:threadid/replies"
          element={<Replies />}
        />

        <Route
          exact
          path="/jersey-cattle-sale-coimbatore"
          element={<Jerseycattlesaleincoimbatore />}
        />
        <Route
          exact
          path="/farm-animals-pets-in-coimbatore"
          element={<Farmanimalspetsincoimbatore />}
        />
        <Route
          exact
          path="/goat-farming-in-coimbatore"
          element={<Goatfarmingincoimbatore />}
        />
        <Route
          exact
          path="/pet-shops-in-coimbatore"
          element={<Petshopsincoimbatore />}
        />
        <Route
          exact
          path="/indian-milk-cow-breeds-sale"
          element={<Indanmilkcowbreedssale />}
        />
      </Switch>
      <UserTestimonial />
      <UserTestimonialmobile />
      <FooterBanner />
      <Footer />
    </Router>
  );
}

export default App;
