import React, { useEffect } from "react";
import NavMenu from "./components/NavMenu";
import Footer from "./components/Pages/LandingPage/Footer";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import Buy from "./components/Pages/ProductBuy/Buy";
import ProductDetail from "./components/Pages/ProductBuy/ProductDetail";
import Sell from "./components/Pages/ProductSell/Sell";
import SellerProdct from "./components/Pages/SellerProducts/SellerProductsPage";
import VetProductListPage from "./components/Pages/VetPages/VetProductListPage";
import Cart from "./components/Pages/UserCart/CartPage";
import Home from "./components/Pages/Home/Home";
import AboutUs from "./components/Pages/Site-details/AboutUs";
import Howitworks from "./components/Pages/Site-details/How-it-works";
import Careers from "./components/Pages/Site-details/Careers";
import Support from "./components/Pages/Site-details/Support";
import UserProfileDetail from "./components/Pages/User/UserProfile";
import UserOrders from "./components/Pages/UserOrder/OrderPage";
import PlaceOrder from "./components/Pages/UserOrder/PlaceOrder";
import UserAddresses from "./components/Pages/User/UserAddress";
import Userlogout from "./components/Pages/User/UserLogout";
import ServicesUnderConstruction from "./components/Pages/UnderConstruction";
import Terms from "./components/Pages/Site-details/Terms";
import Privacypolicy from "./components/Pages/Site-details/Privacypolicy";
import Enquiry from "./components/Pages/UserRequirement/Enquiry";
import ViewRequirements from "./components/Pages/UserRequirement/RequirementsList";
import ViewMyEnquiries from "./components/Pages/UserRequirement/UserRequirementsList";
import Threads from "./components/Pages/DiscussionForum/Threads";
import Replies from "./components/Pages/DiscussionForum/Replies";
import ReactGA from "react-ga";
import UserTestimonial from "./components/Pages/LandingPage/UserTestimonial";
import FooterBanner from "./components/Pages/LandingPage/FooterBanner";
import UserTestimonialmobile from "./components/Pages/LandingPage/UserTestimonialmobile";
import Header from "./components/Pages/LandingPage/Header";
import Menubar from "./components/Pages/LandingPage/Menubar";
import Services from "./components/Pages/LandingPage/Services";

import jerseycattlesaleincoimbatore from "./components/Pages/SEO/jerseycattlesaleincoimbatore";
import farmanimalspetsincoimbatore from "./components/Pages/SEO/farmanimalspetsincoimbatore";
import goatfarmingincoimbatore from "./components/Pages/SEO/goatfarmingincoimbatore";
import petshopsincoimbatore from "./components/Pages/SEO/petshopsincoimbatore";
import indanmilkcowbreedssale from "./components/Pages/SEO/Indianmilkcowbreedssale";

const TRACKING_ID = "UA-249679917-1"; // OUR_TRACKING_ID
ReactGA.initialize(TRACKING_ID);

//ReactGA.initialize("G-T8SVNTPL8G");

function App() {
  window.dataLayer.push({
    event: "pageview",
    url: window.location.pathname,
  });

  useEffect(() => {
    ReactGA.send({
      hitType: "pageview",
      page: window.location.pathname + window.location.search,
    });
  }, []);

  return (
    <Router>
      {/* <NavMenu /> */}
      <Header />
      <Menubar />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/refcode/:loginrefcode" component={Home} />
        <Route exact path="/buy/:typeId" component={Buy} />
        <Route exact path="/product/:productId" component={ProductDetail} />
        <Route
          exact
          path="/product/related/:productId"
          component={ProductDetail}
        />
        <Route exact path="/sell" component={Sell} />
        <Route exact path="/seller-product/" component={SellerProdct} />
        <Route exact path="/vet-verify/" component={VetProductListPage} />
        <Route
          exact
          path="/userprofile/:userId"
          component={UserProfileDetail}
        />
        <Route exact path="/my-cart" component={Cart} />
        <Route exact path="/confirm-order/:addressId" component={PlaceOrder} />
        <Route exact path="/orders" component={UserOrders} />
        <Route
          exact
          path="/my-addresses/:type/:orderId"
          component={UserAddresses}
        />
        <Route exact path="/aboutus" component={AboutUs} />
        <Route exact path="/how-it-works" component={Howitworks} />
        <Route exact path="/careers" component={Careers} />
        <Route exact path="/support" component={Support} />
        <Route exact path="/logout" component={Userlogout} />
        <Route
          exact
          path="/other-services"
          component={ServicesUnderConstruction}
        />
        <Route exact path="/terms" component={Terms} />
        <Route exact path="/privacy-policy" component={Privacypolicy} />
        <Route exact path="/add-enquiry" component={Enquiry} />
        <Route exact path="/view-enquiry/:enquiryid" component={Enquiry} />
        <Route exact path="/view-requirements" component={ViewRequirements} />
        <Route exact path="/my-enquiries" component={ViewMyEnquiries} />
        <Route exact path="/forum" component={Threads} />
        <Route exact path="/services" component={Services} />
        <Route
          exact
          path="/forum/thread/:threadid/replies"
          component={Replies}
        />

        <Route
          exact
          path="/jersey-cattle-sale-coimbatore"
          component={jerseycattlesaleincoimbatore}
        />
        <Route
          exact
          path="/farm-animals-pets-in-coimbatore"
          component={farmanimalspetsincoimbatore}
        />
        <Route
          exact
          path="/goat-farming-in-coimbatore"
          component={goatfarmingincoimbatore}
        />
        <Route
          exact
          path="/pet-shops-in-coimbatore"
          component={petshopsincoimbatore}
        />
        <Route
          exact
          path="/indian-milk-cow-breeds-sale"
          component={indanmilkcowbreedssale}
        />
      </Switch>
      <UserTestimonial />
      <UserTestimonialmobile />
      <FooterBanner />
      <Footer />
    </Router>
  );
}

export default App;
