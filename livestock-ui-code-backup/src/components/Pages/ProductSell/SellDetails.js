import React, { useEffect, useState } from "react";
import Select from "react-select";
import Container from "react-bootstrap/Container";
import { Link } from "react-router-dom";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Button } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
import NumericInput from "react-bootstrap-input-spinner";
import { Label, UncontrolledTooltip } from "reactstrap";
import { Radio, RadioGroup } from "react-radio-group";
import DatePicker from "react-datepicker";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import Cookies from "js-cookie";
import CryptoJS from "crypto-js";

import pincodeDirectory from "india-pincode-lookup";

import "react-datepicker/dist/react-datepicker.css";
import "./SellingInfo.css";
import configData from "../../../config.json";
import Loader from "../../Loader";

import { TiInfoOutline } from "react-icons/ti";
import { FaAddressBook } from "react-icons/fa";

export default function SellDetails(props) {
  let indianRupeeLocale = Intl.NumberFormat("en-IN");

  const [isLoading, setLoading] = useState(false);
  const [categoryTypes, setCategoryTypes] = useState(null);

  const [allCategoryTypes, setAllCategoryTypes] = useState([]);

  const [category, setCategory] = useState(null);
  const [categoryList, setCategoryList] = useState([]);

  const [ProductName, setProductName] = useState(null);
  const [ProductDescription, setProductDescription] = useState(null);

  const [countryList, setCountryList] = useState([]);

  const [city, setCity] = useState(null);
  const [cityList, setCityList] = useState([]);

  const [state, setState] = useState(null);
  const [stateList, setStateList] = useState([]);

  const [pincode, setPincode] = useState(null);
  const [deliverablePincodes, setDeliverablePincodes] = useState([]);

  const [pickup, setPickup] = useState(null);
  const [pickupList, setPickupList] = useState(props.pickupaddress);

  const [breed, setBreed] = useState(null);
  const [breedList, setBreedList] = useState([]);

  const [age, setAge] = useState(null);
  const [ageList, setAgeList] = useState([]);

  const [color, setColor] = useState(null);
  const [colorList, setColorList] = useState([]);

  const [gender, setGender] = useState(null);
  const [genderList, setGenderList] = useState([]);

  const [pregnant, setPregnant] = useState(null);
  const [teethCount, setTeethCount] = useState(null);

  const [weight, setWeight] = useState();
  const [milkProduce, setMilkProduce] = useState();

  const [vaccineList, setVaccineList] = useState([]);
  const [vaccineDosageList, setVaccineDosageList] = useState([]);

  const [vaccine1, setVaccine1] = useState(null);
  const [vaccine2, setVaccine2] = useState(null);
  const [vaccineDosage1, setVaccineDosage1] = useState(null);
  const [vaccineDosage2, setVaccineDosage2] = useState(null);

  const [originalPrice, setOriginalPrice] = useState();
  const [offerPrice, setOfferPrice] = useState();

  const [recommendOriginalPrice, setRecommendOriginalPrice] = useState(0);
  const [recommendedOfferPrice, setRecommendedOfferPrice] = useState(0);
  const [sellerProfit, setSellerProfit] = useState(0);
  const [brokerageamount, setBrokerageamount] = useState(0);

  const [noOfOffspring, setNoOfOffspring] = useState();
  const [quantityAvailable, setQuantityAvailable] = useState();

  const [showVaccine1, setShowVaccine1] = useState(true);
  const [showVaccine2, setShowVaccine2] = useState(true);

  const toggleShowVaccine1 = () => setShowVaccine1(!showVaccine1);
  const toggleShowVaccine2 = () => setShowVaccine2(!showVaccine2);

  const [latestVaccinatedDate1, setLatestVaccinatedDate1] = useState(
    new Date()
  );
  const [latestVaccinatedDate2, setLatestVaccinatedDate2] = useState(
    new Date()
  );

  const [recommendedproductprice, setRecommendedproductprice] = useState();
  const [suggestedOfferPercent, setSuggestedOfferPercent] = useState(0);
  const [siteprofitshare, setSiteprofitshare] = useState(0);

  const [disableSubmit, setDisableSubmit] = useState(true);
  const [errorRequiredFieldMissing, seterrorRequiredFieldMissing] =
    useState(false);

  const [validationErrors, setValidationErrors] = useState({});

  const initialImageFile = {
    name: "",
    slider_images: [], // (array of strings)
  };

  const [imageUpload, setImageUpload] = useState(initialImageFile);

  const [uploadFileCountalert, setUploadFileCountalert] = useState(false);

  const [SaveProductResponseData, setSaveProductResponseData] = useState(false);
  const [SaveProductImagecompleted, setSaveProductImagecompleted] =
    useState(false);
  const [SaveProductError, setSaveProductError] = useState(false);

  const [
    offerPriceHigherThanOriginalError,
    setOfferPriceHigherThanOriginalError,
  ] = useState(false);

  const handlefileuploadChange = (e) => {
    if (e.target.files) {
      if (e.target.files.length > 3) {
        setUploadFileCountalert(true);
        return;
      }
      setImageUpload({ ...imageUpload, slider_images: [...e.target.files] });
      setUploadFileCountalert(false);
      setDisableSubmit(false);
    }
    console.log("Update slider images", imageUpload);
  };

  const vaccinationDoseListValues = [
    {
      id: "1",
      name: "Primary dose",
    },
    {
      id: "2",
      name: "Booster dose",
    },
    {
      id: "3",
      name: "Revaccination dose",
    },
  ];

  const uploadImage = (productsaveresponse) => {
    var data = new FormData();
    for (const key of Object.keys(imageUpload.slider_images)) {
      data.append("images", imageUpload.slider_images[key]);
    }
    data.append("product", productsaveresponse.productVariantId);

    fetch("/api/v1/productimages", {
      method: "POST",
      headers: {
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
      body: data,
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then((result) => {
        console.log(result);
        setSaveProductImagecompleted(true);
      })
      .catch((err) => {
        console.log(err);
        setSaveProductImagecompleted(false);
      });
    setLoading(false);
  };

  const saveProduct = (requestJson) => {
    fetch("/api/v1/product", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
      body: JSON.stringify(requestJson),
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then((result) => {
        setSaveProductResponseData(true);
        uploadImage(result);
        console.log(result);
      })
      .catch((err) => {
        setLoading(false);
        setSaveProductError(true);
        console.log(err);
      });
  };

  const handleSubmit = (evt) => {
    setLoading(true);
    evt.preventDefault();

    if (!categoryTypes || !category || offerPriceHigherThanOriginalError) {
      seterrorRequiredFieldMissing(true);
      setLoading(false);
    } else {
      seterrorRequiredFieldMissing(false);

      var JsonRequestData = {
        product: {
          name: ProductName,
          description: ProductDescription,
          seller: CryptoJS.AES.decrypt(
            Cookies.get("id"),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(CryptoJS.enc.Utf8),
          categorytype: categoryTypes.id,
          categorytypename: categoryTypes.shortname,
          category: category.id,
          categoryname: category.shortname,
          breed: breed ? breed.id : null,
          breedname: breed ? breed.shortname : null,
          pickup: pickup ? pickup.id : null,
          created_by: CryptoJS.AES.decrypt(
            Cookies.get("id"),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(CryptoJS.enc.Utf8),
        },
        product_variant: [
          {
            age: age ? age.id : null,
            gender: gender ? gender.id : null,
            feedtype: 2,
            stock: quantityAvailable,
            adult_teeth_pair_count: teethCount,
            original_price: originalPrice,
            offer_price: offerPrice ? offerPrice : 0,
            weight: weight,
            color: color ? color.id : null,
            is_pregnant: pregnant,
            milk_produce_per_Day: milkProduce,
            no_of_offsprings: noOfOffspring,
          },
        ],
      };

      if (vaccine1 || vaccine2) {
        let vaccine1detail = {};
        let vaccine2detail = {};

        if (vaccine1) {
          vaccine1detail = {
            vaccine: vaccine1 ? vaccine1.id : null,
            completed_dose: vaccineDosage1 ? vaccineDosage1.id : null,
            vaccinated_on: latestVaccinatedDate1,
          };
        }

        if (vaccine2) {
          vaccine2detail = {
            vaccine: vaccine2 ? vaccine2.id : null,
            completed_dose: vaccineDosage2 ? vaccineDosage2.id : null,
            vaccinated_on: latestVaccinatedDate2,
          };
        }

        JsonRequestData.vaccines = [vaccine1detail, vaccine2detail];
      } else {
        JsonRequestData.vaccines = [];
      }

      saveProduct(JsonRequestData);
    }
  };

  const handleResetButtonClick = (e) => {
    setCategoryTypes(1);
    setCategory(null);
    setProductName(null);
    setProductDescription(null);
    setPickup(null);

    initializeAllInputFieldValues();
  };

  useEffect(() => {
    window.scrollTo(0, 0);

    fetch("/api/v1/category-type", {
      method: "GET",
      headers: {
        "content-type": "application/json",
      },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then(function (resp) {
        console.log(resp);
        setAllCategoryTypes(resp);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const handleCategoryTypeChange = (obj) => {
    setCategoryTypes(obj);
    setCategoryList(obj.categories);
    setCategory(null);
  };

  const initializeAllInputFieldValues = () => {
    setAge(null);
    setBreed(null);
    setCity(null);
    setCityList([]);
    setColor(null);
    setOfferPrice(0);
    setGender(null);
    setImageUpload(initialImageFile);
    setLatestVaccinatedDate1(null);
    setLatestVaccinatedDate2(null);
    setMilkProduce(0);
    setNoOfOffspring(0);
    setOriginalPrice(0);
    setPincode(null);
    setPregnant(0);
    setQuantityAvailable(1);
    setState(null);
    setTeethCount(0);
    setVaccine1(null);
    setVaccineDosage1(null);
    setVaccine2(null);
    setVaccineDosage2(null);
    setWeight(0);
    seterrorRequiredFieldMissing(false);

    setDisableSubmit(true);
  };
  // handle change event of the Category dropdown
  const handleCategoryChange = (obj) => {
    setCategory(obj);
    setBreedList(obj.breeds);
    setVaccineList(obj.vaccines);

    initializeAllInputFieldValues();

    //calling age api on category change
    fetch("/api/v1/livestock-age", {
      method: "GET",
      headers: {
        "content-type": "application/json",
      },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then(function (resp) {
        setAgeList(resp);
      })
      .catch((err) => {
        console.log(err);
      });

    //calling color api on category change
    fetch("/api/v1/livestock-color", {
      method: "GET",
      headers: {
        "content-type": "application/json",
      },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then(function (resp) {
        setColorList(resp);
      })
      .catch((err) => {
        console.log(err);
      });

    //calling gender api on category change
    fetch("/api/v1/livestock-gender", {
      method: "GET",
      headers: {
        "content-type": "application/json",
      },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then(function (resp) {
        setGenderList(resp);
      })
      .catch((err) => {
        console.log(err);
      });

    // fetching deliverable pincode details
    fetch("/api/v1/deliverable-pincodes", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then(function (resp) {
        console.log(resp);
        setDeliverablePincodes(resp);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // handle change event of the Product name
  const handleProductNameChange = (e) => {
    setProductName(e.target.value);
  };

  // handle change event of the Product Description
  const handleProductDescriptionChange = (e) => {
    setProductDescription(e.target.value);
  };

  // handle change event of the Breed dropdown
  const handleBreedChange = (obj) => {
    setBreed(obj);
  };

  // handle change event of the Age dropdown
  const handleAgeChange = (obj) => {
    setAge(obj);
  };

  // handle change event of the Color dropdown
  const handleColorChange = (obj) => {
    setColor(obj);
  };

  // handle change event of the City dropdown
  const handleCityChange = (obj) => {
    setCity(obj);
  };

  // handle change event of the State dropdown
  const handleStateChange = (obj) => {
    setState(obj);
  };

  // handle change event of the product pickup address field
  const handlePickupChange = (obj) => {
    setPickup(obj);
  };

  // handle change event of the Gender dropdown
  const handleGenderChange = (obj) => {
    setGender(obj);
  };

  // handle change event of the Is Pregnant dropdown
  const handlePregnantChange = (obj) => {
    setPregnant(obj);
  };

  // handle change event of the MilkProduce field
  const handleMilkProduceChange = (obj) => {
    setMilkProduce(obj);
  };

  // handle change event of the Offspring count field
  const handleNoOfOffspringChange = (obj) => {
    setNoOfOffspring(obj);
  };

  // handle change event of the Teeth Count dropdown
  const handleTeethCountChange = (obj) => {
    setTeethCount(obj);
  };

  // handle change event of the Quantity available field
  const handleQuantityAvailableChange = (obj) => {
    setQuantityAvailable(obj);
  };

  // handle change event of the weight field
  const handleWeightChange = (obj) => {
    setWeight(obj);
  };

  const setpricedetails = () => {
    if (!recommendedproductprice) {
      // fetching product price recommendations
      fetch("/api/v1/recommendprice", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error(response);
          }
        })
        .then(function (resp) {
          console.log(resp);
          setRecommendedproductprice(resp);
          setSuggestedOfferPercent(resp.recommendedOfferpercent);
          setSiteprofitshare(resp.profitmargin);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };
  // handle change event of the Original price field
  const handleOriginalPriceChange = (e) => {
    const amount = e.target.value;

    if (!amount || amount.match(/^\d{1,}(\.\d{0,4})?$/)) {
      setOriginalPrice(amount);
    }
    // setOriginalPrice(obj);
    setpricedetails();

    // var now = new Date();
    // var activediscount = discountList ? discountList.filter(discount => discount.is_active == true && new Date(discount.applicable_to) < now) : null;
    // if (activediscount && activediscount.length > 0) {
    //   var discount = activediscount[0];
    //   var calculatedofferprice = 0
    //   if (discount.discount_percent > 0) {
    //     calculatedofferprice = obj - (obj * (discount.discount_percent / 100))
    //   } else if (discount.discount_rupees > 0) {
    //     calculatedofferprice = obj - discount.discount_rupees
    //   }

    //   if (discount.max_discount_rupees > 0 && calculatedofferprice > discount.max_discount_rupees) {
    //     calculatedofferprice = discount.max_discount_rupees
    //   }
    //   setOfferPrice(calculatedofferprice)
    // }
  };

  // handle change event of the discount price field
  const handleOfferPriceChange = (e) => {
    const amount = e.target.value;

    if (!amount || amount.match(/^\d{1,}(\.\d{0,4})?$/)) {
      if (parseFloat(amount) >= parseFloat(originalPrice)) {
        setOfferPriceHigherThanOriginalError(true);
      } else {
        setOfferPrice(parseFloat(amount));
        // setOfferPrice(obj);
        setOfferPriceHigherThanOriginalError(false);
      }
    }
  };

  const calculaterecommendedprice = () => {
    if (recommendedproductprice) {
      var original = parseFloat(originalPrice);
      setSellerProfit(original);
      var tempbrokerage = Math.ceil(
        original * (parseFloat(siteprofitshare) / 100)
      );
      setBrokerageamount(tempbrokerage);

      var originalwithbrokerage = original + tempbrokerage;
      var temporiginal = Math.ceil(
        originalwithbrokerage / (1 - parseFloat(suggestedOfferPercent) / 100)
      );
      setRecommendOriginalPrice(temporiginal);

      var tempoffer = Math.ceil(
        temporiginal - temporiginal * (parseFloat(suggestedOfferPercent) / 100)
      );
      setRecommendedOfferPrice(tempoffer);
    } else {
      setRecommendOriginalPrice(0);
      setRecommendedOfferPrice(0);
      setBrokerageamount(0);
    }
  };

  const applyrecommendedprice = () => {
    setOriginalPrice(recommendOriginalPrice);
    setOfferPrice(recommendedOfferPrice);
  };

  // handle change event of the Vaccine dropdown
  const handleVaccine1Change = (obj) => {
    setVaccine1(obj);
    setVaccineDosageList(vaccinationDoseListValues);
  };

  const handleVaccine2Change = (obj) => {
    setVaccine2(obj);
    setVaccineDosageList(vaccinationDoseListValues);
  };

  // handle change event of the Vaccine dosage dropdown
  const handleVaccineDosage1Change = (obj) => {
    setVaccineDosage1(obj);
  };

  const handleVaccineDosage2Change = (obj) => {
    setVaccineDosage2(obj);
  };

  const handlePincodeChange = (e) => {
    const errors = {};
    let isdeliverablepincode = false;
    let selectedpincodedetail = null;

    if (e.target.value.length == 6) {
      setValidationErrors({});
      for (let deliverypincode of deliverablePincodes) {
        if (deliverypincode.pincode == e.target.value) {
          isdeliverablepincode = deliverypincode.isdeliverable;
          selectedpincodedetail = deliverypincode;
          break;
        }
      }
    }

    if (selectedpincodedetail == null) {
      errors.pincode =
        "This Pincode is not valid/We donot have active operation in this area now. Please try different code.";
      setDisableSubmit(true);
      setValidationErrors(errors);
    } else if (!isdeliverablepincode) {
      errors.pincodewarning =
        "Our Delivery service is not available in this area now. But buyers can always contact to your phone.";
      setValidationErrors(errors);
    } else {
      setValidationErrors({});
    }

    if (
      !errors.pincode &&
      imageUpload &&
      imageUpload.slider_images &&
      imageUpload.slider_images.length > 0
    ) {
      setDisableSubmit(false);
    }

    setPincode(e.target.value);
    const locationjson = pincodeDirectory.lookup(e.target.value);
    if (
      locationjson &&
      locationjson.length > 0 &&
      isdeliverablepincode &&
      selectedpincodedetail
    ) {
      // setCountry(countryList.filter(x => x.id == selectedpincodedetail.country)[0])
      setState(stateList.filter((x) => x.id == selectedpincodedetail.state)[0]);
      setCity(cityList.filter((x) => x.id == selectedpincodedetail.city)[0]);
    }
  };

  const customStyles = {
    control: (base, state) => ({
      ...base,
      background: "#green",
      // match with the menu
      borderRadius: state.isFocused ? "3px 3px 0 0" : 3,
      // Overwrittes the different states of border
      borderColor: state.isFocused ? "blue" : "green",
      // Removes weird border around container
      boxShadow: state.isFocused ? null : null,
      "&:hover": {
        // Overwrittes the different states of border
        borderColor: state.isFocused ? "red" : "red",
      },
    }),
    menu: (base) => ({
      ...base,
      // override border radius to match the box
      borderRadius: 0,
      // kill the gap
      marginTop: 0,
    }),
    menuList: (base) => ({
      ...base,
      background: "white",
      // kill the white space on first and last option
      padding: 0,
    }),
  };

  return (
    <div className="sell-details">
      {isLoading ? (
        <Loader />
      ) : (
        <div>
          <div>
            <Modal
              size="lg"
              centered
              backdrop="static"
              show={SaveProductResponseData || SaveProductError}
            >
              <Modal.Header>
                <Modal.Title id="sell-product-save-response">
                  {SaveProductResponseData
                    ? "Successfully registered new product!"
                    : "Failure during registration!"}
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                {SaveProductResponseData ? (
                  <div>
                    Your new product has been registered successfully and will
                    be listed for buyers after verification. Happy Selling!!{" "}
                    {!SaveProductImagecompleted ? (
                      <div>Processing image upload, Please Wait!</div>
                    ) : (
                      <div>Image Upload has been completed!</div>
                    )}
                  </div>
                ) : (
                  <div>
                    Sorry! there seems to be some issue while registering your
                    new product for sale. Please contact our team for more
                    details.
                  </div>
                )}
              </Modal.Body>
              <Modal.Footer>
                <Button>
                  <a
                    href="/"
                    className={`modal-btn-close${
                      SaveProductResponseData && !SaveProductImagecompleted
                        ? " disabled"
                        : ""
                    }`}
                  >
                    Close
                  </a>
                </Button>
              </Modal.Footer>
            </Modal>
          </div>

          <form onSubmit={handleSubmit}>
            <Container fluid className="sell-container">
              <Row xs={1} sm={2} md={2} className="sell-row">
                <Col>
                  <div>
                    <Label className="sell-label">Product pickup address</Label>
                    <span> </span> /
                    <span href="#" id="tooltip-add-new-address">
                      <Link to="/my-addresses/sell/selleraddress">
                        add new <FaAddressBook />
                      </Link>
                    </span>
                    <div className="sell-dropdown-field">
                      <Select
                        className="sell-dropdown"
                        placeholder="Pickup"
                        value={pickup}
                        options={props.pickupaddress}
                        onChange={handlePickupChange}
                        styles={customStyles}
                        getOptionLabel={(x) =>
                          x.address.name + " -> " + x.address.address_1
                        }
                        getOptionValue={(x) => x.id}
                      />
                    </div>
                  </div>
                  <UncontrolledTooltip
                    placement="right"
                    target="tooltip-add-new-address"
                  >
                    If you want to set a new address as pickup address, please
                    create one here
                  </UncontrolledTooltip>
                </Col>
                <Col></Col>
              </Row>
              {pickup ? (
                <Row xs={1} sm={2} md={2} className="sell-row">
                  <Col>
                    <Label className="sell-label">Product Category type</Label>
                    <div className="sell-dropdown-field">
                      <Select
                        className="sell-dropdown"
                        placeholder="Category Type"
                        value={categoryTypes}
                        options={allCategoryTypes}
                        required
                        onChange={handleCategoryTypeChange}
                        styles={customStyles}
                        getOptionLabel={(x) => x.name}
                        getOptionValue={(x) => x.id}
                      />
                    </div>
                  </Col>
                  <Col>
                    <Label className="sell-label">Product Category</Label>
                    <div className="sell-dropdown-field">
                      <Select
                        className="sell-dropdown"
                        placeholder="Category"
                        value={category}
                        options={categoryList}
                        onChange={handleCategoryChange}
                        styles={customStyles}
                        getOptionLabel={(x) => x.name}
                        getOptionValue={(x) => x.id}
                      />
                    </div>
                  </Col>
                </Row>
              ) : null}

              {category === null ? (
                <div>
                  <br />
                  <br />
                  <br />
                  <br />
                </div>
              ) : null}

              {categoryTypes && category ? (
                <div>
                  <Row xs={1} sm={2} md={2} className="sell-row">
                    <Col>
                      <Label className="sell-label">Product Name</Label>
                      <div className="sell-name-field">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Name to be displayed as banner"
                          value={ProductName}
                          onChange={handleProductNameChange}
                        />
                      </div>
                    </Col>
                    <Col>
                      <Label className="sell-label">Product Description</Label>
                      <div className="sell-description-field">
                        <textarea
                          className="form-control"
                          placeholder="Description about your product for buyers to know about"
                          value={ProductDescription}
                          onChange={handleProductDescriptionChange}
                        />
                      </div>
                    </Col>
                  </Row>{" "}
                </div>
              ) : null}

              {ProductName &&
              ProductDescription &&
              (categoryTypes.id == 1 || categoryTypes.id == 2) ? (
                <div>
                  <Row xs={1} sm={2} md={2} className="sell-row">
                    <Col>
                      <Label className="sell-label">Livestock Breed</Label>
                      <div className="sell-dropdown-field">
                        <Select
                          className="sell-dropdown"
                          placeholder="Breed"
                          value={breed}
                          options={breedList}
                          onChange={handleBreedChange}
                          styles={customStyles}
                          getOptionLabel={(x) => x.name}
                          getOptionValue={(x) => x.id}
                        />
                      </div>
                    </Col>
                    <Col>
                      <Label className="sell-label">Livestock Age</Label>
                      <div className="sell-dropdown-field">
                        <Select
                          className="sell-dropdown"
                          placeholder="Age"
                          value={age}
                          options={ageList}
                          onChange={handleAgeChange}
                          styles={customStyles}
                          getOptionLabel={(x) => x.age_range}
                          getOptionValue={(x) => x.id}
                        />
                      </div>
                    </Col>
                  </Row>{" "}
                </div>
              ) : null}
              {breed !== null && age !== null ? (
                <div>
                  <Row xs={1} sm={2} md={2} className="sell-row">
                    <Col>
                      <Label className="sell-label">Livestock Color</Label>
                      <div className="sell-dropdown-field">
                        <Select
                          className="sell-dropdown"
                          placeholder="Color"
                          value={color}
                          options={colorList}
                          onChange={handleColorChange}
                          styles={customStyles}
                          getOptionLabel={(x) => x.name}
                          getOptionValue={(x) => x.id}
                        />
                      </div>
                    </Col>
                    <Col>
                      <Label className="sell-label">Livestock Gender</Label>
                      <div className="sell-dropdown-field">
                        <Select
                          className="sell-dropdown"
                          placeholder="Gender"
                          value={gender}
                          options={genderList}
                          onChange={handleGenderChange}
                          styles={customStyles}
                          getOptionLabel={(x) => x.name}
                          getOptionValue={(x) => x.id}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              ) : null}

              {category &&
              category.varianttype == 1 &&
              gender !== null &&
              gender.id === 0 ? (
                <div>
                  <Row xs={1} sm={2} md={2} className="sell-row">
                    <Col>
                      <Label className="sell-label">
                        Is Currently Pregnant
                      </Label>
                      <div className="sell-radio-button">
                        <RadioGroup
                          name="livestock-pregnancy"
                          onChange={(e) => handlePregnantChange(e)}
                          value={pregnant}
                        >
                          <Radio value="1" className="sell-radiobutton" />
                          Yes
                          <Radio value="0" className="sell-radiobutton" />
                          No
                        </RadioGroup>
                      </div>
                    </Col>
                    <Col>
                      <Label className="sell-label">
                        Avg. Milk Produce (in ltrs/day)
                      </Label>
                      <br />
                      <div className="sell-numericfield">
                        <NumericInput
                          type={"decimal"}
                          variant={"secondary"}
                          size="sm"
                          step={0.5}
                          precision={1}
                          size="3"
                          placeholder={0}
                          value={milkProduce}
                          onChange={handleMilkProduceChange}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              ) : null}

              {category &&
              category.varianttype == 1 &&
              gender !== null &&
              color !== null ? (
                <div>
                  <Row xs={1} sm={2} md={2} className="sell-row">
                    <Col>
                      {gender && gender.id === 0 ? (
                        <div>
                          <Label className="sell-label">
                            No of Pregnancies
                          </Label>
                          <br />
                          <div className="sell-numericfield">
                            <NumericInput
                              type={"real"}
                              variant={"secondary"}
                              size="sm"
                              step={1}
                              precision={0}
                              min={0}
                              max={10}
                              value={noOfOffspring}
                              onChange={handleNoOfOffspringChange}
                            />
                          </div>
                        </div>
                      ) : null}
                    </Col>

                    <Col>
                      <Label className="sell-label">Livestock Teethcount</Label>
                      <br />
                      <div className="sell-numericfield">
                        <NumericInput
                          type={"real"}
                          variant={"secondary"}
                          size="sm"
                          value={teethCount}
                          onChange={(e) => handleTeethCountChange(e)}
                          step={1}
                          min={0}
                          max={10}
                          precision={0}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              ) : null}
              {gender !== null && color !== null ? (
                <div>
                  <Row xs={1} sm={2} md={2} className="sell-row">
                    <Col>
                      <Label className="sell-label">Quantity available</Label>
                      <br />
                      <div className="sell-numericfield">
                        <NumericInput
                          type={"real"}
                          variant={"secondary"}
                          size="sm"
                          step={1}
                          size="3"
                          min={1}
                          precision={0}
                          placeholder={0}
                          value={quantityAvailable}
                          onChange={handleQuantityAvailableChange}
                        />
                      </div>
                    </Col>
                    <Col>
                      <Label className="sell-label">
                        Avg. Weight (in Kg/item)
                      </Label>
                      <br />
                      <div className="sell-numericfield">
                        <NumericInput
                          type={"decimal"}
                          variant={"secondary"}
                          size="sm"
                          step={0.5}
                          size="5"
                          precision={2}
                          min={0}
                          value={weight}
                          onChange={handleWeightChange}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              ) : null}

              {quantityAvailable &&
              weight &&
              quantityAvailable !== 0 &&
              weight !== 0 ? (
                <div>
                  <Row xs={1} sm={2} md={2} className="sell-row">
                    <Col>
                      <Label className="sell-label">
                        Product Original Price(in &#8377;/item){" "}
                      </Label>
                      <br />
                      <div className="sell-flex-div">
                        <div className="sell-numericfield">
                          <input
                            type="text"
                            className="form-control"
                            value={originalPrice}
                            onChange={handleOriginalPriceChange}
                          />
                          {originalPrice && weight ? (
                            <span>
                              <Label>
                                &#8377; {originalPrice / weight} / Kg{" "}
                              </Label>
                            </span>
                          ) : null}
                        </div>
                        <div className="sell-calculate-recommend-price">
                          <Button
                            variant="info"
                            onClick={calculaterecommendedprice}
                          >
                            Calculate recommended price
                          </Button>
                        </div>
                      </div>
                      {/* <div className="sell-message">
                        *On successful sale, farmzonn will take a commission as
                        mentioned in the Terms & condition document.
                      </div> */}
                    </Col>
                    <Col>
                      {originalPrice > 0 ? (
                        <div>
                          <div>
                            <Label className="sell-label">
                              Product Offer Price(in &#8377;/item){" "}
                            </Label>
                            <br />
                            <div className="sell-flex-div">
                              <div className="sell-numericfield">
                                <input
                                  type="text"
                                  className="form-control"
                                  value={offerPrice}
                                  onChange={handleOfferPriceChange}
                                />
                                {offerPrice && weight ? (
                                  <span>
                                    <Label>
                                      &#8377; {offerPrice / weight} / Kg{" "}
                                    </Label>
                                  </span>
                                ) : null}
                              </div>
                              {/* <div className="sell-calculate-recommend-price">
                                {recommendOriginalPrice > 0 ?
                                  <Button variant="info" onClick={applyrecommendedprice} >Apply recommended price
                                  </Button>
                                  :
                                  null
                                }
                              </div> */}
                            </div>
                            {recommendOriginalPrice > 0 ? (
                              <div>
                                <div className="sell-recommended-price">
                                  Recommended Original price: &#8377;{" "}
                                  {indianRupeeLocale.format(
                                    recommendOriginalPrice
                                  )}
                                </div>

                                <div className="sell-recommended-price">
                                  Recommended Offer price: &#8377;{" "}
                                  {indianRupeeLocale.format(
                                    recommendedOfferPrice
                                  )}
                                </div>
                                <div className="sell-recommended-price">
                                  Amount you get post sale: &#8377;{" "}
                                  {indianRupeeLocale.format(sellerProfit)}{" "}
                                  *subject to change
                                </div>
                              </div>
                            ) : null}
                            {offerPriceHigherThanOriginalError ? (
                              <div className="error">
                                *Offer price cannot be greater than Original
                                price
                              </div>
                            ) : null}
                          </div>
                        </div>
                      ) : null}
                    </Col>
                  </Row>
                </div>
              ) : null}

              {categoryTypes &&
              categoryTypes.id === 1 &&
              originalPrice &&
              originalPrice > 0 &&
              !offerPriceHigherThanOriginalError ? (
                <div>
                  <Row>
                    <Col>
                      <Button
                        variant="link"
                        className="sell-addVaccine"
                        onClick={toggleShowVaccine1}
                      >
                        Add/Remove Vaccination detail #1
                      </Button>
                    </Col>
                  </Row>
                  <div className="sell-vaccination1" hidden={showVaccine1}>
                    <Row xs={1} sm={2} md={2} className="sell-row">
                      <Col>
                        <Label className="sell-label">Vaccine - 1</Label>
                        <div className="sell-dropdown-field">
                          <Select
                            className="sell-dropdown"
                            placeholder="Vaccine"
                            value={vaccine1}
                            options={vaccineList}
                            onChange={handleVaccine1Change}
                            styles={customStyles}
                            getOptionLabel={(x) => x.name}
                            getOptionValue={(x) => x.id}
                          />
                        </div>
                      </Col>
                      <Col>
                        <Label className="sell-label">
                          Latest Vaccinated dosage - Vaccine - 1
                        </Label>
                        <div className="sell-dropdown-field">
                          <Select
                            className="sell-dropdown"
                            placeholder="Vaccine dosage"
                            value={vaccineDosage1}
                            options={vaccineDosageList}
                            onChange={handleVaccineDosage1Change}
                            styles={customStyles}
                            getOptionLabel={(x) => x.name}
                            getOptionValue={(x) => x.id}
                          />
                        </div>
                      </Col>
                    </Row>
                    <Row xs={1} sm={2} md={2} className="sell-row">
                      <Col>
                        <Label className="sell-label">
                          Latest Vaccinated date - Vaccine - 1
                        </Label>
                        <div className="sell-date-picket-field">
                          <DatePicker
                            className="form-control"
                            dateFormat="dd-MM-yyyy"
                            isClearable
                            selected={latestVaccinatedDate1}
                            onChange={(date) => setLatestVaccinatedDate1(date)}
                          />
                        </div>
                      </Col>
                      <Col>
                        <Button
                          variant="link"
                          className="sell-addVaccine"
                          onClick={toggleShowVaccine2}
                        >
                          Add/Remove Vaccination detail #2
                        </Button>
                      </Col>
                    </Row>
                  </div>
                  <div className="sell-vaccination2" hidden={showVaccine2}>
                    <Row xs={1} className="sell-row">
                      <Col>
                        <Label className="sell-label">Vaccine - 2</Label>
                        <div className="sell-dropdown-field">
                          <Select
                            className="sell-dropdown"
                            placeholder="Vaccine"
                            value={vaccine2}
                            options={vaccineList}
                            onChange={handleVaccine2Change}
                            styles={customStyles}
                            getOptionLabel={(x) => x.name}
                            getOptionValue={(x) => x.id}
                          />
                        </div>
                      </Col>
                      <Col>
                        <Label className="sell-label">
                          Latest Vaccinated dosage - Vaccine - 2
                        </Label>
                        <div className="sell-dropdown-field">
                          <Select
                            className="sell-dropdown"
                            placeholder="Vaccine dosage"
                            value={vaccineDosage2}
                            options={vaccineDosageList}
                            onChange={handleVaccineDosage2Change}
                            styles={customStyles}
                            getOptionLabel={(x) => x.name}
                            getOptionValue={(x) => x.id}
                          />
                        </div>
                      </Col>
                    </Row>
                    <Row xs={1} className="sell-row">
                      <Col>
                        <Label className="sell-label">
                          Latest Vaccinated date - Vaccine - 2
                        </Label>
                        <div className="sell-date-picket-field">
                          <DatePicker
                            className="form-control"
                            dateFormat="dd-MM-yyyy"
                            isClearable
                            selected={latestVaccinatedDate2}
                            onChange={(date) => setLatestVaccinatedDate2(date)}
                          />
                        </div>
                      </Col>
                      <Col></Col>
                    </Row>
                  </div>
                </div>
              ) : null}

              {originalPrice &&
              originalPrice > 0 &&
              !offerPriceHigherThanOriginalError ? (
                <div>
                  {/* <Row className="sell-row">
                <Col>
                  <Label className="sell-label">Product available Pincode</Label>
                  <div className="sell-pininput-field">
                    <input type="number"
                      className="form-control"
                      placeholder="Pincode"
                      maxLength="6"
                      value={pincode}
                      onChange={handlePincodeChange}
                    />
                  </div>
                  {validationErrors.pincode ? <span className="error">*{validationErrors.pincode}</span> : null}
                  {validationErrors.pincodewarning ? <span className="warn"><TiInfoOutline /> {validationErrors.pincodewarning}</span> : null}
                </Col>
                <Col>
                </Col>
              </Row> */}

                  {/* <Row className="sell-row">
                <Col>
                  <Label className="sell-label">Product available in State</Label>
                  <div className="sell-dropdown-field">
                    <Select
                      className="sell-dropdown"
                      placeholder="State"
                      value={state}
                      options={stateList}
                      onChange={handleStateChange}
                      getOptionLabel={(x) => x.name}
                      getOptionValue={(x) => x.id}
                    />
                  </div>
                </Col>
                <Col>
                  <Label className="sell-label">Product available in City</Label>
                  <div className="sell-dropdown-field">
                    <Select
                      className="sell-dropdown"
                      placeholder="City"
                      value={city}
                      options={cityList}
                      onChange={handleCityChange}
                      getOptionLabel={(x) => x.name}
                      getOptionValue={(x) => x.id}
                    />
                  </div>
                </Col>
              </Row> */}
                </div>
              ) : null}

              {pickup &&
              originalPrice &&
              quantityAvailable &&
              weight &&
              !offerPriceHigherThanOriginalError ? (
                <div>
                  <Row xs={1} className="sell-row-image">
                    <Col>
                      <label>Upload Product photos (Max 3)</label>
                      <br />
                      <input
                        id="fileupload"
                        className="fileupload-bar"
                        type="file"
                        multiple
                        accept=".jpg,.JPG,.jpeg,.JPEG,.png,.PNG"
                        label="File"
                        onChange={handlefileuploadChange}
                      />
                      <Label>Accepts only .jpeg/.jpg/.png files</Label>
                      <br />
                      <span
                        hidden={!uploadFileCountalert}
                        className="error-msg"
                      >
                        *Error: Max 3 files allowed!
                      </span>
                    </Col>
                    <Col>
                      <div className="div-image-preview">
                        {(imageUpload.slider_images || []).map((url) => (
                          <img
                            id={url}
                            src={URL.createObjectURL(url)}
                            alt="..."
                            className="sell-imagepreview"
                          />
                        ))}
                      </div>
                    </Col>
                  </Row>
                </div>
              ) : null}
            </Container>
            <div>
              {errorRequiredFieldMissing ? (
                <div className="sell-error-message-required-field-missing">
                  <span>
                    *Fill out required details and try submitting again
                  </span>
                </div>
              ) : null}
            </div>

            <Button
              className="sell-submitreset"
              variant="secondary"
              onClick={handleResetButtonClick}
            >
              Reset
            </Button>

            {disableSubmit ? (
              <OverlayTrigger
                placement="right"
                overlay={
                  <Tooltip id="sell-submit-btn-tooltip" show={disableSubmit}>
                    Fill out required fields to enable submit
                  </Tooltip>
                }
              >
                <span className="sell-submitbutton">
                  <Button
                    type="submit"
                    className="sell-submitbutton"
                    disabled={disableSubmit}
                  >
                    Submit
                  </Button>
                </span>
              </OverlayTrigger>
            ) : (
              <span className="sell-submitbutton">
                <Button
                  type="submit"
                  className="sell-submitbutton"
                  disabled={disableSubmit}
                >
                  Submit
                </Button>
              </span>
            )}
          </form>
          {category === null ? (
            <div>
              <br />
            </div>
          ) : null}
        </div>
      )}
    </div>
  );
}
