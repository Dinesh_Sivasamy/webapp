import React, { useEffect, useState } from "react";
import Cookies from "js-cookie";
import CryptoJS from "crypto-js";
import { useHistory, Link } from "react-router-dom";

import configData from "../../../config.json";
import SignInModal from "../User/SignInModal";
import SellDetails from "./SellDetails";
import { Button } from "react-bootstrap";

export default function Sell() {
  const history = useHistory();
  const [userAddress, setUserAddress] = useState([]);

  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);
  const handleClose = () => {
    setShow(false);
    window.location.reload();
  };

  useEffect(() => {
    if (
      Cookies.get("id") != null &&
      Cookies.get("type") != null &&
      ["2", "3", "4", "9"].indexOf(
        CryptoJS.AES.decrypt(
          Cookies.get("type"),
          `${configData.CRYPTO_SECRET_KEY}`
        ).toString(CryptoJS.enc.Utf8)
      ) > -1
    ) {
      fetch(
        `/api/v1/user-address/user=${encodeURIComponent(
          CryptoJS.AES.decrypt(
            Cookies.get("id"),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(CryptoJS.enc.Utf8)
        )}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "X-CSRFToken": Cookies.get("csrftoken"),
          },
        }
      )
        .then((resp) => {
          if (resp.ok) {
            return resp.json();
          } else {
            throw new Error(resp.statusText);
          }
        })
        .then(function (resp) {
          console.log(resp);
          if (!resp || (resp && resp.length == 0)) {
            history.push("/my-addresses/sell/selleraddress");
          }
          setUserAddress(resp);
        })
        .catch((err) => {
          console.log(err);
          history.push("/my-addresses/sell/selleraddress");
        });
    }
  }, []);

  return (
    <div>
      <div>
        <div className="page-title-with-border-space">
          Add details of your Farm animals here to list it for Sale!!!
          <hr />
        </div>
      </div>

      {Cookies.get("id") != null ? (
        <div>
          {Cookies.get("type") != null &&
          ["2", "3", "4", "9"].indexOf(
            CryptoJS.AES.decrypt(
              Cookies.get("type"),
              `${configData.CRYPTO_SECRET_KEY}`
            ).toString(CryptoJS.enc.Utf8)
          ) > -1 ? (
            <div>
              <SellDetails pickupaddress={userAddress} />
            </div>
          ) : (
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
              }}
              className="sell-user-login-request"
            >
              <div className="sell-user-login-request-header">
                Contact our{" "}
                <a
                  href={
                    "https://api.whatsapp.com/send/?text=Hi admin, Please change my user type to seller to post products for sale.&phone=" +
                    configData.ADMIN_CONTACT_WHATSAPP
                  }
                  target="_blank"
                >
                  admin
                </a>{" "}
                to change your type to Seller/Agent to post products for sale
                here with NO COST!
              </div>
            </div>
          )}
        </div>
      ) : (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
          }}
          className="sell-user-login-request"
        >
          <div className="sell-user-login-request-header">
            You have not logged in yet!
          </div>
          <div className="sell-user-login-request-header-link-div">
            <Button
              className="sell-user-login-request-header-link"
              onClick={handleShow}
            >
              Signin here to register your products
            </Button>
          </div>
          <div>
            <SignInModal
              show={show}
              setModalClose={handleClose}
              onHide={handleClose}
            />
          </div>
        </div>
      )}
    </div>
  );
}
