import './Sitedetail.css';
import React, { useState } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';
import { MobilePDFReader  } from 'react-read-pdf';
import { Button } from 'react-bootstrap';

export default function Terms() {

    pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

    const [numPages, setNumPages] = useState(null);
    const [pageNumber, setPageNumber] = useState(1);
  
    function onDocumentLoadSuccess({ numPages }) {
        setNumPages(numPages);
        setPageNumber(1);
    }

    const renderpdf = () => {
        return './TERMS_AND_CONDITIONS.pdf'
    }
    
    const renderpdfformobileview = () => {
        return 'https://farmzonn.com/TERMS_AND_CONDITIONS.pdf'
    }


    function changePage(offset) {
        setPageNumber(prevPageNumber => prevPageNumber + offset);
    }
    
    function previousPage() {
        changePage(-1);
    }
    
    function nextPage() {
        changePage(1);
    }
    
    return (
        <>
            <div className="terms-download-div">
                <div>
                    <Button variant='danger' className='terms-download-link' href={renderpdf()} download>
                        Download as PDF document
                    </Button>
                </div>
            </div>
            <div className="terms-view-div">
                <div>
                    <Document file={renderpdf()} onLoadSuccess={onDocumentLoadSuccess} onLoadError={console.error}>
                        <Page pageNumber={pageNumber} />
                    </Document>
                </div>
                <div className='terms-view-page-detal'>
                    <p>
                        Page {pageNumber || (numPages ? 1 : "--")} of {numPages || "--"}
                    </p>
                    <Button disabled={pageNumber <= 1} onClick={previousPage}>
                        &#60;
                    </Button>
                    <span> </span>
                    <Button
                        disabled={pageNumber >= numPages}
                        onClick={nextPage}
                    >
                        &#62;
                    </Button>
                </div>
            </div>
            <div className="terms-mobile-view-div">
                <div style={{ overflow: 'scroll', height: 300 }}>
                    <MobilePDFReader url={renderpdfformobileview()} />
                </div>
            </div>
        </>
    );
}