import "./Sitedetail.css";

export default function Careers() {
  return (
    <div className="Site-detail-career">
      <p>Email your updated resume to</p>
      <a href="mailto:hr@farmzonn.com"> contact@farmzonn.com </a>
    </div>
  );
}
