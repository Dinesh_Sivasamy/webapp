import './Sitedetail.css';

export default function AboutUs() {
    return (
        <>
            <div className="About-us">
            <p className="para-title">Livestock in India</p>
                <p className="starting-para">
                    Livestock plays an important role in Indian economy. About 20.5 million people depend upon livestock for their livelihood. Livestock contributed 16% to the income of small farm households as against an average of 14% for all rural households. Livestock provides livelihood to two-third of rural community. It also provides employment to about 8.8 % of the population in India. India has vast livestock resources. Livestock sector contributes 4.11% GDP and 25.6% of total Agriculture GDP. <a href="https://vikaspedia.in/agriculture/livestock/role-of-livestock-in-indian-economy">Source</a></p>

                    <p className="para-title">Livestock Sector in Tamil Nadu</p>
                <p className="starting-para">
                The livestock sector in Tamil Nadu, a state located in the southern part of India, plays a significant role in the economy and food security of the state. Livestock farming in Tamil Nadu is a major source of income for small and marginal farmers and provides employment opportunities to a large number of people, particularly in rural areas.

Some of the major livestock species raised in Tamil Nadu include cattle, buffalo, sheep, goats, pigs, and poultry. Among these, cattle and buffalo are the most important, and they are primarily raised for milk production. Tamil Nadu is one of the leading states in India in terms of milk production, and the state government has taken various initiatives to promote dairy farming and improve the productivity of milch animals.
<br />
Apart from milk production, livestock farming in Tamil Nadu also includes the production of meat, eggs, and wool. The state has a significant market for meat, and the demand for poultry products has been increasing in recent years. The Tamil Nadu government has been providing various subsidies and incentives to encourage farmers to take up livestock farming and to promote the establishment of meat and poultry processing units.

In addition, the state government has also been promoting the adoption of scientific and modern farming practices in the livestock sector, including the use of improved breeds, better feed and fodder management, and the use of modern technologies like artificial insemination and embryo transfer. The government has also established veterinary hospitals and dispensaries in rural areas to provide animal health care services to farmers.
<br />
Overall, the livestock sector in Tamil Nadu is an important contributor to the state's economy and food security, and the government has been taking various measures to promote and support this sector.</p>

                <p className="para-title">Our Product</p>
                <p className="starting-para">                
                    FarmZonn.com is an online livestock marketplace that helps farmers and buyers to trade livestock and livestock needs in a more efficient, cost effective and secure way. Our goal is to create a safe and secure platform which buyers and sellers from anywhere in India can use to connect and trade in a wider market and increase thier profit.</p>

                <p className="para-title">Problem that we solve</p>
                <p className="starting-para">
                    As of now, if a farmer or seller wants to sell his high-quality and healthy livestock, he must contact his local agent or get a transport service by himself and he has take his livestock to a nearby physical weekly marketplace to sell  his livestock. Also, if he could not manage to sell his livestock on that day the seller has to bring back his livestock to his farm/husbandry on his own cost and so he would be foreced to sell his livestock for what ever the price he gets on the given date and because of this pressure the seller would not get the right price for his product. Also sometimes in the markets buyers might get cheated with unhealthy livestocks.</p>
                
                <p className="para-title"> Our Goal</p>
                <p className="starting-para">
                    To reduce this painful selling and buying experience and to make the buyers and sellers work easier, FarmZonn helps the farmers and sellers to list their products online and find their buyer from anywhere in India and sell their high-quality livestock and livestock related products around India through its platform. Also, to make the trading easy delivering the livestock will be taken care by FarmZonn which will make the buyer experience also smooth and easy. We also provide vet check and seller verification facilities which will help the buyers to find the right and trusted product that they need.</p>

                <p>
                    Any buyer can find good quality livestocks, livestock accessories, feeds and medicines through FarmZonn.com from anywhere in India, buy them and get it delivered to their doorstep through FarmZonn.com.
                    We pledge to help our farmers to offer their animals to a wider circle of potential buyers and to do that in best moment to achieve maximum profit and provide buyers faster cheaper and easier way to find appropriate animals as per their needs. We help the farmers to sell their livestock from their own place without having to transport the animals around markets in search of buyers.
                </p>
                
                <p className="para-title">We stand with our Farmers</p>
                <p className="starting-para">
                    FarmZonn aims to provide the biggest online market for indian farmers and livestock traders. Using FarmZonn anyone should be able buy or sell their livestocks, livestock feeds and accessories for livestocks. Also we provide services like live Vet-on-call, Vet-at-your-doorstep, transporting agriculture products and many more services at very low cost which the farmers can utilize to achieve maximum profit.</p>
                
                <p className="para-title">Farmer space</p>
                <p className="starting-para">
                    You can find more tips on how to start goat farming in <a href="https://www.agrifarming.in/goat-farming-in-tamil-nadu-how-to-start-breeds">here</a>. Here you can find advantages of goat farming and goat farming products and its usage along with its profit and suitable conditions for goat farming. <br />
                    Goats are commonly grown livestock in tamil nadu only next to poultry. There are various breeds of goats available in tamil nadu. You can find the list of common goat breeds in <a href="http://www.agritech.tnau.ac.in/expert_system/sheepgoat/breeds.html#goatbreeds">here</a> with their characteristics. There are many common sheep breeds that are available in tamil nadu and some of the common sheep breeds in tamil nadu are <a href="http://www.agritech.tnau.ac.in/expert_system/sheepgoat/breeds.html#sheepbreeds">listed here</a>
                    <br /> <br />
                    Vaccination of livestocks are very important part of farming. you can find the <a href="https://vikaspedia.in/agriculture/livestock/cattle-buffalo/vaccination-schedule-in-cattle-and-buffalo">Vaccines for cattles</a> and <a href="https://vikaspedia.in/agriculture/livestock/sheep-and-goat-farming/vaccination-schedule-for-goats">Vaccines for Goats</a> in more detailed manner along with the vaccine usage and vaccine dosages.
                    <br />
                    Govt of India has been providing various schemes for our farmer and you can get the updated list of farmer schemes in <a href="https://vikaspedia.in/agriculture/national-schemes-for-farmers">National Farmers scheme</a> by which you can get benefits directly and help others benefited.<br />
                    <br />
                    Tamil farmers can get more details on goats, cattles, sheep, cow, ox and buffalo farming in <a href="https://xn--clcu6av0at5becfj8m.xn--xkc2dl3a5ee0h/agriculture/%E0%AE%95%E0%AE%BE%E0%AE%B2%E0%AF%8D%E0%AE%A8%E0%AE%9F%E0%AF%88-%E0%AE%AA%E0%AE%B0%E0%AE%BE%E0%AE%AE%E0%AE%B0%E0%AE%BF%E0%AE%AA%E0%AF%8D%E0%AE%AA%E0%AF%81">tamil here</a>.<br />

                    There are approximately <a href="https://www.nddb.coop/information/stats/pop">more than 53,58,00,000</a> livestock (goats, sheeps, cows, cattles, buffalo and others combined) in india. These livestock provide livelihood for most of the farmers and normal people in india. For many Livestock farming provides primary and scondary income to about more than half of farmers in india directly or indirectly <a href="https://cprindia.org/mapping-indias-livestock-economy/">Livestock contribution in indian economy</a> <br />
                </p>

            </div>
        </>
    );
}