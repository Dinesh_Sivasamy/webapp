import React from "react";
import { Col, Container, Row } from "react-bootstrap";

export default function FooterBanner() {
  return (
    <div className="bgcolor-primary-mid-light-brown height-280">
      <Container>
        <Row className="padding-top-2">
          <Col>
            <div>
              <p className="header-sm color-white text-align-center">
                Our Goal
              </p>
              <div className="display-inline align-items-center display-inline">
                <p className="header-xsm padding-left-1 padding-top-1">
                  Build world class
                  <span className="header-sm color-white"> platform</span> that
                  can transform livestock trade!
                </p>
              </div>

              <div className="display-inline text-align-center hide-in-mobile">
                <p className="header-xsm">
                  Help Farmers' generate more revenue through{" "}
                  <span className="color-white header-sm">Livestock.</span>
                </p>
              </div>

              <div>
                <figure className="button--wrap">
                  <p className="button--wrap-box bg-color-brown">Learn more</p>
                </figure>
              </div>
            </div>
          </Col>
          <Col>
            <div className="text-align-right position-abs-bottom-minus-110 hide-in-mobile">
              <img
                className="img-size-farm-animals"
                src="/images/farm-animals.png"
                alt="livestock"
              />
            </div>
            <div className="text-align-right position-abs-bottom-5 hide-in-desktop">
              <img
                className="img-size-170-110"
                src="/images/goat.png"
                alt="livestock"
              />
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
