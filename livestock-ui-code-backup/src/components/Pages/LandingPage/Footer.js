import React, { useState } from "react";
import { Col, Container, Row } from "react-bootstrap";

import Cookies from "js-cookie";
import CryptoJS from "crypto-js";

import configData from "../../../config.json";

export default function Footer() {
  const [email, setEmail] = useState(null);

  const handleemailchange = (e) => {
    setEmail(e.target.value);
  };

  return (
    <div className="bgcolor-primary-lighter-brown">
      <div>
        <Container>
          <Row xs={2} md={4} className="padding-top-2">
            <Col className="display-inline header-sm">
              <img
                className="icon-size-xs"
                src="/icons/petface.png"
                alt="livestock"
              />
              <p className="padding-left-2 padding-top-1">
                {" "}
                Wide range of Livestock
              </p>
            </Col>
            <Col className="display-inline text-align-left header-sm">
              <img
                className="icon-size-xs"
                src="/icons/lorrydelivery.png"
                alt="livestock"
              />
              <p className="padding-left-2 padding-top-1">
                {" "}
                Delivery at doorstep
              </p>
            </Col>
            <Col className="display-inline text-align-left header-sm">
              {" "}
              <img
                className="icon-size-xs"
                src="/icons/support.png"
                alt="livestock"
              />
              <p className="padding-left-2 padding-top-1">
                Contact Seller directly
              </p>
            </Col>
            <Col className="display-inline header-sm">
              {" "}
              <img
                className="icon-size-xs"
                src="/icons/gift.png"
                alt="livestock"
              />
              <p className="padding-left-2 padding-top-1">
                Connect and grow your business
              </p>
            </Col>
          </Row>
        </Container>
      </div>
      <hr />
      <div>
        <Container>
          <Row xs={1} md={2}>
            <Col>
              <Container>
                <Row className="header-lg">
                  <Col>Useful Links</Col>
                  <Col>My Account</Col>
                  <Col>Company</Col>
                </Row>
                <Row className="sub-header-xs">
                  <Col>
                    <a href="/buy/0" className="text-decor-none color-black">
                      Newly Listed
                    </a>
                  </Col>
                  <Col>
                    <a
                      href={
                        Cookies.get("id")
                          ? `/userprofile/${CryptoJS.AES.decrypt(
                              Cookies.get("id"),
                              `${configData.CRYPTO_SECRET_KEY}`
                            ).toString(CryptoJS.enc.Utf8)}`
                          : "/"
                      }
                      className="text-decor-none color-black"
                    >
                      My Profile
                    </a>
                  </Col>
                  <Col>
                    {" "}
                    <a href="/aboutus" className="text-decor-none color-black">
                      About Us
                    </a>
                  </Col>
                </Row>
                <Row className="sub-header-xs">
                  <Col>
                    <a href="/forum" className="text-decor-none color-black">
                      Community
                    </a>
                  </Col>
                  <Col>
                    <a href="/orders" className="text-decor-none color-black">
                      My Orders
                    </a>
                  </Col>
                  <Col>
                    <a href="/support" className="text-decor-none color-black">
                      Contact Us
                    </a>
                  </Col>
                </Row>
                <Row className="sub-header-xs">
                  <Col>
                    <a href="/buy/0" className="text-decor-none color-black">
                      Best Seller
                    </a>
                  </Col>
                  <Col>
                    <a href="/my-cart" className="text-decor-none color-black">
                      My Cart
                    </a>
                  </Col>
                  <Col>
                    <a
                      href="/how-it-works"
                      className="text-decor-none color-black"
                    >
                      How-it-Works
                    </a>
                  </Col>
                </Row>
                <Row className="sub-header-xs">
                  <Col></Col>
                  <Col></Col>
                  <Col>
                    <a
                      href="/privacy-policy"
                      className="text-decor-none color-black"
                    >
                      Privacy Policy
                    </a>
                  </Col>
                </Row>
                <Row className="sub-header-xs">
                  <Col></Col>
                  <Col></Col>
                  <Col>
                    <a href="/terms" className="text-decor-none color-black">
                      Terms & Conditions
                    </a>
                  </Col>
                </Row>
              </Container>
            </Col>
            <Col>
              <Container className="padding-top-3">
                <Row className="text-align-center">
                  <Col>
                    <div>
                      <div>
                        <input
                          className="form-control"
                          onChange={handleemailchange}
                          placeholder="Enter your email..."
                          value={email}
                        />
                      </div>
                      <div>
                        <p> </p>
                        <p className="header-xxs">
                          STAY UP TO DATE ON OUR LATEST NEWS
                        </p>
                        <p>
                          Subscribe & get discounts. Get E-mail updates about
                          our latest shop and Special offers!
                        </p>
                      </div>
                      <div>
                        <p className="header-xxs">
                          2/216(a) Bhagavathi Nagar, Coimbatore, Tamil Nadu,
                          India - 641105
                        </p>
                      </div>
                      <div>
                        <a href="https://twitter.com/Farmzonnmarket">
                          <img
                            className="icon-size-xxs"
                            src="/icons/whatsapp-color.png"
                            alt="livestock"
                          />
                        </a>
                        <span> </span>
                        <span> </span>
                        <a href="https://www.instagram.com/farmzonnmarketplace/">
                          <img
                            className="icon-size-xxs"
                            src="/icons/instagram.png"
                            alt="livestock"
                          />
                        </a>
                        <span> </span>
                        <span> </span>
                        <a href="https://www.facebook.com/profile.php?id=100087890924536">
                          <img
                            className="icon-size-xxs"
                            src="/icons/facebook.png"
                            alt="livestock"
                          />
                        </a>
                      </div>
                      <p></p>
                      <div>
                        {/* <a
                          className="header-xs text-decor-none"
                          href={"tel:+919629620681"}
                        >
                          <img
                            className="icon-size-xxs"
                            src="/icons/phone-call.png"
                            alt="livestock"
                          />
                          <span> </span>
                          +91-9629620681
                        </a> */}
                        <span> </span> <span> </span>
                        <a
                          className="header-xs text-decor-none"
                          href={"mailto:hello@farmzonn.com"}
                        >
                          <img
                            className="icon-size-xxs"
                            src="/icons/email.png"
                            alt="livestock"
                          />
                          <span> </span>
                          hello@farmzonn.com
                        </a>
                      </div>
                    </div>
                  </Col>
                </Row>
              </Container>
            </Col>
          </Row>
        </Container>
      </div>
      <hr />
      <div className="footer text-align-center">
        <p>©2024 Farmzonn.com. All rights reserved.</p>
      </div>
    </div>
  );
}
