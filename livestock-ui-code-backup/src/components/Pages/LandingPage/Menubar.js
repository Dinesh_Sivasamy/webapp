import React, { useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import "../../NavMenu.css";

import { FaUser } from "react-icons/fa";

// import React, { useEffect, useState } from "react";
import Dropdown from "react-bootstrap/Dropdown";

import Cookies from "js-cookie";
import CryptoJS from "crypto-js";

import SignInModal from "../User/SignInModal";

import configData from "../../../config.json";

export default function Menubar() {
  const [collapsed, setCollapsed] = useState(true);

  let indianRupeeLocale = Intl.NumberFormat("en-IN");
  const [referredUsersCount, setReferredUsersCount] = useState(0);
  const manageAddress = "manage/invalid";

  const [show, setShow] = useState(false);

  const handleShow = () => {
    let loginname = Cookies.get("name");

    if (!loginname || loginname == null || loginname == "") {
      setShow(true);
    } else {
      console.log("Already logged in user, hence not showing signin popup");
    }
  };
  const handleClose = () => {
    setShow(false);
    window.location.reload();
  };

  const handleManageAddressOnClick = (e) => {
    console.log(e);
    e.currentTarget.href = e.currentTarget.href + manageAddress;
  };

  const toggleNavbar = () => {
    setCollapsed(!collapsed);
  };

  const [userrefcode, setUserrefcode] = useState(
    Cookies.get("refcode") && Cookies.get("refcode") != "None" ? 1 : 0
  );

  const [referralcode, setReferralcode] = useState(
    userrefcode ? Cookies.get("refcode") : null
  );

  return (
    <div className="padding-top-1">
      <div>
        <SignInModal
          show={show}
          header="User Login "
          hideclosebutton={false}
          onHide={handleClose}
          setModalClose={handleClose}
        />
      </div>
      <div>
        <Container>
          <Row md={3}>
            <Col>
              <div className="padding-top-3">
                <a
                  href="/"
                  className="text-decor-none header-xxs transform-y-5"
                >
                  <img
                    className="icon-size-xs img-border-6"
                    src="/images/FZ_LOGO.png"
                    alt="livestock"
                  />
                </a>
              </div>
            </Col>
            <Col></Col>
            <Col>
              <div className="display-inline padding-top-1 text-align-right">
                <a
                  onClick={handleShow}
                  className="text-decor-none padding-top-2"
                >
                  <img
                    className="icon-size-xs"
                    src="/icons/contact.png"
                    alt="livestock"
                  />
                </a>
                <span className="padding-left-2"></span>
                <a
                  href="/my-cart"
                  className="text-decor-none padding-top-2 header-xxs"
                >
                  <img
                    className="icon-size-xs"
                    src="/icons/add-to-cart.png"
                    alt="livestock"
                  />
                </a>
                <span className="padding-left-2"></span>

                <span className="padding-left-2"></span>

                <Dropdown>
                  <Dropdown.Toggle
                    size="sm"
                    variant="Secondary"
                    className="collasible-nav-dropdown"
                  >
                    <img
                      className="icon-size-xs"
                      src="/icons/square.png"
                      alt="livestock"
                    />
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    {Cookies.get("name") != null ? (
                      <div>
                        <Dropdown.Item
                          href={`/userprofile/${CryptoJS.AES.decrypt(
                            Cookies.get("id"),
                            `${configData.CRYPTO_SECRET_KEY}`
                          ).toString(CryptoJS.enc.Utf8)}`}
                        >
                          My Profile
                        </Dropdown.Item>
                      </div>
                    ) : (
                      <div>
                        <Dropdown.Item href="#" onClick={handleShow}>
                          <FaUser /> Login/register
                        </Dropdown.Item>
                        <SignInModal
                          show={show}
                          setModalClose={handleClose}
                          onHide={handleClose}
                        />
                      </div>
                    )}
                    <Dropdown.Divider />
                    {Cookies.get("type") != null ? (
                      <div>
                        <div>
                          {["2", "3", "4"].indexOf(
                            CryptoJS.AES.decrypt(
                              Cookies.get("type"),
                              `${configData.CRYPTO_SECRET_KEY}`
                            ).toString(CryptoJS.enc.Utf8)
                          ) > -1 ? (
                            <div>
                              <Dropdown.Item href="/seller-product/">
                                View Listed Products
                              </Dropdown.Item>
                            </div>
                          ) : null}
                        </div>

                        <div>
                          {CryptoJS.AES.decrypt(
                            Cookies.get("type"),
                            `${configData.CRYPTO_SECRET_KEY}`
                          ).toString(CryptoJS.enc.Utf8) == 9 ? (
                            <div>
                              <Dropdown.Item href="/orders">
                                View Orders
                              </Dropdown.Item>
                              <Dropdown.Item href="/seller-product/">
                                View Listed Products
                              </Dropdown.Item>
                            </div>
                          ) : null}
                        </div>
                        <div>
                          {["1", "3", "5"].indexOf(
                            CryptoJS.AES.decrypt(
                              Cookies.get("type"),
                              `${configData.CRYPTO_SECRET_KEY}`
                            ).toString(CryptoJS.enc.Utf8)
                          ) > -1 ? (
                            <div>
                              <Dropdown.Item href="/orders">
                                My Orders
                              </Dropdown.Item>
                              <Dropdown.Item>
                                Your total bonus:
                                <span className="nav-ref-bonus-amount">
                                  &#8377;{" "}
                                  {indianRupeeLocale.format(
                                    parseInt(referredUsersCount, 10) * 25 +
                                      parseInt(userrefcode, 10) * 150
                                  )}
                                </span>
                              </Dropdown.Item>
                            </div>
                          ) : null}
                        </div>
                      </div>
                    ) : (
                      <div>
                        <Dropdown.Item href="/orders">My Orders</Dropdown.Item>
                      </div>
                    )}
                    <Dropdown.Divider />
                    {Cookies.get("type") != null &&
                    ["5"].indexOf(
                      CryptoJS.AES.decrypt(
                        Cookies.get("type"),
                        `${configData.CRYPTO_SECRET_KEY}`
                      ).toString(CryptoJS.enc.Utf8)
                    ) > -1 ? (
                      <div>
                        <Dropdown.Item href="/vet-verify/">
                          Livestock Checkup
                        </Dropdown.Item>
                        <Dropdown.Divider />
                      </div>
                    ) : null}

                    {Cookies.get("name") != null ? (
                      <div>
                        <Dropdown.Item
                          href="/my-addresses/"
                          onClick={handleManageAddressOnClick}
                        >
                          Manage Address
                        </Dropdown.Item>
                        <Dropdown.Item href="/my-enquiries">
                          View My Enqueries
                        </Dropdown.Item>
                        <Dropdown.Item href="/logout">SignOut</Dropdown.Item>
                      </div>
                    ) : null}
                  </Dropdown.Menu>
                </Dropdown>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
      <div className="padding-bottom-1">
        <hr />
        <div className="padding-bottom-1">
          <Container>
            <Row xs={3} md={6}>
              <Col>
                <a
                  href="/buy/0"
                  className="text-decor-none-onhover color-black header-xxs transform-y-5"
                >
                  Buy
                </a>
              </Col>
              <Col>
                <a
                  href="/sell"
                  className="text-decor-none-onhover color-black header-xxs"
                >
                  Sell
                </a>
              </Col>
              <Col>
                <a
                  href="/forum"
                  className="text-decor-none-onhover color-black header-xxs"
                >
                  Ask Experts
                </a>
              </Col>
              <Col>
                <a
                  href="/view-requirements"
                  className="text-decor-none-onhover color-black header-xxs"
                >
                  View Enquiries
                </a>
              </Col>
              <Col>
                <a
                  href="/add-enquiry"
                  className="text-decor-none-onhover color-black header-xxs"
                >
                  Post Enquiries
                </a>
              </Col>
              <Col>
                <a
                  href="/services"
                  className="text-decor-none-onhover color-black header-xxs"
                >
                  Services
                </a>
              </Col>
            </Row>
          </Container>
        </div>
        <hr />
      </div>
    </div>
  );
}
