import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import configData from "../../../config.json";

import cattle from "../Home/images/default_product_img.jpg";

export default function Trendingmobile(props) {
  const getproductimage = (productvariantimages) => {
    if (
      productvariantimages &&
      productvariantimages.length > 0 &&
      productvariantimages[0].image
    ) {
      return `${configData.IMG_FOLDER_PATH}${productvariantimages[0].image}`;
    } else {
      return `${cattle}`;
    }
  };

  return (
    <div className="hide-in-desktop padding-top-5">
      <div className="display-inline padding-left-5 padding-bottom-1">
        <span className="header-sm">Trending this week</span> <span> </span>{" "}
        <img
          className="icon-size-sm padding-left-2"
          src="/icons/dogwalk.png"
          alt="livestock"
        />{" "}
      </div>
      <div className="bgcolor-primary-pista img-border-6 padding-bottom-1 horizontal-scroll-mobile-only">
        <Container>
          <Row xs={6}>
            {props.productlist && props.productlist.length > 0
              ? props.productlist.map((product, id) => (
                  <Col key={id} className="col-6">
                    <a
                      href={`/product/${product.id}`}
                      className="text-decor-none-onhover"
                    >
                      <div className="list-box-mobile bgcolor-neutral-white">
                        <div>
                          <img
                            className="img-size-list-box-mobile padding-7"
                            src={getproductimage(
                              product.product_variant[0].product_images
                            )}
                            alt="livestock"
                          />
                        </div>
                        <div className="text-align-left sub-header-xxs padding-left-5">
                          <span className="header-xxxs width-20px">
                            {product.name}
                          </span>
                          <span className="sub-header-xxxs">
                            {" "}
                            {product.breed +
                              " | " +
                              product.product_variant[0].weight +
                              " Kgs"}
                          </span>
                        </div>
                      </div>
                    </a>
                  </Col>
                ))
              : null}

            {/* // <Col className="col-6">
            //   <div className="list-box-mobile bgcolor-neutral-white">
            //     <div>
            //       <img
            //         className="img-size-list-box-mobile padding-7"
            //         src="/images/cattle.jpg"
            //         alt="livestock"
            //       />
            //     </div>
            //     <div className="text-align-left sub-header-xxs padding-left-5">
            //       <span className="header-xxxs">Traditional Country Dogs</span>
            //       <br />
            //       <span className="sub-header-xxxs">
            //         {" "}
            //         Country Breed | 2.00 Kgs
            //       </span>
            //     </div>
            //   </div>
            // </Col>             */}
          </Row>
        </Container>
      </div>
    </div>
  );
}
