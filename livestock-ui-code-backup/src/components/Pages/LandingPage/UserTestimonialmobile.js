import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import Carousel from "react-bootstrap/Carousel";

export default function UserTestimonialmobile() {
  return (
    <div className="bgcolor-primary-pink hide-in-desktop">
      <Container className="height-160">
        <Row className="padding-top-2" xs={2}>
          <Col>
            <div className="display-inline">
              <p className="header-xsm text-align-center">
                Our Happy Customers !!
              </p>
              <img
                className="icon-size-xs padding-left-1"
                src="/icons/pawprints.png"
                alt="livestock"
              />
            </div>
            <div>
              <Carousel controls={true} interval={5000} indicators={false}>
                <Carousel.Item>
                  <h6>Very useful to get animal buyers quicker and easier!</h6>
                  <p>-Yash</p>
                </Carousel.Item>
                <Carousel.Item>
                  <h6>
                    Easy to connect with Sellers and get details of cattles!
                  </h6>
                  <p>-Ramamurthy</p>
                </Carousel.Item>
                <Carousel.Item>
                  <h6>
                    Transparent and easy to use, very good platform for farmers!
                  </h6>
                  <p>-Balamurugan</p>
                </Carousel.Item>
              </Carousel>
            </div>
          </Col>
          <Col>
            <div className="position-abs-bottom-5 align-items-right">
              <img
                className="img-size-170-110"
                src="/images/Animalgroup.png"
                alt="livestock"
              />
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
