import React, { useEffect, useState } from "react";
import Cookies from "js-cookie";
import CryptoJS from "crypto-js";
import configData from "../../../config.json";

import Likes from "./utils/Likes";
import Comments from "./utils/Comments";

import "./Threads.css";
import { Button, Col, Container } from "react-bootstrap";
import { Row } from "reactstrap";

import { useHistory } from "react-router-dom";

import SignInModal from "../User/SignInModal";
export default function Threads() {
  const history = useHistory();

  const [content, setContent] = useState("");
  const [threadList, setThreadList] = useState([]);

  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);
  const handleClose = () => {
    setShow(false);
    window.location.reload();
  };

  const [loggedInUserId, setLoggedInUserId] = useState(
    Cookies.get("id")
      ? CryptoJS.AES.decrypt(
          Cookies.get("id"),
          `${configData.CRYPTO_SECRET_KEY}`
        ).toString(CryptoJS.enc.Utf8)
      : null
  );

  const handleAddComment = (threadId, content) => {
    Cookies.set("currentthread", content);
    history.push(`/forum/thread/${threadId}/replies`);
  };

  const [validationMessages, setValidationMessages] = useState(null);

  useEffect(() => {
    fetch("/api/v1/thread", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then((result) => {
        setThreadList(result);
      })
      .catch((err) => {
        console.log("Failed to fetch threads");
        console.log(err);
      });

    window.scrollTo(0, 0);
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    if (content.trim().length == 0) {
      setValidationMessages("Discussion Content cannot be blank");
      return;
    } else {
      setValidationMessages(null);
    }

    var requestJson = {
      PostedBy: loggedInUserId,
      content: content,
    };

    fetch("/api/v1/thread", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
      body: JSON.stringify(requestJson),
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response);
        }
      })
      .then((result) => {
        console.log(result);
        window.location.reload();
      })
      .catch((err) => {
        console.log("Failed to save threads");
        console.log(err);
        setValidationMessages(
          "Failed to share your discussion content, Please try again after sometime."
        );
      });
  };

  return (
    <div className="CreateThread">
      <div className="thread-heading">
        Farmers' Forum
        <hr />
      </div>

      {Cookies.get("id") != null ? (
        <Container className="thread-container-main">
          <Row xs={1} md={3} lg={3}>
            <Col>
              <h2 className="thread-homeTitle">Create new discussion</h2>
            </Col>
          </Row>
          <br />
          <Row>
            <Col md={3}></Col>
            <Col md={5}>
              <form
                className="thread-new-content-submit"
                onSubmit={handleSubmit}
              >
                Post your question / the topic you want to discuss in below!
                <br />
                <textarea
                  className="form-control"
                  rows={5}
                  value={content}
                  onChange={(e) => setContent(e.target.value)}
                />
                <span className="span-warning-message">
                  **Any content violating our community guidelines, including
                  hate speech, explicit material, or harassment, will be
                  promptly removed.
                </span>
                <br />
                <span className="error">{validationMessages}</span>
                {/* <Button type="submit">Create discussion</Button> */}
                <button className="homeBtn">CREATE DISCUSSION</button>
              </form>
            </Col>
          </Row>
          <Row>
            <Col>
              <h2 className="thread-listTitle">Discussions</h2>
            </Col>
          </Row>
          <br />
          <Row>
            <Col>
              <div className="thread__container">
                {threadList.map((thread) => (
                  <div className="thread__item" key={thread.id}>
                    <a
                      onClick={() =>
                        handleAddComment(thread.id, thread.content)
                      }
                    >
                      {thread.content}
                    </a>
                    <div className="react__container">
                      <Likes
                        numberOfLikes={thread.likescount}
                        threadId={thread.id}
                      />
                      <Comments
                        numberOfComments={thread.thread_reply?.length}
                        threadId={thread.id}
                        content={thread.content}
                      />
                    </div>
                  </div>
                ))}
              </div>
            </Col>
          </Row>
        </Container>
      ) : (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
          }}
          className="sell-user-login-request"
        >
          <div className="sell-user-login-request-header">
            You have not logged in yet! Please login to view / add questions and
            discussion points related to agriculture, farming, livestock and
            pets! <br />
            <br />
            Now connect with experts for FREE!
          </div>
          <div className="sell-user-login-request-header-link-div">
            <Button
              className="sell-user-login-request-header-link"
              onClick={handleShow}
            >
              Sign In/Register here!
            </Button>
          </div>
          <div>
            <SignInModal
              show={show}
              setModalClose={handleClose}
              onHide={handleClose}
            />
          </div>
        </div>
      )}
    </div>
  );
}
