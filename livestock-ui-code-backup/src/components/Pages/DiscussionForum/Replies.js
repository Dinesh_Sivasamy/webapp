import React, { useEffect, useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import Cookies from "js-cookie";
import CryptoJS from "crypto-js";
import configData from "../../../config.json";
import { Button, Col, Container, Row } from "react-bootstrap";

export default function Replies() {
  const [replyList, setReplyList] = useState([]);
  const [reply, setReply] = useState("");
  const [title, setTitle] = useState(Cookies.get("currentthread"));
  const [validationError, setvalidationError] = useState(null);

  const history = useHistory();
  const { threadid } = useParams();

  const [userId, setUserId] = useState(
    Cookies.get("id")
      ? CryptoJS.AES.decrypt(
          Cookies.get("id"),
          `${configData.CRYPTO_SECRET_KEY}`
        ).toString(CryptoJS.enc.Utf8)
      : null
  );

  useEffect(() => {
    const fetchReplies = () => {
      fetch(`/api/v1/thread/Replies/id=${encodeURIComponent(threadid)}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setReplyList(data);
        })
        .catch((err) => console.error(err));
    };
    fetchReplies();
  }, [threadid]);

  const addReply = () => {
    if (reply.trim().length === 0) {
      setvalidationError("Reply cannot be blank.");
      return;
    } else {
      setvalidationError(null);
    }

    var request = {
      thread: threadid,
      RepliedBy: userId,
      reply: reply,
    };

    fetch(`/api/v1/thread/Replies/id=${encodeURIComponent(threadid)}`, {
      method: "POST",
      body: JSON.stringify(request),
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log("reply stored successfully");
        console.log(data);
        alert("Your reply sent successfully!");
        window.location.reload();
      })
      .catch((err) => console.error(err));
  };

  //👇🏻 This function when triggered when we add a new reply
  const handleSubmitReply = (e) => {
    e.preventDefault();
    console.log({ reply });
    addReply();
    setReply("");
  };

  return (
    <div className="CreateThread">
      <div className="thread-heading">
        Farmers' Forum
        <hr />
      </div>

      <Container className="thread-container-main">
        <Row>
          <Col md={3}></Col>
          <Col md={8}>
            <h1 className="repliesTitle">{title}</h1>
            {/* <h1 className="repliesTitle">{title}</h1> */}
          </Col>
        </Row>
        <br />
        <Row xs={1} md={3} lg={3}>
          <Col md={3}></Col>
          <Col md={5}>
            <form
              className="thread-new-content-submit"
              onSubmit={handleSubmitReply}
            >
              <label htmlFor="reply">Add your comment/reply here</label>
              <br />
              <textarea
                className="form-control"
                rows={3}
                value={reply}
                onChange={(e) => setReply(e.target.value)}
                type="text"
                name="reply"
              />
              <span className="span-warning-message">
                **Any content violating our community guidelines, including hate
                speech, explicit material, or harassment, will be promptly
                removed.
              </span>
              <span className="error">{validationError}</span>
              <br />
              {/* <Button type="submit">SEND</Button> */}
              <button className="modalBtn">POST</button>
            </form>
          </Col>
        </Row>
        <Row>
          <Col>
            <h2 className="thread-listTitle">Recent Replies</h2>
          </Col>
        </Row>
        <Row>
          <Col md={2}></Col>
          <Col md={8}>
            <div className="reply__container">
              {replyList.map((reply) => (
                <div className="reply__item" key={reply.id}>
                  <label>{reply.reply}</label>
                  <p className="reply_replyby"> -By: {reply.RepliedBy_name}</p>
                </div>
              ))}
            </div>
          </Col>
        </Row>
        <Row>
          <Col xs={6} md={8}></Col>
          <Col xs={5} md={3}>
            <a className="thread-back" href="/forum">
              {" "}
              {"<- Back to forum"}
            </a>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
