import React, { useEffect, useState } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Cookies from "js-cookie";

import Banner from "../ProductBuy/Banner";
import "../ProductBuy/Banner.css";
// import ct from './images/slide1.jpg';
import { Container, Button } from "react-bootstrap";
import configData from "../../../config.json";
import Loader from "../../Loader";

import cattle from "../Home/images/livestock.png";

export default function Bann() {
  const [isLoading, setLoading] = useState(true);
  const [productDetail, setProductDetail] = useState([]);

  useEffect(() => {
    localStorage.removeItem("productlist");
    fetch("/api/v1/product-detail-list/top=8&category=0&page=home", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": Cookies.get("csrftoken"),
      },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          setLoading(false);
          throw new Error(response);
        }
      })
      .then(function (resp) {
        console.log(resp);
        setProductDetail(resp);
        localStorage.setItem("productlist", JSON.stringify(resp));
        setLoading(false);
      });
  }, []);

  const getproductimage = (productvariantimages) => {
    if (
      productvariantimages &&
      productvariantimages.length > 0 &&
      productvariantimages[0].image
    ) {
      return `${configData.IMG_FOLDER_PATH}${productvariantimages[0].image}`;
    } else {
      return `${cattle}`;
    }
  };

  return (
    <div className="homepage-section-unlimted-height">
      <p className="title">Newest</p>
      {isLoading ? (
        <Loader />
      ) : (
        <div>
          <Container>
            <div className="cards__container">
              <div className="cards__wrapper">
                <Row xs={1} md={4}>
                  {productDetail.map((product, id) => (
                    <Col key={id}>
                      <div>
                        <Banner
                          key={id}
                          src={getproductimage(
                            product.product_variant[0].product_images
                          )}
                          text={product.description}
                          productId={product.id}
                          label={product.category}
                          isp={
                            product.product_variant[0].is_pregnant
                              ? "Pregnant"
                              : ""
                          }
                          new="newest"
                          path="/Product-Detail"
                          name={product.name}
                          issellerverified={product.seller.is_verified}
                          breed={product.breed}
                          gender={product.product_variant[0].gender}
                          milkproduce={
                            product.product_variant[0].milk_produce_per_Day
                          }
                          offsprings={
                            product.product_variant[0].no_of_offsprings
                          }
                          discountprice={product.product_variant[0].offer_price}
                          orignialprice={
                            product.product_variant[0].original_price
                          }
                          weight={product.product_variant[0].weight}
                          city={product.pickup.address.city}
                          state={product.pickup.address.state}
                          vetcheck={product.veterinary_check}
                          isSold={
                            product.product_variant[0].stock == 0 ||
                            product.is_sold
                          }
                          modifieddate={
                            product.modified_date
                              ? product.modified_date
                              : product.created_date
                          }
                          stock={product.product_variant[0].stock}
                        />
                      </div>
                      <br />
                    </Col>
                  ))}
                </Row>
              </div>
            </div>
          </Container>
        </div>
      )}
    </div>
  );
}
