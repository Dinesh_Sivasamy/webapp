import React from "react";
import "./Footer.css";
import { Link } from "react-router-dom";
import {
  TiSocialTwitter,
  TiSocialInstagram,
  TiSocialFacebook,
} from "react-icons/ti";

export default function Footer() {
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <footer className="footer-distributed">
      <div className="footer-left">
        <Link to="/" onClick={scrollToTop}>
          <p className="footer-right">FarmZonn.com</p>
        </Link>

        <h3>Feedback from Customers</h3>
        <p className="footer-links">
          <p className="footer-pipe">
            <i>
              I have used the Farmzonn website for selling goat and chicken. I
              have always had an excellent response. It is an easy website to
              use, and I have uploaded a variety of photos, and easily updated
              them. I think it is now the go-to website for farmers.
            </i>
          </p>
          <p className="footer-pipe">- Vivek</p>
        </p>

        <p className="footer-links">
          <p className="footer-pipe">
            <i>
              I was able to create my listing in minutes. Farmzonn agents helped
              me with the listing and all the communications and transactions
              were tranparent and I was getting quick response from Farmzonn.
            </i>
          </p>
          <p className="footer-pipe">- Krisha Prabhu</p>
        </p>

        <h3>
          <a name="contact-us">Contact Us</a>
        </h3>

        <div className="footer-links">
          <a href="https://twitter.com/Farmzonnmarket">
            <TiSocialTwitter />
          </a>
          <span> </span>
          <a href="https://www.instagram.com/farmzonnmarketplace/">
            <TiSocialInstagram />
          </a>
          <span> </span>
          <a href="https://www.facebook.com/profile.php?id=100087890924536">
            <TiSocialFacebook />
          </a>
        </div>

        <p className="footer-links">
          <p className="footer-pipe">
            2/216(a) Bhagavathi Nagar, Coimbatore, Tamil Nadu, India - 641105
          </p>
          <p className="footer-pipe">hello@farmzonn.com</p>
          <p className="footer-pipe">+91-9629620681</p>
        </p>

        <p className="footer-company-name">
          ©2024 Farmzonn.com. All rights reserved.
        </p>
      </div>
      <div className="footer-center">
        <p>
          <h3>
            <a name="site-links">Site Links</a>
          </h3>
          <a href="/aboutus" onClick={scrollToTop}>
            About us
          </a>
          <p className="footer-pipe">|</p>
          <a href="/how-it-works" onClick={scrollToTop}>
            How-it-works
          </a>
          <p className="footer-pipe">|</p>
          <a href="/terms" onClick={scrollToTop}>
            Terms & Conditions
          </a>
          <p className="footer-pipe">|</p>
          <a href="/privacy-policy" onClick={scrollToTop}>
            Privacy Policy
          </a>
          <p className="footer-pipe">|</p>
          <a href="/support" onClick={scrollToTop}>
            Support
          </a>
        </p>
      </div>
    </footer>
  );
}
