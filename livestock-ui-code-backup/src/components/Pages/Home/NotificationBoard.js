import { Image } from "react-bootstrap";
import Cookies from "js-cookie";
import { useState } from "react";

import "./Home.css";
import Slidercomp from "./Slider";

export default function NotificationBoard() {
  const [referralcode, setReferralcode] = useState(
    Cookies.get("refcode") && Cookies.get("refcode") != "None"
      ? " Use referral code " + Cookies.get("refcode") + " and register."
      : ""
  );

  return (
    <div className="notification-div">
      <a
        href={
          "https://api.whatsapp.com/send/?text=Hi! Now buying and Selling livestocks is SO EASY. Checkout https://Farmzonn.com to buy/sell healthy livestock and high quality livestock needs from verified sellers anywhere in India and get delivered to your doorstep." +
          referralcode +
          " Flat 50% DISCOUNT on all orders. REFFER and WIN upto Rs.5,000/- off!&phone="
        }
        target="_blank"
      >
        {/* <Image className="notification-img" src='/notification-referral.png' fluid /> */}
        <Slidercomp />
      </a>
      {/* <span className="notification-image-attribution">Designed by Freepik</span> */}
    </div>
  );
}
