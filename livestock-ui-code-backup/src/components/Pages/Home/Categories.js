import CardItems from "./CardItems";
import "./CardItem.css";

export default function Categories() {
  return (
    <div className="homepage-section">
      <p className="title">Browse by Category</p>
      <CardItems />
    </div>
  );
}
