import React from "react";
import { Link } from "react-router-dom";
import "./CardItem.css";
import all from "./images/all.png";
import cattle from "./images/cattle.png";
import goat from "./images/goat.png";
import sheep from "./images/sheep.png";
import hen from "./images/hen.png";
import dog from "./images/dog.png";
import { Container, Row, Col } from "react-bootstrap";

export default function CardItems() {
  return (
    <Container>
      <div className="boxes">
        <Row>
          <div className="carditem-box">
            <Col>
              <a href="/buy/0">
                <div className="box">
                  {/* <div className="icon">
                    <img src={all} alt="All" width="40px" height="35px"></img>
                  </div> */}
                  <div className="content-3letter">
                    <h3>BUY</h3>
                  </div>
                </div>
              </a>
            </Col>
            <Col>
              <a href="/Sell">
                <div className="box">
                  {/* <div className="icon">
                    <img src={cattle} alt="cow" width="25%" height="35px"></img>
                  </div> */}
                  <div className="content-4letter">
                    <h3>SELL</h3>
                  </div>
                </div>
              </a>
            </Col>
            <Col>
              <a href="/add-enquiry">
                <div className="box">
                  {/* <div className="icon">
                    <img src={goat} alt="goat" width="40px" height="35px"></img>
                  </div> */}
                  <div className="content-morethan4letter">
                    <h3>POST ENQUIRY</h3>
                  </div>
                </div>
              </a>
            </Col>
            <Col>
              <a href="/view-requirements">
                <div className="box">
                  {/* <div className="icon">
                    <img
                      src={sheep}
                      alt="sheep"
                      width="40px"
                      height="35px"
                    ></img>
                  </div> */}
                  <div className="content-morethan4letter">
                    <h3>View ENQUIRY</h3>
                  </div>
                </div>
              </a>
            </Col>
            <Col>
              <a href="/forum">
                <div className="box">
                  {/* <div className="icon">
                    <img src={hen} alt="hen" width="40px" height="35px"></img>
                  </div> */}
                  <div className="content-morethan4letterOneliner">
                    <h3>Ask Experts</h3>
                  </div>
                </div>
              </a>
            </Col>
            {/* <Col>
              <a href="/buy/5">
                <div className="box">
                  <div className="icon">
                    <img src={dog} alt="dog" width="40px" height="35px"></img>
                  </div>
                  <div className="content-3letter">
                    <h3>Dog</h3>
                  </div>
                </div>
              </a>
            </Col> */}
          </div>
        </Row>
      </div>
    </Container>
  );
}
