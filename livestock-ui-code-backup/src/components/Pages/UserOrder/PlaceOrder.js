import React, { useEffect, useState } from "react";
import { Alert, Button, Card, Col, Container, Row } from "react-bootstrap";
import Cookies from "js-cookie";
import CryptoJS from "crypto-js";
import OrderedItem from "./Sections/OrderedItem";
import Loader from "../../Loader";
import {
  IoMdCheckmarkCircleOutline,
  IoMdCloseCircleOutline,
} from "react-icons/io";

import "./OrderPage.css";
import { Link, useParams } from "react-router-dom";
import configData from "../../../config.json";

export default function PlaceOrder() {
  let indianRupeeLocale = Intl.NumberFormat("en-IN");
  const { addressId } = useParams();
  const [isLoading, setLoading] = useState(true);
  const [orderPlaceSuccess, setOrderPlaceSuccess] = useState(false);
  const [orderPlaceFail, setOrderPlaceFail] = useState(false);

  const [orderDetail, setOrderDetail] = useState([]);
  const [orderPriceTotal, setOrderPriceTotal] = useState(0);
  const [orderDiscountTotal, setOrderDiscountTotal] = useState(0);
  const [orderDeliveryTotal, setOrderDeliveryTotal] = useState(0);
  const [orderGrandTotal, setOrderGrandTotal] = useState(0);

  const [orderdiscounts, setOrderdiscounts] = useState([]);
  const [isorderdeliverable, setIsorderdeliverable] = useState(false);

  const [siteDiscount, setSiteDiscount] = useState(0);
  let sitediscount = 0;

  const [firstOrderdiscount, setFirstOrderdiscount] = useState(0);
  const [referraldiscount, setReferraldiscount] = useState(0);

  const [dicountdict, setDicountdict] = useState({});

  // let orderPriceTotal = 0
  // let orderDiscountTotal = 0
  // let orderDeliveryTotal = 0
  // let orderGrandTotal = 0
  // let isorderdeliverable = false

  // let siteDiscount = 0
  // let firstOrderdiscount = 0
  // let referraldiscount = 0
  // let dicountdict = {}

  // let count = 0
  // console.log(count)

  useEffect(() => {
    if (Cookies.get("id") != null && orderDetail.length == 0) {
      window.scrollTo(0, 0);
      localStorage.removeItem("productlist");

      console.log("getting order for user");
      fetch(
        `/api/v1/buyercart/buyer=${encodeURIComponent(
          CryptoJS.AES.decrypt(
            Cookies.get("id"),
            `${configData.CRYPTO_SECRET_KEY}`
          ).toString(CryptoJS.enc.Utf8)
        )}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "X-CSRFToken": Cookies.get("csrftoken"),
          },
        }
      )
        .then((resp) => {
          if (resp.ok) {
            return resp.json();
          } else {
            throw new Error(resp.statusText);
          }
        })
        .then(function (resp) {
          console.log(resp);

          let deliveredpincodesdict = {};
          let deliveryaddressdetail = {};

          if (Cookies.get("deliveredpincodesdict") != null) {
            console.log("getting del pincode dict for user");
            deliveredpincodesdict = JSON.parse(
              CryptoJS.AES.decrypt(
                Cookies.get("deliveredpincodesdict"),
                `${configData.CRYPTO_SECRET_KEY}`
              ).toString(CryptoJS.enc.Utf8)
            );

            if (Cookies.get("deliveryaddressdetail") != null) {
              deliveryaddressdetail = JSON.parse(
                CryptoJS.AES.decrypt(
                  Cookies.get("deliveryaddressdetail"),
                  `${configData.CRYPTO_SECRET_KEY}`
                ).toString(CryptoJS.enc.Utf8)
              );

              checkIfAddressIsdeliverable(
                deliveredpincodesdict,
                deliveryaddressdetail,
                resp
              );
            }
          }

          getdiscount(resp);
          setOrderDetail(resp);
        })
        .catch((err) => {
          console.log(err);
        });
    }
    setLoading(false);
  }, []);

  const getdiscount = (order) => {
    console.log("get discount for user");

    let buyerpk = CryptoJS.AES.decrypt(
      Cookies.get("id"),
      `${configData.CRYPTO_SECRET_KEY}`
    ).toString(CryptoJS.enc.Utf8);
    let refcode = Cookies.get("refcode");

    fetch(
      `/api/v1/userdiscount/buyer=${encodeURIComponent(
        buyerpk
      )}&code=${encodeURIComponent(refcode)}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
      }
    )
      .then((resp) => {
        if (resp.ok) {
          return resp.json();
        } else {
          throw new Error(resp.statusText);
        }
      })
      .then(function (resp) {
        console.log(resp);
        calculateGrandTotal(order, resp);
      })
      .catch((err) => {
        console.log("caught error while fetching discount");
        calculateGrandTotal(order, null);
      });
  };

  // const getDeliveryAddressdetail  = (order) => {

  //     if (addressId) {

  //         let deliveredpincodesdict = {}

  //         fetch(`/api/v1/user-delivery-address/id=${encodeURIComponent(addressId)}`, {
  //             method: "GET",
  //             headers: {
  //                 'Content-Type': 'application/json',
  //                 'X-CSRFToken': Cookies.get('csrftoken')
  //             },
  //         })
  //             .then((resp) => {
  //                 if (resp.ok) {
  //                     return resp.json();
  //                 } else {
  //                     throw new Error(resp.statusText);
  //                 }
  //             })
  //             .then(function (resp) {
  //                 console.log(resp);

  //                 if (Cookies.get('deliveredpincodesdict') == null){

  //                     console.log('getting del pincode dict for user')
  //                     fetch("/api/v1/deliverable-pincodes", {
  //                         method: "GET",
  //                         headers: {
  //                             'Content-Type': 'application/json',
  //                             'X-CSRFToken': Cookies.get('csrftoken')
  //                         },
  //                     })
  //                         .then((response) => {
  //                             if (response.ok) {
  //                                 return response.json();
  //                             } else {
  //                                 throw new Error(response);
  //                             }
  //                         })
  //                         .then(function (resp) {
  //                             console.log(resp);

  //                             resp.forEach(pincodes => {
  //                                 deliveredpincodesdict[pincodes.pincode] = pincodes.isdeliverable
  //                             });

  //                             checkIfAddressIsdeliverable(order, resp, deliveredpincodesdict)
  //                             Cookies.set('deliveredpincodesdict', CryptoJS.AES.encrypt(JSON.stringify(deliveredpincodesdict), `${configData.CRYPTO_SECRET_KEY}`).toString(), { path: '/' });
  //                         })
  //                         .catch((err) => {
  //                             console.log(err);
  //                         })
  //                 } else {
  //                     console.log('getting delivery pincode dict for user')
  //                     deliveredpincodesdict = JSON.parse(CryptoJS.AES.decrypt(Cookies.get('deliveredpincodesdict'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8))
  //                     checkIfAddressIsdeliverable(order, resp, deliveredpincodesdict)
  //                 }
  //             })
  //             .catch((err) => {
  //                 console.log(err);
  //             })
  //     } else {
  //         setIsorderundeliverable(true);
  //     }
  // }

  const checkIfAddressIsdeliverable = (
    deliveredpincodesdict,
    deliveryaddress_detail,
    order
  ) => {
    let isorderpickupdeliverable = false;
    let isdeliveryaddressreachable = false;

    if (deliveredpincodesdict) {
      let ordercount = 0;
      if (order && order.length > 0) {
        for (let i = 0; i < order.length; i++) {
          if (deliveredpincodesdict[order[i].product.pickup.address.pincode]) {
            ordercount++;
          }
        }

        if (ordercount == order.length) {
          isorderpickupdeliverable = true;
        }
      }

      if (
        deliveryaddress_detail &&
        deliveredpincodesdict[deliveryaddress_detail.address.pincode]
      ) {
        isdeliveryaddressreachable = true;
      }

      if (isorderpickupdeliverable && isdeliveryaddressreachable) {
        setIsorderdeliverable(true);
      }
    }
  };

  const calculateTotalAmount = (order, discount) => {
    let total = 0;
    let discounttotal = 0;
    var productdiscounts = {};

    if (order && order.length > 0) {
      let ordercount = 0;
      order.map((item) => {
        let item_offer_price = item.product.product_variant[0].offer_price
          ? item.product.product_variant[0].offer_price
          : 0;
        let item_original_price =
          item.product.product_variant[0].original_price;
        let orderprice = parseFloat(item_offer_price) * item.items_count;
        let ordertotal = orderprice;
        let discountedprice = 0;
        let priceoffer =
          (parseFloat(item_original_price) - parseFloat(item_offer_price)) *
          item.items_count;
        let orderdiscount = 0;

        ordercount += 1;

        if (discount && ordercount == 1) {
          console.log("calculating discount for user");
          console.log(discount);

          if (discount.discount_percent > 0) {
            discountedprice =
              orderprice * (parseFloat(discount.discount_percent) / 100);
          } else if (discount.discount_rupees > 0) {
            discountedprice = parseFloat(discount.discount_rupees);
          }

          if (discountedprice > parseFloat(discount.max_discount_rupees)) {
            discountedprice = parseFloat(discount.max_discount_rupees);
          }

          ordertotal = orderprice;

          if (discount.typeid == 1) {
            setFirstOrderdiscount(discountedprice);
          } else if (discount.typeid == 2) {
            setReferraldiscount(discountedprice);
          }
          setSiteDiscount(discountedprice);
          sitediscount = discountedprice;
        }

        total += ordertotal;
        orderdiscount = priceoffer;
        discounttotal += orderdiscount + discountedprice;
        productdiscounts[item.id] = orderdiscount;
      });
    }

    setOrderPriceTotal(total);
    setOrderDiscountTotal(discounttotal);
    setDicountdict(productdiscounts);
    return total;
  };

  const calculateDelivaryAmount = (order) => {
    // setOrderDeliveryTotal(0)
    return 0;
  };

  const calculateGrandTotal = (order, discount) => {
    console.log("calculating total for user");
    let price = orderPriceTotal;
    let delivery = orderDeliveryTotal;

    price = calculateTotalAmount(order, discount);
    console.log("calc price amount: " + price);
    delivery = calculateDelivaryAmount(order);
    console.log("calc delivery amount: " + delivery);
    console.log("site discount: " + siteDiscount);
    console.log("site discount variable: " + sitediscount);

    setOrderGrandTotal(price + delivery - sitediscount);
  };

  const confirmOrderPlacement = () => {
    let refcode = Cookies.get("refcode");

    fetch(
      `/api/v1/confirm-order/buyer=${encodeURIComponent(
        CryptoJS.AES.decrypt(
          Cookies.get("id"),
          `${configData.CRYPTO_SECRET_KEY}`
        ).toString(CryptoJS.enc.Utf8)
      )}&address=${encodeURIComponent(addressId)}&code=${encodeURIComponent(
        refcode
      )}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": Cookies.get("csrftoken"),
        },
      }
    )
      .then((resp) => {
        if (resp.ok) {
          return resp.json();
        } else {
          throw new Error(resp.statusText);
        }
      })
      .then(function (resp) {
        console.log(resp);
        setLoading(false);
        setOrderPlaceSuccess(true);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
        setOrderPlaceFail(true);
      });
  };

  const handleConfirmClick = (e) => {
    console.log(e);
    confirmOrderPlacement();
  };

  return (
    <div>
      {isLoading ? (
        <Loader />
      ) : (
        <div>
          {orderPlaceSuccess || orderPlaceFail ? (
            <div className="place-order-response-div">
              {orderPlaceSuccess ? (
                <div>
                  <Alert key={1} variant="success">
                    <div className="order-heading">
                      Your order has been placed successfully
                      <hr />
                    </div>
                    <div className="order-message-content">
                      <IoMdCheckmarkCircleOutline /> Congrats, your order
                      confirmation has been completed, you can track your orders{" "}
                      <Link className="help-link" to="/orders">
                        here
                      </Link>
                      .
                    </div>
                  </Alert>
                </div>
              ) : orderPlaceFail ? (
                <div>
                  <Alert key={1} variant="danger">
                    <div className="order-heading">
                      Failed to place your order
                      <hr />
                    </div>
                    <div className="order-message-content">
                      <IoMdCloseCircleOutline /> Sorry! Failure during your
                      order confirmation, please{" "}
                      <Link className="help-link" to="/support">
                        contact our team
                      </Link>{" "}
                      for more details.
                    </div>
                  </Alert>
                </div>
              ) : null}
            </div>
          ) : (
            <div className="place-order-containers">
              <div className="order-container">
                <Container>
                  <div className="review-order-heading">Review your order</div>
                  <div className="review-order-heading-edit-option">
                    <a
                      href="/my-cart"
                      className="review-order-heading-edit-link"
                    >
                      {" "}
                      click here
                    </a>{" "}
                    to edit your cart items.
                  </div>
                  <span></span>
                  <hr />
                  <div className="order-card-list">
                    <div>
                      {orderDetail.map((orderItem, id) => {
                        return (
                          <div key={id}>
                            <Card style={{ width: "95%" }}>
                              <OrderedItem
                                key={id}
                                id={orderItem.id}
                                order={orderItem}
                                buyer={orderItem.buyer}
                                buyerId={CryptoJS.AES.decrypt(
                                  Cookies.get("id"),
                                  `${configData.CRYPTO_SECRET_KEY}`
                                ).toString(CryptoJS.enc.Utf8)}
                                loggedInUserType={CryptoJS.AES.decrypt(
                                  Cookies.get("type"),
                                  `${configData.CRYPTO_SECRET_KEY}`
                                ).toString(CryptoJS.enc.Utf8)}
                                product={orderItem.product}
                                productImage={
                                  orderItem.product.product_variant[0]
                                    .product_images &&
                                  orderItem.product.product_variant[0]
                                    .product_images.length > 0
                                    ? orderItem.product.product_variant[0]
                                        .product_images[0].image
                                    : null
                                }
                                status={orderItem.status}
                                statusId={orderItem.statusId}
                                count={orderItem.items_count}
                                delivery={orderItem.delivery}
                                totalpayable={orderItem.total_payable}
                                discountamount={dicountdict[orderItem.id]}
                                deliveryCharge={orderItem.delivery_charge}
                                tax={orderItem.tax_amount}
                                orderedOn={orderItem.created_date}
                                orderPlaceSuccess={setOrderPlaceSuccess}
                                orderPlaceFail={setOrderPlaceFail}
                              />
                            </Card>
                            <br />
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </Container>
              </div>

              <div className="place-order-container">
                <Container>
                  <div>
                    <Card border="success" style={{ width: "140%" }}>
                      <Card.Body>
                        <Card.Header>
                          <Card.Title>
                            <div className="place-order-title">
                              Order summary
                            </div>
                          </Card.Title>
                        </Card.Header>
                        <Card.Text>
                          <div>
                            <Container className="place-order-summary-container">
                              <Row className="place-order-summary-row">
                                <Col>Price</Col>
                                <Col className="place-order-col-price">
                                  &#8377;{" "}
                                  {indianRupeeLocale.format(orderPriceTotal)}
                                </Col>
                              </Row>
                              <Row className="place-order-summary-row">
                                <Col>First Order Discount</Col>
                                <Col className="place-order-col-disc-price">
                                  &#8377;{" "}
                                  {indianRupeeLocale.format(firstOrderdiscount)}
                                </Col>
                              </Row>
                              <Row className="place-order-summary-row">
                                <Col>Referral Discount</Col>
                                <Col className="place-order-col-disc-price">
                                  &#8377;{" "}
                                  {indianRupeeLocale.format(referraldiscount)}
                                </Col>
                              </Row>
                              {/* <Row className="place-order-summary-row">
                                                                <Col>Total Discount</Col>
                                                                <Col className="place-order-col-disc-price">
                                                                    &#8377; {indianRupeeLocale.format(orderDiscountTotal)}
                                                                </Col>
                                                            </Row> */}
                              <Row className="place-order-summary-row">
                                <Col>Delivery charge</Col>
                                <Col className="place-order-col-del-price">
                                  &#8377;{" "}
                                  {indianRupeeLocale.format(orderDeliveryTotal)}
                                </Col>
                              </Row>
                              {orderDeliveryTotal == 0 ? (
                                <div>
                                  <Row>
                                    <Col className="place-order-col-free-delivery-message">
                                      Your Delivery is FREE!!
                                    </Col>
                                  </Row>
                                </div>
                              ) : null}
                              <hr />
                              <Row className="place-order-summary-row-total">
                                <Col>Total payable</Col>
                                <Col className="place-order-col-price">
                                  &#8377;{" "}
                                  {indianRupeeLocale.format(orderGrandTotal)}
                                </Col>
                              </Row>
                              <hr />
                              <Row>
                                <Col className="place-order-summary-save-total">
                                  Your total saving is :
                                  <span className="place-oder-total-discount-amount-rupee">
                                    &#8377;{" "}
                                    {indianRupeeLocale.format(
                                      orderDiscountTotal
                                    )}{" "}
                                    /-
                                  </span>
                                </Col>
                              </Row>
                            </Container>
                          </div>
                        </Card.Text>
                        <Card.Footer>
                          <div className="place-order-btn">
                            <Button
                              variant="primary"
                              onClick={handleConfirmClick}
                              disabled={!isorderdeliverable}
                            >
                              Place Order
                            </Button>
                          </div>
                          <br />
                          <div className="place-order-btn">
                            {!isorderdeliverable
                              ? "Sorry! We don't have active operation on one of the address placed in your order. You can contact the seller for delivery options"
                              : null}
                          </div>
                        </Card.Footer>
                      </Card.Body>
                    </Card>
                  </div>
                </Container>
              </div>
            </div>
          )}
        </div>
      )}
    </div>
  );
}
