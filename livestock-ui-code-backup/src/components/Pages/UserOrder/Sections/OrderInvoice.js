import React, { useState } from "react";

import { Button, Col, Modal, Row, Table } from 'react-bootstrap';
import Moment from "react-moment";
import html2canvas from 'html2canvas'
import { jsPDF } from "jspdf";

import './OrderInvoice.css'
export default function OrderInvoice(props) {

    let indianRupeeLocale = Intl.NumberFormat('en-IN');
    console.log(props)
    console.log(props.order)

    const handleinvoicemodalClose = () => {
        props.setshowmodal(false)
    }

    const printDocument = () => {
        const input = document.getElementById('divToPrint');
        html2canvas(input)
            .then((canvas) => {
                const imgData = canvas.toDataURL('image/png');
                const pdf = new jsPDF();
                pdf.addImage(imgData, 'JPEG', 0, 0);
                // pdf.output('dataurlnewwindow');
                const filename = 'farmzonn_invoice_' + props.order.order_no + '.pdf'
                pdf.save(filename);
            })
            ;
    }

    const [sellerDetail, setSellerDetail] = useState(props.order.product.seller)
    const [pickupAddress, setPickupAddress] = useState(props.order.product.pickup.address)
    const [deliveryAddress, setDeliveryAddress] = useState(props.order.delivery.delivery_address.address)
    const [invoice, setInvoice] = useState(props.order.invoice)
    const [itemprice, setItemprice] = useState(props.order.price_per_item)
    const [itemcount, setItemcount] = useState(props.order.items_count)
    
    return (
        <div style={{ padding: 40 }}>
            <Modal
                {...props}
                backdrop="static"
                keyboard={true}
                show={props.showmodal}
                onHide={handleinvoicemodalClose}
            >
                <Modal.Header closeButton >
                    <Modal.Title>
                        Receipt
                        <div className="order-invoice-print">
                            <Button onClick={printDocument}>Download Invoice</Button>
                        </div>
                        <div className="order-invoice-close">
                            <Button variant="link" onClick={handleinvoicemodalClose}>X</Button>
                        </div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div id="divToPrint" className="order-invoce-detail">
                        <Row>
                            <Col>
                                <div className="order-invoice-heading">
                                    Invoice
                                </div>
                            </Col>
                        </Row>
                        <hr />
                        <Row gutter={24} style={{ marginTop: 32 }}>
                            <Col span={8}>
                                <h3>{sellerDetail.username}</h3>
                                <div>{pickupAddress.address_1},</div>
                                <div>{pickupAddress.address_2},</div>
                                <div>{pickupAddress.city + ' - ' + pickupAddress.pincode}</div>
                                {sellerDetail.pannumber ? <div>PAN - {sellerDetail.pannumber}</div> : null }
                            </Col>
                            <Col span={8} offset={8}>
                                <Table>
                                    <tbody>
                                        <tr>
                                            <th>Invoice # :</th>
                                            <td>{invoice.invoice_number}</td>
                                        </tr>
                                        <tr>
                                            <th>Invoice Date :</th>
                                            <td><Moment format="DD-MMMM-yyyy">{invoice.invoice_date}</Moment></td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
  
                        <Row style={{ marginTop: 48 }}>
                            <div>Bill To: <strong>{deliveryAddress.name}</strong></div>
                            <div>{deliveryAddress.address_1},</div>
                            <div>{deliveryAddress.address_2},</div>
                            <div>{deliveryAddress.city + ' - ' + deliveryAddress.pincode}</div>
                            <div>Phone: {deliveryAddress.phone}</div>
                        </Row>
  
  
                        <Row style={{ marginTop: 48 }}>
                            <Table size="sm">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Items</th>
                                        <th>Description</th>
                                        <th>Quantity</th>
                                        <th>Price(in Rs)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{props.order.order_no}</td>
                                        <td>{props.order.product.category}</td>
                                        <td>{'Live ' + props.order.product.breed + ' ' + props.order.product.category}</td>
                                        <td>{itemcount}</td>
                                        <td>{indianRupeeLocale.format(parseFloat(itemprice))}</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </Row>
  
                        <Row style={{ marginTop: 48 }}>
                            <Col span={8} offset={16}>
                                <Table>
                                    <tbody>
                                        <tr>
                                            <th>Gross Total :</th>
                                            <td>&#8377;  {indianRupeeLocale.format(parseFloat(parseFloat(itemprice) * parseFloat(itemcount)))}</td>
                                        </tr>
                                        <tr>
                                            <th>Discount :</th>
                                            <td>&#8377;  {indianRupeeLocale.format(parseFloat(props.order.discountedamount))}</td>
                                        </tr>
                                        <tr>
                                            <th>IGST @0% :</th>
                                            <td>&#8377;  0</td>
                                        </tr>
                                        <tr>
                                            <th>CGST @0% :</th>
                                            <td>&#8377;  0</td>
                                        </tr>
                                        <tr>
                                            <th>Nett Total :</th>
                                            <td>&#8377;  {indianRupeeLocale.format(parseFloat(props.order.total_payable))}</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
  
                        <Row style={{ marginTop: 28, textAlign: 'center' }}>
                            <div>
                                Thanks for choosing FarmZonn.com, Please visit us again.
                            </div>
                            <div className="order-invoice-bottom-condition">
                                Items delivered will not be replaced or returned.
                            </div>
                        </Row>
                    </div>
                </Modal.Body>
            </Modal>
        </div>
    );
}