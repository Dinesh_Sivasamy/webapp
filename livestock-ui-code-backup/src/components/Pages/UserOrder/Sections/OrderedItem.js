import React, { useState } from "react";
import Select from "react-select";
import { Button, Card, Col, Container, Row } from 'react-bootstrap'
import Modal from 'react-bootstrap/Modal'
import Image from 'react-bootstrap/Image'
import { GrDeliver } from "react-icons/gr";
import { MdVerifiedUser } from 'react-icons/md';
import Moment from 'react-moment';
import { Label } from "reactstrap";

import Cookies from 'js-cookie'
import CryptoJS from 'crypto-js';

import configData from "../../../../config.json";
import '../OrderPage.css'
import OrderInvoicePage from "./OrderInvoice"

export default function OrderedItem(props) {

    console.log("inside order item")
    console.log(props)

    let indianRupeeLocale = Intl.NumberFormat('en-IN');
    
    const [newStatus, setNewStatus] = useState({ id: props.statusId, name: props.status })

    const [existingStatusName, setExistingStatusName] = useState(props.status)

    const [invoicedetail, setInvoicedetail] = useState({})
    const [showInvoice, setShowInvoice] = useState(false)

    const [markOrderasCancelled, setMarkOrderasCancelled] = useState(false);
      
    const renderImage = (image) => {
        
        if (image) {
            return `${configData.IMG_FOLDER_PATH}${image}`
        }
        return `${configData.DEFAULT_PRODUCT_IMG}`
    }

    // handle change event of the order status dropdown
    const handleStatusChange = (obj) => {
        setNewStatus(obj);
    };

    const CalculatetotalPayable = () => {
        if (props.totalpayable == 0) {

            if (props.product.product_variant[0].offer_price
                && props.product.product_variant[0].offer_price > 0) {
                return parseFloat(props.product.product_variant[0].offer_price)
                    * props.count
            } else {
                return parseFloat(props.product.product_variant[0].original_price)
                    * props.count
            }
        }
        else {
            return parseFloat(props.totalpayable)
        }
    }

    const handleSaveOrderStatus = () => {
    
        fetch(`/api/v1/order-detail-status/id=${encodeURIComponent(props.id)}/${encodeURIComponent(newStatus.id)}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
        })
            .then((response) => {
                if (response.ok) {
                    return response;
                } else {
                    throw new Error(response);
                }
            })
            .then(function (resp) {
                console.log(resp);
                setExistingStatusName(newStatus.name)
                window.location.reload()
            })
            .catch((err) => {
                console.log(err);
            })
    }

    const handleCancelOrderResponse = (hasconfirmedCancel) => {

        if (hasconfirmedCancel){
            fetch(`/api/v1/order-detail-status/id=${encodeURIComponent(props.id)}/${encodeURIComponent(100)}`, {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': Cookies.get('csrftoken')
                },
            })
                .then((response) => {
                    if (response.ok) {
                        return response;
                    } else {
                        throw new Error(response);
                    }
                })
                .then(function (resp) {
                    console.log(resp);
                    setExistingStatusName(newStatus.name)                    
                })
                .catch((err) => {
                    console.log(err);
                })
        }

        setMarkOrderasCancelled(false)
        window.location.reload()
    }

    const customStyles = {
        control: (base, state) => ({
            ...base,
            background: "#green",
            // match with the menu
            borderRadius: state.isFocused ? "3px 3px 0 0" : 3,
            // Overwrittes the different states of border
            borderColor: state.isFocused ? "blue" : "red",
            // Removes weird border around container
            boxShadow: state.isFocused ? null : null,
            "&:hover": {
                // Overwrittes the different states of border
                borderColor: state.isFocused ? "red" : "blue"
            }
        }),
        menu: (base) => ({
            ...base,
            // override border radius to match the box
            borderRadius: 0,
            // kill the gap
            marginTop: 0
        }),
        menuList: (base) => ({
            ...base,
            background: "white",
            // kill the white space on first and last option
            padding: 0
        })
    };

    const handleViewInvoice = () => {
        setInvoicedetail(props.order)
        setShowInvoice(true)
    }

    return (
        <div className="order-item-card">
            {showInvoice ?
                <div>
                    <OrderInvoicePage
                        order={props.order}
                        showmodal={showInvoice}
                        setshowmodal={setShowInvoice}
                    />
                </div>
                :
                null
            }
                        <div>
                <Modal
                    size="sm"
                    centered
                    backdrop="static"
                    onHide={() => setMarkOrderasCancelled(false)}
                    show={markOrderasCancelled}
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="order-save-response">
                            You are about to cancel your order
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className='order-change-row'>
                            <div className='order-save-cancel-response-heading'>
                                Please confirm if you want to Cancel your Order
                            </div>
                        <br />
                            <div className='order-save-cancel-response'>
                                <Button variant="primary" onClick={() => { handleCancelOrderResponse(true) }}>Yes</Button>
                            <span> </span>
                                <Button variant="primary" onClick={() => { handleCancelOrderResponse(false) }}>No</Button>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
            <Card.Header>
                <Container>
                    <Row>
                        <Col>
                            <a className="order-item-name" href={`/product/${props.product.id}`}> {props.product.name}  </a>
                            {props.product.seller.is_verified ?
                                <span className="order-item-verified-seller"> |  <MdVerifiedUser /> Verified Seller  </span>
                                :
                                null
                            }
                        </Col>
                        <Col>
                            {existingStatusName.indexOf('Delivered') > -1 ?
                                <div className="order-delivered-header"> Delivered  <GrDeliver /></div>
                                :
                                <div className="order-inprogress-header">{existingStatusName}</div>
                            }
                        </Col>
                    </Row>
                </Container>
            </Card.Header>
            <Card.Body>
                <Card.Text>
                    <Container>
                        <Row xs={1} sm={1} md={props.delivery ? 4 : 3}>
                            <Col>
                                <a href={`/product/${props.product.id}`}>
                                        <div className="order-img-div">
                                            <Image className="order-img" src={renderImage(props.productImage)} fluid />
                                        </div>
                                </a>
                            </Col>
                            <Col>
                                <div className="order-total-div">
                                    {props.ordernumber ?
                                        <div>Order #: {props.ordernumber}</div>
                                        :
                                        null
                                    }
                                    <div>
                                        <span> </span> {props.product.description}
                                    </div>
                                    <div>
                                        Product #: {props.product.uniqueidentifier}
                                    </div>
                                    <div>
                                        {props.product.breed} Breed, {props.product.product_variant[0].age} of age
                                    </div>
                                    <div>
                                        {props.product.product_variant[0].gender} - {props.product.product_variant[0].weight} kg
                                    </div>
                                    <div>
                                        Quantity: <span> </span> {props.count}
                                    </div>
                                </div>
                            </Col>
                            <Col>
                                <div className="order-price-detail">
                                    <div>
                                        Total : <span> </span> &#8377; {indianRupeeLocale.format(CalculatetotalPayable())}
                                    </div>
                                    {props.discountamount > 0 ?
                                        <div className="order-discount-price">
                                            You Save: <span> </span> &#8377; {indianRupeeLocale.format(parseFloat(props.discountamount))} in this order
                                        </div>
                                        :
                                        null
                                    }
                                    {/* </div>
                        <div className="order-delivery-detail-div"> */}
                                    {props.status.indexOf('Delivered') > -1 ?
                                        <div>
                                            <div className="order-delivered">
                                                Delivered On:  <span> </span>
                                                <Moment format="DD MMMM yyyy">{props.delivery.delivered_on}</Moment>
                                            </div>
                                            <div>
                                                <Button onClick={handleViewInvoice} >View Invoice</Button>
                                            </div>
                                        </div>
                                        :
                                        null
                                    }
                                </div>
                            </Col>
                            {props.delivery != null ?
                                <Col>
                                
                                    <div>
                                        <div className="order-delivery-address">
                                            <div className="order-delivery-address-heading">Delivery address</div>
                                            <div>
                                                {props.delivery.delivery_address.buyer}
                                            </div>
                                            <div>
                                                {props.delivery.delivery_address.address.address_1 + ', ' + props.delivery.delivery_address.address.address_2}
                                            </div>
                                            <div>
                                                {props.delivery.delivery_address.address.city + ', ' + props.delivery.delivery_address.address.state + ' - ' + props.delivery.delivery_address.address.pincode}
                                            </div>
                                            <div>
                                                Phone: {props.delivery.delivery_address.address.phone}
                                            </div>
                                            <div className="order-delivery-type">
                                                <div>Address type: </div>
                                                <div className="order-delivery-address-type">
                                                    {props.delivery.delivery_address.address.type}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                                : null
                            }
                            {props.loggedInUserType == 9 ?
                                <Col>
                                    <div>
                                        <div className="order-delivery-address">
                                            <div className="order-delivery-address-heading">Pickup address</div>
                                            <div>
                                                {props.product.pickup.address.name}
                                            </div>
                                            <div>
                                                {props.product.pickup.address.address_1 + ', ' + props.product.pickup.address.address_2}
                                            </div>
                                            <div>
                                                {props.product.pickup.address.city + ', ' + props.product.pickup.address.state + ' - ' + props.product.pickup.address.pincode}
                                            </div>
                                            <div>
                                                Phone: {props.product.pickup.address.phone}
                                            </div>
                                            <div className="order-delivery-type">
                                                <div>Address type: </div>
                                                <div className="order-delivery-address-type">
                                                    {props.product.pickup.address.type}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                                : null
                            }
                        </Row>
                        {props.loggedInUserType == 9 
                            && props.statusId != 6 && props.statusId < 100
                            && props.orderstatusList
                            && props.orderstatusList.length > 0 ?
                            <Row xs={1} sm={1} md={4} className="order-status-modify-row">
                                <Col>
                                    <Select
                                        className="order-status-dropdown"
                                        placeholder="Order Status"
                                        value={newStatus}
                                        options={props.orderstatusList}
                                        onChange={handleStatusChange}
                                        styles={customStyles}
                                        getOptionLabel={(x) => x.name}
                                        getOptionValue={(x) => x.id}
                                    />
                                </Col>
                                <Col className="order-status-modify-col">
                                    <Button variant="primary" type="submit" disabled={newStatus.name == existingStatusName} onClick={handleSaveOrderStatus}>Save</Button>
                                </Col>
                            </Row>
                            :
                            null
                        }

                        {props.loggedInUserType != 2 
                            && props.statusId != 6 && props.statusId < 100 ?
                            <Row xs={1} sm={1} md={4} className="order-status-modify-row">
                               <Col>
                               </Col>
                                <Col className="order-status-modify-col">
                                    <Button variant="primary" type="submit" onClick={() => setMarkOrderasCancelled(true)}>Cancel Order</Button>
                                </Col>
                            </Row>
                            :
                            null
                        }
                    </Container>
                </Card.Text>
            </Card.Body>
        </div>
    )
}