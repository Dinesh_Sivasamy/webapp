import React from 'react'
import configData from "../../config.json";

import './UnderConstruction.css'

export default function UnderConstruction() {

    const getbackgroundimage = () => {
        return `url(/comming_soon_bg.png)`
    }

    return (
        <div style={{
            backgroundImage: getbackgroundimage(), backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            height: '100vh'
        }} >
            <div className="center-heading">
                <h1>
                    Coming Soon!
                </h1>
            </div>
            <div className="split-line">
                <hr style={{
                    height: 8
                }} />
            </div>
            <div className="center-sentence">
                <h5>
                    Sorry for the inconvenience
                </h5>
                <h5>
                    This service is not enabled in your area
                </h5>
                <h5>
                    Will be Launching Soon
                </h5>
            </div>
        </div>
    )
}