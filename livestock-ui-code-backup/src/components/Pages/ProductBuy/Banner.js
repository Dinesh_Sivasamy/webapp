import React from "react";

import "./Banner.css";
import { MdVerifiedUser } from "react-icons/md";
import { useHistory, Link } from "react-router-dom";
import BannerDetail from "./BannerDetail";

export default function Banner(props) {
  const history = useHistory();

  const handlecardclick = () => {
    history.push({
      pathname: `/product/related/${props.productId}`,
    });
  };

  // To set two dates to two variables
  var modifieddate = new Date(props.modifieddate);
  var currentdate = new Date();

  // To calculate the time difference of two dates
  var Difference_In_Time = currentdate.getTime() - modifieddate.getTime();

  // To calculate the no. of days between two dates
  var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);

  return (
    <>
      {/* <li className="cards__item">
        <a
          className={
            props.isSold || Difference_In_Days > 90
              ? "cards__item__link disabledCursor"
              : "cards__item__link"
          }
          href={`/product/${props.productId}`}
        >
          <BannerDetail detail={props} />
        </a>
      </li> */}
      <a
        href={`/product/${props.productId}`}
        className="text-decor-none-onhover"
      >
        <div className="list-box bgcolor-primary-lighter-brown">
          <div>
            <img
              className="img-size-list-box img-border-25 padding-7-5"
              src={props.src}
              alt="livestock"
            />
          </div>
          <div className="text-align-left sub-header-xxs padding-left-5">
            <span className="header-xs">{props.name}</span>
            <br />
            {props.breed + " | " + props.weight + " Kgs"}
            <br />
            {props.gender + " | " + props.stock + " Qty"}
            <br />
            {props.city + ", " + props.state}
          </div>
        </div>
      </a>
    </>
  );
}
