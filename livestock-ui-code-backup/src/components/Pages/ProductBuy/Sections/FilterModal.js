import React from "react";
import "./Filter.css";
import { Button, Modal } from "react-bootstrap";

const FilterModal = React.forwardRef(
  ({ children, open, onSelect, onCancel, onClear }, ref) => {
    return (
      <div>
        <Modal
          show={open}
          backdrop="static"
          keyboard={true}
          centered
          onHide={() => onCancel()}
        >
          <Modal.Header closeButton>
            <Modal.Title>Filter results</Modal.Title>
          </Modal.Header>
          <Modal.Body>{children}</Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={() => onSelect()}>
              Confirm
            </Button>
            <Button variant="primary" onClick={() => onClear()}>
              Clear
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
);
export default FilterModal;
