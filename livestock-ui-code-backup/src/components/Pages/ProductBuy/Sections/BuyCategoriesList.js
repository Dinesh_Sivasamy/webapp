import React from "react";
import "../../Home/CardItem.css";
import all from "../../Home/images/all.png";
import cattle from "../../Home/images/cattle.png";
import goat from "../../Home/images/goat.png";
import sheep from "../../Home/images/sheep.png";
import hen from "../../Home/images/hen.png";
import dog from "../../Home/images/dog.png";
import { Container, Row, Col } from "react-bootstrap";

export default function BuyCategoriesList({
  handleCategorySelect,
  categoryfiltersAvailable,
}) {
  const handleCategoryChange = (category) => {
    let allfilter = categoryfiltersAvailable;

    let filteredcategory = allfilter.filter(
      (current) => current.id == category
    );
    console.log(category);
    console.log(filteredcategory);
    handleCategorySelect(filteredcategory);
  };

  return (
    <Container>
      <div className="buy-list-boxes">
        <Row>
          <div className="buy-list-carditem-box">
            <Col>
              <a onClick={() => handleCategorySelect(null)}>
                <div className="buy-list-box">
                  <div className="icon">
                    <img src={all} alt="All" width="40px" height="35px"></img>
                  </div>
                </div>
              </a>
            </Col>
            <Col>
              <a onClick={() => handleCategoryChange(1)}>
                <div className="buy-list-box">
                  <div className="icon">
                    <img src={cattle} alt="cow" width="25%" height="35px"></img>
                  </div>
                </div>
              </a>
            </Col>
            <Col>
              <a onClick={() => handleCategoryChange(2)}>
                <div className="buy-list-box">
                  <div className="icon">
                    <img src={goat} alt="goat" width="40px" height="35px"></img>
                  </div>
                </div>
              </a>
            </Col>
            <Col>
              <a onClick={() => handleCategoryChange(3)}>
                <div className="buy-list-box">
                  <div className="icon">
                    <img
                      src={sheep}
                      alt="sheep"
                      width="40px"
                      height="35px"
                    ></img>
                  </div>
                </div>
              </a>
            </Col>
            <Col>
              <a onClick={() => handleCategoryChange(4)}>
                <div className="buy-list-box">
                  <div className="icon">
                    <img src={hen} alt="hen" width="40px" height="35px"></img>
                  </div>
                </div>
              </a>
            </Col>
            <Col>
              <a onClick={() => handleCategoryChange(5)}>
                <div className="buy-list-box">
                  <div className="icon">
                    <img src={dog} alt="dog" width="40px" height="35px"></img>
                  </div>
                </div>
              </a>
            </Col>
          </div>
        </Row>
      </div>
    </Container>
  );
}
