import React, { useState } from "react";

import { Button, Col, Modal, Row, Table } from 'react-bootstrap';
import Moment from "react-moment";
import html2canvas from 'html2canvas'
import { jsPDF } from "jspdf";

import '../../UserOrder/Sections/OrderInvoice.css'
export default function SellerInvoice(props) {

    console.log(props)
    console.log(props.order)

    const handleinvoicemodalClose = () => {
        props.setshowmodal(false)
    }

    let indianRupeeLocale = Intl.NumberFormat('en-IN'); 

    const printDocument = () => {
        const input = document.getElementById('divToPrint');
        html2canvas(input)
            .then((canvas) => {
                const imgData = canvas.toDataURL('image/png');
                const pdf = new jsPDF();
                pdf.addImage(imgData, 'JPEG', 0, 0);
                // pdf.output('dataurlnewwindow');
                const filename = 'farmzonn_sale_invoice_' + props.order.order_no + '.pdf'
                pdf.save(filename);
            })
            ;
    }

    const [sellerDetail, setSellerDetail] = useState(props.order.product.seller)
    const [pickupAddress, setPickupAddress] = useState(props.order.product.pickup.address)
    const [deliveryAddress, setDeliveryAddress] = useState(props.order.delivery.delivery_address.address)
    const [invoice, setInvoice] = useState((props.order.sale_invoice && props.order.sale_invoice.length > 0) ? props.order.sale_invoice[0] : null)
    const [itemcount, setItemcount] = useState(props.order.items_count)
    
    return (
        <div style={{ padding: 20 }}>
            <Modal
                {...props}
                backdrop="static"
                keyboard={true}
                show={props.showmodal}
                onHide={handleinvoicemodalClose}
            >
                <Modal.Header closeButton>
                    <Modal.Title>
                        Receipt
                        <div className="order-invoice-print">
                            <Button onClick={printDocument}>Download Invoice</Button>
                        </div>
                        <div className="order-invoice-close">
                            <Button variant="link" onClick={handleinvoicemodalClose}>X</Button>
                        </div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div id="divToPrint">
                        <Row>
                            <Col>
                                <div className="order-invoice-heading">
                                    Invoice
                                </div>
                                <hr />
                            </Col>
                        </Row>
  
                        <Row gutter={24} style={{ marginTop: 32 }}>
                            <Col span={8}>
                                <h3>FarmZonn.com</h3>
                                <div>2(216), Bhagavathi Nagar,</div>
                                <div>Thirumalayampalayam,</div>
                                <div>Coimbatore - 641105</div>
                                <div>Reg No: TN/AIL4CBE/NFSH/68-22-00091</div>
                            </Col>
                            <Col span={8} offset={8}>
                                <Table>
                                    <tbody>
                                        <tr>
                                            <th>Invoice # :</th>
                                            <td>{invoice.invoicenumber}</td>
                                        </tr>
                                        <tr>
                                            <th>Invoice Date :</th>
                                            <td><Moment format="DD-MMMM-yyyy">{invoice.invoice_date}</Moment></td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
  
                        <Row style={{ marginTop: 48 }}>
                            <div>Bill To: <strong>{sellerDetail.username}</strong></div>
                            <div>{pickupAddress.address_1},</div>
                            <div>{pickupAddress.address_2},</div>
                            <div>{pickupAddress.city + ' - ' + pickupAddress.pincode}</div>
                            <div>Phone: {sellerDetail.phone}</div>
                        </Row>
  
                        <Row style={{ marginTop: 48 }}>
                            <Table size="sm">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Items</th>
                                        <th>Description</th>
                                        <th>Sold Qty</th>
                                        <th>Sold for Price (Rs)</th>
                                        <th>Fee (%)</th>
                                        <th>Amount to pay (Rs)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{props.order.order_no}</td>
                                        <td>{props.order.product.breed + ' - ' + props.order.product.category}</td>
                                        <td>Farmzonn Sale commission</td>
                                        <td>{itemcount}</td>
                                        <td>{indianRupeeLocale.format(parseFloat(invoice.totalprice))}</td>
                                        <td>{parseFloat(invoice.profitpercent)}</td>
                                        <td>{indianRupeeLocale.format(parseFloat(invoice.farmzonncommission))}</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </Row>
  
                        <Row style={{ marginTop: 48 }}>
                            <Col span={8} offset={16}>
                                <Table>
                                    <tbody>
                                        <tr>
                                            <th>Gross Total :</th>
                                            <td>&#8377; {indianRupeeLocale.format(parseFloat(invoice.farmzonncommission))}</td>
                                        </tr>
                                        <tr>
                                            <th>IGST @0% :</th>
                                            <td>&#8377; 0</td>
                                        </tr>
                                        <tr>
                                            <th>CGST @0% :</th>
                                            <td>&#8377; 0</td>
                                        </tr>
                                        <tr>
                                            <th>Nett Total :</th>
                                            <td>&#8377; {indianRupeeLocale.format(parseFloat(invoice.farmzonncommission))}</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
  
                        <Row style={{ marginTop: 28, textAlign: 'center' }}>
                            <div>
                                Thanks for choosing FarmZonn.com, Please visit us again.
                            </div>
                            <div className="order-invoice-bottom-condition">
                                Seller is sole responsible for the quality and health condition of the stock sold.
                            </div>
                        </Row>
                    </div>
                </Modal.Body>
            </Modal>
        </div>
    );
}