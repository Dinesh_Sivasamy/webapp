import React, { Component, useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  NavLink,
} from "reactstrap";
import { Link } from "react-router-dom";
import "./NavMenu.css";

// import React, { useEffect, useState } from "react";
import Dropdown from "react-bootstrap/Dropdown";
import SignInModal from "./Pages/User/SignInModal";
import {
  FaShoppingCart,
  FaUser,
  FaBars,
  FaWindowClose,
  FaWhatsappSquare,
} from "react-icons/fa";
import { AiOutlineWhatsApp } from "react-icons/ai";
// import { AiOutlinePlus } from "react-icons/ai";
// import { BsSearch } from "react-icons/bs";

import Cookies from "js-cookie";
import CryptoJS from "crypto-js";
import { useHistory } from "react-router-dom";

import configData from "../config.json";
// import "./Navbar.css";
// import Nav from "react-bootstrap/Nav";
// import { UncontrolledTooltip } from "reactstrap";

export default function NavMenu() {
  const [collapsed, setCollapsed] = useState(true);

  let indianRupeeLocale = Intl.NumberFormat("en-IN");
  const [referredUsersCount, setReferredUsersCount] = useState(0);
  const manageAddress = "manage/invalid";

  const [show, setShow] = useState(false);

  const handleShow = () => setShow(true);
  const handleClose = () => {
    setShow(false);
    window.location.reload();
  };

  const handleManageAddressOnClick = (e) => {
    console.log(e);
    e.currentTarget.href = e.currentTarget.href + manageAddress;
  };

  const toggleNavbar = () => {
    setCollapsed(!collapsed);
  };

  const [userrefcode, setUserrefcode] = useState(
    Cookies.get("refcode") && Cookies.get("refcode") != "None" ? 1 : 0
  );

  const [referralcode, setReferralcode] = useState(
    userrefcode ? Cookies.get("refcode") : null
  );

  return (
    <Navbar
      className="navbar-head navbar-expand-sm navbar-toggleable-sm ng-white border-bottom box-shadow mb-3"
      container
      light
    >
      <NavbarBrand className="navbar-brand-title" tag={Link} to="/">
        Farmzonn
      </NavbarBrand>
      <NavbarToggler onClick={toggleNavbar} className="mr-2" />
      <Collapse
        className="d-sm-inline-flex flex-sm-row-reverse"
        isOpen={!collapsed}
        navbar
      >
        <ul className="navbar-nav flex-grow">
          <NavItem>
            <hr className="nav-splitter" />
            <NavLink
              tag={Link}
              onClick={toggleNavbar}
              className="text-dark"
              to="/Buy/0"
            >
              Buy
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              tag={Link}
              onClick={toggleNavbar}
              className="text-dark"
              to="/sell"
            >
              Sell
            </NavLink>
            <hr className="nav-splitter" />
          </NavItem>
          <NavItem>
            <NavLink
              tag={Link}
              onClick={toggleNavbar}
              className="text-dark"
              to="/forum"
            >
              Ask Experts for FREE!
            </NavLink>
            <hr className="nav-splitter" />
          </NavItem>
          <NavItem>
            <NavLink
              tag={Link}
              onClick={toggleNavbar}
              className="text-dark"
              to="/view-requirements"
            >
              View Enquiries
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              tag={Link}
              onClick={toggleNavbar}
              className="text-dark"
              to="/add-enquiry"
            >
              Post My Enquiry
            </NavLink>
          </NavItem>

          <NavItem>
            <NavLink
              tag={Link}
              onClick={toggleNavbar}
              className="text-dark"
              to="/my-cart"
            >
              <FaShoppingCart /> My Cart
            </NavLink>
            {/* <hr className="nav-splitter" />
          </NavItem>
          <NavItem>
            <NavLink
              tag={Link}
              onClick={toggleNavbar}
              className="text-dark"
              to="/#site-links"
            >
              Site links
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              tag={Link}
              onClick={toggleNavbar}
              className="text-dark"
              to="/#contact-us"
            >
              Contact us
            </NavLink> */}
            <hr className="nav-splitter" />
          </NavItem>
          <NavItem>
            <a
              className="nav-anchor-link"
              onClick={toggleNavbar}
              href={
                "https://api.whatsapp.com/send/?text=Hi, Easy Livestock Trading in India! Buy healthy, high-quality livestock from verified sellers nationwide, delivered to your doorstep. Sign up now for a chance to WIN up to Rs. 5,000 off! Join us at https://farmzonn.com/?refcode=" +
                referralcode +
                " for hassle-free livestock transactions."
              }
              target="_blank"
            >
              <AiOutlineWhatsApp /> Refer & Win
            </a>
          </NavItem>

          <NavItem>
            <Dropdown>
              {Cookies.get("name") != null ? (
                <Dropdown.Toggle
                  variant="link"
                  size="sm"
                  className="collasible-nav-dropdown"
                >
                  <FaUser /> Welcome{" "}
                  {CryptoJS.AES.decrypt(
                    Cookies.get("name"),
                    `${configData.CRYPTO_SECRET_KEY}`
                  ).toString(CryptoJS.enc.Utf8)}
                </Dropdown.Toggle>
              ) : (
                <Dropdown.Toggle
                  variant="link"
                  size="sm"
                  className="collasible-nav-dropdown"
                >
                  More ...
                </Dropdown.Toggle>
              )}
              <Dropdown.Menu>
                {Cookies.get("name") != null ? (
                  <div>
                    <Dropdown.Item
                      href={`/userprofile/${CryptoJS.AES.decrypt(
                        Cookies.get("id"),
                        `${configData.CRYPTO_SECRET_KEY}`
                      ).toString(CryptoJS.enc.Utf8)}`}
                    >
                      My Profile
                    </Dropdown.Item>
                  </div>
                ) : (
                  <div>
                    <Dropdown.Item href="#" onClick={handleShow}>
                      <FaUser /> Login/register
                    </Dropdown.Item>
                    <SignInModal
                      show={show}
                      setModalClose={handleClose}
                      onHide={handleClose}
                    />
                  </div>
                )}
                <Dropdown.Divider />
                {Cookies.get("type") != null ? (
                  <div>
                    <div>
                      {["2", "3", "4"].indexOf(
                        CryptoJS.AES.decrypt(
                          Cookies.get("type"),
                          `${configData.CRYPTO_SECRET_KEY}`
                        ).toString(CryptoJS.enc.Utf8)
                      ) > -1 ? (
                        <div>
                          <Dropdown.Item href="/seller-product/">
                            View Listed Products
                          </Dropdown.Item>
                        </div>
                      ) : null}
                    </div>

                    <div>
                      {CryptoJS.AES.decrypt(
                        Cookies.get("type"),
                        `${configData.CRYPTO_SECRET_KEY}`
                      ).toString(CryptoJS.enc.Utf8) == 9 ? (
                        <div>
                          <Dropdown.Item href="/orders">
                            View Orders
                          </Dropdown.Item>
                          <Dropdown.Item href="/seller-product/">
                            View Listed Products
                          </Dropdown.Item>
                        </div>
                      ) : null}
                    </div>
                    <div>
                      {["1", "3", "5"].indexOf(
                        CryptoJS.AES.decrypt(
                          Cookies.get("type"),
                          `${configData.CRYPTO_SECRET_KEY}`
                        ).toString(CryptoJS.enc.Utf8)
                      ) > -1 ? (
                        <div>
                          <Dropdown.Item href="/orders">
                            My Orders
                          </Dropdown.Item>
                          <Dropdown.Item>
                            Your total bonus:
                            <span className="nav-ref-bonus-amount">
                              &#8377;{" "}
                              {indianRupeeLocale.format(
                                parseInt(referredUsersCount, 10) * 25 +
                                  parseInt(userrefcode, 10) * 150
                              )}
                            </span>
                          </Dropdown.Item>
                        </div>
                      ) : null}
                    </div>
                  </div>
                ) : (
                  <div>
                    <Dropdown.Item href="/orders">My Orders</Dropdown.Item>
                  </div>
                )}
                <Dropdown.Divider />
                {Cookies.get("type") != null &&
                ["5"].indexOf(
                  CryptoJS.AES.decrypt(
                    Cookies.get("type"),
                    `${configData.CRYPTO_SECRET_KEY}`
                  ).toString(CryptoJS.enc.Utf8)
                ) > -1 ? (
                  <div>
                    <Dropdown.Item href="/vet-verify/">
                      Livestock Checkup
                    </Dropdown.Item>
                    <Dropdown.Divider />
                  </div>
                ) : null}

                {Cookies.get("name") != null ? (
                  <div>
                    <Dropdown.Item
                      href="/my-addresses/"
                      onClick={handleManageAddressOnClick}
                    >
                      Manage Address
                    </Dropdown.Item>
                    <Dropdown.Item href="/my-enquiries">
                      View My Enqueries
                    </Dropdown.Item>
                    <Dropdown.Item href="/logout">SignOut</Dropdown.Item>
                  </div>
                ) : null}
              </Dropdown.Menu>
            </Dropdown>
          </NavItem>
        </ul>
      </Collapse>
    </Navbar>
  );
}
